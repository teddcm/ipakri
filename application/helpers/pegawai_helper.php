<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function split_nip($nip) {
    $nip = preg_replace('/^.{8}/', "$0 ", $nip);
    $nip = preg_replace('/^.{15}/', "$0 ", $nip);
    $nip = preg_replace('/^.{17}/', "$0 ", $nip);
    return $nip;
}

function convertNoSurat($mode = 'kgb') {
    $CI = & get_instance();
    $mode = strtolower($mode);
    $format = getPengaturan($mode . '_no_surat_format');
    $no_urut = getPengaturan($mode . '_no_surat');
    $tahun = getPengaturan($mode . '_no_surat_tahun');

    if ($tahun != date('Y')) {
        $tahun = date('Y');
        $no_urut = 1;
        $CI->db->where('nama_pengaturan', 'kgb_no_surat');
        $CI->db->set('value_pengaturan', $no_urut);
        $query = $CI->db->update('admin_pengaturan');

        $CI->db->where('nama_pengaturan', 'kgb_no_surat_tahun');
        $CI->db->set('value_pengaturan', $tahun);
        $query = $CI->db->update('admin_pengaturan');
    }
    /*
      %n1%   nomor urut surat biasa 1, 10, 100
      %n2%   nomor urut surat 2 digit 01, 10, 100
      %n3%   nomor urut surat 3 digit 001, 010, 100
      %n4%   nomor urut surat 4 digit 0001, 0010, 0100
      %d%    tanggal 01-31
      %da%   tanggal 1-31
      %m%    bulan 01-12
      %ma%   tanggal 1-12
      %mb%   tanggal I-XII
      %y%    tanggal 2000-2017
      %ya%   tanggal 00-17


     */
    $format = str_replace('%n1%', $no_urut, $format);
    $format = str_replace('%n2%', str_pad($no_urut, 2, '0', STR_PAD_LEFT), $format);
    $format = str_replace('%n3%', str_pad($no_urut, 3, '0', STR_PAD_LEFT), $format);
    $format = str_replace('%n4%', str_pad($no_urut, 4, '0', STR_PAD_LEFT), $format);
    $format = str_replace('%d%', date('d'), $format);
    $format = str_replace('%da%', date('j'), $format);
    $format = str_replace('%m%', date('m'), $format);
    $format = str_replace('%ma%', date('n'), $format);
    $format = str_replace('%mb%', convertRomawi(date('n')), $format);
    $format = str_replace('%y%', date('y'), $format);
    $format = str_replace('%ya%', date('Y'), $format);

    //UPDATE NO URUT KGB DI TABEL PENGATURAN
    $no_urut ++;
    $CI->db->where('nama_pengaturan', 'kgb_no_surat');
    $CI->db->set('value_pengaturan', $no_urut);
    $query = $CI->db->update('admin_pengaturan');

    return $format;
}

/* function duplicate_table_pupns($id_instansi, $table_name = 'kanreg8_pupns', $table_target = 'kanreg8_pupns_backup') {
  $CI = & get_instance();
  $CI->db->trans_begin();
  $isExist = $CI->db->query("SHOW TABLES LIKE '" . $table_name . "'");
  if ($isExist->num_rows() > 0) {
  $CI->db->query("RENAME TABLE " . $table_name . " TO " . $table_name . "_" . date("Ymd_His"));
  }
  $CI->db->query("CREATE TABLE $table_name LIKE $table_target");
  $CI->db->where('PNS_INSKER', $id_instansi);
  $query = $CI->db->get($table_target);
  foreach ($query->result() as $row) {
  $CI->db->insert($table_name, $row);
  }
  if ($CI->db->trans_status() === FALSE) {
  $CI->db->trans_rollback();
  return false;
  } else {
  $CI->db->trans_commit();
  return true;
  }
  }
 */

function convert_tempat_lahir($string) {
    $CI = & get_instance();
    $CI->db->select('name');
    if (substr($string, 2, 8) == '00000000') {
        $CI->db->where('id', substr($string, 0, 2));
        $query = $CI->db->get('wil_tk1');
    } else if (substr($string, 4, 6) == '000000') {
        $CI->db->where('id', substr($string, 0, 4));
        $query = $CI->db->get('wil_tk2');
    } else if (substr($string, 7, 3) == '000') {
        $CI->db->where('id', substr($string, 0, 7));
        $query = $CI->db->get('wil_tk3');
    } else if (strlen($string) == 10) {
        $CI->db->where('id', substr($string, 0, 7));
        $query = $CI->db->get('wil_tk3');
//$CI->db->where('id', substr($string, 0, 10));
//$query = $CI->db->get('wil_tk4');
//wil_tk4 sudah dihapus
//        echo 'kok sampe kelurahan?';
//        return '';
    } else {
        $CI->db->where('id', '03540313');
        $query = $CI->db->get('wil_tk3');
    } 
//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row()->name;
    } else {
        return '';
    }
}

function convert_ortu($ID) { //menghasilkan nama dari ortu anak
    $CI = & get_instance();
    $CI->db->where('RWISU_ID', $ID);
    $query = $CI->db->get('z_datarwissu');
    if ($query->num_rows() > 0) {
        return $query->row()->RWISU_NAMISU;
    }
}

function convert_agama($ID) {
    $CI = & get_instance();
    $CI->db->where('AGA_KODAGA', $ID);
    $query = $CI->db->get('kanreg8_agama');
    if ($query->num_rows() > 0) {
        return $query->row()->AGA_NAMAGA;
    }
}

function convert_jenis_cuti($ID) {
    $CI = & get_instance();
    $CI->db->where('JNSCT_ID', $ID);
    $query = $CI->db->get('q_jenis_cuti');
    if ($query->num_rows() > 0) {
        return $query->row()->JNSCT_NAMA;
    }
}

function convert_instansi($ID) {
    $CI = & get_instance();
    $CI->db->where('INS_KODINS', $ID);
    $query = $CI->db->get('kanreg8_instansi');
    if ($query->num_rows() > 0) {
        return $query->row()->INS_NAMINS;
    }
}

function convert_tktpendik($ID, $fromDB = false) {
    if ($fromDB) {
        $CI = & get_instance();
        $CI->db->where('DIK_TKTDIK', $ID);
        $query = $CI->db->get('kanreg8_tktpendik');
// echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row()->DIK_NAMDIK;
        }
    } else {
        $koddik = array('5' => 'SD', '05' => 'SD', '10' => 'SMP', '12' => 'SMP Kejuruan', '15' => 'SMA', '17' => 'SMA Kejuruan', '18' => 'SMA Keguruan', '20' => 'D-1', '25' => 'D-2', '30' => 'D-3', '35' => 'D-4', '40' => 'S-1', '45' => 'S-2', '50' => 'S-3');
        return $koddik[$ID];
    }
}

function convert_pangkat($ID) {
    $CI = & get_instance();
    $CI->db->where('GOL_KODGOL', $ID);
    $query = $CI->db->get('kanreg8_golru');
    if ($query->num_rows() > 0) {
        return $query->row()->GOL_PKTNAM;
    }
}

function convert_status_anak($ID) {
    $CI = & get_instance();
    $CI->db->where('STA_ID', $ID);
    $query = $CI->db->get('q_status_anak');
    if ($query->num_rows() > 0) {
        return $query->row()->STA_NAMA;
    }
}

function convert_kedhuk($ID) {
    $CI = & get_instance();
    $CI->db->where('KED_KEDKOD', $ID);
    $query = $CI->db->get('kanreg8_kedhuk');
    if ($query->num_rows() > 0) {
        return $query->row()->KED_KEDNAM;
    }
}

function convert_prosedur_unor($ID) {
    $CI = & get_instance();
    $CI->db->where('PRUNO_ID', $ID);
    $query = $CI->db->get('q_unor_prosedur');
    if ($query->num_rows() > 0) {
        return str_replace('_', ' ', $query->row()->PRUNO_NAMA);
    }
}

function convert_status_kawin($ID) {
    $CI = & get_instance();
    $CI->db->where('STS_ID', $ID);
    $query = $CI->db->get('q_status_kawin');
    if ($query->num_rows() > 0) {
        return $query->row()->STS_NAMA;
    }
}

function convert_nama_dik($DIK_KODIK) {
    $CI = & get_instance();
    $CI->db->where('DIK_KODIK', $DIK_KODIK);
    $query = $CI->db->get('kanreg8_pendik');
    if ($query->num_rows() > 0) {
        return $query->row()->DIK_NMDIK;
    }
}

function convert_jabfun($JBF_KODJAB, $COLUMN = 'JBF_NAMJAB') {
    $CI = & get_instance();
    $CI->db->where('JBF_KODJAB', $JBF_KODJAB);
    $query = $CI->db->get('kanreg8_jabfun');
    if ($query->num_rows() > 0) {
        return $query->row()->$COLUMN;
    }
}

function convert_diklat($DLT_ID, $COLUMN = 'DLT_NAME') {
    $CI = & get_instance();
    $CI->db->where('DLT_ID', $DLT_ID);
    $query = $CI->db->get('q_diklat');
    if ($query->num_rows() > 0) {
        return $query->row()->$COLUMN;
    }
}

function convert_unor($UNO_ID, $COLUMN = 'UNO_NAMUNO') {
    $CI = & get_instance();
    $CI->db->where('UNO_ID', $UNO_ID);
    $query = $CI->db->get('kanreg8_unor');
    if ($query->num_rows() > 0) {
        return $query->row()->$COLUMN;
    }
}

function convert_eselon($ID) {
    $CI = & get_instance();
    $CI->db->where('ESE_KODESL', $ID);
    $query = $CI->db->get('kanreg8_eselon');
    if ($query->num_rows() > 0) {
        return $query->row()->ESE_NAMESL;
    }
}

function convert_jenjab($JJB_JJBKOD, $short = FALSE) {
    $shortener = array('', 'Struktural', 'JFT', 'Rangkap', 'JFU');
    if ($short == TRUE) {
        return $shortener[$JJB_JJBKOD];
    } else {
        $CI = & get_instance();
        $CI->db->where('JJB_JJBKOD', $JJB_JJBKOD);
        $query = $CI->db->get('kanreg8_jenjab');
        if ($query->num_rows() > 0) {
            return $query->row()->JJB_JJBNAM;
        }
    }
}

function getSpesimen($NIP, $kolom = 'SPES_FORMAT') {
    $CI = & get_instance();
    $CI->db->where('SPES_NIPBARU', $NIP);
    $query = $CI->db->get('q_spesimen');
    if ($query->num_rows() > 0) {
        return $query->row()->$kolom;
    }
}

function convert_jk($ID) {
    $CI = & get_instance();
    if ($ID == 1) {
        return 'Pria';
    } else if ($ID == 2) {
        return 'Wanita';
    } else {
        return NULL;
    }
}

function convert_jenis_pmk($val) {
    if ($val == 100) {
        $string = 'Negeri';
    } else if ($val == 50) {
        $string = 'Swasta';
    } else {
        $string = 'Pengurangan';
    }
    return $string;
}

function convert_jenis_kp($JKP_JPNKOD) {
    $CI = & get_instance();
    $CI->db->where('JKP_JPNKOD', $JKP_JPNKOD);
    $query = $CI->db->get('kanreg8_jenis_kp');
    if ($query->num_rows() > 0) {
        return $query->row()->JKP_JPNNAMA;
    }
}

function get_induk_unor($UNO_ID, $x = 0) {
    $CI = & get_instance();
    $CI->db->where('UNO_ID', $UNO_ID);
    $query = $CI->db->get('kanreg8_unor');
    $result = $query->row();
    if ($query->num_rows() > 0) {
        $CI->db->where('UNO_ID', $result->UNO_DIATASAN_ID);
        $query2 = $CI->db->get('kanreg8_unor');
        if ($query2->num_rows() == 0 || @$result->UNO_DIATASAN_ID == NULL) {
            return $result;
        } else {
            return get_induk_unor($result->UNO_DIATASAN_ID, 1);
        }
    }
}

function is_child_unor($kode_unor, $kode_unor_atasan) {
    $CI = & get_instance();
    $CI->db->where('UNO_ID', $kode_unor);
    $query = $CI->db->get('kanreg8_unor');
    if (($query->num_rows() > 0)) {
//echo $query->row()->UNO_NAMUNO;
        if ($query->row()->UNO_DIATASAN_ID == $kode_unor_atasan) {
            return true;
        } else {
            return is_child_unor($query->row()->UNO_DIATASAN_ID, $kode_unor_atasan);
        }
    } else {
        return false;
    }
}

function get_foto($NIP, $URL = FALSE) {
    $path = base_url('contents/foto');
    $filename = get_data_pns($NIP, 'PNS_NIPBARU') . '_' . str_replace('.', '_', str_replace(' ', '_', get_data_pns($NIP, 'PNS_PNSNAM'))) . '.jpg';
//echo $path . '/' . $filename;
    $rand_num = isset($_SESSION['rand_num']) ? $_SESSION['rand_num'] : $_SESSION['rand_num'] = rand(1, 1000);
    $nip_now = isset($_SESSION['nip_now']) ? $_SESSION['nip_now'] : $_SESSION['nip_now'] = $NIP;
    $nip_last = isset($_SESSION['nip_last']) ? $_SESSION['nip_last'] : $_SESSION['nip_last'] = NULL;
    if ($nip_now == $nip_last) {
        @$foto = $_SESSION['foto'];
    } else {
        if (@file_get_contents($path . '/' . $filename)) {
            $foto = $path . '/' . $filename . '?=' . $rand_num;
        } else {
            $foto = $path . '/nofoto.jpg';
        }
    }
    $_SESSION['foto'] = $foto;
    $_SESSION['nip_last'] = $nip_now;
    if ($URL == FALSE) {
        echo '<img src="' . $foto . '" class="img-rounded" width="100%" onclick="loadContent(\'' . base_url('admin/pegawai/upload_foto/' . $NIP) . '\')" style="cursor:pointer;max-width:250px">';
    } else {
        $new = $path . '/' . (@file_get_contents($path . '/' . $filename) ? 'nofoto.jpg' : 'nofoto.jpg');
        return $new;
    }
}

function get_data_pns($NIP, $COLUMN = 'PNS_PNSNAM') {
    $CI = & get_instance();
    $CI->db->select($COLUMN);
    $CI->db->where('PNS_NIPBARU', $NIP);
    $query = $CI->db->get('kanreg8_pupns');
    if (($query->num_rows() > 0)) {
        return $query->row()->$COLUMN;
    } else {
        return NULL;
    }
}

function get_nama_pns($NIP, $FULL = TRUE) {
    $CI = & get_instance();
    $CI->db->select("PNS_PNSNAM, PNS_GLRDPN, PNS_GLRBLK");
    $CI->db->where('PNS_NIPBARU', $NIP);
    $query = $CI->db->get('kanreg8_pupns');
    if (($query->num_rows() > 0)) {
        $data = $query->row();
        $PNS_PNSNAM = $data->PNS_PNSNAM;
        $PNS_GLRDPN = $data->PNS_GLRDPN == NULL ? '' : $data->PNS_GLRDPN . ' ';
        $PNS_GLRBLK = $data->PNS_GLRBLK == NULL ? '' : ', ' . $data->PNS_GLRBLK;

        return $PNS_GLRDPN . $PNS_PNSNAM . $PNS_GLRBLK;
    } else {
        return NULL;
    }
}

function uuid() {
    $CI = & get_instance();
    $CI->db->select('UUID() AS id', true);
    $query = $CI->db->get();
//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return str_replace('-', '', strtoupper($query->row()->id));
    }
}

function get_mks($tmt_cpns, $pmk_month = 0, $enddate = null) {
    $start = strtotime($tmt_cpns);
    $end = $enddate == null ? now() : strtotime($enddate);
    //echo $end . ' ' . now();
    $month = 0;
    $start = strtotime("+1 month", $start);
    while ($start < $end) {
        //echo date('d F Y', $start), PHP_EOL;
        $start = strtotime("+1 month", $start);
        $month++;
    }
    $month += $pmk_month;
    //echo ' '.$month;
    $tahun = floor($month / 12);
    $bulan = $month % 12;
    $data = (object) array('tahun' => $tahun, 'bulan' => $bulan);
    return $data;
}

function get_mkg($gol_awal, $gol_akhir, $tmt_cpns, $pmk_month = 0, $enddate = null) {
    $start = strtotime($tmt_cpns);
    $end = $enddate == null ? now() : strtotime($enddate);
    //echo $end . ' ' . now();
    $month = 0;
    $start = strtotime("+1 month", $start);
    while ($start < $end) {
        //echo date('F Y', $month), PHP_EOL;
        $start = strtotime("+1 month", $start);
        $month++;
    }
    $month += $pmk_month;
    //echo ' '.$month;
//PENAMBAHAN MASA KERJA FIKTIF//
    if ($gol_awal == 12 || $gol_awal == 13 || $gol_awal == 14) {
        $month += 3 * 12;
    } else if ($gol_awal == 22 || $gol_awal == 23 || $gol_awal == 24) {
        $month += 3 * 12;
    }
//PENYESUAIAN PINDAH GOLONGAN//
    if (substr($gol_awal, 0, 1) == 1 && substr($gol_akhir, 0, 1) == 2) { //AWAL GOL I & AKHIR GOL 2
        $month -= 6 * 12;
    } else if (substr($gol_awal, 0, 1) == 1 &&
            (substr($gol_akhir, 0, 1) == 3 || substr($gol_akhir, 0, 1) == 4)) { //AWAL GOL I & AKHIR (GOL III / IV)
        $month -= 11 * 12;
    } else if (substr($gol_awal, 0, 1) == 2 &&
            (substr($gol_akhir, 0, 1) == 3 || substr($gol_akhir, 0, 1) == 4)) { //AWAL GOL 2 & AKHIR (GOL III / IV)
        $month -= 5 * 12;
    }
    $tahun = floor($month / 12);
    $bulan = $month % 12;
    $data = (object) array('tahun' => $tahun, 'bulan' => $bulan);
    return $data;
}

function convert_golongan($input) {
    if ($input == NULL) {
        $x = '';
    } else if (is_numeric($input)) {
        $x = '';
        switch (substr($input, 0, 1)) {
            case 1: $x .= 'I';
                break;
            case 2: $x .= 'II';
                break;
            case 3: $x .= 'III';
                break;
            case 4: $x .= 'IV';
                break;
            default : break;
        }
        switch (substr($input, 1, 1)) {
            case 1: $x .= '/a';
                break;
            case 2: $x .= '/b';
                break;
            case 3: $x .= '/c';
                break;
            case 4: $x .= '/d';
                break;
            case 5: $x .= '/e';
                break;
            default : break;
        }
    } else {
        $n = explode('/', $input);
        $x = '';
        switch (strtoupper($n[0])) {
            case 'I':$x .= 1;
                break;
            case 'II':$x .= 2;
                break;
            case 'III':$x .= 3;
                break;
            case 'IV':$x .= 4;
                break;
            default : break;
        }switch (strtolower($n[1])) {
            case 'a':$x .= 1;
                break;
            case 'b':$x .= 2;
                break;
            case 'c':$x .= 3;
                break;
            case 'd':$x .= 4;
                break;
            default : break;
        }
    }
    return $x;
}

function cek_cuti_this_year($NIP) {
    $CI = & get_instance();
    $CI->db->where('RWCUTI_THNCUTI', date('Y'));
    $querya = $CI->db->get('q_datarwcuti');
    if ($querya->num_rows() > 0) {
        $CI->db->where('RWCUTI_NIP', $NIP);
        $CI->db->where('RWCUTI_THNCUTI', date('Y'));
        $CI->db->group_start();
        $CI->db->where('RWCUTI_JNS', -1); //sisa tahun lalu
        $CI->db->or_where('RWCUTI_JNS', 0); //jatah tahun ini
        $CI->db->group_end();
        $query = $CI->db->get('q_datarwcuti');
//echo $CI->db->last_query();
//echo $query->num_rows();
        if ($query->num_rows() == 2) { //ada jatah tahun ini dan ada sisa tahun lalu
            return 'TERDEFINISI';
        } else {
            $CI->db->where('RWCUTI_NIP', $NIP);
            $CI->db->where('RWCUTI_JNS<=', 1);
            $CI->db->where('RWCUTI_JNS>=', -1);
            $CI->db->where('RWCUTI_THNCUTI ', date('Y') - 1);
            $CI->db->select('IFNULL(SUM(RWCUTI_JUM),0) as JUM');
            $query2 = $CI->db->get('q_datarwcuti');
//echo $CI->db->last_query();
            $data['sisa_tahun_lalu'] = $query2->row()->JUM; //Sisa Cuti Y -1

            /*
              $CI->db->where('RWCUTI_NIP', $NIP);
              $CI->db->where('RWCUTI_JNS<=', 1);
              $CI->db->where('RWCUTI_JNS>=', 0);
              $CI->db->where('RWCUTI_THNCUTI ', date('Y') - 2);
              $CI->db->select('IFNULL(SUM(RWCUTI_JUM),0) as JUM');
              $query3 = $CI->db->get('q_datarwcuti');
              echo $CI->db->last_query();
              $data[2] = $query3->row()->JUM; //Sisa Cuti Y-2

              $CI->db->where('RWCUTI_NIP', $NIP);
              $CI->db->where('RWCUTI_JNS', 1);
              $CI->db->where('RWCUTI_THNCUTI ', date('Y') - 1);
              $CI->db->select('count(*) as JUM');
              $query3 = $CI->db->get('q_datarwcuti');
              //echo $CI->db->last_query();
              $data[3] = $Y1 = $query3->row()->JUM; //Jumlah Pemakaian Y-1


              $CI->db->where('RWCUTI_NIP', $NIP);
              $CI->db->where('RWCUTI_JNS', 1);
              $CI->db->where('RWCUTI_THNCUTI ', date('Y') - 2);
              $CI->db->select('count(*) as JUM');
              $query3 = $CI->db->get('q_datarwcuti');
              //echo $CI->db->last_query();
              $data[4] = $Y2 = $query3->row()->JUM; //Jumlah Pemakaian Y-2


              if (($data[2] >= 6) && ($Y1 == 0 && $data[1] >= 12)) { //tahun-2 masih sisa >=6 dan (tahun-1 tidak digunakan sama sekali dan sisanya >=12) (maksimal 12)
              $available = 12;
              } else if ($data[1] >= 6) {//cuti tahun-1 sisa >=6 (maksimal 6)
              $available = 6;
              } else {//cuti tahun-1 sisa <6
              $available = $data[1];
              }
              $data[5] = $available; //sisa yang dapat diambil
             * 
             */
            return $data;
        }
    } else {
        return 'BELUM TERDEFINISI SEMUA';
    }
}

function getDataCuti($NIP = NULL) {
    $CI = & get_instance();
    $sql = "
SELECT a.RWCUTI_NIP as nip,
b.PNS_PNSNAM as nama,
SUM(IF(a.RWCUTI_JNS=-1,a.RWCUTI_JUM,0)) AS sisa_tahun_lalu, 
SUM(IF(a.RWCUTI_JNS=0,a.RWCUTI_JUM,0)) AS jatah_tahun_ini,
SUM(IF(a.RWCUTI_JNS=1,a.RWCUTI_JUM,0)) AS cuti_tertanggung,
SUM(IF(a.RWCUTI_JNS>1,a.RWCUTI_JUM,0)) AS cuti_tak_tertanggung,
SUM(IF(a.RWCUTI_JNS<=1,a.RWCUTI_JUM,0)) AS sisa_cuti_tertanggung
FROM q_datarwcuti a
RIGHT JOIN `kanreg8_pupns` b
ON a.RWCUTI_NIP=b.PNS_NIPBARU AND b.PNS_KEDHUK<=20
WHERE a.RWCUTI_THNCUTI=YEAR(NOW()) " . ($NIP != NULL ? (' AND b.PNS_NIPBARU="' . $NIP . '"') : '' ) . "
GROUP BY a.RWCUTI_NIP 
ORDER BY b.PNS_PNSNAM 
";
    $query = $CI->db->query($sql);
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $d) {
            $data[] = $d;
        }
        return $data;
    } else {
        return NULL;
    }
}

function cek_jatah_cutinew($TAHUN = NULL) {
    $CI = & get_instance();
    $CI->db->where('tahun ', $TAHUN == NULL ? date('Y') : $TAHUN);
    $CI->db->select('jatah');
    $query = $CI->db->get('q_jatah_cuti');
//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row()->jatah;
    } else {
        return 0;
    }
}

function cek_sisa_cutinew($NIP, $TAHUN = NULL) {
    $TAHUN = $TAHUN == date('Y') ? NULL : $TAHUN;
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('RWCUTI_NIP', $NIP);
    $CI->db->where('RWCUTI_JNS<=', 1);
    $CI->db->where('RWCUTI_THNCUTI ', $TAHUN == NULL ? date('Y') : $TAHUN);
    $CI->db->select('IFNULL(SUM(RWCUTI_JUM),0) as JUM');
    $query3 = $CI->db->get('q_datarwcutinew');
//echo $CI->db->last_query();
//    return $query3->row()->JUM;

    $CI->db->where('tahun ', $TAHUN == NULL ? date('Y') : $TAHUN);
    $CI->db->select('jatah');
    $query = $CI->db->get('q_jatah_cuti');

    $CI->db->where('RWCUTI_NIP', $NIP);
    $CI->db->where('RWCUTI_JNS', -1);
    $CI->db->where('RWCUTI_THNCUTI ', date('Y'));
    $queryx = $CI->db->get('q_datarwcutinew');
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return FALSE;
    } else {
        $CI->db->trans_commit();
        if ($queryx->num_rows() > 0 || $TAHUN != NULL) {
            return $query3->row()->JUM + $query->row()->jatah;
        } else {
            return NULL;
        }
    }
//echo $CI->db->last_query();
}

function get_detail_cuti($NIP) {
    $CI = & get_instance();
    $CI->db->where('RWCUTI_NIP', $NIP);
    $CI->db->where('RWCUTI_THNCUTI ', date('Y'));
    $query3 = $CI->db->get('q_datarwcutinew');
    if ($query3->num_rows() > 0) {
        foreach ($query3->result() as $d) {
            $data[] = $d;
        }
        return $data;
    } else {
        return NULL;
    }
//echo $CI->db->last_query();
}

function get_nip_unor($UNO_ID, $getNIP = TRUE, $showPensiun = FALSE, $firstRun = 1) { //getNIP=menghasilkan NIP, FALSE menghasilkan unor bawahan
    $CI = & get_instance();
    $result = array('147258369741852963');
    if ($CI->ion_auth->is_admin()) {
        $CI->db->trans_begin();
        if ($getNIP == TRUE) {
            $CI->db->select("PNS_NIPBARU");
            $CI->db->where('PNS_INSKER', INSTANSI_KERJA);
            if ($showPensiun == FALSE) {
                $CI->db->where('PNS_KEDHUK <', 20);
            }
            $query = $CI->db->get('kanreg8_pupns');

//echo $this->db->last_query();
            if ($CI->db->trans_status() === FALSE) {
                $CI->db->trans_rollback();
            } else {
                $CI->db->trans_commit();
            }
            if ($query->num_rows() > 0) {

                foreach ($query->result() as $p) {
                    $result[] = $p->PNS_NIPBARU;
                }
            }
        } else {
            $result[] = "'" . $UNO_ID . "'";
            $CI->db->trans_begin();
            $sql2 = "SELECT UNO_NAMUNO,UNO_ID"
                    . " FROM kanreg8_unor where UNO_DIATASAN_ID='" . $UNO_ID . "' AND UNO_NAMUNO NOT LIKE '%###DELETED###%'  order by UNO_KODUNO,UNO_KODESL ";
            $query2 = $CI->db->query($sql2);

            if ($CI->db->trans_status() === FALSE) {
                $CI->db->trans_rollback();
            } else {
                $CI->db->trans_commit();
            }
            if ($query2->num_rows() > 0) {
                foreach ($query2->result() as $q) {
                    $newresult = get_nip_unor($q->UNO_ID, $getNIP, $showPensiun, 0);
                    array_shift($newresult);
                    $result = array_merge($result, $newresult);
                }
            }
        }
    } else {
//        if ($firstRun) {
//            $UNO_ID = $CI->ion_auth->is_admin() ? SKPD : $UNO_ID;
//        }
        $CI->db->trans_begin();
        if ($getNIP == TRUE) {
            $CI->db->select("PNS_NIPBARU");
            $CI->db->where('PNS_UNOR', $UNO_ID);
            if ($showPensiun == FALSE) {
                $CI->db->where('PNS_KEDHUK <', 20);
            }
            $query = $CI->db->get('kanreg8_pupns');

//echo $this->db->last_query();
            if ($CI->db->trans_status() === FALSE) {
                $CI->db->trans_rollback();
            } else {
                $CI->db->trans_commit();
            }
            if ($query->num_rows() > 0) {

                foreach ($query->result() as $p) {
                    $result[] = $p->PNS_NIPBARU;
                }
            }
        } else {
            $result[] = "'" . $UNO_ID . "'";
        }
        $CI->db->trans_begin();
        $sql2 = "SELECT UNO_NAMUNO,UNO_ID"
                . " FROM kanreg8_unor where UNO_DIATASAN_ID='" . $UNO_ID . "' AND UNO_NAMUNO NOT LIKE '%###DELETED###%'  order by UNO_KODUNO,UNO_KODESL ";
        $query2 = $CI->db->query($sql2);

        if ($CI->db->trans_status() === FALSE) {
            $CI->db->trans_rollback();
        } else {
            $CI->db->trans_commit();
        }
//echo $this->db->last_query();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result() as $q) {
                $newresult = get_nip_unor($q->UNO_ID, $getNIP, $showPensiun, 0);
                array_shift($newresult);
                $result = array_merge($result, $newresult);
            }
        }
    }


    return $result;
}

function searchForId($id, $array, $name = NULL) {
    foreach ($array as $key => $val) {
        if ($name == NULL) {
            if ($val == $id) {
                return $key;
            }
        } else {
            if ($val[$name] == $id) {
                return $key;
            }
        }
    }
    return null;
}

function create_unor_jqtree($array, $parent_id = NULL) {
    $return = array();
    foreach ($array as $k => $v) {
        $temp = array();
        $run = false;
        if ($parent_id == $v['UNO_DIATASAN_ID'] || $parent_id == NULL) {
            $run = true;
        }
        if ($run) {
            $temp['name'] = $v['UNO_NAMUNO'];
            $temp['id'] = $v['UNO_ID'];
            $temp['pejabat'] = $v['UNO_NAMJAB'];
            $temp['eselon'] = $v['UNO_KODESL'];
            $temp['kode'] = $v['UNO_KODUNO'];
//$temp['namatasan'] = $v['NAMATASAN'];
//$temp['endChild'] = $v['endChild'];
            if ($parent_id != NULL) {
                if ($v['endChild'] > 0) {
                    $temp['children'] = create_unor_jqtree($array, $v['UNO_ID']);
                    $temp['load_on_demand'] = true;
                }
            } else {
                $temp['load_on_demand'] = true;
            }
            $return[] = $temp;
        }
    }
    return $return;
}

function create_unor2_jqtree($array, $parent_id = '', $no = 1, $parent = 0, $depth = 0) {
    $return = array();
    $return2 = array();
    foreach ($array as $k => $v) {
        $temp = array();
        $run = false;

        if (($parent_id == '' && (strtoupper($v['UNO_DIATASAN_ID']) == '' || strtoupper($v['UNO_ID']) == SKPD)) || $parent_id == strtoupper($v['UNO_DIATASAN_ID'])) {
            $run = true;
        }
        if ($run) {
            $temp['no'] = $no;
            $temp['pejabat'] = $v['pejabat'];
            $temp['xpejabat'] = $v['xpejabat'];
            $temp['name'] = $v['UNO_NAMUNO'];
            $temp['id'] = strtoupper($v['UNO_ID']);
            $temp['jabatan'] = $v['UNO_NAMJAB'];
            $temp['eselon'] = $v['UNO_KODESL'];
            $temp['kode'] = $v['UNO_KODUNO'];
            $temp['atasan'] = strtoupper($v['UNO_DIATASAN_ID']);
            $temp['parent'] = $parent;
            $temp['depth'] = $depth;
            $temp['endChild'] = $v['endChild'];
            $return[] = $temp;
            $return2[] = $temp['id'];
            $no++;
            if ($v['endChild'] > 0) {
                $result = create_unor2_jqtree($array, strtoupper($v['UNO_ID']), $no, $temp['no'], $depth+1);
                if ($result) {
                    foreach ($result[0] as $x) {
                        $return[] = $x;
                        $return2[] = @$x['id'];
                    }
                }
            }
            $no = $no + $v['endChild'];
        }
    }

    if (!empty($return)) {
        //return $return;
        return array('0' => $return, '1' => $return2);
    } else {
        return FALSE;
    }
}

function create_pegawai_jqtree($array, $parent_id = '') {
    $return = array();
    $return2 = array();
    foreach ($array as $k => $v) {
        $temp = array();
        $run = false;
        if (($parent_id == '' && strtoupper($v['UNO_ID']) == SKPD) || $parent_id == strtoupper($v['UNO_DIATASAN_ID'])) {
            $run = true;
        }
        if ($run) {
            $temp['nama'] = $v['PNS_PNSNAM'];
            $temp['nip'] = $v['PNS_NIPBARU'];
            $temp['jabatan'] = $v['RWJAB_NAMAJAB'];
            $temp['unor'] = $v['RWJAB_NAMUNO'];
            $temp['id'] = strtoupper($v['UNO_ID']);
            $temp['atasan'] = strtoupper($v['UNO_DIATASAN_ID']) == SKPD ? '' : strtoupper($v['UNO_DIATASAN_ID']);
            $return[] = $temp;
            $return2[] = $temp['nip'];
            if ($v['endChild'] > 0) {
                $result = create_pegawai_jqtree($array, strtoupper($v['UNO_ID']));
                if ($result) {
                    foreach ($result[0] as $x) {
                        $return[] = $x;
                        $return2[] = @$x['nip'];
                    }
                }
            }
        }
    }
    if (!empty($return)) {
        //return $return;
        return array('0' => $return, '1' => $return2);
    } else {
        return FALSE;
    }
}

function toArray($obj) {
    $obj = (array) $obj;
    return $obj;
}

function get_gapok($golru, $mkg, $tahun = NULL) {
    $CI = & get_instance();
    $CI->db->where('golongan', $golru);
    $CI->db->where($mkg . "-mkg>=", 0, FALSE);
    if ($tahun != NULL) {
        $CI->db->where($tahun_buat . "-tahun_buat>=", 0, FALSE);
    }
    $CI->db->order_by('tahun_buat', 'DESC');
    $CI->db->order_by('mkg', 'DESC');
    $CI->db->limit(1);


    $query = $CI->db->get('q_gapok');

//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row()->gaji_pokok;
    }
}

function get_jab_pegawai($nip) {
    $CI = & get_instance();
    $CI->db->order_by('RWJAB_TMTJAB', 'DESC');
    $CI->db->where('RWJAB_NIP', $nip);
    $query = $CI->db->get('z_datarwjabatan');
//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row()->RWJAB_NAMAJAB;
    }
}

function get_spesimen($NIP = FALSE) {
    $CI = & get_instance();
    if ($NIP == FALSE) {
        $query = $CI->db->get('q_spesimen');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    } else {
        $query = $CI->db->get('q_spesimen');
        $CI->db->where('SPES_NIPBARU', $NIP);
        if ($query->num_rows() > 0) {
            return $query->row()->SPES_FORMAT;
        } else {
            return false;
        }
    }
}

function get_eselon() {
    $CI = & get_instance();
    $query = $CI->db->get('kanreg8_eselon');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return $return;
    } else {
        return false;
    }
}

function get_data_unor($id) {
    $CI = & get_instance();
    $CI->db->where('UNO_ID', $id);
    $query = $CI->db->get('kanreg8_unor');
//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return false;
    }
}

function next_golru($gol) {
    $golru = array(11, 12, 13, 14, 21, 22, 23, 24, 31, 32, 33, 34, 41, 42, 43, 44, 45);
    $return = 0;
    $val = NULL;
    foreach ($golru as $g) {
        if ($return == 1) {
            $return = 0;
            $val = $g;
        }
        if ($gol == $g) {
            $return = 1;
        }
    }
    return $val;
}

function get_periode_kp($id_date = NULL) {
    if ($id_date != NULL && (date('Y-m-d', strtotime($id_date)) > date('Y-m-d'))) {
        $ardate = explode('-', $id_date);
    } else {
        $ardate = explode('-', date('d-m-Y'));
    }
    //print_r($ardate);
    $bln = (string) $ardate[1];
    if (($bln >= 1 && $bln <= 4)) {
        $newdate = '01-04-' . $ardate[2];
    } else if ($bln >= 5 && $bln <= 10) {
        $newdate = '01-10-' . $ardate[2];
    } else {
        $newdate = '01-04-' . ($ardate[2] + 1);
    }


    return $newdate;
}
