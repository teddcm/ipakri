<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function json_result($title, $message, $etc = null) {//digunakan untuk result ajax dengan data json
    echo json_encode(array($title, $message, $etc));
    exit;
}

function getPengaturan($nama_pengaturan) {
    $CI = & get_instance();
    $CI->db->where('nama_pengaturan', $nama_pengaturan);
    $query = $CI->db->get('admin_pengaturan');
    if ($query->num_rows() > 0) {
        return $query->row()->value_pengaturan;
    }
}

function savePengaturan($nama_pengaturan, $value) {
    $CI = & get_instance();
    $CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->set('value_pengaturan', $value);
    if ($query = $CI->db->update('admin_pengaturan')) {
        return TRUE;
    }
}

function reverseDate($date) { //membalik format YYYY-MM-DD ke DD-MM-YYY dan sebaliknya
    $date = substr($date, 0, 10);
    $date = str_replace(' ', '', $date);
    if ($date != '') {
        $newdate = explode('-', $date);
        return $newdate[2] . '-' . $newdate[1] . '-' . $newdate[0];
    }
}

function convertDate($date, $format = 'd F Y', $bahasa = "id") {
    $en = array(
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tueday", "Wedday", "Thursday", "Friday", "Saturday",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

    $id = array(
        "Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab",
        "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",
        "Januari", "Pebruari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember",
        "Jan", "Peb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nop", "Des");
    return str_replace($en, $$bahasa, date($format, $date));
}

function arraytoupper($input) {
    $narray = array();
    if (!is_array($input)) {
        return $narray;
    }
    foreach ($input as $key => $value) {
        if (is_array($value)) {
            $narray[$key] = array_change_value_case($value, $case);
            continue;
        }
        $narray[$key] = strtoupper($value);
    }
    return $narray;
}

function ucwords_strtolower($string) {
    return ucwords(strtolower($string));
}

function terbilang_rupiah($x) {
    $terbilang = terbilang($x);
    return trim($terbilang . " rupiah");
}

function angka_rupiah($x) {
    return "Rp. " . number_format($x, "0", ",", ".") . ",-";
}

function terbilang($x, $first = false) {
    $ambil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh",
        "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x <= 0) {
        return $first == false ? 'nol' : '';
    } elseif ($x < 12) {
        return " " . $ambil[$x];
    } elseif ($x < 20) {
        return terbilang($x - 10, true) . " belas";
    } elseif ($x < 100) {
        return terbilang($x / 10, true) . " puluh" . terbilang($x % 10, true);
    } elseif ($x < 200) {
        return " seratus" . terbilang($x - 100, true);
    } elseif ($x < 1000) {
        return terbilang($x / 100, true) . " ratus" . terbilang($x % 100, true);
    } elseif ($x < 2000) {
        return " seribu" . terbilang($x - 1000, true);
    } elseif ($x < 1000000) {
        return terbilang($x / 1000, true) . " ribu" . terbilang($x % 1000, true);
    } elseif ($x < 1000000000) {
        return terbilang($x / 1000000, true) . " juta" . terbilang($x % 1000000, true);
    }
}

function convertRomawi($n) {
    $result = "";
    $iromawi = array("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
        20 => "XX", 30 => "XXX", 40 => "XL", 50 => "L", 60 => "LX",
        70 => "LXX", 80 => "LXXX", 90 => "XC", 100 => "C", 200 => "CC",
        300 => "CCC", 400 => "CD", 500 => "D", 600 => "DC",
        700 => "DCC", 800 => "DCCC",
        900 => "CM", 1000 => "M",
        2000 => "MM", 3000 => "MMM");
    if (array_key_exists($n, $iromawi)) {
        $result = $iromawi[$n];
    } elseif ($n >= 11 && $n <= 99) {
        $i = $n % 10;
        $result = $iromawi[$n - $i] . convertRomawi($n % 10);
    } elseif ($n >= 101 && $n <= 999) {
        $i = $n % 100;
        $result = $iromawi[$n - $i] . convertRomawi($n % 100);
    } else {
        $i = $n % 1000;
        $result = $iromawi[$n - $i] . convertRomawi($n % 1000);
    }
    return $result;
}

function safe_encode($string) {
    $CI = & get_instance();
    $enc = $CI->encryption->encrypt($string);
    return str_replace(array('+', '/', '='), array('-', '_', '~'), $enc);
}

function safe_decode($string) {
    $CI = & get_instance();
    $dec = str_replace(array('-', '_', '~'), array('+', '/', '='), $string);
    return $CI->encryption->decrypt($dec);
}

function no_access() {
    json_result('error', 'Anda tidak memiliki akses ke halaman ini.<br>Silakan hubungi Administrator');
}

function duplicate_table($table_name, $table_target) {
    $CI = & get_instance();
    $CI->db->trans_begin();
    $isExist = $CI->db->query("SHOW TABLES LIKE '" . $table_name . "'");
    if ($isExist->num_rows() > 0) {
        $CI->db->query("DROP TABLE $table_name");
    }
    $CI->db->query("CREATE TABLE $table_name LIKE $table_target");
    $query = $CI->db->get($table_target);
    foreach ($query->result() as $row) {
        $CI->db->insert($table_name, $row);
    }
    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

function UniqueMachineID($salt = NULL) {
    return md5($salt . md5($_SERVER['HTTP_HOST']));
}

function MatchID($UniqueMachineID) {
    return MACHINE_ID ? ($UniqueMachineID === UniqueMachineID() ? true : false) : true;
}

function isValidDate($date, $separator) { //untuk DD-MM-YYYY
    if (count(explode($separator, $date)) == 3) {
        $pattern = "/^([0-9]{2})" . $separator . "([0-9]{2})" . $separator . "([0-9]{4})$/";
        if (preg_match($pattern, $date, $parts)) {
            //echo $parts[1]. $parts[2]. $parts[3];
            if (checkdate($parts[2], $parts[1], $parts[3]))
                return true;
            /* This is a valid date */
            else
                return false;
            /* This is an invalid date */
        } else {
            return false;
            /* This is an invalid date in terms of format */
        }
    } else {
        return false;
        /* Day, Month, Year - either of them not present */
    }
}

function scaleImage($source_image_path, $maxWidth, $maxHeight, $thumbnail_image_path) {
    list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
            $source_gd_image = imagecreatefromgif($source_image_path);
            break;
        case IMAGETYPE_JPEG:
            $source_gd_image = imagecreatefromjpeg($source_image_path);
            break;
        case IMAGETYPE_PNG:
            $source_gd_image = imagecreatefrompng($source_image_path);
            break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    $thumbnail_image_width = $maxWidth;
    $thumbnail_image_height = $maxHeight;

    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
    imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}

function get_timeago($ptime) {
    $estimate_time = time() - $ptime;

    if ($estimate_time < 1) {
        return 'baru saja';
    }

    $condition = array(
        12 * 30 * 24 * 60 * 60 => 'tahun',
        30 * 24 * 60 * 60 => 'bulan',
        24 * 60 * 60 => 'hari',
        60 * 60 => 'jam',
        60 => 'menit',
        1 => 'detik'
    );

    foreach ($condition as $secs => $str) {
        $d = $estimate_time / $secs;

        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ' yang lalu';
        }
    }
}

?>