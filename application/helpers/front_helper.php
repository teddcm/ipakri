<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function getPengaturanFront($nama_pengaturan) {
    $CI = & get_instance();
    $CI->db->where('nama_pengaturan', $nama_pengaturan);
    $query = $CI->db->get('front_pengaturan');
    if ($query->num_rows() > 0) {
        return $query->row()->value_pengaturan;
    }
}

/* * ******************** linker ***************** */

function getLinker() {
    $CI = & get_instance();
    //$CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->order_by('link_order', 'asc');
    $query = $CI->db->get('front_linker');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

function linkerRevision($id) {
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('link_id', $id);
    $CI->db->set('link_revision', 'link_revision+1', FALSE);
    $query = $CI->db->update('front_linker');
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

/* * ******************** slider ***************** */

function getSlider() {
    $CI = & get_instance();
    //$CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->order_by('slid_order', 'asc');
    $query = $CI->db->get('front_slider');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

function sliderRevision($id) {
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('slid_id', $id);
    $CI->db->set('slid_revision', 'slid_revision+1', FALSE);
    $query = $CI->db->update('front_slider');
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

/* * ******************** welcome ***************** */

function getWelcome() {
    $CI = & get_instance();
    //$CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->order_by('welc_order', 'asc');
    $query = $CI->db->get('front_welcome');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

/* * ******************** post ***************** */

function getPost($type = 0, $offset = 0, $limit = 0) {
    $CI = & get_instance();
    if ($type > 0) {
        $CI->db->where('post_type', $type);
    }
    $queryx = $CI->db->get('front_post');
    if ($type > 0) {
        $CI->db->where('post_type', $type);
    }
    $CI->db->order_by('post_created', 'desc');
    $query = $CI->db->get('front_post', $limit, $offset);
    //echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        $return['num_rows'] = $queryx->num_rows();
        $return['data'] = $data;
        return $return;
    }
}

function postRevision($id) {
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('post_id', $id);
    $CI->db->set('post_revision', 'post_revision+1', FALSE);
    $query = $CI->db->update('front_post');
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

function simpanTempCuti($data) {
    $nip = $data['nip'];
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('id', $data['id']);
    $CI->db->delete('front_cuti_temp');
    $data['created'] = date(now());
    $query = $CI->db->insert('front_cuti_temp', $data);
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

/* * ******************** halaman ***************** */

function getHalaman($type = 0) {
    $CI = & get_instance();
    if ($type > 0) {
        $CI->db->where('hlmn_type', $type);
    }
    //$CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->order_by('hlmn_order', 'asc');
    $query = $CI->db->get('front_halaman');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

function halamanRevision($id) {
    $CI = & get_instance();
    $CI->db->trans_begin();
    $CI->db->where('hlmn_id', $id);
    $CI->db->set('hlmn_revision', 'hlmn_revision+1', FALSE);
    $query = $CI->db->update('front_halaman');
    //echo $CI->db->last_query();

    if ($CI->db->trans_status() === FALSE) {
        $CI->db->trans_rollback();
        return false;
    } else {
        $CI->db->trans_commit();
        return true;
    }
}

/* * ******************** galeri ***************** */

function getGaleri($id_album) {
    $CI = & get_instance();
    $CI->db->where('glri_album', $id_album);
    $CI->db->order_by('glri_id', 'asc');
    $query = $CI->db->get('front_galeri');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

function getAlbum($offset = 0, $limit = 0, $id = NULL) {
    $CI = & get_instance();
    $queryx = $CI->db->get('front_album');

    if ($id != NULL) {
        $return['foto'] = (getGaleri($id));
        $CI->db->where('albm_id', $id);
    }
    $CI->db->order_by('albm_created', 'desc');
    $query = $CI->db->get('front_album', $limit, $offset);
    //echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            if ($id == NULL) {
                $tmpArray = (getGaleri($x->albm_id));
                if ($tmpArray !== NULL) {
                    $x->albm_preview = array_shift($tmpArray)->glri_id;
                }
            }
            $data[] = $x;
        }
        $return['num_rows'] = $queryx->num_rows();
        $return['data'] = $data;
        return $return;
    }
}

/* * ******************** data PNS ***************** */

function getPNS($NIP) {
    $CI = & get_instance();
    $sql = "
 SELECT A.*,
       C.`PEN_PENKOD`,
       C.`PEN_TAHLUL`,
       D.`DIK_NMDIK`,
       E.*,
       F.`UNO_NAMUNO`,
       F.`UNO_KODUNO`,
       F.`UNO_ID`,
       G.`RWJAB_NAMAJAB`,
       G.`RWJAB_TMTJAB`,
       G.`RWJAB_IDJENJAB`
FROM   kanreg8_pupns A
       LEFT JOIN (SELECT *
                  FROM   kanreg8_pupns_pendidikan
                  WHERE  `PNS_NIPBARU` = '" . $NIP . "'
                  ORDER  BY `PEN_TAHLUL` DESC
                  LIMIT  1) C
              ON A.`PNS_NIPBARU` = C.`PNS_NIPBARU`
       LEFT JOIN kanreg8_pendik D
              ON C.`PEN_PENKOD` = D.`DIK_KODIK`
       LEFT JOIN kanreg8_instansi E
              ON A.`PNS_INSKER` = E.`INS_KODINS`
       LEFT JOIN kanreg8_unor F
              ON A.`PNS_UNOR` = F.`UNO_ID`
       LEFT JOIN (SELECT *
                  FROM   z_datarwjabatan
                  WHERE  RWJAB_NIP = '" . $NIP . "'
                  ORDER  BY `RWJAB_TMTJAB` DESC
                  LIMIT  1) G
              ON A.`PNS_NIPBARU` = G.`RWJAB_NIP`
WHERE  A.`PNS_NIPBARU` = '" . $NIP . "'   
";
    $query = $CI->db->query($sql);
    if (($query->num_rows() > 0)) {
        return $query->row();
    } else {
        return NULL;
    }
}

/* * ******************** unduhan ***************** */

function getUnduhan() {
    $CI = & get_instance();
    //$CI->db->where('nama_pengaturan', $nama_pengaturan);
    $CI->db->order_by('undh_created', 'asc');
    $query = $CI->db->get('front_unduhan');
    if ($query->num_rows() > 0) {
        foreach ($query->result() as $x) {
            $data[] = $x;
        }
        return $data;
    }
}

?>