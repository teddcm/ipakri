<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('session', 'database', 'bcrypt', 'form_validation', 'encryption', 'ion_auth');
$autoload['drivers'] = array();
$autoload['helper'] = array('url', 'security', 'auth', 'tcm', 'form', 'pegawai', 'string', 'language');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();
