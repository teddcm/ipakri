<div class="container">
    <div class="footer-top">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-menu">
                    <h5>Lokasi</h5>
                    <a href="https://goo.gl/maps/faoDge4c8PM2" title="Menuju Dinas PU Kalteng" target="_BLANK">
                        <img src="<?php echo base_url('frontend/img/peta.jpg'); ?>" class="img-responsive">
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-menu">
                    <h5>Kontak</h5>
                    <div class="detail-company">
                        <span><i class="fa fa-map"></i>Jl. S. Parman No. 3 Palangka Raya 73112 </span>
                        <span><i class="fa fa-envelope"></i>pu@kalteng.go.id</span>
                        <span><i class="fa fa-phone"></i>(0536) 3221015</span>
                        <span><i class="fa fa-fax"></i>(0536) 3224758</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-menu">
                    <h5>Info Terkini Kalteng</h5>
                    <ul style="list-style-type: square"> <?php
                        $this->load->library('rssparser');
                        $rss = $this->rssparser->set_feed_url('https://kalteng.antaranews.com/rss/terkini.xml')->set_cache_life(30)->getFeed(6);
                        foreach ($rss as $item) {
                            ?>
                            <li style="color: white"><a href="<?php echo $item['link']; ?>" target="_BLANK"><?php echo $item['title']; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="copyright">© 2015 Copyright. All rights reserved</div>
    </div>
</div>