<?php
$page = $this->uri->segment(4) == null ? 0 : $this->uri->segment(4);
$perpage = 4;
$data = getAlbum($page, $perpage);
$numrows = $data['num_rows'];
$data = $data['data'];
?>
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <input id="input-foto" name="userfile[]" type="file" multiple class="file-loading">
                <input id="foto_id" name="foto_id" type="hidden">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"> 

                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="albm_id" name="albm_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-9">
                            <label>Judul</label>
                            <input class="form-control" name="albm_nama" id="albm_nama">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
    .img-wrap {
        position: relative;
        height: 75px;
        width: 75px;
        float:left;
        margin-right: 2px;
        display: flex;
        justify-content: center;
        align-items: center;
        overflow: hidden
    }
    .img-wrap .hapus-foto {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Album</h3></div>
<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Album</a></li>
            <li class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button>  
            </li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kepala">
                &nbsp;
                <table class="table table-striped table-border table-condensed dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:20%">Foto</th>
                            <th>Konten</th>
                            <th style="width:15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $n = $page + 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {
                                echo "<tr>";
                                echo "<td>" . $n++ . "</td>";
                                echo "<td>" . $d->albm_nama . "</td>";
                                echo "<td>";
                                $foto = getGaleri($d->albm_id);
                                if ($foto != NULL) {
                                    foreach ($foto as $f) {
                                        echo '<div class="img-wrap">'
                                        . '<i class="hapus-foto btn btn-xs btn-danger btn-flat" id="' . $f->glri_id . '"><i class="fa fa-close"></i></i>'
                                        . '<img height="75px" src="' . base_url('frontend/img/galleries/thumbs/' . $f->glri_id . '_thumb.jpg') . '">'
                                        . '</div>';
                                    }
                                }
                                echo '<div class="tambah-foto btn bg-orange btn-flat" style="height:75px!important;width:75px!important;" id="' . $d->albm_id . '"><i style="margin-top:20px" class="fa  fa-plus-circle fa-2x"></i></div>';
                                echo "</td>";
                                echo "<td>"
                                . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->albm_id . "'>edit</span>"
                                . "<span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->albm_id . "'>hapus</span>"
                                . "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
                <div class="col-md-12 text-right"> 
                    <button <?php echo $page > 0 ? '' : 'disabled'; ?> onclick="loadContent('<?php echo base_url('front/back/album/' . ($page - $perpage)); ?>')" class="btn btn-flat btn-success btn-sm">«prev</button>
                    <span style="background-color: whitesmoke;padding: 7px"><?php echo ($page + 1) . ' - ' . (($page + $perpage) < $numrows ? ($page + $perpage) : $numrows); ?></span>
                    <button <?php echo $numrows - ($page + $perpage) > 0 ? '' : 'disabled'; ?> onclick="loadContent('<?php echo base_url('front/back/album/' . ($page + $perpage)); ?>')" class="btn btn-flat btn-success btn-sm">next»</button>
                </div>     
                <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $("#cancel").click(function () {
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })

    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#albm_id').val($(this).attr('id'));
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("option").attr("disabled", false);
        $("#formModal").modal('show');
        $.getJSON("<?php echo base_url('front/back/getJsonAlbum/'); ?>" + $(this).attr('id'), function (result) {
            $.each(result, function (i, field) {
                $("#" + i).val(field);
            });
        });
    })
    /********ketika batal edit *********/
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('front/back/album'); ?>";
        $.ajax({
            type: "post",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/albumHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })

    $('#myTable tbody').on('click', '.hapus-foto', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/galeriAlbumHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })
    /************foto ************/
    $('#myTable tbody').on('click', '.tambah-foto', function () {
        $('#myModal').modal('show');
        $('#foto_id').val($(this).attr('id'));

    })

    $("#close").click(function () {
        $("#myModal").modal('hide');
        $("#input-foto").fileinput('reset');
    })
    $("#input-foto").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('front/back/uploadAlbum/'); ?>",
        uploadExtraData: function () {
            return {
                id: $('#foto_id').val()
            };
        },
        autoReplace: true,
        maxFileSize: 2000,
        allowedFileTypes: ["image"],
        allowedFileExtensions: ["jpg", "jpeg"],
        disableImageResize: false,
        resizeImage: true,
        maxImageHeight: 800,
        minFileCount: 1,
        maxFileCount: 10,
        elErrorContainer: '#kv-error',
    }).on('filebatchuploadcomplete', function (event, files, extra) {
        console.log('File batch upload complete');
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    });
</script>
