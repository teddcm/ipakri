<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pelayanan</h3></div>
<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#prosedur" data-toggle="tab">Prosedur</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="prosedur">
                    <textarea id="layanan_prosedur" name="layanan_prosedur" ><?php echo getPengaturanFront('layanan_prosedur'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

    $(function () {
        CKEDITOR.replace('layanan_prosedur', {height: 320});
    })
    $("#myForm").submit(function (e) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('front/back/layanan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('front/back/layanan'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>