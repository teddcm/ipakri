<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Kelola Profil</h3></div>
<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#kepala" data-toggle="tab">Profil Kepala DPU</a></li>
                <li><a href="#sambutan" data-toggle="tab">Sambutan Kepala DPU</a></li>
                <li><a href="#tentang" data-toggle="tab">Tentang</a></li>
                <li><a href="#visimisi" data-toggle="tab">Visi dan Misi</a></li>
                <li><a href="#tupoksi" data-toggle="tab">Tugas Pokok dan Fungsi</a></li>
                <li><a href="#struktur" data-toggle="tab">Struktur Organisasi</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="kepala">
                    <textarea id="profil_kepala" name="profil_kepala" ><?php echo getPengaturanFront('profil_kepala'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="sambutan">
                    <textarea id="profil_sambutan" name="profil_sambutan" ><?php echo getPengaturanFront('profil_sambutan'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="tentang">
                    <textarea id="profil_tentang" name="profil_tentang" ><?php echo getPengaturanFront('profil_tentang'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="visimisi">
                    <textarea id="profil_visimisi" name="profil_visimisi" ><?php echo getPengaturanFront('profil_visimisi'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="tupoksi">
                    <textarea id="profil_tupoksi" name="profil_tupoksi" ><?php echo getPengaturanFront('profil_tupoksi'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="struktur">
                    <textarea id="profil_struktur" name="profil_struktur" ><?php echo getPengaturanFront('profil_struktur'); ?></textarea>                 
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

    $(function () {
        CKEDITOR.replace('profil_kepala', {height: 320});
        CKEDITOR.replace('profil_sambutan', {height: 320});
        CKEDITOR.replace('profil_tentang', {height: 320});
        CKEDITOR.replace('profil_visimisi', {height: 320});
        CKEDITOR.replace('profil_tupoksi', {height: 320});
        CKEDITOR.replace('profil_struktur', {height: 320});
    })
    $("#myForm").submit(function (e) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('front/back/profil'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('front/back/profil'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>