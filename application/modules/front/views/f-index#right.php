<div class="general-content-title">Profil PNS</div>
<form class="form cari-nip" method="post" action="<?php echo base_url('frontpage/p/pns'); ?>">
    <div class="form-group">
        <input class="form-control" id="carinip" name="carinip" placeholder="Masukkan NIP..." maxlength="18">
    </div>
    <div class="form-group pull-right">
        <button class="btn btn-sm btn-info">Cari</button>
    </div>        
    <div class="clearfix"></div>

</form>
<div class="spacer"></div>
<div class="spacer"></div>


<div class="general-content-title">Tautan Terkait</div>
<?php
$data = getLinker();
foreach ($data as $d) {
    echo '<div class="sidebar-content">';
    echo '<a title="' . $d->link_title . '"target="' . $d->link_target . '" href="' . $d->link_href . '"><img src="' . base_url() . 'frontend/img/contents/link_' . $d->link_id . '.jpg?' . $d->link_revision . '" border="1"></a>';
    echo '</div>';
    echo '<div class="spacer"></div>';
}
?>
<div class="spacer"></div>
<div class="spacer"></div>
