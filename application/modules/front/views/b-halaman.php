<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <input id="input-foto" name="userfile" type="file" class="file-loading">
                <input id="foto_id" name="foto_id" type="hidden">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"> 

                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="hlmn_id" name="hlmn_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-9">
                            <label>Judul</label>
                            <input class="form-control" name="hlmn_title" id="hlmn_title">
                        </div>
                        <div class="form-group col-lg-3">
                            <label>Tipe</label>
                            <select class="form-control" name="hlmn_type" id="hlmn_type">
                                <option value="1">First Menu</option>
                                <option value="2">Second Menu</option>
                                <option value="9">Tidak Tampil</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Isi</label>
                            <textarea class="form-control" id="hlmn_content" name="hlmn_content" ></textarea>                 
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Halaman</h3></div>
<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Halaman</a></li>
            <li class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button>   
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kepala">
                &nbsp;
                <table class="table table-striped table-border table-condensed dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:100px">Foto</th>
                            <th>Konten</th>
                            <th style="width:15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data = getHalaman();
                        $n = 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {
                                $warna = [1 => 'orange', 2 => 'purple', 9 => 'black'];
                                $text = [1 => 'FirstMenu', 2 => 'SecondMenu', 9 => 'TidakTampil'];
                                if (strlen($d->hlmn_content) > 320) {
                                    // truncate string
                                    $stringCut = substr($d->hlmn_content, 0, 320);
                                    $stringCut = str_replace('</p>', ' ', $stringCut);
                                    $stringCut = str_replace('<p>', ' ', $stringCut);
                                    $stringCut = str_replace('<ol>', ' ', $stringCut);
                                    $stringCut = str_replace('<li>', ' ', $stringCut);

                                    // make sure it ends in a word so assassinate doesn't become ass...
                                    $d->hlmn_content = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                                }
                                echo "<tr>";
                                echo "<td rowspan=2>" . $n++ . "</td>";
                                echo "<td rowspan=2 class='text-center'><span id='" . $d->hlmn_id . "' class='foto' style='cursor:pointer'>"
                                . "" . (@file_get_contents(base_url('frontend/img/contents/hlmn_' . $d->hlmn_id) . ".jpg") ? ("<img width='100px' src='" . base_url('frontend/img/contents/thumbs/hlmn_' . $d->hlmn_id) . "_thumb.jpg?" . $d->hlmn_revision . "'>" ) : ("<span class='btn btn-soundcloud btn-lg btn-flat'><i class='fa fa-upload'><br>upload<br>gambar</span>")) . ""
                                . "</span></td>";
                                echo "<td style='height:10px;'>" . '<b class="bg-' . $warna[$d->hlmn_type] . '">&nbsp;' . $text[$d->hlmn_type] . '&nbsp;</b> ' . $d->hlmn_title . "</td>";
                                echo "<td>"
                                . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->hlmn_id . "'>edit</span>"
                                . "<span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->hlmn_id . "'>hapus</span>"
                                . "<span class='copy btn btn-flat btn-xs btn-warning' id='" . $d->hlmn_id . "'>link</span>"
                                . "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                echo "<td colspan=2>" . $d->hlmn_content . "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $('#myTable tbody').on('click', '.copy', function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val('<?php echo base_url('frontpage/c/'); ?>' + $(this).attr('id')).select();
        document.execCommand("copy");
        $temp.remove();
        $.notify('Link telah dicopy ke clipboard', 'success');
    })

    $(function () {
        CKEDITOR.replace('hlmn_content', {height: 250});
    })
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $("#cancel").click(function () {
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })
    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#hlmn_id').val($(this).attr('id'));
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("option").attr("disabled", false);
        $("#formModal").modal('show');
        $.getJSON("<?php echo base_url('front/back/getJsonHalaman/'); ?>" + $(this).attr('id'), function (result) {
            $.each(result, function (i, field) {
                if (i == 'hlmn_content') {
                    $("textarea#" + i).val(field);
                    CKEDITOR.instances.hlmn_content.setData(field);
                } else {
                    $("#" + i).val(field);
                    //alert(i);
                }
            });
        });
    })
    /********ketika batal edit *********/
    $('#formModal').on('hidden.bs.modal', function () {
        $('#myForm').trigger("reset");
        CKEDITOR.instances.hlmn_content.setData(null);

    })
    $("#myForm").submit(function (e) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('front/back/halaman'); ?>";
        $.ajax({
            type: "post",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/halamanHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })

    /************foto ************/
    $('#myTable tbody').on('click', '.foto', function () {
        $('#myModal').modal('show');
        $('#foto_id').val($(this).attr('id'));

    })

    $("#close").click(function () {
        $("#myModal").modal('hide');
        $("#input-foto").fileinput('reset');
    })
    $("#input-foto").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('front/back/uploadHalaman/'); ?>",
        uploadExtraData: function () {
            return {
                id: $('#foto_id').val()
            };
        },
        autoReplace: true,
        maxFileSize: 1000,
        allowedFileTypes: ["image"],
        allowedFileExtensions: ["jpg", "jpeg"],
        disableImageResize: false,
        resizeImage: true,
        maxImageHeight: 600,
        minFileCount: 1,
        elErrorContainer: '#kv-error'
    }).on('fileuploaded', function (event, data, msg) {
        $.notify(data.response[1], data.response[0]);
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    })

</script>
