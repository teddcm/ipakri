<li>
    <a class="has-submenu">Profil</a>
    <ul class="dropdown-menu">
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/kepala">Profil Kepala DPU</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/sambutan">Sambutan Kepala DPU</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/tentang">Tentang</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/visimisi">Visi dan Misi</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/tupoksi">Tugas Pokok dan Fungsi</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/profil/struktur">Struktur Organisasi</a></li>
    </ul>
</li>
<li>
    <a class="has-submenu">Statistik Pegawai</a>
    <ul class="dropdown-menu">
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/statistik/golru">Statistik Berdasarkan Golongan</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/statistik/usia">Statistik Berdasarkan Usia</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/statistik/gender">Statistik Berdasarkan Gender</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/statistik/jenjab">Statistik Berdasarkan Jenis Jabatan</a></li>
        <li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/statistik/pendidikan">Statistik Berdasarkan Pendidikan</a></li>
    </ul>
</li>
<li><a target="_parent" href="<?php echo base_url(); ?>frontpage/p/galeri">Galeri</a></li>
<li><a target="_parent" href="<?php echo base_url('frontpage/p/layanan/prosedur'); ?>">Pelayanan</a></li>
<li><a target="_parent" href="<?php echo base_url('frontpage/p/unduhan'); ?>">Pusat Unduhan</a></li>
<li><a target="_parent" href="<?php echo base_url('frontpage/p/kontak'); ?>">Kontak Kami</a></li>
<?php
$data = getHalaman(1);
if ($data != NULL) {
    foreach ($data as $d) {
        ?>
        <li><a target="_parent" href="<?php echo base_url('frontpage/c/' . $d->hlmn_id); ?>"><?php echo $d->hlmn_title; ?></a></li>
        <?php
    }
}
?>