<div class="flexslider home-slider">
    <ul class="slides">
        <?php
        $data = getSlider();
        foreach ($data as $d) {
            ?>
            <li style="background-image: url('<?php echo base_url('frontend/img/contents/slid_' . $d->slid_id . '.jpg?' . $d->slid_revision); ?>');" >
                <div class="slide-text">
                    <span class="slide-title">
                        <h3><?php echo $d->slid_title; ?></h3>
                    </span>
                    <?php if ($d->slid_subtitle != '') { ?>
                        <span class="slide-subtitle">
                            <p><?php echo $d->slid_subtitle; ?></p>
                        </span>
                    <?php } ?>
                </div>
            </li>
            <?php
        }
        ?>
    </ul>
</div>