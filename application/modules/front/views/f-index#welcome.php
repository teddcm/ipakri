<div class="spacer"></div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="newsticker">
                <div class="newsticker_left"><?php echo getPengaturanFront('welcome_running_title'); ?></div>
                <div id="marquee"><?php echo getPengaturanFront('welcome_running'); ?></div>
            </div>
        </div>
    </div>
</div>
<div class="pre-main">
    <div class="container">
        <div class="pre-title">
            <h1 style="font-size: 37px"><?php echo getPengaturanFront('welcome_primary'); ?></h1>
            <span><?php echo getPengaturanFront('welcome_secondary'); ?></span>      
        </div>
        <div class="pre-content">
            <div class="row center-block">
                <?php
                $data = getWelcome();
                foreach ($data as $d) {
                    ?>
                    <div class="col-md-3">
                        <div class="pre-content-list">
                            <i class="fa <?php echo $d->welc_icon; ?>"></i>
                            <h3><?php echo $d->welc_title; ?></h3>
                            <p><?php echo $d->welc_subtitle; ?></p>
                            <a href="<?php echo $d->welc_href; ?>">Selengkapnya</a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>