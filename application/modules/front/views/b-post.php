<?php
$page = $this->uri->segment(4) == null ? 0 : $this->uri->segment(4);
$perpage = 5;
$data = getPost(0, $page, $perpage);
$numrows = $data['num_rows'];
$data = $data['data'];
?>
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <input id="input-foto" name="userfile" type="file" class="file-loading">
                <input id="foto_id" name="foto_id" type="hidden">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body"> 

                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="post_id" name="post_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-9">
                            <label>Judul</label>
                            <input class="form-control" name="post_title" id="post_title">
                        </div>
                        <div class="form-group col-lg-3">
                            <label>Tipe</label>
                            <select class="form-control" name="post_type" id="post_type">
                                <option value="1">Berita</option>
                                <option value="2">Pengumuman</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Isi</label>
                            <textarea class="form-control" id="post_content" name="post_content" ></textarea>                 
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Post</h3></div>
<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Post</a></li>
            <li class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button> 
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kepala">
                <table class="table table-striped table-border table-condensed dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:80%" colspan="4">Konten</th>
                            <th style="width:15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $n = $page + 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {

                                if (strlen($d->post_content) > 320) {
                                    // truncate string
                                    $stringCut = substr($d->post_content, 0, 320);
                                    $stringCut = str_replace('</p>', ' ', $stringCut);
                                    $stringCut = str_replace('<p>', ' ', $stringCut);
                                    $stringCut = str_replace('<ol>', ' ', $stringCut);
                                    $stringCut = str_replace('<li>', ' ', $stringCut);
                                    // make sure it ends in a word so assassinate doesn't become ass...
                                    $d->post_content = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                                }
                                echo "<tr>";
                                echo "<td rowspan=2>" . $n++ . "</td>";
                                echo "<td colspan=3 style='height:10px;'><b>" . $d->post_title . "</b></td>";
                                echo "<td style='width:10%;color:green;font-weight:bold;'>" . ($d->post_type == 1 ? 'Berita' : 'Pengumuman') . "</td>";
                                echo "<td>"
                                . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->post_id . "'>edit</span>"
                                . "<span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->post_id . "'>hapus</span>"
                                . "<span class='copy btn btn-flat btn-xs btn-warning' id='" . $d->post_id . "'>link</span>"
                                . "</td>";
                                echo "</tr>";
                                echo "<tr>";
                                echo "<td style='width:10%;' class='text-center'><span id='" . $d->post_id . "' class='foto' style='cursor:pointer'>"
                                . "" . (@file_get_contents(base_url('frontend/img/contents/post_' . $d->post_id) . ".jpg") ? ("<img width='100px' src='" . base_url('frontend/img/contents/thumbs/post_' . $d->post_id) . "_thumb.jpg?" . $d->post_revision . "'>" ) : ("<span class='btn btn-soundcloud btn-lg btn-flat'><i class='fa fa-upload'><br>upload<br>gambar</span>")) . ""
                                . "</span></td>";
                                echo "<td colspan=3>" . $d->post_content . "</td>";
                                echo "<td>" . mdate('<i class="fa fa-calendar-o"></i> %d %M %Y <br><i class="fa fa-clock-o"></i> %h:%i:%s', $d->post_created) . "</td>";

                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 text-right"> 
                <button <?php echo $page > 0 ? '' : 'disabled'; ?> onclick="loadContent('<?php echo base_url('front/back/post/' . ($page - $perpage)); ?>')" class="btn btn-flat btn-success btn-sm">«prev</button>
                <?php echo ($page + 1) . ' - ' . (($page + $perpage) < $numrows ? ($page + $perpage) : $numrows); ?></span>
                <button <?php echo $numrows - ($page + $perpage) > 0 ? '' : 'disabled'; ?> onclick="loadContent('<?php echo base_url('front/back/post/' . ($page + $perpage)); ?>')" class="btn btn-flat btn-success btn-sm">next»</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $('#myTable tbody').on('click', '.copy', function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val('<?php echo base_url('frontpage/vb/'); ?>' + $(this).attr('id')).select();
        document.execCommand("copy");
        $temp.remove();
        $.notify('Link telah dicopy ke clipboard', 'success');
    })

    function copyToClipboard(link = 'hahahahaha') {
    }
    $(function () {
        CKEDITOR.replace('post_content', {height: 250});
    })
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });
    $("#cancel").click(function () {
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })
    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#post_id').val($(this).attr('id'));
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("option").attr("disabled", false);
        $("#formModal").modal('show');
        $.getJSON("<?php echo base_url('front/back/getJsonPost/'); ?>" + $(this).attr('id'), function (result) {
            $.each(result, function (i, field) {
                if (i == 'post_content') {
                    $("textarea#" + i).val(field);
                    CKEDITOR.instances.post_content.setData(field);
                } else {
                    $("#" + i).val(field);
                    //alert(i);
                }
            });
        });
    })
    /********ketika batal edit *********/
    $('#formModal').on('hidden.bs.modal', function () {
        $('#myForm').trigger("reset");
        CKEDITOR.instances.post_content.setData(null);
    })
    $("#myForm").submit(function (e) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('front/back/post'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/postHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })

    /************foto ************/
    $('#myTable tbody').on('click', '.foto', function () {
        $('#myModal').modal('show');
        $('#foto_id').val($(this).attr('id'));
    })

    $("#close").click(function () {
        $("#myModal").modal('hide');
        $("#input-foto").fileinput('reset');
    })
    $("#input-foto").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('front/back/uploadPost/'); ?>",
        uploadExtraData: function () {
            return {
                id: $('#foto_id').val()
            };
        },
        autoReplace: true,
        maxFileSize: 1000,
        allowedFileTypes: ["image"],
        allowedFileExtensions: ["jpg", "jpeg"],
        disableImageResize: false,
        resizeImage: true,
        minFileCount: 1,
        maxImageHeight: 500,
        elErrorContainer: '#kv-error'
    }).on('fileuploaded', function (event, data, msg) {
        $.notify(data.response[1], data.response[0]);
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    })

</script>
