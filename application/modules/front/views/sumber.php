<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>BKD Provinsi Jawa Tengah</title>

        <!-- Bootstrap core CSS -->
        <link href="http://bkd.jatengprov.go.id/new/assets/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://bkd.jatengprov.go.id/new/assets/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="http://bkd.jatengprov.go.id/new/assets/template/flexslider/flexslider.css" rel="stylesheet">
        <link href="http://bkd.jatengprov.go.id/new/assets/js/fancybox2/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen" />

        <!--link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700' rel='stylesheet' type='text/css'!-->

        <!-- Custom styles for this template -->
        <link href="http://bkd.jatengprov.go.id/new/assets/template/css/web.css" rel="stylesheet">
        <link href="http://bkd.jatengprov.go.id/new/assets/template/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">

        <script src="http://bkd.jatengprov.go.id/new/assets/template/js/jquery-1.11.2.min.js"></script>
        <script src="http://bkd.jatengprov.go.id/new/assets/template/js/bootstrap.min.js"></script>
        <script src="http://bkd.jatengprov.go.id/new/assets/template/js/jquery.smartmenus.min.js"></script>
        <script src="http://bkd.jatengprov.go.id/new/assets/template/js/jquery.smartmenus.bootstrap.js"></script>
        <script src="http://bkd.jatengprov.go.id/new/assets/template/flexslider/jquery.flexslider-min.js"></script>
        <script src="http://bkd.jatengprov.go.id/new/assets/js/highcharts/js/highcharts.js"></script>
        <script language="javascript" src="http://bkd.jatengprov.go.id/new/assets/js/fancybox2/jquery.fancybox.js?v=2.1.5"></script>
        <script language="javascript" src="http://bkd.jatengprov.go.id/new/assets/js/jquery.easing.min.js"></script>
        <script language="javascript" src="http://bkd.jatengprov.go.id/new/assets/js/jquery.easy-ticker.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
        <script type="text/javascript" src="http://bkd.jatengprov.go.id/new/assets/js/gmap3.js"></script>
        <script type="text/javascript" src="http://bkd.jatengprov.go.id/new/assets/js/jquery.vticker.js"></script>
        <script language="javascript" src="http://bkd.jatengprov.go.id/new/assets/js/jquery.marquee.min.js"></script>
        <script language="javascript" src="http://bkd.jatengprov.go.id/new/assets/js/jquery.pause.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.home-slider').flexslider({
                    animation: "fade",
                });

                $(window).scroll(function () {
                    if ($(document).scrollTop() > 0) {
                        $('nav').addClass('shrink');
                    } else {
                        $('nav').removeClass('shrink');
                    }
                });

                $('.conbtn').fancybox();

                $(".fancybox").fancybox({
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                });

                $('.news_ticker').vTicker({
                    speed: 500,
                    pause: 3000,
                    showItems: 3,
                    animation: 'fade',
                    mousePause: false,
                    height: 0,
                    direction: 'up'
                });

                $(document).on("submit", ".show", function () {
                    var alamat = this.action;
                    var targetdata = $(this).attr('targetdata');
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: alamat,
                        data: $(this).serializeArray(),
                        success: function (data) {
                            $.fancybox(data, {
                                helpers: {
                                    overlay: {
                                        locked: false
                                    }
                                }
                            });
                        }
                    });
                    return false;
                });

                var newsticker = $('.ticker').easyTicker({
                    direction: 'up',
                    easing: 'easeInOutBack',
                    speed: 'slow',
                    interval: 3000,
                    height: 'auto',
                    visible: 1,
                    mousePause: 1,
                    controls: {
                        up: '.up',
                        down: '.down',
                        toggle: '.toggle',
                        stopText: 'Stop !!!'
                    }
                }).data('easyTicker');

                $(window).scroll(function () {
                    if ($(this).scrollTop() > 400) {
                        $('.scrollToTop').fadeIn();
                    } else {
                        $('.scrollToTop').fadeOut();
                    }
                });

                //Click event to scroll to top
                $('.scrollToTop').click(function () {
                    $('html, body').animate({scrollTop: 0}, 800);
                    return false;
                });

                $('#marquee').marquee({
                    speed: 30000,
                    gap: 0,
                    delayBeforeStart: 0,
                    direction: 'left',
                    duplicated: true,
                    pauseOnHover: true
                });
            });
        </script>
    </head>

    <body>
        <a href="#" class="scrollToTop"><img src="http://bkd.jatengprov.go.id/new/assets/images/back_to_top.png"></a>

        <header>
            <div class="sticky-top-nav">
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="top-navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a href='http://bkd.jatengprov.go.id/new/'>Profil</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/411'>Profil Kepala BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Sambutan Kepala BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/395'>Tentang / Sejarah BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/398'>Visi Misi</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/397'>Tugas Pokok dan Fungsi</a></li><li><a  target='_blank' href='https://www.youtube.com/embed/764Zv6ncRow?list=PLwI50MLTwIW3rZeyhQxi_fV7xVcj8eo1t'>Profil BKD Provinsi Jawa Tengah </a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/sotk'>Struktur Organisasi SKPD Prov. Jateng</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Struktur_ORG_BKD.pdf'>Struktur Organisasi BKD Jateng</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article'>Berita</a></li><li><a href='http://bkd.jatengprov.go.id/new/article/view/452'>Info Kepegawaian</a><ul class='dropdown-menu'><li><a href='http://bkd.jatengprov.go.id/new/'>Statistik Pegawai</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/golongan'>Statistik Berdasarkan Golongan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/usia'>Statistik Berdasarkan Usia</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/struktural'>Statistik Berdasarkan Pendidikan Struktural</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/gender'>Statistik Berdasarkan Gender</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/eselon'>Statistik Berdasarkan Eselon</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/pendidikan'>Statistik Berdasarkan Pendidikan Umum</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/452'>Buku Profil PNS Pemprov. Jateng</a></li><li><a  target='_blank' href='http://simpeg.bkd.jatengprov.go.id/maps/'>Sebaran PNS dalam GIS</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Profil Bulanan PNS Prov. Jateng</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/516'>Profil PNS Tahun 2016</a></li></ul></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/gallery'>Gallery</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Web Link</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/396'>Tautan / Link Instansi Pusat</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/393'>Tautan / Link SKPD Provinsi Jawa Tengah</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Tautan / Link Kabupaten / Kota Jawa Tengah</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Open DATA</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/580'>Rekap data Hukuman disiplin</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Profil PNS</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/581'>Profil PNS per Jabatan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/583'>Profil PNS per Golongan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil PNS Per Jabatan dan Jenis Kelamin</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil PNS per Usia</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil PNS per Jenis Kelamin dan Tingkat Pendidikan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil Pejabat Struktural per Jenis Kelamin</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil PNS Eselon dan Umur, Jenis Kelamin</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/0'>Profil Pejabat Struktural per Tingkat Pendidikan</a></li></ul></li></ul></li><li><a  target='_blank' href='http://www.bkn.go.id/penerimaan-cpns-th-2017'>CPNS 2017</a></li>					</ul>
                        </div>
                    </div>
                </nav>
                <ul class="search-box">
                    <li>
                        <form id="custom-search-form" class="form-search form-horizontal pull-right">
                            <input type="text" class="search-query mac-style" placeholder="Cari artikel..">
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </li>
                    <li><a href="#">EN</a></li>
                    <li><a href="#">ID</a></li>
                </ul>
            </div>

            <nav class="navbar navbar-default navbar-fixed-top normal">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img class="img-responsive" src="http://bkd.jatengprov.go.id/new/assets/template/img/logo.png"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-top">
                        <ul class="nav navbar-nav navbar-left">
                            <li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Beranda</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Layanan</a><ul class='dropdown-menu'><li><a href='http://bkd.jatengprov.go.id/new/'>Assessment Center</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/487'>Apa itu Assessment Center (AC)</a></li><li><a href=''>LAYANAN ASSESSMENT CENTER </a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/488'>PCAP</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/489'>QAP</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/502'>SAP</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/490'>KONSELING</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/494'>SDM Assessment Center</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/495'>Referensi/Pengalaman</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/501'>PRESTASI ASSESMENT CENTER</a></li><li><a href=''>JADUAL KEGIATAN</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/515'>JADUAL TAHUN 2016</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/608'>Jadual Assesment 2017</a></li></ul></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>CPNS / PNS</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/473'>Pengangkatan PNS</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/471'>Informasi Pengadaan CPNS</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Ijin Kepegawaian</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/504'>Ijin Cuti</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/508'>Ijin Belajar</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/511'>Ijin Penggunaan Gelar</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/530'>Keterangan Cerai</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/531'>Ijin Cerai</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Mutasi Pegawai</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/prosedur_mutasi.pdf'>Prosedur dan Tahapan Mutasi / Perpindahan PNS</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Kenaikan Pangkat</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/KP_REGULER.pdf'>REGULER</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/KP_STRUKTURAL.pdf'>STRUKTURAL</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/KP_JFT.pdf'>JABATAN FUNGSIONAL TERTENTU</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/KP_PI_JAB_NEG.pdf'>PENYESUAIAN IJASAH & PEJABAT NEGARA</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>IDENTITAS PNS</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/408'>Kartu Istri / Kartu Suami</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/409'>Kartu Pegawai / KPE</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Pemberian Tanda Kehormatan Satya Lencana Karya Satya (SLKS)</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/satya/final'>Pengumuman Satya Lencana yang sudah Disetujui</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/536'>Satya Lencana dan Prosedur Pengusulan</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Kenaikan Jabatan Fungsional</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/WIDYA_ISWARA.pdf'>WIDYA ISWARA</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/JAFUNG.pdf'>JAB FUNGSIONAL NON WI</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/407'>Ujian Kedinasan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/510'>SUMPAH JANJI</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/605'>Tugas Belajar</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/512'>Pembekalan Purna Tugas</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Promosi Terbuka</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Promosi_terbuka.pdf'>SELEKSI JPT</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/TALENT_SCOUTING.pdf'>TALENT SCOUTING</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/537'>Alur Pengajuan JKK dan JKM</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/pensiun.pdf'>Pensiun</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Keg, Keu & Aset</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/RENSTRA_BKD_2013-2018.pdf'>RENSTRA BKD</a></li><li><a href=''>AGENDA KEGIATAN</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/AGENDA_KEGIATAN.pdf'>Agenda 2016</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Agenda_2017.pdf'>Agenda 2017</a></li></ul></li><li><a href=''>KERANGKA ACUAN KERJA (KAK)</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/412'>KAK TH 2015</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/590'>KAK TH 2017</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>ANGGARAN & KEUANGAN</a><ul class='dropdown-menu'><li><a href='http://bkd.jatengprov.go.id/new/'>Pengadaan</a><ul class='dropdown-menu'><li><a href='http://bkd.jatengprov.go.id/new/'>TAHUN 2015</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Rekap_Tender_SD_BL_NOP_2015.pdf'>Rekap Tender SD BL NOP 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/RUP_BKD_2015.pdf'>RUP BKD 2015</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>TAHUN 2016</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/RUP_BKD_2016.pdf'>RUP BKD 2016</a></li></ul></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Pengadaan_2017.pdf'>TAHUN 2017</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Pelaksanaan Anggaran (DPA)</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/DPA_2015.pdf'>DPA 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/DPA_2016.pdf'>DPA 2016</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/DPA_2017.pdf'>DPA 2017</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Rencana Kerja Anggaran (RKA)</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/RKA_2016.pdf'>RKA 2016</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/RKA_2017.pdf'>RKA 2017</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Laporan Realisasi Anggaran (LRA)</a><ul class='dropdown-menu'><li><a href='http://bkd.jatengprov.go.id/new/'>APBD</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Laporan_Realisasi_Anggaran_SKPD_2014.pdf'>Laporan Realisasi Anggaran SKPD 2014</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>SKPD</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Laporan_Realisasi_Anggaran_BKD_2014.pdf'>Laporan Realisasi Anggaran BKD 2014</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Lap_Keu_2015.pdf'>Laporan Realisasi Anggaran BKD 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Laporan_Realisasi_Anggaran_BKD_2016.pdf'>Laporan Realisasi Anggaran BKD 2016</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Realisasi_Anggaran_BKD_2017.pdf'>Laporan Realisasi Anggaran BKD 2017</a></li></ul></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Rencana Pengeluaran Anggaran (RPA) </a><ul class='dropdown-menu'><li><a href=''>Rincian Pelaks Anggaran 2016</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_1.pdf'>Bagian 1</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_2.pdf'>Bagian 2</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Bagian 3</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Rincian_Pelaks_Anggaran_2016.pdf'>Bagian 4</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Rincian Pelaks Anggaran 2017</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_1.pdf'>Bagian 1</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_2.pdf'>Bagian 2</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_3.pdf'>Bagian 3</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_4.pdf'>Bagian 4</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_5.pdf'>Bagian 5</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Bagian_6.pdf'>Bagian 6</a></li></ul></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Catatan Atas Laporan Keuangan (CALK)</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Catatan_Atas_Laporan_Keuangan_2014.pdf'>Catatan Atas Laporan Keuangan 2014</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Catatan_Atas_Laporan_Keuangan_2015.pdf'>Catatan Atas Laporan Keuangan 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Catatan_Atas_Laporan_Keuangan_2016.pdf'>Catatan Atas Laporan Keuangan 2016</a></li></ul></li><li><a href='http://bkd.jatengprov.go.id/new/'>Dokumen Neraca Keuangan SKPD</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Dokumen_Neraca_Keuangan_SKPD_2015.pdf'>Dokumen Neraca Keuangan SKPD 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Dokumen_Neraca_Keuangan_SKPD_2016.pdf'>Dokumen Neraca Keuangan SKPD 2016</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/600'>Laporan Anggaran Tahun Berjalan 2017</a></li></ul></li><li><a href=''>LAPORAN KINERJA </a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Lap_Kinerja_2015.pdf'>Lap Kinerja 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/LAKIP_2016.pdf'>Lap Kinerja 2016</a></li></ul></li><li><a href=''>ASSET & INVESTASI</a><ul class='dropdown-menu'><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Daftar_Asset_dan_Investasi_TH_2013.pdf'>Daftar Asset dan Investasi TH 2013</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Daftar_Asset_dan_Investasi_TH_2014.pdf'>Daftar Asset dan Investasi TH 2014</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Daftar_Asset_dan_Investasi_TH_2015.pdf'>Daftar Asset dan Investasi TH 2015</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Daftar_Asset_dan_Investasi_TH_2016.pdf'>Daftar Asset dan Investasi TH 2016</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Daftar_Asset_dan_Investasi_Smt_I_2017.pdf.pdf'>Daftar Asset dan Investasi  Smt I TH 2017</a></li></ul></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/legal'>Legal Searching</a></li><li><a  target='_blank' href='http://simpeg.bkd.jatengprov.go.id/ppid/'>PPID</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/guestbook'>Buku Tamu</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/download'>Download</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Kontak_Center.pdf'>Hubungi Kami</a></li>	        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="wrapper">
            <div class="flexslider home-slider">
                <ul class="slides">
                    <li style="background-image: url('http://bkd.jatengprov.go.id/new/assets/slider/20160114090321.jpg')">
                        <div class="slide-text">
                            <span class="slide-title"><h3>Kantor BKD </h3></span>
                            <span class="slide-subtitle"><p></p></span>
                        </div>
                    </li>
                    <li style="background-image: url('http://bkd.jatengprov.go.id/new/assets/slider/20161014130235.jpg')">
                        <div class="slide-text">
                            <span class="slide-title"><h3>Penipuan</h3></span>
                            <span class="slide-subtitle"><p>Penipuan</p></span>
                        </div>
                    </li>
                    <li style="background-image: url('http://bkd.jatengprov.go.id/new/assets/slider/20161118105311.jpg')">
                        <div class="slide-text">
                            <span class="slide-title"><h3>Nawacita</h3></span>
                            <span class="slide-subtitle"><p></p></span>
                        </div>
                    </li>
                    <li style="background-image: url('http://bkd.jatengprov.go.id/new/assets/slider/20170927153112.jpg')">
                        <div class="slide-text">
                            <span class="slide-title"><h3>Hari utk Tahu</h3></span>
                            <span class="slide-subtitle"><p>Hari utk Tahu</p></span>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="spacer"></div>
            <div class="row">
                <div class="container">
                    <div class="col-md-12">
                        <div class="newsticker">
                            <div class="newsticker_left">NEWS</div>
                            <div id="marquee">Pengumuman penerimaan CPNS Sampai dengan saat ini belum ada&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-play' aria-hidden='true'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pre-main">
                <div class="container">
                    <div class="pre-title">
                        <h1>Selamat Datang di Website BKD Provinsi Jawa Tengah</h1>
                        <span>Menjadi Pengelola Manajemen Kepegawaian yang Profesional dan Unggul</span>      </div>

                    <div class="pre-content">
                        <div class="row">
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-files-o"></i>
                                    <h3>Sekretariat</h3>
                                    <p>Sekretariat membawahi Sub Bagian Program, Sub Bagian Umum, Kepegawaian dan Sub Bagian Keuangan </p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/403">Read more</a>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <h3>Perencanaan dan Pengembangan Pegawai</h3>
                                    <p>Bidang Perencanaan dan Pengembangan Pegawai membawahi Sub Bidang Formasi dan Pengembangan, Subbidang Jabatan Struktural dan Sub Bidang Pengembangan Jabatan Fungsional</p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/402">Read more</a>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-exchange"></i>
                                    <h3>Mutasi</h3>
                                    <p>Bidang Mutasi membawahkan Sub Bidang Kenaikan Pangkat dan Layanan Administrasi Kepegawaian, Subbidang Pengangkatan dan  Sub Bidang Pemindahan Dan Pemberhentian.
                                    </p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/404">Read more</a>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-group"></i>
                                    <h3>Pembinaan dan Kesejahteraan Pegawai</h3>
                                    <p>Bidang Pembinaan dan Kesejahteraan Pegawai membawahi  Sub Bidang Kesejahteraan Pegawai, Sub Bidang Pembinaan dan  Sub Bidang Pengelolaan Korps Profesi Aparatur Sipil Negara.
                                    </p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/405">Read more</a>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-database"></i>
                                    <h3>Informasi Kepegawaian</h3>
                                    <p>Bidang Informasi Kepegawaian, membawahkan Subbidang Pengelola Data Kepegawaian dan Subbidang Dokumentasi data Kepegawaian. </p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/406">Read more</a>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="pre-content-list">
                                    <i class="fa fa-group"></i>
                                    <h3>Unit Penilai Kompetensi ASN</h3>
                                    <p>Unit Pelayanan Kompetensi ASN, membawahi Subag Tata Usaha, Seksi Perencanaan dan Evaluasi, Seksi Penilaian Kompetensi</p>
                                    <a href="http://bkd.jatengprov.go.id/new/article/view/607">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="col-md-3">
                    <div class="info-terbaru">
                        <div class="sidebar-content-title">
                            Info Terbaru
                        </div>
                        <ul>
                            <li>
                                <span class="info-title"><a href="http://bkd.jatengprov.go.id/new/article/view/598">Gubernur Ganjar Pranowo, SH, MIP melantik 110 Pejabat Prov. jateng</a></span>
                                <span>
                                    <ul>
                                        <li><i class="fa fa-calendar"></i>14 August 2017</li>
                                        <li><i class="fa fa-clock-o"></i>08.06</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </span>
                            </li>
                            <li>
                                <span class="info-title"><a href="http://bkd.jatengprov.go.id/new/article/view/529">Pergub Jateng No 51 Tahun 2016 tentang Hari dan Jam</a></span>
                                <span>
                                    <ul>
                                        <li><i class="fa fa-calendar"></i>19 January 2017</li>
                                        <li><i class="fa fa-clock-o"></i>15.43</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="jadwal">
                        <div class="sidebar-content-title">
                            Jadwal Hari Ini
                        </div>
                        <div class="news_ticker">
                            <ul>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>31 October 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Pemberkasan Pensiun SKPD Prov. Jateng pengampu Bidang Mutasi</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>13 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>14 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>20 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>21 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>27 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">PCAP Kab Kendal  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>28 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>07 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Sidang Pembinaan Disiplin diampu Bidang PKP</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>06 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Rakor Pengendalian diampu SubidProgram</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>23 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Rakor Mutasi diampu bidang Mutasi</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>22 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Rakor Kenaikan Pangkat April 2018 diampu bidang Mutasi</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>21 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Workshop Simpeg - SAPK di ruang TMMK diampu Bidang INKA</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>20 November 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Workshop Simpeg - SAPK di ruang TMMK diampu bidang INKA</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>06 December 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Seleksi Widya Iswara diampu bidang PPP</span>
                                    </span>
                                </li>
                                <li>                
                                    <span>
                                        <ul>
                                            <li><i class="fa fa-calendar"></i>04 December 2017</li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <span class="info-title">Quasi Assesment Center QAP 2017 diampu Bidang PPP dan UPNKOM</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="login-form">
                        <form action="http://bkd.jatengprov.go.id/new/nip" method="post" accept-charset="iso-8859-1" class="show">        <div class="form-group ">
                                <input type="text" class="form-control" placeholder="NIP " id="nip" name="nip">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="form-group log-status">
                                <input type="text" class="form-control" placeholder="Captcha" id="Captcha" name="captcha">
                                <i class="fa fa-lock"></i>
                            </div>
                            <div class="form-group ">
                                <img src="http://bkd.jatengprov.go.id/new/captcha/default" width="225" height="60" alt="Captcha" class="captcha" />        </div>
                            <input type="submit" name="submit" value="Cek NIP" class="log-btn" /></form>      </div>
                    <div class="sidebar-content-title">Jumlah Pengunjung</div>
                    <center>
                        <!-- Histats.com  START (html only)-->
                        <a href="http://www.histats.com" alt="page hit counter" target="_blank" >
                            <embed src="http://s10.histats.com/443.swf"  flashvars="jver=1&acsid=3435261&domi=4"  quality="high"  width="220" height="120" name="443.swf"  align="middle" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" /></a>
                        <img  src="http://sstatic1.histats.com/0.gif?3435261&101" alt="hitcounter" border="0">
                        <!-- Histats.com  END  -->  
                    </center>
                </div>

                <div class="col-md-6">
                    <div class="berita-pertama">
                        <img class='img-responsive' src='http://bkd.jatengprov.go.id/new/assets/icon/20171109132723.jpg'>        <h3>Pengumuman Hasil Analisis Problem Talend Scouting 2017 Prov. Jateng</h3>
                        <ul>
                            <li><i class="fa fa-calendar"></i>Thursday, 09 November 2017</li>
                            <li><i class="fa fa-user"></i>ADM</li>
                        </ul>
                        <p>Berdasarkan Keputusan Tim Pengarah Talent Scouting Jabatan Administrator dan Jabatan Pengawas di Lingkungan Pemerintah Provinsi Jawa Tengah Nomor 821.2/1093/2017 tanggal 9November 2017 tentang Penetapan Passing Grade/Nilai Ambang Batas dan Penetapan Peserta Talent Scouting Jabatan Administrator dan Jabatan Pengawas yang dinyatakan lolos seleksi Analisi Problem, disampaikan sebagai berikut :</p>
                        <a class="lanjut-baca" href="http://bkd.jatengprov.go.id/new/article/view/619">Read more</a>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="berita-terbaru">
                                <div class="thumb">
                                    <img class='img-responsive' src='http://bkd.jatengprov.go.id/new/assets/icon/20171107073340.jpg'>                </div>
                                <ul>
                                    <li><i class="fa fa-calendar"></i>Thu, 09 November 2017</li>
                                    <li><i class="fa fa-user"></i>ADM</li>
                                </ul>
                                <h3>Jadual Pengambilan Sumpah/Janji PNS Prov. Jateng 2017</h3>
                                <p>Dalam rangka tertib administrasi, Pemerintah Provinsi Jawa Tengah lewat Badan Kepegawaian Daerah Provinsi Jawa Tengah, akan menyelenggarakan Upacara Pengambilan Sumpah/Janji PNS Pemerintah Provinsi Jawa Tengah Tahun 2017 sebanyak 670 PNS yang akan dilaksanakan pada :</p>
                                <a class="lanjut-baca" href="http://bkd.jatengprov.go.id/new/article/view/618">Read more</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="berita-terbaru">
                                <div class="thumb">
                                    <img class='img-responsive' src='http://bkd.jatengprov.go.id/new/assets/images/news-thumb.jpg'>                </div>
                                <ul>
                                    <li><i class="fa fa-calendar"></i>Tue, 07 November 2017</li>
                                    <li><i class="fa fa-user"></i>ADM</li>
                                </ul>
                                <h3>Peserta Lulus Uji Peningkatan Pendidikan Periode II 2017</h3>
                                <p>Semarang(23/10) menindaklanjuti Pelaksanaan Ujian Peningkatan Pendidikan Periode II tahun 2017 yang telah dilaksanakan, dan sebagaimana diputuskan dalam surat KeputusanGubernur Jawa Tengah Nomor  333/1068/2017 tanggal 9 Oktober 2017 terlampir Daftar Peserta yang lulus Uji Peningkatan Pendidikan periode II tahun 2017.</p>
                                <a class="lanjut-baca" href="http://bkd.jatengprov.go.id/new/article/view/616">Read more</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="berita-terbaru">
                                <div class="thumb">
                                    <img class='img-responsive' src='http://bkd.jatengprov.go.id/new/assets/icon/20171027141132.jpg'>                </div>
                                <ul>
                                    <li><i class="fa fa-calendar"></i>Fri, 27 October 2017</li>
                                    <li><i class="fa fa-user"></i>ADM</li>
                                </ul>
                                <h3>Pendaftaran 7 JPT Pratama Kab. Pati</h3>
                                <p>Menunjuk surat Bupati Pat Nomor 800/4787 Tanggal 23 Oktober 2017 Perihal Penyampaian Pengumuman Seleksi Terbuka Pengisin Jabatan Pimpinan Tinggi Pratama Pemerintah Kab. Pati disampaikan bahwa, Kabupaten Pati membuka Formasi Jabatan Pimpinan Tinggi Pratama sejumlah 7 Jabatan, sebagai berikut :</p>
                                <a class="lanjut-baca" href="http://bkd.jatengprov.go.id/new/article/view/617">Read more</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="berita-terbaru">
                                <div class="thumb">
                                    <img class='img-responsive' src='http://bkd.jatengprov.go.id/new/assets/icon/20171018213347.jpg'>                </div>
                                <ul>
                                    <li><i class="fa fa-calendar"></i>Mon, 23 October 2017</li>
                                    <li><i class="fa fa-user"></i>ADM</li>
                                </ul>
                                <h3>Pendaftaran Talent Scouting bagi PNS Prov. Jateng</h3>
                                <p>Dalam rangka mendapatkan Talent Pool sebagai bahan pertimbangan utama bagi PNS yang akan dipromosikan dalam jabatan Administrator dan jabatan Pengawas, Pemerintah Provinsi Jawa Tengah menerapkan kebijakan Talent Scouting untuk jabatan Administrator dan jabatan Pengawas.</p>
                                <a class="lanjut-baca" href="http://bkd.jatengprov.go.id/new/article/view/615">Read more</a>
                            </div>
                        </div>
                        <div class='col-md-6'><div class='berita-terbaru'><div class='thumb'><iframe width='235' height='160' src='https://www.youtube.com/embed/OijYM_YW3xA'></iframe></div></div></div><div class='col-md-6'><div class='berita-terbaru'><div class='thumb'><iframe width='235' height='160' src='https://www.youtube.com/embed/LSxRn4B77YI'></iframe></div></div></div>      </div>
                </div>
                <div class="col-md-3">
                    <div class='sidebar-content'><a target='_blank' href='http://bkd.jatengprov.go.id/talentscouting/'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20170306171910.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://www.bkn.go.id/'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20160317223258.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://yogya-bkn.id/bknone/home'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20160323105255.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://jatengprov.go.id/'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20150615103000.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://laporgub.jatengprov.go.id'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20150701152057.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://simpegonline.jatengprov.go.id/auth'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20151117022945.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://ppid.jatengprov.go.id/'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20151117022755.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://lpse.jatengprov.go.id'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20150615100758.jpg'></a></div><div class='sidebar-content'><a target='_blank' href='http://www.menpan.go.id/'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20160317223709.jpg'></a></div><div class='sidebar-content'><a target='_blank' href=' https://sscn.bkn.go.id'><img border='1' src='http://bkd.jatengprov.go.id/new/assets/link/20170731144930.jpg'></a></div>
                    <div class="sidebar-content">
                        <div class="sidebar-content-title">MENUJU BKD JAWA TENGAH</div>
                        <div class="sidebar-content-detail">
                            <a target='_blank' href='https://www.google.com/maps/dir/Bandar+Udara+Internasional+Ahmad+Yani,+Jalan+Puad+Ahmad+Yani,+Jawa+Tengah+50145,+Indonesia/Jalan+Stadion+Selatan,+Semarang+Tengah,+Kota+Semarang,+Jawa+Tengah+50241,+Indonesia/@-6.98755,110.404356,13z/data=!4m14!4m13!1m5!1m1!1s0x2e70f52a325cb5bb:0x70e6b034f1d4fedc!2m2!1d110.378348!2d-6.9788785!1m5!1m1!1s0x2e708ca136bc07d5:0x53b405b00f234586!2m2!1d110.4305287!2d-6.9908426!3e0?hl=en-US'><img src='http://bkd.jatengprov.go.id/new/assets/images/bkd_info.jpg'></a>        </div>
                    </div>
                    <div class="sidebar-content">
                        <div class="sidebar-content-title">
                            Twitter
                        </div>
                        <div class="sidebar-content-detail">
                            <a class="twitter-timeline" href="https://twitter.com/bkdjatengprov" data-widget-id="610244286819401728">Tweets by @bkdjatengprov</a>
                            <script>!function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + "://platform.twitter.com/widgets.js";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, "script", "twitter-wjs");</script>
                        </div>
                    </div>      
                </div>
            </div>

            <footer>
                <div class="container">
                    <div class="footer-top">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer-logo">
                                    <img class="img-responsive" src="http://bkd.jatengprov.go.id/new/assets/template/img/logo.png">                
                                </div>
                                <div class="detail-company">
                                    <span><i class="fa fa-envelope"></i>bkd@jatengprov.go.id</span>
                                    <span><i class="fa fa-phone"></i>(024) 8318846, 8415813, 8319421</span>
                                    <span><i class="fa fa-fax"></i>(024) 8318890</span>
                                    <span><i class="fa fa-send"></i>Jl. Stadion Selatan No. 1, Semarang 50136</span>
                                </div>        </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="footer-menu">
                                            <h5>Profil</h5>
                                            <ul>
                                                <li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/411'>Profil Kepala BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Sambutan Kepala BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/395'>Tentang / Sejarah BKD</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/398'>Visi Misi</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/397'>Tugas Pokok dan Fungsi</a></li><li><a  target='_blank' href='https://www.youtube.com/embed/764Zv6ncRow?list=PLwI50MLTwIW3rZeyhQxi_fV7xVcj8eo1t'>Profil BKD Provinsi Jawa Tengah </a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/sotk'>Struktur Organisasi SKPD Prov. Jateng</a></li><li><a class='conbtn fancybox.iframe' target='' href='http://bkd.jatengprov.go.id/new/assets/download/Struktur_ORG_BKD.pdf'>Struktur Organisasi BKD Jateng</a></li>                </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="footer-menu">
                                            <h5>Info Kepegawaian</h5>
                                            <ul>
                                                <li><a href='http://bkd.jatengprov.go.id/new/'>Statistik Pegawai</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/golongan'>Statistik Berdasarkan Golongan</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/usia'>Statistik Berdasarkan Usia</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/struktural'>Statistik Berdasarkan Pendidikan Struktural</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/gender'>Statistik Berdasarkan Gender</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/eselon'>Statistik Berdasarkan Eselon</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/statistik/pendidikan'>Statistik Berdasarkan Pendidikan Umum</a></li></ul></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/452'>Buku Profil PNS Pemprov. Jateng</a></li><li><a  target='_blank' href='http://simpeg.bkd.jatengprov.go.id/maps/'>Sebaran PNS dalam GIS</a></li><li><a href='http://bkd.jatengprov.go.id/new/'>Profil Bulanan PNS Prov. Jateng</a><ul class='dropdown-menu'><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/516'>Profil PNS Tahun 2016</a></li></ul></li>                </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="footer-menu">
                                            <h5>Web Link</h5>
                                            <ul>
                                                <li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/396'>Tautan / Link Instansi Pusat</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/article/view/393'>Tautan / Link SKPD Provinsi Jawa Tengah</a></li><li><a  target='_parent' href='http://bkd.jatengprov.go.id/new/'>Tautan / Link Kabupaten / Kota Jawa Tengah</a></li>                </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-bottom">
                        <div class="copyright">Â© 2015 Copyright. All rights reserved</div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>
