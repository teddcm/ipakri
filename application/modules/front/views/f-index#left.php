<div class="info-terbaru">
    <div class="general-content-title">
        Info Terbaru
    </div>
    <ul>
        <li>
            <span class="info-title"><a href="http://bkd.jatengprov.go.id/new/article/view/598">Gubernur Ganjar Pranowo, SH, MIP melantik 110 Pejabat Prov. jateng</a></span>
            <span>
                <ul>
                    <li><i class="fa fa-calendar"></i>14 August 2017</li>
                    <li><i class="fa fa-clock-o"></i>08.06</li>
                </ul>
                <div class="clearfix"></div>
            </span>
        </li>
        <li>
            <span class="info-title"><a href="http://bkd.jatengprov.go.id/new/article/view/529">Pergub Jateng No 51 Tahun 2016 tentang Hari dan Jam</a></span>
            <span>
                <ul>
                    <li><i class="fa fa-calendar"></i>19 January 2017</li>
                    <li><i class="fa fa-clock-o"></i>15.43</li>
                </ul>
                <div class="clearfix"></div>
            </span>
        </li>
    </ul>
</div>
<div class="jadwal">
    <div class="general-content-title">
        Jadwal Hari Ini
    </div>
    <div class="news_ticker" style="overflow: hidden; position: relative; height: 216px;">
        <ul style="position: absolute; margin: 0px; padding: 0px; top: -46.2525px;">
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item; opacity: 0.357604;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>21 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>27 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>28 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item; opacity: 0.642396;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>07 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Sidang Pembinaan Disiplin diampu Bidang PKP</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>06 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Rakor Pengendalian diampu SubidProgram</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>23 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Rakor Mutasi diampu bidang Mutasi</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>22 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Rakor Kenaikan Pangkat April 2018 diampu bidang Mutasi</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>21 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Workshop Simpeg - SAPK di ruang TMMK diampu Bidang INKA</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>20 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Workshop Simpeg - SAPK di ruang TMMK diampu bidang INKA</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>31 October 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Pemberkasan Pensiun SKPD Prov. Jateng pengampu Bidang Mutasi</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>13 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>14 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>20 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
            <li style="margin: 0px; padding: 0px; height: 73px; display: list-item;">
                <span>
                    <ul>
                        <li><i class="fa fa-calendar"></i>21 November 2017</li>
                    </ul>
                    <div class="clearfix"></div>
                    <span class="info-title">Quasi Assesment Center QAP 2017  diampu Bidang PPP dan UPNKOM</span>
                </span>
            </li>
        </ul>
    </div>
</div>
<div class="login-form">
    <form action="http://bkd.jatengprov.go.id/new/nip" method="post" accept-charset="iso-8859-1" class="show">
        <div class="form-group ">
            <input class="form-control" placeholder="NIP " id="nip" name="nip" type="text">
            <i class="fa fa-user"></i>
        </div>
        <div class="form-group log-status">
            <input class="form-control" placeholder="Captcha" id="Captcha" name="captcha" type="text">
            <i class="fa fa-lock"></i>
        </div>
        <div class="form-group ">
            <img src="img/default.png" alt="Captcha" class="captcha" width="225" height="60">
        </div>
        <input name="submit" value="Cek NIP" class="log-btn" type="submit">
    </form>
</div>