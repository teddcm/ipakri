<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="welc_id" name="welc_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-12">
                            <label>Judul</label>
                            <input class="form-control" name="welc_title" id="welc_title">
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Keterangan</label>
                            <textarea class="textarea form-control" name="welc_subtitle" id="welc_subtitle" style="height: 100px"></textarea>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Link</label>
                            <input class="form-control" name="welc_href" id="welc_href">
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Icon</label>
                            <input class="form-control icp icp-auto" type="text"name="welc_icon" id="welc_icon"  />
                        </div>
                        <div class="form-group col-lg-2">
                            <label>Order</label>
                            <input class="form-control" name="welc_order" id="welc_order">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Selamat Datang</h3></div>

<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Welcome</a></li>
        </ul>
        <div class="tab-content">

            <form id="secondForm"> 
                <div class="form-group">
                    <label>Running Text</label>
                    <div class="input-group" disabled="disabled">
                        <input class="form-control" style="text-transform: none;" size="1" name="welcome_running_title" id="welcome_running_title" value="<?php echo getPengaturanFront('welcome_running_title'); ?>">
                        <span class="input-group-addon" disabled="disabled">:</span>
                        <input class="form-control" style="text-transform: none" name="welcome_running" id="welcome_running" value="<?php echo getPengaturanFront('welcome_running'); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Kalimat Primer</label>
                    <input class="form-control" style="text-transform: none" name="welcome_primary" id="welcome_primary" value="<?php echo getPengaturanFront('welcome_primary'); ?>">
                </div>
                <div class="form-group">
                    <label>Kalimat Sekunder</label>
                    <input class="form-control" style="text-transform: none" name="welcome_secondary" id="welcome_secondary" value="<?php echo getPengaturanFront('welcome_secondary'); ?>">
                </div>
                <div class="pull-right">
                    <button class="btn btn-sm btn-success btn-flat">simpan</button>   
                </div>
            </form>
            <div class="clearfix"></div>
            <hr>
            <div class="tab-pane active" id="kepala">
                &nbsp;
                <table class="table table-bordered table-condensed table-hover dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="15%">Judul</th>
                            <th width="35%">Keterangan</th>
                            <th>Link</th>
                            <th colspan="2">Icon</th>
                            <th>Order</th>
                            <th style="width:10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data = getWelcome();
                        $n = 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {
                                echo "<tr>";
                                echo "<td>" . $n++ . "</td>";
                                echo "<td>" . $d->welc_title . "</td>";
                                echo "<td>" . $d->welc_subtitle . "</td>";
                                echo "<td>" . $d->welc_href . "</td>";
                                echo "<td><i class='fa " . $d->welc_icon . "'></i></td>";
                                echo "<td>" . $d->welc_icon . "</td>";
                                echo "<td>" . $d->welc_order . "</td>";
                                echo "<td>" . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->welc_id . "'>edit</span><span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->welc_id . "'>hapus</span>" . "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button>   
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $('.icp-auto').iconpicker();
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $("#cancel").click(function () {
        $(".form-control").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $(".form-control").attr("disabled", false);
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })
    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#welc_id').val($(this).attr('id'));
        $(".form-control").attr("disabled", false);
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("#welc_title").val(selected.find('td:eq(1)').html());
        $("#welc_subtitle").val(selected.find('td:eq(2)').html());
        $("#welc_href").val(selected.find('td:eq(3)').html());
        $("#welc_icon").val(selected.find('td:eq(5)').html());
        $("#welc_order").val(selected.find('td:eq(6)').html());
        $("option").attr("disabled", false);
        $("#formModal").modal('show');
    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('front/back/welcome'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#secondForm").submit(function (e) {
        var url = "<?php echo base_url('front/back/profil'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#secondForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url(uri_string()); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    $('#myTable tbody').on('click', '.foto', function () {
        $('#myModal').modal('show');
        $('#foto_id').val($(this).attr('id'));

    })
    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/welcomeHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })
</script>
