
<!DOCTYPE html>
<html class="" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=windows-1252">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dinas Pekerjaan Umum Provinsi Kalimantan Tengah</title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>frontend/img/icon.png" type="image/x-icon" rel="icon">
        <link href="<?php echo base_url(); ?>frontend/assets/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>frontend/assets/plugins/fancyBox/fancyBox.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>frontend/assets/plugins/flexslider/flexslider.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>frontend/assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>frontend/assets/dist/css/jquery.smartmenus.bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>frontend/assets/dist/css/content.css" rel="stylesheet">


        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery.vticker.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery.easy-ticker.min.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery.marquee.min.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery.pause.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery.easing.min.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/plugins/fancyBox/fancyBox.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/plugins/flexslider/flexslider.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/plugins/smartmenus/jquery.smartmenus.min.js"></script>
        <script src="<?php echo base_url(); ?>frontend/assets/plugins/smartmenus/jquery.smartmenus.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.home-slider').flexslider({
                    animation: "fade",
                });

                $(window).scroll(function () {
                    if ($(document).scrollTop() > 0) {
                        $('nav').addClass('shrink');
                    } else {
                        $('nav').removeClass('shrink');
                    }
                });

                $('.conbtn').fancybox();

                $(".fancybox").fancybox({
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                });

                $('.news_ticker').vTicker({
                    speed: 500,
                    pause: 3000,
                    showItems: 3,
                    animation: 'fade',
                    mousePause: false,
                    height: 0,
                    direction: 'up'
                });

                $(document).on("submit", ".show", function () {
                    var alamat = this.action;
                    var targetdata = $(this).attr('targetdata');
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: alamat,
                        data: $(this).serializeArray(),
                        success: function (data) {
                            $.fancybox(data, {
                                helpers: {
                                    overlay: {
                                        locked: false
                                    }
                                }
                            });
                        }
                    });
                    return false;
                });

                var newsticker = $('.ticker').easyTicker({
                    direction: 'up',
                    easing: 'easeInOutBack',
                    speed: 'slow',
                    interval: 3000,
                    height: 'auto',
                    visible: 1,
                    mousePause: 1,
                    controls: {
                        up: '.up',
                        down: '.down',
                        toggle: '.toggle',
                        stopText: 'Stop !!!'
                    }
                }).data('easyTicker');

                $(window).scroll(function () {
                    if ($(this).scrollTop() > 400) {
                        $('.scrollToTop').fadeIn();
                    } else {
                        $('.scrollToTop').fadeOut();
                    }
                });

                //Click event to scroll to top
                $('.scrollToTop').click(function () {
                    $('html, body').animate({scrollTop: 0}, 800);
                    return false;
                });

                $('#marquee').marquee({
                    speed: 30000,
                    gap: 0,
                    delayBeforeStart: 0,
                    direction: 'left',
                    duplicated: true,
                    pauseOnHover: true
                });
            });
        </script>
    </head>
    <body>
        <a href="<?php echo base_url(); ?>frontend/#" class="scrollToTop">
            <img src="<?php echo base_url(); ?>frontend/img/back_to_top.png"></a>
        <header>
            <?php require('f-header.php'); ?>
        </header>
        <div class="wrapper">

            <span class="isi">

                <!--?php
                if ($target = $this->uri->segment(2) == NULL) {
                    require('f-index#slider.php');
                }
                require('f-index#runningtext.php');
                if ($target = $this->uri->segment(2) == NULL) {
                    require('f-index#welcome.php');
                }
                ?-->
                <div class="container">
                    <!--div class="col-md-3">
                    <?php require('f-index#left.php'); ?>
                    </div-->
                    <div class="col-md-9">
                        <?php require('page/fp-' . $page . ($subpage != NULL ? '#' . $subpage : '') . '.php'); ?>
                    </div>
                    <div class="col-md-3">
                        <?php require('f-index#right.php'); ?>
                    </div>
                </div>
            </span>
            <footer>
                <?php require('f-footer.php'); ?>
            </footer>
        </div>
    </body>
</html>

