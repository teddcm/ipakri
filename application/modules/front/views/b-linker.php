<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <input id="input-foto" name="userfile" type="file" class="file-loading">
                <input id="foto_id" name="foto_id" type="hidden">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body"> 

                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="link_id" name="link_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-12">
                            <label>Nama</label>
                            <input class="form-control" name="link_title" id="link_title">
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Link</label>
                            <input class="form-control" name="link_href" id="link_href">
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Target</label>
                            <select class="form-control" name="link_target" id="link_target">
                                <option value="_blank">_blank</option>
                                <option value="_parent">_parent</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Order</label>
                            <input class="form-control" name="link_order" id="link_order">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Tautan</h3></div>
<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Tautan</a></li>
            <li class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button>   
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kepala">
                &nbsp;
                <table class="table table-bordered table-condensed table-hover dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Gambar</th>
                            <th>Link</th>
                            <th>Target</th>
                            <th>Order</th>
                            <th style="width:10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data = getLinker();
                        $n = 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {
                                echo "<tr>";
                                echo "<td>" . $n++ . "</td>";
                                echo "<td>" . $d->link_title . "</td>";
                                echo "<td class='text-center'><span id='" . $d->link_id . "' class='foto' style='cursor:pointer'>"
                                . "" . (@file_get_contents(base_url('frontend/img/contents/link_' . $d->link_id) . ".jpg") ? ("<img width='150px' src='" . base_url('frontend/img/contents/thumbs/link_' . $d->link_id) . "_thumb.jpg?" . $d->link_revision . "'>" ) : ("<span class='btn btn-soundcloud btn-lg btn-flat'><i class='fa fa-upload'> upload gambar</span>")) . ""
                                . "</span></td>";
                                echo "<td>" . $d->link_href . "</td>";
                                echo "<td>" . $d->link_target . "</td>";
                                echo "<td>" . $d->link_order . "</td>";
                                echo "<td>" . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->link_id . "'>edit</span><span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->link_id . "'>hapus</span>" . "</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $("#myForm *").attr("disabled", true);
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $("#cancel").click(function () {
        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $("#myForm * [name],#myForm [name]").attr("disabled", false);
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })
    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#link_id').val($(this).attr('id'));
        $("#myForm * [name],#myForm [name]").attr("disabled", false);
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("#link_title").val(selected.find('td:eq(1)').html());
        $("#link_href").val(selected.find('td:eq(3)').html());
        $("#link_target").val(selected.find('td:eq(4)').html());
        $("#link_order").val(selected.find('td:eq(5)').html());
        $("option").attr("disabled", false);
        $("#formModal").modal('show');

    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('front/back/linker'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $('#myTable tbody').on('click', '.foto', function () {
        $('#myModal').modal('show');
        $('#foto_id').val($(this).attr('id'));

    })

    $("#close").click(function () {
        $("#myModal").modal('hide');
        $("#input-foto").fileinput('reset');
    })
    $("#input-foto").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('front/back/uploadLinker/'); ?>",
        uploadExtraData: function () {
            return {
                id: $('#foto_id').val()
            };
        },
        autoReplace: true,
        maxFileSize: 1000,
        allowedFileTypes: ["image"],
        allowedFileExtensions: ["jpg","jpeg"],
        disableImageResize: false,
        resizeImage: true,
        maxImageWidth: 255,
        minFileCount: 1,
        maxImageHeight: 500,
        elErrorContainer: '#kv-error'
    }).on('fileuploaded', function (event, data, msg) {
        $.notify(data.response[1], data.response[0]);
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    })

    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/linkerHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })
</script>