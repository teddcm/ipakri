<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span class="text-red">Ukuran file maksimal 10MB</span>
            </div>
            <div class="modal-body">
                <input id="input-file" name="userfile" type="file" class="file-loading">
                <input id="file_id" name="file_id" type="hidden">
                <input id="file_ext" name="file_ext" type="hidden">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body"> 

                <form id="myForm" enctype="multipart/form-data" > 
                    <div>
                        <input type="submit" style="display: none" name="ok">           
                        <input type="hidden" id="mode" name="mode" value="edit" >
                        <input id="undh_id" name="undh_id" type="hidden">
                    </div>        
                    <div class="row">            
                        <div class="form-group col-lg-12">
                            <label>Nama Unduhan</label>
                            <input class="form-control" name="undh_title" id="undh_title">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-success btn-flat" id="simpan"style="display: none">simpan</button>  
                <button class="btn btn-sm btn-danger btn-flat" id="cancel"style="display: none">batal</button>   
            </div>
        </div>
    </div>
</div>
<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Unduhan</h3></div>
<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#kepala" data-toggle="tab">Unduhan</a></li>
            <li class="pull-right">
                <button class="btn btn-sm btn-info btn-flat" id="tambah">tambah</button>   
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kepala">
                &nbsp;
                <table class="table table-bordered table-condensed table-hover dataTable" id="myTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th style="width:30%">Nama</th>
                            <th style="width:30%">File</th>
                            <th>Tanggal Unggah</th>
                            <th style="width:20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data = getUnduhan();
                        $this->load->helper('date');
                        $n = 1;
                        if ($data != NULL) {
                            foreach ($data as $d) {
                                echo "<tr>";
                                echo "<td>" . $n++ . "</td>";
                                echo "<td>" . $d->undh_title . "</td>";
                                echo "<td>" . ($d->undh_size != NULL ? ("<a class='btn btn-xs btn-default btn-flat' target='_BLANK' href='" . base_url('front/download/unduhan/' . $d->undh_id) . "' title='download file'><i class='fa fa-download'></i></a>&nbsp;" . $d->undh_file . " " . "(" . (floor($d->undh_size / 1024)) . "KB)") : '') . "</td>";
                                echo "<td>" . ($d->undh_uploaded != NULL ? unix_to_human($d->undh_uploaded, TRUE, 'id') : '') . "</td>";
                                echo "<td>" . "<span class='edit btn btn-flat btn-xs btn-info' id='" . $d->undh_id . "'>edit</span><span class='hapus btn btn-flat btn-xs btn-danger' id='" . $d->undh_id . "' ext='" . $d->undh_type . "'>hapus</span><span id='" . $d->undh_id . "'  ext='" . $d->undh_type . "' class='file' style='cursor:pointer'><span class='btn btn-" . ($d->undh_file == NULL ? 'success' : 'warning') . " btn-xs btn-flat'><i class='fa fa-upload'></i>  " . ($d->undh_file == NULL ? 'upload' : 'reupload') . "</span></td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $('#formModal').on('shown.bs.modal', function () {
        $('input:text:visible:first', this).focus();
    })
    $("#myForm *").attr("disabled", true);
    $('#myForm').trigger("reset");
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $("#cancel").click(function () {
        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#cancel").hide();
        $("#simpan").hide();
        $("#tambah").show();
        $("#formModal").modal('hide');
    })
    $("#tambah").click(function () {
        $("#myForm * [name],#myForm [name]").attr("disabled", false);
        $("option").attr("disabled", false);
        $("#mode").attr("disabled", false);
        $("#mode").val('tambah');
        $("#cancel").show();
        $("#simpan").show();
        $("#formModal").modal('show');
    })
    $('#myTable tbody').on('click', '.edit', function () {
        $('#myForm').trigger("reset");
        $('#undh_id').val($(this).attr('id'));
        $("#myForm * [name],#myForm [name]").attr("disabled", false);
        $("#cancel").show();
        $("#simpan").show();
        $("#mode").val('edit');
        $("#mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("#undh_title").val(selected.find('td:eq(1)').html());
        $("#undh_href").val(selected.find('td:eq(3)').html());
        $("#undh_target").val(selected.find('td:eq(4)').html());
        $("#undh_order").val(selected.find('td:eq(5)').html());
        $("option").attr("disabled", false);
        $("#formModal").modal('show');

    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('front/back/unduhan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    $("#formModal").modal('hide');
                    setTimeout(function () {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }, 100);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    $('#myTable tbody').on('click', '.file', function () {
        $('#myModal').modal('show');
        $('#file_id').val($(this).attr('id'));
        $('#file_ext').val($(this).attr('ext'));

    })

    $("#close").click(function () {
        $("#myModal").modal('hide');
        $("#input-file").fileinput('reset');
    })
    $("#input-file").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('front/back/uploadUnduhan/'); ?>",
        uploadExtraData: function () {
            return {
                id: $('#file_id').val(),
                ext: $('#file_ext').val()
            };
        },
        autoReplace: true,
        maxFileSize: 10240,
        minFileCount: 1,
        maxFileCount: 1,
        elErrorContainer: '#kv-error'
    }).on('fileuploaded', function (event, data, msg) {
        $.notify(data.response[1], data.response[0]);
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    })

    $('#myTable tbody').on('click', '.hapus', function () {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('front/back/unduhanHapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": $(this).attr('id'), "ext": $(this).attr('ext')},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    })
</script>