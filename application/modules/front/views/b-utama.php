<style>
    form *{
        text-transform: none!important  ;
    }
    h3{
        margin-top: 0px;
    }
</style>
<div class="col-md-12"><h3>Pengaturan Utama</h3></div>
<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#kepala" data-toggle="tab">Profil Kepala DPU</a></li>
                <li><a href="#sambutan" data-toggle="tab">Sambutan Kepala DPU</a></li>
                <li><a href="#tentang" data-toggle="tab">Tentang</a></li>
                <li><a href="#visimisi" data-toggle="tab">Visi dan Misi</a></li>
                <li><a href="#tupoksi" data-toggle="tab">Tugas Pokok dan Fungsi</a></li>
                <li><a href="#struktur" data-toggle="tab">Struktur Organisasi</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="kepala">
                    <textarea id="utama_kepala" name="utama_kepala" ><?php echo getPengaturanFront('utama_kepala');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="sambutan">
                    <textarea id="utama_sambutan" name="utama_sambutan" ><?php echo getPengaturanFront('utama_sambutan');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="tentang">
                    <textarea id="utama_tentang" name="utama_tentang" ><?php echo getPengaturanFront('utama_tentang');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="visimisi">
                    <textarea id="utama_visimisi" name="utama_visimisi" ><?php echo getPengaturanFront('utama_visimisi');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="tupoksi">
                    <textarea id="utama_tupoksi" name="utama_tupoksi" ><?php echo getPengaturanFront('utama_tupoksi');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane" id="struktur">
                    <textarea id="utama_struktur" name="utama_struktur" ><?php echo getPengaturanFront('utama_struktur');?></textarea>                 
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

    $(function () {
        CKEDITOR.editorConfig = function( config )
        {
            config.height = '800px';
        };
        CKEDITOR.replace('utama_kepala');
        CKEDITOR.replace('utama_sambutan');
        CKEDITOR.replace('utama_tentang');
        CKEDITOR.replace('utama_visimisi');
        CKEDITOR.replace('utama_tupoksi');
        CKEDITOR.replace('utama_struktur');
    })
    $("#myForm").submit(function (e) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var url = "<?php echo base_url('front/back/profil'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('front/back/profil'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>