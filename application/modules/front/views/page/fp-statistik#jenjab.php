<div class="news-main">
    <h3>Statistik Berdasarkan Jenis Jabatan</h3>
    <div class="spacer"></div>
    <div class="chart" style="height: 300px">
        <canvas id="pieChart2"></canvas>
    </div>
</div>
<script>

    var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    var pieChart2 = new Chart(pieChartCanvas2);
    var PieData2 = [
<?php
$n = 0;
if (!empty($sebaranJenJab)) {
    $color = array('grey', '#dd4b39', '#00a65a', '#f39c12', '#00c0ef');
    foreach ($sebaranJenJab as $x) {
        $n++;
        echo""
        . "{"
        . "value:" . $x->jum . ","
        . "color:'" . @$color[$x->PNS_JNSJAB] . "',"
        . "label:'" . ($x->PNS_JNSJAB == 0 ? 'N/A' : convert_jenjab($x->PNS_JNSJAB, TRUE)) . "'"
        . "},";
    }
}
?>];


    var pieOptions = {
        percentageInnerCutout: 0,
        responsive: true,
        maintainAspectRatio: false,
    };
    pieChart2.Doughnut(PieData2, pieOptions);





</script>