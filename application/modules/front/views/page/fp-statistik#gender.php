<div class="news-main">
    <h3>Statistik Berdasarkan Gender</h3>
    <div class="spacer"></div>
    <div class="chart" style="height: 300px">
        <canvas id="pieChart2"></canvas>
    </div>
</div>
<script>

    var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    var pieChart2 = new Chart(pieChartCanvas2);
    var PieData2 = [
<?php
$n = 0;
if (!empty($sebaranGender)) {
    $color = array('grey', 'lightblue', 'pink',);
    foreach ($sebaranGender as $x) {
        $n++;
        echo""
        . "{"
        . "value:" . $x->jum . ","
        . "color:'" . @$color[$n] . "',"
        . "label:'" . ($x->PNS_PNSSEX == 1 ? 'Pria' : 'Wanita') . "'"
        . "},";
    }
}
?>];


    var pieOptions = {
        percentageInnerCutout: 0,
        responsive: true,
        maintainAspectRatio: false,
    };
    pieChart2.Doughnut(PieData2, pieOptions);





</script>