<h3>Pusat Unduhan</h3>
<div class="spacer"></div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class='text-center'>Nama File</th>
                    <th class='text-center'>Ukuran</th>
                    <th class='text-center'>Jenis File</th>
                    <th class='text-center'>Unduh File</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $unduhan = getUnduhan();
                foreach ($unduhan as $u) {
                   echo '<tr>';
                   echo "<td>$u->undh_title</td>";
                   echo "<td>".number_format(($u->undh_size/1024), 2, ',', '.')." KB</td>";
                   echo "<td class='text-center'>".str_replace('.', '', $u->undh_type)."</td>";
                   echo "<td class='text-center'><a class='btn btn-xs btn-default btn-flat' target='_BLANK' href='" . base_url('front/download/unduhan/' . $u->undh_id) . "' title='unduh file'><i class='fa fa-download'></i></a></td>";
                   echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<style>
    .kontak-kami span{
        margin-bottom: 10px;
        display: inline-block;
        white-space: nowrap;
        display:block;
    }
</style>