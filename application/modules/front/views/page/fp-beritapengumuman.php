<h3><?php echo ucfirst($this->uri->segment(2)); ?></h3>
<div class="row">	
    <?php
    $n = ($this->uri->segment(3)) + 1;
    //print_r($datax);
    if ($datax != NULL) {
        foreach ($datax as $d) {

            if (strlen($d->post_content) > 320) {
                // truncate string
                $stringCut = substr($d->post_content, 0, 320);
                $stringCut = str_replace('</p>', ' ', $stringCut);
                $stringCut = str_replace('<p>', ' ', $stringCut);
                $stringCut = str_replace('<ol>', ' ', $stringCut);
                $stringCut = str_replace('<li>', ' ', $stringCut);
                // make sure it ends in a word so assassinate doesn't become ass...
                $d->post_content = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
            }
            ?>
            <div class="col-md-4">
                <div class="list-berita">
                    <div class="thumb">
                        <?php if (@file_get_contents(base_url('frontend/img/contents/post_' . $d->post_id . '.jpg'))) { ?>
                            <img class="img-responsive" src="<?php echo base_url('frontend/img/contents/post_' . $d->post_id . '.jpg?' . $d->post_revision) ?>">
                        <?php } else { ?>
                            <img class="img-responsive" src="<?php echo base_url('frontend/img/no_image.png'); ?>">
                        <?php } ?>                    
                    </div> 
                    <ul>
                        <li><i class="fa fa-calendar"></i><?php echo mdate('%d %F %Y', $d->post_created); ?></li>
                    </ul>
                    <a href="<?php echo base_url('frontpage/vb/' . $d->post_id); ?>"><h3><?php echo $d->post_title; ?></h3></a>
                    <span>
                        <p><?php echo @$d->post_content; ?></p>
                        <a class="readmore" href="<?php echo base_url('frontpage/vb/' . $d->post_id); ?>">Selengkapnya</a>
                    </span>
                </div>
            </div>  
            <?php
        }
    }
    ?>
</div><div class="paginasi">
    <nav class="shrink">
        <ul class="pagination">
            <?php echo $pagination; ?>
        </ul>
    </nav>    
</div> 