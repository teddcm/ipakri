<?php
if ($this->input->post()) {
    $datapeg = getPNS($this->input->post('carinip'));
    $this->session->set_flashdata('datapeg', $datapeg);
    $datacuti = get_detail_cuti($this->input->post('carinip'));
    if ($datacuti != NULL && cek_jatah_cutinew(date('Y')) > 0) {
        //print_r($datacuti);
        ?>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/typeahead/typeahead.css">
        <h3>Cuti PNS : <?php echo get_data_pns($this->input->post('carinip'), 'PNS_PNSNAM'); ?></h3>     
        <form id="myForm" action='../cetakcuti' target='cuti' method="POST">
            <table width="100%" class="table table-bordered" >
                <thead>

                    <tr>
                        <th width="30%">Jenis Cuti</th>
                        <th>Tanggal Mulai</th>
                        <th>Tanggal Selesai</th>
                        <th width="20%">Keterangan</th>
                        <th>Jumlah Hari</th>
                    </tr>
                </thead>
                <tfoot>  
                    <tr>
                        <th colspan="4">Sisa Cuti yang dapat diambil</th>
                        <th class="text-right" style="color: red"><?php echo $sisacuti = cek_sisa_cutinew($this->input->post('carinip')); ?></th>
                    </tr>
                </tfoot>
                <tr>
                    <td>Jatah Tahun Ini</td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td></td>
                    <td class="text-right"><?php echo cek_jatah_cutinew(); ?></td>
                </tr>
                <?php
                foreach ($datacuti as $d) {
                    ?>
                    <tr>
                        <td><?php echo convert_jenis_cuti($d->RWCUTI_JNS); ?></td>
                        <td class="text-center"><?php echo reverseDate($d->RWCUTI_TGLAWL); ?></td>
                        <td class="text-center"><?php echo reverseDate($d->RWCUTI_TGLAKR); ?></td>
                        <td><?php echo ($d->RWCUTI_KET); ?></td>
                        <td class="text-right"><?php echo ($d->RWCUTI_JUM); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Tanggal Mulai Cuti</label>
                    <input id="RWCUTI_TGLAWL" name="date_awal" required type="text" class="form-control" placeholder="tanggal mulai">
                </div>
                <div class="form-group col-md-4">
                    <label>Tanggal Selesai Cuti</label>
                    <input id="RWCUTI_TGLAKR" name="date_akhir" required type="text" class="form-control" placeholder="tanggal akhir">            
                </div>
                <div class="form-group col-md-4">
                    <label>Jumlah Hari</label>
                    <input id="RWCUTI_JUM" name="kuantitas" required type="number" class="form-control" placeholder="jumlah hari" max="<?php echo $sisacuti; ?>" min="0">
                </div>
                <div class="form-group col-md-5">
                    <label>Atasan Langsung</label>
                    <input class="form-control" type="text" id="NIP" name="atasan" placeholder="Cari NIP / Nama . . . " required>
                </div>
                <div class="form-group col-md-7">
                    <label>&nbsp;</label>
                    <input class="form-control" type="text" id="namaatasan" disabled>
                </div>
                <div class="form-group col-md-9">
                    <label>Pejabat yang Berwenang </label>
                    <select class="form-control" name="pjb">
                        <?php
                        foreach ($dataSpesimen as $x) {
                            echo '<option value="' . $x->SPES_NIPBARU . '">' . $x->SPES_PNSNAM . '</option>';
                        }
                        ?>
                    </select> 
                </div>
                <div class="form-group col-md-9">
                    <label>Alasan Cuti</label>
                    <input id="alasan_cuti" name="alasan_cuti" class="form-control" required>
                </div>
                <div class="form-group col-md-3">
                    <label>Telepon</label>
                    <input id="telepon_cuti" name="telepon_cuti" class="form-control" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Alamat Selama Cuti</label>
                    <input id="alamat_cuti" name="alamat_cuti" class="form-control" required>
                </div>
                <input name="nip" type="hidden" value="<?php echo $this->input->post('carinip'); ?>">
                <input name="id" type="hidden" value="<?php echo round(microtime(true) * 100); ?>">
                <input name="ip" type="hidden">
                <input name="city" type="hidden">
                <input name="loc" type="hidden">
                <input name="org" type="hidden">
                <div class="text-right col-md-12">
                    <button type="submit" class="btn btn-success">cetak form</button>
                </div>
            </div>
        </form>

        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/moment.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/typeahead/typeahead.bundle.js"></script>
        <script>
            var vpendidikan = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: '<?= base_url('admin/json/json_pegawai'); ?>?q=y',
                remote: {
                    url: '<?= base_url('admin/json/json_pegawai'); ?>?q=%QUERY',
                    wildcard: '%QUERY',
                    cache: false
                }
            });
            $('#NIP').typeahead(null, {
                name: 'pendidikan',
                display: 'PNS_NIPBARU',
                limit: 15,
                source: vpendidikan,
                templates: {
                    empty: [
                        '<div class="tt-suggestion">',
                        'DATA TIDAK DITEMUKAN',
                        '</div>'
                    ].join('\n'),
                    suggestion: function (data) {
                        return '<p><strong>' + data.PNS_NIPBARU + '</strong><br><u> ' + data.PNS_PNSNAM + '</u></p>';
                    }
                }
            }).on('typeahead:selected', function (event, data) {
                $('#NIP').val(data.PNS_NIPBARU);
                $('#namaatasan').val(data.PNS_PNSNAM);
            });
            $.get("https://ipinfo.io", function (response) {
                $.each(response, function (i, value) {
                    $('input[name=' + i + ']').val(value);
                });
            }, "jsonp");
            $('#myForm').submit(function () {
                //history.back();
            })
            $(function () {
                //$("#RWCUTI_TGLAWL,#RWCUTI_TGLAKR").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});

                $("#RWCUTI_TGLAWL").on("dp.change", function (e) {
                    $('#RWCUTI_TGLAKR').data("DateTimePicker").minDate(e.date);
                    countDiff();
                });
                $("#RWCUTI_TGLAKR").on("dp.change", function (e) {
                    $('#RWCUTI_TGLAWL').data("DateTimePicker").maxDate(e.date);
                    countDiff();
                });
                $('#RWCUTI_TGLAWL').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    format: 'DD-MM-YYYY',
                    daysOfWeekDisabled: [0, 6]
                });
                $('#RWCUTI_TGLAKR').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    format: 'DD-MM-YYYY',
                    daysOfWeekDisabled: [0, 6]
                });

                function countDiff() {
                    var iWeeks, iDateDiff, iAdjust = 0;
                    var tglAwl = $('#RWCUTI_TGLAWL').val().split('-');
                    var tglAkr = $('#RWCUTI_TGLAKR').val().split('-');
                    var dDate1 = new Date(tglAwl[2] + '-' + tglAwl[1] + '-' + tglAwl[0]);
                    var dDate2 = new Date(tglAkr[2] + '-' + tglAkr[1] + '-' + tglAkr[0]);
                    //alert(dDate1);
                    var iWeeks, iDateDiff, iAdjust = 0;
                    if (dDate2 < dDate1)
                        return -1; // error code if dates transposed
                    var iWeekday1 = dDate1.getDay(); // day of week
                    var iWeekday2 = dDate2.getDay();
                    iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
                    iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
                    if ((iWeekday1 == 6 && iWeekday2 == 6) || (iWeekday1 == 7 && iWeekday2 == 7)) {
                        iAdjust = 1; // adjustment if both days on weekend
                    } else if (iWeekday1 == 6 && iWeekday2 == 7) {
                        iAdjust = 2;
                    }
                    // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
                    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

                    if (iWeekday1 <= iWeekday2) {
                        iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
                    } else {
                        iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
                    }
                    iDateDiff -= iAdjust // take into account both days on weekend
                    $('#RWCUTI_JUM').val(iDateDiff + 1); // add 1 because dates are inclusive
                }
            });
        </script>
        <?php
    } else {
        ?>
        <h3>Cuti PNS : <?php echo get_data_pns($this->input->post('carinip'), 'PNS_PNSNAM'); ?></h3>     
        <div class="spacer"></div>
        <strong>Cuti tahun <?php echo date('Y'); ?> untuk NIP tersebut belum dialokasikan. Silakan hubungi admin.</strong>
        <?php
    }
}
?>