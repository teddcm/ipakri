<?php
if ($this->input->post() && cek_sisa_cuti($this->input->post('nip')) >= $this->input->post('kuantitas')) {
    $data = $this->input->post();
    $data['id'] = round(microtime(true) * 100);
    simpanTempCuti($data);
    ?>
    <html moznomarginboxes mozdisallowselectionprint>
        <head>
            <title>Form Usul Cuti</title>
        </head>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>frontend/assets/dist/css/bootstrap.css" rel="stylesheet">
        <body onload="cetak()">
            <style type="text/css" media="print">
                @page { size: landscape; }
            </style>
            <style>
                .table-main{
                    font-size: 14px !important;
                }
                table{
                    font-size: 10px;
                    font-family: times;
                }
                td{
                    padding-top: 5px;
                    padding-left: 4px;
                    padding-right: 4px;
                }@media print {
                    @page { margin: 0; }
                    body { margin: 1.6cm; }
                }
            </style>
            <table style="width: 100%" class="table-main">

                <tr><td style="width: 65%" rowspan="6"></td><td>Banjarbaru, <?php echo convertDate(now()); ?></td></tr>

                <?php
                $kepada = $header = explode(PHP_EOL, getPengaturan('cuti_form_kepada'));
                foreach ($kepada as $x) {
                    echo '<tr><td>';
                    echo str_replace(' ', '&nbsp', $x);
                    echo '</td></tr>';
                }
                ?>
            </table>

            <table style="width: 100%" class="table-main">
                <tr>
                    <td style="width: 100%" colspan="4">
                        Yang bertanda tangan di bawah ini :
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%; padding-left: 70px">Nama</td><td>: <?php echo ($datapeg->PNS_GLRDPN ? $datapeg->PNS_GLRDPN . ' ' : '') . $datapeg->PNS_PNSNAM . ($datapeg->PNS_GLRBLK ? ', ' . $datapeg->PNS_GLRBLK : ''); ?></td>
                </tr>
                <tr>
                    <td style="width: 40%; padding-left: 70px">NIP</td><td>: <?php echo $datapeg->PNS_NIPBARU; ?></td>
                </tr>
                <tr>
                    <td style="width: 40%; padding-left: 70px">Pangkat / Golongan Ruang</td><td>: <?php echo convert_golongan($datapeg->PNS_GOLRU) . ' / ' . convert_pangkat($datapeg->PNS_GOLRU); ?></td>
                </tr>
                <tr>
                    <td style="width: 40%; padding-left: 70px">Satuan Organisasi</td><td>: <?php echo get_induk_unor($datapeg->PNS_UNOR)->UNO_NAMUNO; ?></td>
                </tr>
                <tr>
                    <td colspan="2">Dengan ini mengajukan permohonan cuti tahunan untuk tahun <b><?php echo date('Y'); ?></b> selama <b><?php echo $this->input->post('kuantitas') . ' (' . terbilang($this->input->post('kuantitas')) . ')'; ?></b> hari kerja. Terhitung mulai tanggal
                        <b><?php echo convertDate(strtotime(reverseDate($this->input->post('date_awal')))); ?></b> s/d <b><?php echo convertDate(strtotime(reverseDate($this->input->post('date_akhir')))); ?></b>.</td>
                </tr>
                <tr>
                    <td colspan="2">Selama menjalankan cuti alamat saya adalah di <b><?php echo strtoupper($this->input->post('alamat_cuti')); ?></b>.</td>
                </tr>
                <tr>
                    <td colspan="2">Demikian permintaan ini untuk dapat dipertimbangkan sebagaimana mestinya.</td>
                </tr>
            </table>
            <table style="width: 100%" class="table-main">

                <tr>
                    <td style="width: 60%" rowspan="3"></td>
                    <td align='center' style="align-content: center">Hormat saya,<br><br><br><br></td>
                </tr>
                <tr>
                    <td align='center' style="align-content: center"> <?php echo ($datapeg->PNS_GLRDPN ? $datapeg->PNS_GLRDPN . ' ' : '') . $datapeg->PNS_PNSNAM . ($datapeg->PNS_GLRBLK ? ', ' . $datapeg->PNS_GLRBLK : ''); ?></td>
                </tr>
                <tr>
                    <td align='center'> NIP. <?php echo $datapeg->PNS_NIPBARU; ?></td>
                </tr>
            </table><br>
            <table style="width: 100%; vertical-align: top" border='1'>
                <tr>
                    <td rowspan="3" style="width: 65%">
                        CATATAN PEJABAT KEPEGAWAIAN

                        <table class="table table-bordered" >
                            <?php
                            $n = 0;
                            $a = 0;
                            $b = 0;
                            $c = 0;
                            //print_r($datacuti);
                            foreach ($datacuti as $x) {
                                if ($x->RWCUTI_JNS <= 1) {
                                    $n = $n + $x->RWCUTI_JUM;
                                }
                                switch ($x->RWCUTI_JNS) {
                                    case '0':
                                        $a += $x->RWCUTI_JUM;
                                        break;
                                    case '-1':
                                        $b += $x->RWCUTI_JUM;
                                        break;
                                    case '1':
                                        $c += $x->RWCUTI_JUM;
                                        break;
                                    default:break;
                                }
                            }
                            echo '<tr><td>1</td>   <td>Hak Cuti Tahun ' . date('Y') . '</td><td align="right">' . $a . ' hari</td></tr>';
                            echo '<tr><td>2</td>   <td>Sisa Cuti Tahun ' . (date('Y') - 1) . '</td><td align="right">' . $b . ' hari</td></tr>';
                            echo '<tr><td>3</td>   <td>Cuti yang Telah Diambil</td><td align="right">' . $c . ' hari</td></tr>';
                            ?>
                            <tr><td colspan="2">Sisa cuti yang dapat diambil</td><td align="right" width="20%"><?php echo $n; ?> hari</td></tr>
                        </table>
                        <table class="table table-bordered" >
                            <tr><td colspan="2">Cuti yang akan diambil tanggal <br><?php echo convertDate(strtotime(reverseDate($this->input->post('date_awal')))); ?> s/d <?php echo convertDate(strtotime(reverseDate($this->input->post('date_akhir')))); ?></td><td align="right" width="20%"><?php echo $this->input->post('kuantitas'); ?> hari</td></tr>

                            <tr><td colspan="2">Sisa Cuti</td><td align="right"><?php echo $n - $this->input->post('kuantitas'); ?> hari</td></tr>
                            <?php
                            $kepada = $header = explode(PHP_EOL, getSpesimen(getPengaturan('cuti_pjb_kepegawaian')));
                            echo '<tr><td colspan="3" align="center" style="padding-left: 50%;font-size:12px";">';
                            $enter = '';
                            foreach ($kepada as $x) {
                                echo $enter . str_replace(' ', '&nbsp', $x);
                                $enter = '<br>';
                            }
                            echo '</td></tr>';
                            ?>
                        </table>
                    </td>
                    <td align="center">
                        CATATAN PERTIMBANGAN ATASAN LANGSUNG<br><br><br><br><br><br><br><br><br><br>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        MENGETAHUI KABID/KABAG<br><br><br><br><br><br><br><br><br><br>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        KEPUTUSAN PEJABAT YANG BERWENANG<br><br>
                        <?php
                        $kepada = $header = explode(PHP_EOL, getSpesimen(getPengaturan('cuti_pjb_berwenang')));
                        echo '<span style="font-size:12px">';
                        $enter = '';
                        foreach ($kepada as $x) {
                            echo $enter . str_replace(' ', '&nbsp', $x);
                            $enter = '<br>';
                        }
                        echo '</span>';
                        ?>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td colspan="2" style="font-size: 8px"><div class="pull-right" style="font-size: 14px;color: white;background-color: black">&nbsp;<?php echo $data['id']; ?>&nbsp;</div>Dicetak: <?php echo date('d-m-Y H:i:s') . ' | ' . $this->input->post('ip') . ' | ' . $this->input->post('loc') . ' | ' . $this->input->post('city') . ' | ' . $this->input->post('org'); ?></td>
                </tr>
            </table>
        </body>
    </html>
    <script>
        function cetak() {
            window.print();
            //window.top.close();
        }
    </script>
    <?php
} else {
    ?>
    <script>
        window.top.close();
    </script>
    <?php
}?>