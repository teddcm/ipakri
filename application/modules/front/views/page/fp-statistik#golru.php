<div class="news-main">
    <h3>Statistik Berdasarkan Golongan</h3>
    <div class="spacer"></div>
    <div class="chart" style="height: 300px">
        <canvas id="barChartGol"></canvas>
    </div>
</div>
<script>
//CHART GOLONGAN

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
    };
    barChartOptions.datasetFill = false;
<?php
$jumgol = '[';
$labgol = '[';
$coma = '';
if (!empty($sebaranGolru)) {
    foreach ($sebaranGolru as $x) {
        $jumgol .= $coma . '"' . ($x->jum) . '"';
        $labgol .= $coma . '"' . convert_golongan($x->PNS_GOLRU) . '"';
        $coma = ',';
    }
}
$jumgol .= ']';
$labgol .= ']';
?>
    var chartDataGol = {
        labels: <?php echo $labgol; ?>,
        datasets: [
            {
                data: <?php echo $jumgol; ?>,
            }
        ]
    };
    var barChartCanvasGol = $("#barChartGol").get(0).getContext("2d");
    var barChartGol = new Chart(barChartCanvasGol)
    chartDataGol.datasets[0].fillColor = "green";
    chartDataGol.datasets[0].strokeColor = "green";
    chartDataGol.datasets[0].pointColor = "green";
    barChartGol.Bar(chartDataGol, barChartOptions);


</script>