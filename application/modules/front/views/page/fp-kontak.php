<h3>Kontak Kami</h3>
<div class="spacer"></div>
<div class="row">
    <div class="col-lg-6">
        <a href="https://goo.gl/maps/faoDge4c8PM2" title="Menuju Dinas PU Kalteng" target="_BLANK">
            <img src="<?php echo base_url('frontend/img/peta.jpg'); ?>" class="img-responsive">
        </a>
    </div>
    <div class="col-lg-6">
        <div class="kontak-kami">
            <span><i class="fa fa-map"></i> Jl. S. Parman No. 3 Palangka Raya 73112 </span>
            <span><i class="fa fa-envelope"></i> pu@kalteng.go.id</span>
            <span><i class="fa fa-phone"></i> (0536) 3221015</span>
            <span><i class="fa fa-fax"></i> (0536) 3224758</span>
        </div>
    </div>
</div>
<style>
    .kontak-kami span{
        margin-bottom: 10px;
        display: inline-block;
        white-space: nowrap;
        display:block;
    }
</style>