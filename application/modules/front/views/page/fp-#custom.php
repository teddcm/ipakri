<div class="single-content">
    <h3><?php echo $hlmn_title; ?></h3>
    <ul>
        <li><i class="fa fa-calendar"></i><?php echo mdate("%l, %d %F %Y %H:%i:%s", $hlmn_edited); ?></li>
    </ul> 
    <?php if (@file_get_contents(base_url('frontend/img/contents/hlmn_' . $hlmn_id . '.jpg'))) { ?>
        <div class="thumb">
            <a title="<?php echo $hlmn_title; ?>" id="single_image" href="<?php echo base_url('frontend/img/contents/hlmn_' . $hlmn_id . '.jpg?' . $hlmn_revision); ?>">
                <img class="img-responsive" src="<?php echo base_url('frontend/img/contents/hlmn_' . $hlmn_id . '.jpg?' . $hlmn_revision); ?>">
            </a>
        </div>  
    <?php } ?>
    <?php echo $hlmn_content ?>
    <div class="clearfix"></div>
</div>
<script>
    $("a#single_image").fancybox({
        'titlePosition': 'inside'
    });

</script>