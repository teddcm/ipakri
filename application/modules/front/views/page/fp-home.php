<?php
$data = getPost(1, 0, 4);
if ($data != NULL) {
    ?>
    <div class="general-content-title">Berita Terbaru</div>
    <div class="row">
        <?php
        foreach ($data['data'] as $d) {
            if (strlen($d->post_content) > 320) {
                // truncate string
                $stringCut = substr($d->post_content, 0, 320);
                $stringCut = str_replace('</p>', ' ', $stringCut);
                $stringCut = str_replace('<p>', ' ', $stringCut);
                $stringCut = str_replace('<ol>', ' ', $stringCut);
                $stringCut = str_replace('<li>', ' ', $stringCut);
                // make sure it ends in a word so assassinate doesn't become ass...
                $d->post_content = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
            }
            ?>
            <div class="col-md-6">
                <div class="main-news">
                    <a href="<?php echo base_url('frontpage/vb/' . $d->post_id); ?>">
                        <div class="thumb">
                            <?php if (@file_get_contents(base_url('frontend/img/contents/post_' . $d->post_id . '.jpg'))) { ?>
                                <img class="img-responsive" src="<?php echo base_url('frontend/img/contents/post_' . $d->post_id . '.jpg?' . $d->post_revision) ?>">
                            <?php } else { ?>
                                <img class="img-responsive" src="<?php echo base_url('frontend/img/no_image.png'); ?>">
                            <?php } ?>
                        </div>
                    </a>
                    <ul>
                        <li><i class="fa fa-calendar"></i><?php echo mdate('%d %F %Y', $d->post_created); ?></li>
                    </ul>
                    <h3><a href="<?php echo base_url('frontpage/vb/' . $d->post_id); ?>"><?php echo $d->post_title; ?></a></h3>
                    <span><?php echo @$d->post_content; ?></span>
                    <a class="readmore" href="<?php echo base_url('frontpage/vb/' . $d->post_id); ?>">Selengkapnya</a>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <?php
}
$datax = getPost(2, 0, 4);
if ($datax != NULL) {
    ?>

    <div class="clearfix" style="padding-top: 100px"></div>
    <div class="general-content-title">Pengumuman Terbaru</div>
    <div class="row">
        <?php
        foreach ($datax['data'] as $d) {
            if (strlen($d->post_content) > 320) {
                // truncate string
                $stringCut = substr($d->post_content, 0, 320);
                $stringCut = str_replace('</p>', ' ', $stringCut);
                $stringCut = str_replace('<p>', ' ', $stringCut);
                $stringCut = str_replace('<ol>', ' ', $stringCut);
                $stringCut = str_replace('<li>', ' ', $stringCut);
                // make sure it ends in a word so assassinate doesn't become ass...
                $d->post_content = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
            }
            ?>
            <div class="col-md-6">
                <div class="main-news">
                    <a href="<?php echo base_url('frontpage/vp/' . $d->post_id); ?>">
                        <div class="thumb">
                            <?php if (@file_get_contents(base_url('frontend/img/contents/post_' . $d->post_id . '.jpg'))) { ?>
                                <img class="img-responsive" src="<?php echo base_url('frontend/img/contents/post_' . $d->post_id . '.jpg?' . $d->post_revision) ?>">
                            <?php } else { ?>
                                <img class="img-responsive" src="<?php echo base_url('frontend/img/no_image.png'); ?>">
                            <?php } ?>
                        </div>
                    </a>
                    <ul>
                        <li><i class="fa fa-calendar"></i><?php echo mdate('%d %F %Y', $d->post_created); ?></li>
                    </ul>
                    <h3><a href="<?php echo base_url('frontpage/vp/' . $d->post_id); ?>"><?php echo $d->post_title; ?></a></h3>
                    <span><?php echo @$d->post_content; ?></span>
                    <a class="readmore" href="<?php echo base_url('frontpage/vp/' . $d->post_id); ?>">Selengkapnya</a>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <div class="spacer"></div>
    <?php
}?>