<?php
@$id = $this->uri->segment(3) == null ? NULL : $this->uri->segment(3);
@$newdata = getAlbum(0, 0, $id);
@$data = $newdata['foto'];
?>
<h3><a href="<?php echo base_url('frontpage/p/galeri');?>">Galeri</a> / Album <?php echo @$newdata['data'][0]->albm_nama; ?></h3>
<div class="spacer"></div>
<div class="album">
    <?php
    if ($data != NULL) {
        foreach ($data as $d) {
            ?>
            <a href="<?php echo base_url('frontend/img/galleries/' . $d->glri_id . '.jpg'); ?>" data-fancybox="group" data-caption="<?php echo $newdata['data'][0]->albm_nama; ?>">
                <img src="<?php echo base_url('frontend/img/galleries/thumbs/' . $d->glri_id . '_thumb.jpg'); ?>" />
            </a>
            <?php
        }
    }
    ?>
</div>
<script>
    $('[data-fancybox]').fancybox({
        protect: true,
        loop: true,
    });
</script>
