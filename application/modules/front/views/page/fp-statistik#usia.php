<div class="news-main">
    <h3>Statistik Berdasarkan Usia</h3>
    <div class="spacer"></div>
    <div class="chart" style="height: 300px">
        <canvas id="barChart"></canvas>
    </div>
</div>
<script>
//CHART GOLONGAN

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
    };
    barChartOptions.datasetFill = false;
<?php
$jum = '[';
$lab = '[';
$coma = '';
if (!empty($sebaranUsia)) {
    foreach ($sebaranUsia as $x) {
        $jum .= $coma . '"' . ($x->jum) . '"';
        $lab .= $coma . '"' .$x->usia . '"';
        $coma = ',';
    }
}
$jum .= ']';
$lab .= ']';
?>
    var chartDataGol = {
        labels: <?php echo $lab; ?>,
        datasets: [
            {
                data: <?php echo $jum; ?>,
            }
        ]
    };
    var barChartCanvasGol = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvasGol)
    chartDataGol.datasets[0].fillColor = "red";
    chartDataGol.datasets[0].strokeColor = "red";
    chartDataGol.datasets[0].pointColor = "red";
    barChart.Bar(chartDataGol, barChartOptions);


</script>