<?php
if ($this->input->post() && cek_sisa_cutinew($this->input->post('nip')) >= $this->input->post('kuantitas')) {
    $data = $this->input->post();
    simpanTempCuti($data);
    ?>
    <html moznomarginboxes mozdisallowselectionprint>
        <head>
            <title>FORMULIR PERMINTAAN DAN PEMBERIAN CUTI</title>
        </head>
        <!-- Bootstrap core CSS -->
        <script src="<?php echo base_url(); ?>frontend/assets/dist/js/jquery-3.2.1.min.js"></script>
        <link href="<?php echo base_url(); ?>frontend/assets/dist/css/bootstrap.css" rel="stylesheet">
        <body>
            <style type="text/css" media="print">
                @page { size: portrait; }
            </style>
            <style type="text/css">
                .fancybox-margin{
                    margin-right:16px;
                }
                body{
                    -webkit-print-color-adjust: exact;
                    color-adjust: exact;
                }
                table{
                    font-size: 12px;
                    font-family: times;
                }
                td{
                    padding: 0px;
                    padding-left: 4px;
                    padding-right: 4px;
                    height: 16px;
                }@media print {
                    @page { margin: 0; }
                    body { margin: 0.5cm; }
                }.head-table{
                    background-color: lightgray!important;
                }
                table.table-border {
                    border-collapse: collapse;
                }

                table.table-border, table.table-border > tbody> tr > th, table.table-border > tbody > tr >  td {
                    border: 1px solid black;
                }
            </style>
            <table style="width: 100%" >
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 65%"></td>
                                <td class="pull-right" style="height: 100px">
                                    <div>
                                        Banjarbaru, <?php echo convertDate(now()); ?>
                                        <br>
                                        <?php
                                        $kepada = $header = explode(PHP_EOL, getPengaturan('cuti_form_kepada'));
                                        foreach ($kepada as $x) {
                                            echo '<br>';
                                            echo str_replace(' ', '&nbsp', $x);
                                        }
                                        ?>
                                        <br>
                                        <br>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h4>FORMULIR PERMINTAAN DAN PEMBERIAN CUTI</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="4">I. DATA PEGAWAI</td>
                            </tr>
                            <tr>
                                <td width="60px">Nama</td>
                                <td><b><?php echo ($datapeg->PNS_GLRDPN ? $datapeg->PNS_GLRDPN . ' ' : '') . $datapeg->PNS_PNSNAM . ($datapeg->PNS_GLRBLK ? ', ' . $datapeg->PNS_GLRBLK : ''); ?></b></td>
                                <td width="60px">NIP</td>
                                <td><b><?php echo split_nip($datapeg->PNS_NIPBARU); ?></b></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td><b><?php echo ($datapeg->RWJAB_NAMAJAB); ?></b></td>
                                <td>Masa Kerja</td>
                                <td>
                                    <b><?php
                                        $mkg = get_mks($datapeg->PNS_TMTCPN);
                                        echo $mkg->tahun . ' tahun ' . $mkg->bulan . ' bulan';
                                        ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Kerja</td>
                                <td colspan="3"><b><?php echo ($datapeg->UNO_NAMUNO); ?></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="4">II. JENIS CUTI YANG DIAMBIL **</td>
                            </tr>
                            <tr>
                                <td width="200px">1. Cuti Tahunan</td>
                                <td class="text-center"><span class="kotak-cek"></span></td>         
                                <td width="200px">2. Cuti Besar</td>
                                <td class="text-center"><span class="kotak"></span></td>         
                            </tr>
                            <tr>
                                <td width="200px">3. Cuti Sakit</td>
                                <td class="text-center"><span class="kotak"></span></td>         
                                <td width="200px">4. Cuti Melahirkan</td>
                                <td class="text-center"><span class="kotak"></span></td>         
                            </tr>
                            <tr>
                                <td width="200px">5. Cuti Karena Alasan Penting</td>
                                <td class="text-center"><span class="kotak"></span></td>         
                                <td width="200px">6. Cuti di Luar Tanggungan Negara</td>
                                <td class="text-center"><span class="kotak"></span></td>         
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table">III. ALASAN CUTI</td>
                            </tr>
                            <tr>
                                <td><b><?php echo strtoupper($this->input->post('alasan_cuti')); ?></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="6">IV. LAMANYA CUTI</td>
                            </tr>
                            <tr>
                                <td width="100px">Selama</td>
                                <td><b><?php echo $this->input->post('kuantitas') . ' (' . terbilang($this->input->post('kuantitas')) . ')'; ?></b> hari kerja</td>
                                <td width="100px" class="text-center">mulai tanggal</td>
                                <td><b><?php echo convertDate(strtotime(reverseDate($this->input->post('date_awal')))); ?></b></td>
                                <td width="50px" class="text-center">s/d</td>
                                <td><b><?php echo convertDate(strtotime(reverseDate($this->input->post('date_akhir')))); ?></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="5">V. CATATAN CUTI ***</td>
                            </tr>
                            <tr>
                                <td colspan="2">1. Cuti Tahunan</td>
                                <td class="text-center"><span class="kotak-cek"></span></td>
                                <td>2. Cuti Besar</td>
                                <td class="text-center"><span class="kotak"></span></td>
                            </tr>
                            <tr>
                                <td width="15%" style="background-color: whitesmoke">Tahun</td>
                                <td width="15%" style="background-color: whitesmoke">Sisa</td>
                                <td width="20%" style="background-color: whitesmoke">Keterangan</td>
                                <td width="30%">3. Cuti Sakit</td>
                                <td class="text-center"  width="20%" ><span class="kotak"></span></td>
                            </tr>
                            <tr>
                                <td style="background-color: whitesmoke">N-2</td>
                                <td style="background-color: whitesmoke"><b>0</b></td>
                                <td style="background-color: whitesmoke"></td>
                                <td width="200px">4. Cuti Melahirkan</td>
                                <td class="text-center"><span class="kotak"></span></td>
                            </tr>
                            <tr>
                                <td style="background-color: whitesmoke">N-1</td>
                                <td style="background-color: whitesmoke"><b><?php echo $totalcuti = (cek_sisa_cutinew($datapeg->PNS_NIPBARU) - cek_jatah_cutinew()); ?></b></td>
                                <td style="background-color: whitesmoke"></td>
                                <td width="200px">5. Cuti Karena Alasan Penting</td>
                                <td class="text-center"><span class="kotak"></span></td>
                            </tr>
                            <tr>
                                <td style="background-color: whitesmoke">N</td>
                                <td style="background-color: whitesmoke"><b><?php echo cek_jatah_cutinew(); ?></b></td>
                                <td style="background-color: whitesmoke"></td>
                                <td width="200px">6. Cuti di Luar Tanggungan Negara</td>
                                <td class="text-center"><span class="kotak"></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="3">VI. ALAMAT SELAMA MENJALANKAN CUTI</td>
                            </tr>
                            <tr>
                                <td rowspan="2" class="text-center"><b><?php echo strtoupper($this->input->post('alamat_cuti')); ?></b></td>
                                <td width="60px">Telepon</td>
                                <td width="200px"><b><?php echo strtoupper($this->input->post('telepon_cuti')); ?></b></td>
                            </tr>
                            <tr>
                                <td colspan="2" width="25%"class="text-center" style="font-size: 10px">
                                    Hormat saya,<br><br><br>
                                    <?php echo ($datapeg->PNS_GLRDPN ? $datapeg->PNS_GLRDPN . ' ' : '') . $datapeg->PNS_PNSNAM . ($datapeg->PNS_GLRBLK ? ', ' . $datapeg->PNS_GLRBLK : ''); ?><br>
                                    NIP. <?php echo split_nip($datapeg->PNS_NIPBARU); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="4">VII. PERTIMBANGAN ATASAN LANGSUNG **</td>
                            </tr>
                            <tr>
                                <td width="25%"><span class="kotak"></span>&nbsp;Disetujui</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Perubahan ****</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Ditangguhkan ****</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Tidak Disetujui ****</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                                <td colspan="2" width="25%" class="text-center" style="font-size: 10px">
                                    <br><br><br>
                                    <?php
                                    echo ($dataatasan->PNS_GLRDPN ? $dataatasan->PNS_GLRDPN . ' ' : '') . $dataatasan->PNS_PNSNAM . ($dataatasan->PNS_GLRBLK ? ', ' . $dataatasan->PNS_GLRBLK : '');
                                    echo "<br>NIP. " . split_nip($dataatasan->PNS_NIPBARU);
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td>
                        <table width="100%" class="table-border">
                            <tr>
                                <td class="head-table" colspan="4">VIII. KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI **</td>
                            </tr>
                            <tr>
                                <td width="25%"><span class="kotak"></span>&nbsp;Disetujui</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Perubahan ****</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Ditangguhkan ****</td>
                                <td width="25%"><span class="kotak"></span>&nbsp;Tidak Disetujui ****</td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                                <td colspan="2" width="25%" class="text-center" style="font-size: 10px">
                                    <br><br><br>
                                    <?php
                                    echo ($datapjb->PNS_GLRDPN ? $datapjb->PNS_GLRDPN . ' ' : '') . $datapjb->PNS_PNSNAM . ($datapjb->PNS_GLRBLK ? ', ' . $datapjb->PNS_GLRBLK : '');
                                    echo "<br>NIP. " . split_nip($datapjb->PNS_NIPBARU);
                                    ?>
                                    <!--?php
                                    $kepada = $header = explode(PHP_EOL, getSpesimen(getPengaturan('cuti_pjb_berwenang')));
                                    $enter = '';
                                    foreach ($kepada as $x) {
                                        echo $enter . str_replace(' ', '&nbsp', $x);
                                        $enter = '<br>';
                                    }
                                    ?-->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="font-size: 11px; border: solid black 1px;background-color: whitesmoke!important">
                            <tr>
                                <td colspan="5">Catatan :</td>
                            </tr>
                            <tr>
                                <td>*</td>
                                <td>Coret yang tidak perlu</td>
                                <td></td>
                                <td>N</td>
                                <td>Sisa cuti tahun berjalan</td>
                            </tr>
                            <tr>
                                <td>**</td>
                                <td>Pilih salah satu dengan memberi tanda centang (&#10004;)</td>
                                <td></td>
                                <td>N-1</td>
                                <td>Sisa cuti 1 tahun sebelumnya</td>
                            </tr>
                            <tr>
                                <td>***</td>
                                <td>Diisi oleh pejabat yang menangani bidang kepegawaian sebelum PNS mengajukan cuti</td>
                                <td></td>
                                <td>N-2</td>
                                <td>Sisa cuti 2 tahun sebelumnya</td>
                            </tr>
                            <tr>
                                <td width="5%">****</td>
                                <td width="60%">Diberi tanda centang (&#10004;) serta alasannya</td>
                                <td width="1%"></td>
                                <td width="5%"></td>
                                <td width="30%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                
                                <td colspan="1" style="font-size: 9px">
                                    Form pengajuan cuti ini berlaku selama 30 (tiga puluh) hari semenjak tanggal cetak. <br>
                                    Segera scan barcode (<?php echo $data['id']; ?>) pada Sistem Informasi Kepegawaian untuk validasi persetujuan.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 9px">
                                    <div class="pull-right barcode" style="font-size: 14px;color: white!important;background-color:grey!important;"><img src="<?php echo base_url(); ?>frontpage/barcode/<?php echo $data['id']; ?>"></div>
                                    Dicetak: <?php echo date('d-m-Y H:i:s') . ' | ' . $this->input->post('ip') . ' | ' . $this->input->post('loc') . ' | ' . $this->input->post('city') . ' | ' . $this->input->post('org'); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>
    <script>
        $('.kotak-cek').html('&#x2611;');
        $('.kotak').html('&#x2610;');

        $('.kotak,.kotak-cek').css({
            'font-size': 14,
            'position': 'float',
        });
        function cetak() {
            window.print();
            window.top.close();
        }
        setTimeout(function () {
            window.onload = cetak();
        }, 1000);

    </script>
    <?php
} else {
    ?>
    <script>
        window.top.close();
    </script>
    <?php
}?>