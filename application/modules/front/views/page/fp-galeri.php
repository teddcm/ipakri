<h3>Galeri</h3>
<div class="row">
    <?php
    $data = getAlbum(0, 0);
    $data = $data['data'];
    if ($data != NULL) {
        foreach ($data as $d) {
            ?>
            <div class="col-md-6">
                <a href="<?php echo base_url('frontpage/album/' . $d->albm_id); ?>">
                    <div class="galeri-grup">
                        <div class="galeri-foto">
                            <img src="<?php echo base_url('frontend/img/galleries/thumbs/' . $d->albm_preview . '_thumb.jpg'); ?>">
                        </div>
                        <h3><?php echo $d->albm_nama; ?></h3>
                    </div>
                </a>
            </div>
            <?php
        }
    }
    ?>
</div>
