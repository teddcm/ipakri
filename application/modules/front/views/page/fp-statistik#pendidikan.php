<div class="news-main">
    <h3>Statistik Berdasarkan Pendidikan</h3>
    <div class="spacer"></div>
    <div class="chart" style="height: 300px">
        <canvas id="barChartGol"></canvas>
    </div>
</div>
<script>
    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
    };
    barChartOptions.datasetFill = false;
<?php
$jumdik = '[';
$labdik = '[';
$coma = '';
if (!empty($sebaranPendidikan)) {
    foreach ($sebaranPendidikan as $x) {
        $jumdik .= $coma . '"' . ($x->jum) . '"';
        $labdik .= $coma . '"' . ($x->PNS_TKTDIK == '' ? 'N/A' : convert_tktpendik($x->PNS_TKTDIK)) . '"';
        $coma = ',';
    }
}
$jumdik .= ']';
$labdik .= ']';
?>
    var chartDataGol = {
        labels: <?php echo $labdik; ?>,
        datasets: [
            {
                data: <?php echo $jumdik; ?>,
            }
        ]
    };
    var barChartCanvasGol = $("#barChartGol").get(0).getContext("2d");
    var barChartGol = new Chart(barChartCanvasGol)
    chartDataGol.datasets[0].fillColor = "orange";
    chartDataGol.datasets[0].strokeColor = "orange";
    chartDataGol.datasets[0].pointColor = "orange";
    barChartGol.Bar(chartDataGol, barChartOptions);


</script>