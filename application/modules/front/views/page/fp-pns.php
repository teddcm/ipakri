<h3>Profil PNS</h3>
<?php
if ($this->input->post()) {
    $datapeg = getPNS($this->input->post('carinip'));
    $this->session->set_flashdata('datapeg', $datapeg);
    redirect(uri_string());
} else {
    $datapeg = $this->session->flashdata('datapeg');
    if ($datapeg != NULL) {
//print_r($datapeg);
        ?>
        <form method="POST" action="<?php echo base_url('frontpage/p/cuti'); ?>" id="cekcuti">
            <input type="hidden" value="<?php echo $datapeg->PNS_NIPBARU; ?>" name="carinip">
        </form>
        <div class="row">
            <div class="col-lg-3">
                <div class="foto">
                    <?php echo get_foto($datapeg->PNS_NIPBARU); ?>
                </div>
                <br>
                <div class="text-center">
                    <button class="tombol btn btn-primary btn-flat" id="cuti">Lihat Cuti</button>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="detail">
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="40%">Nama</td>
                                <td><?php echo ($datapeg->PNS_GLRDPN ? $datapeg->PNS_GLRDPN . ' ' : '') . $datapeg->PNS_PNSNAM . ($datapeg->PNS_GLRBLK ? ', ' . $datapeg->PNS_GLRBLK : ''); ?></td>
                            </tr>
                            <tr>
                                <td>NIP</td>
                                <td><?php echo ($datapeg->PNS_NIPBARU); ?></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td><?php echo get_jab_pegawai($datapeg->PNS_NIPBARU); ?></td>
                            </tr>
                            <tr>
                                <td>Tempat, Tanggal Lahir</td>
                                <td><?php echo convert_tempat_lahir($datapeg->PNS_TEMLHR) . ', ', reverseDate($datapeg->PNS_TGLLHRDT); ?></td>
                            </tr>
                            <tr>
                                <td>Agama</td>
                                <td><?php echo convert_agama($datapeg->PNS_KODAGA); ?></td>
                            </tr>
                            <tr>
                                <td>Pendidikan Terakhir</td>
                                <td><?php echo convert_nama_dik($datapeg->PEN_PENKOD); ?></td>
                            </tr>
                            <tr>
                                <td>Pangkat / Golongan Ruang</td>
                                <td><?php echo convert_pangkat($datapeg->PNS_GOLRU) . ', ' . convert_golongan($datapeg->PNS_GOLRU); ?></td>
                            </tr>
                            <tr>
                                <td>Unit Kerja</td>
                                <td><?php echo $datapeg->UNO_NAMUNO; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <script>
            $('#cuti').on('click', function () {
                $('#cekcuti').submit();
            })
            $.ajax({
                type: "GET",
                url: "<?php echo base_url('admin/json/json_foto_pegawai/' . $datapeg->PNS_NIPBARU); ?>",
                dataType: "html",
                success: function (img) {
                    $('#getFoto').append(img);
                },
                error: function (error, txtStatus) {
                    console.log(txtStatus);
                }
            });
        </script>
        <?php
    } else {
        echo '<p>Silakan masukkan NIP yang valid.</p>';
    }
}
?>