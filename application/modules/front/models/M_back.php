<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_back extends CI_Model {

    function simpanFrontPengaturan($data) {
        $this->db->trans_begin();
        foreach ($data as $x => $value) {
            $this->db->set('value_pengaturan', $value);
            $this->db->where('nama_pengaturan', $x);
            $this->db->update('front_pengaturan');
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    /*     * *********** Linker ************ */

    function simpanLinker($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['link_created'] = time('now');
            $data['link_edited'] = time('now');
            $data['link_id'] = $ID;
            $query = $this->db->insert('front_linker', $data);
        } else if ($mode == 'edit') {
            $data['link_edited'] = time('now');
            $ID = $data['link_id'];
            unset($data['link_id']);
            $this->db->where('link_id', $ID);
            $query = $this->db->update('front_linker', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusLinker($ID) {
        $file = ('frontend/img/contents/link_' . $ID . '.jpg');
        $thumb = ('frontend/img/contents/thumbs/link_' . $ID . '_thumb.jpg');
        $this->db->trans_begin();
        $this->db->where('link_id', $ID);
        $this->db->delete('front_linker');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            @unlink($file);
            @unlink($thumb);
            return true;
        }
    }

    /*     * *********** Slider ************ */

    function simpanSlider($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['slid_created'] = time('now');
            $data['slid_edited'] = time('now');
            $data['slid_id'] = $ID;
            $query = $this->db->insert('front_slider', $data);
        } else if ($mode == 'edit') {
            $data['slid_edited'] = time('now');
            $ID = $data['slid_id'];
            unset($data['slid_id']);
            $this->db->where('slid_id', $ID);
            $query = $this->db->update('front_slider', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusSlider($ID) {
        $file = ('frontend/img/contents/slid_' . $ID . '.jpg');
        $thumb = ('frontend/img/contents/thumbs/slid_' . $ID . '_thumb.jpg');
        $this->db->trans_begin();
        $this->db->where('slid_id', $ID);
        $this->db->delete('front_slider');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            @unlink($file);
            @unlink($thumb);
            return true;
        }
    }

    /*     * *********** Welcome ************ */

    function simpanWelcome($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['welc_created'] = time('now');
            $data['welc_edited'] = time('now');
            $data['welc_id'] = $ID;
            $query = $this->db->insert('front_welcome', $data);
        } else if ($mode == 'edit') {
            $data['welc_edited'] = time('now');
            $ID = $data['welc_id'];
            unset($data['welc_id']);
            $this->db->where('welc_id', $ID);
            $query = $this->db->update('front_welcome', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusWelcome($ID) {
        $this->db->trans_begin();
        $this->db->where('welc_id', $ID);
        $this->db->delete('front_welcome');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    /*     * *********** Post ************ */

    function simpanPost($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['post_created'] = time('now');
            $data['post_edited'] = time('now');
            $data['post_id'] = $ID;
            $query = $this->db->insert('front_post', $data);
        } else if ($mode == 'edit') {
            $data['post_edited'] = time('now');
            $ID = $data['post_id'];
            unset($data['post_id']);
            $this->db->where('post_id', $ID);
            $query = $this->db->update('front_post', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusPost($ID) {
        $file = ('frontend/img/contents/post_' . $ID . '.jpg');
        $thumb = ('frontend/img/contents/thumbs/post_' . $ID . '_thumb.jpg');
        $this->db->trans_begin();
        $this->db->where('post_id', $ID);
        $this->db->delete('front_post');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            @unlink($file);
            @unlink($thumb);
            return true;
        }
    }

    function jsonPost($id) {

        $this->db->trans_begin();
        $this->db->select();
        $this->db->where('post_id', $id);
        $result = $this->db->get('front_post');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            if ($result->num_rows() > 0) {
                return $result->row();
            }
        }
    }

    /*     * *********** Halaman ************ */

    function simpanHalaman($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['hlmn_created'] = time('now');
            $data['hlmn_edited'] = time('now');
            $data['hlmn_id'] = $ID;
            $query = $this->db->insert('front_halaman', $data);
        } else if ($mode == 'edit') {
            $data['hlmn_edited'] = time('now');
            $ID = $data['hlmn_id'];
            unset($data['hlmn_id']);
            $this->db->where('hlmn_id', $ID);
            $query = $this->db->update('front_halaman', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusHalaman($ID) {
        $file = ('frontend/img/contents/hlmn_' . $ID . '.jpg');
        $thumb = ('frontend/img/contents/thumbs/hlmn_' . $ID . '_thumb.jpg');
        @unlink($file);
        @unlink($thumb);
        $this->db->trans_begin();
        $this->db->where('hlmn_id', $ID);
        $this->db->delete('front_halaman');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function jsonHalaman($id) {

        $this->db->trans_begin();
        $this->db->select();
        $this->db->where('hlmn_id', $id);
        $result = $this->db->get('front_halaman');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            if ($result->num_rows() > 0) {
                return $result->row();
            }
        }
    }

    /*     * *********** Album ************ */

    function simpanAlbum($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['albm_created'] = time('now');
            $data['albm_edited'] = time('now');
            $data['albm_id'] = $ID;
            $query = $this->db->insert('front_album', $data);
        } else if ($mode == 'edit') {
            $data['albm_edited'] = time('now');
            $ID = $data['albm_id'];
            unset($data['albm_id']);
            $this->db->where('albm_id', $ID);
            $query = $this->db->update('front_album', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function simpanGaleriAlbum($newdata) {
        $data = $newdata;
        $this->db->trans_begin();

        $data['glri_created'] = time('now');
        $query = $this->db->insert('front_galeri', $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusGaleriAlbum($ID) {
        $file = ('frontend/img/galleries/' . $ID . '.jpg');
        $thumb = ('frontend/img/galleries/thumbs/' . $ID . '_thumb.jpg');
        $this->db->trans_begin();
        $this->db->where('glri_id', $ID);
        $this->db->delete('front_galeri');
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            unlink($file);
            unlink($thumb);
            return true;
        }
    }

    function hapusAlbum($ID) {
        $this->db->trans_begin();
        $data = getGaleri($ID);
        if ($data != NULL)
            foreach ($data as $d) {
                $file = ('frontend/img/galleries/' . $d->glri_id . '.jpg');
                $thumb = ('frontend/img/galleries/thumbs/' . $d->glri_id . '_thumb.jpg');
                unlink($file);
                unlink($thumb);
            }
        $this->db->where('glri_album', $ID);
        $this->db->delete('front_galeri');
        $this->db->where('albm_id', $ID);
        $this->db->delete('front_album');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function jsonAlbum($id) {

        $this->db->trans_begin();
        $this->db->select();
        $this->db->where('albm_id', $id);
        $result = $this->db->get('front_album');
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            if ($result->num_rows() > 0) {
                return $result->row();
            }
        }
    }

    /*     * *********** Unduhan ************ */

    function simpanUnduhan($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        if ($mode == 'tambah') {
            $ID = round(microtime(true) * 100);
            $data['undh_created'] = time('now');
            $data['undh_edited'] = time('now');
            $data['undh_id'] = $ID;
            $query = $this->db->insert('front_unduhan', $data);
        } else if ($mode == 'edit') {
            $data['undh_edited'] = time('now');
            $ID = $data['undh_id'];
            unset($data['undh_id']);
            $this->db->where('undh_id', $ID);
            $query = $this->db->update('front_unduhan', $data);
        }
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapusUnduhan($ID, $ext, $fileonly = false) {
        $file = ('frontend/unduhan/' . $ID . $ext);
        $this->db->trans_begin();
        if (!$fileonly) {
            $this->db->where('undh_id', $ID);
            $this->db->delete('front_unduhan');
        } 
        @unlink($file);
//echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
