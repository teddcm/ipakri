<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Back extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in()) {
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->form_validation->set_error_delimiters('', '');
        $this->load->helper('front');
    }

    public function profil() {
        if ($data = $this->input->post()) {
            $this->load->model('M_Back');
            if ($this->M_Back->simpanFrontPengaturan($data)) {
                json_result('success', 'Profil berhasil disimpan.');
            } else {
                json_result('error', 'Profil gagal disimpan.');
            };
        } else {

            $this->load->view('front/b-profil');
        }
    }

    public function renstra() {
        if ($data = $this->input->post()) {
            $this->load->model('M_Back');
            if ($this->M_Back->simpanFrontPengaturan($data)) {
                json_result('success', 'Rencana Strategis berhasil disimpan.');
            } else {
                json_result('error', 'Rencana Strategis gagal disimpan.');
            };
        } else {

            $this->load->view('front/b-renstra');
        }
    }

    public function utama() {
        if ($data = $this->input->post()) {
            $this->load->model('M_Back');
            if ($this->M_Back->simpanUtama($data)) {
                json_result('success', 'Profil berhasil disimpan.');
            } else {
                json_result('error', 'Profil gagal disimpan.');
            };
        } else {

            $this->load->view('front/b-utama');
        }
    }

    public function linker() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('link_title', 'Nama', 'required|alpha_numeric_spaces');
            $this->form_validation->set_rules('link_href', 'Link', 'required|custom');
            $this->form_validation->set_rules('link_target', 'Target', 'required|custom');
            $this->form_validation->set_rules('link_order', 'Order', 'numeric');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanLinker($data)) {
                    json_result('success', 'Tautan berhasil disimpan.');
                } else {
                    json_result('error', 'Tautan gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-linker');
        }
    }

    public function linkerHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusLinker($id)) {
            json_result('success', 'Tautan berhasil dihapus.');
        } else {
            json_result('error', 'Tautan gagal dihapus.');
        }
    }

    public function uploadLinker($PNS_NIPBARU = NULL) {
        $path = 'frontend/img/contents';
        linkerRevision($this->input->post('id'));
        $file = 'link_' . $this->input->post('id') . ".jpg";
        $filepath = base_url($path) . '/' . $file;

        $is_file_exist = @file_get_contents($filepath);
        //echo $is_file_exist;
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $config['upload_path'] = $path;
            $config['file_name'] = $file;
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', $this->upload->display_errors('', ''));
            } else {
                $image_data = $this->upload->data();
                $config_thumb = array(
                    'image_library' => 'GD2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => $path . '/thumbs',
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 150
                );
                $this->load->library('image_lib');
                $this->image_lib->clear(); // added this line
                $this->image_lib->initialize($config_thumb); // added this line
                if (!$this->image_lib->resize()) {
                    json_result('error', $this->image_lib->display_errors('', ''));
                } else {
                    json_result('success', 'Foto behasil diupload.');
                }
            }
        }
    }

    public function slider() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('slid_title', 'Judul', 'required|alpha_numeric_spaces');
            $this->form_validation->set_rules('slid_subtitle', 'Sub Judul', 'alpha_numeric_spaces');
            $this->form_validation->set_rules('slid_order', 'Order', 'numeric');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanSlider($data)) {
                    json_result('success', 'Slider berhasil disimpan.');
                } else {
                    json_result('error', 'Slider gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-slider');
        }
    }

    public function sliderHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusSlider($id)) {
            json_result('success', 'Slider berhasil dihapus.');
        } else {
            json_result('error', 'Slider gagal dihapus.');
        }
    }

    public function uploadSlider($PNS_NIPBARU = NULL) {
        $path = 'frontend/img/contents';
        sliderRevision($this->input->post('id')); //revisi foto
        $file = 'slid_' . $this->input->post('id') . ".jpg";
        $filepath = base_url($path) . '/' . $file;
        //echo $filepath;

        $is_file_exist = @file_get_contents($filepath);
        //echo $is_file_exist;
        if (empty($this->input->post())) {
            json_result('error', $this->upload->display_errors('', ''));
        } else {
            $config['upload_path'] = $path;
            $config['file_name'] = $file;
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', $this->upload->display_errors());
            } else {
                $image_data = $this->upload->data();
                $this->load->library('image_lib');
                $configx['maintain_ratio'] = FALSE;
                $configx['image_library'] = 'GD2';
                $configx['source_image'] = $image_data['full_path']; //get original image
                $configx['height'] = 500;
                $configx['y_axis'] = (($image_data['image_height'] / 2) - ($configx['height'] / 2));
                $this->image_lib->clear(); // added this line
                $this->image_lib->initialize($configx); // added this line
                if (!$this->image_lib->crop()) {
                    json_result('error', $this->image_lib->display_errors('', ''));
                } else {
                    $config_thumb = array(
                        'image_library' => 'GD2',
                        'source_image' => $image_data['full_path'],
                        'new_image' => $path . '/thumbs',
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'height' => 50
                    );
                    $this->image_lib->clear(); // added this line
                    $this->image_lib->initialize($config_thumb); // added this line
                    if (!$this->image_lib->resize()) {
                        json_result('error', $this->image_lib->display_errors('', ''));
                    } else {
                        json_result('success', 'Foto behasil diupload.');
                    }
                }
            }
        }
    }

    /*     * ******************* welcome ********************** */

    public function welcome() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('welc_title', 'Judul', 'required|alpha_numeric_spaces');
            $this->form_validation->set_rules('welc_subtitle', 'Sub Judul', 'required|custom');
            $this->form_validation->set_rules('welc_icon', 'Icon', 'required|custom');
            $this->form_validation->set_rules('welc_href', 'Link', 'custom');
            $this->form_validation->set_rules('welc_order', 'Order', 'numeric');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanWelcome($data)) {
                    json_result('success', 'Welcome berhasil disimpan.');
                } else {
                    json_result('error', 'Welcome gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-welcome');
        }
    }

    public function welcomeHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusWelcome($id)) {
            json_result('success', 'Welcome berhasil dihapus.');
        } else {
            json_result('error', 'Welcome gagal dihapus.');
        }
    }

    public function uploadWelcome($PNS_NIPBARU = NULL) {
        $path = 'frontend/img/contents';
        $file = 'welc_' . $this->input->post('id') . ".jpg";
        $filepath = base_url($path) . '/' . $file;
        //echo $filepath;

        $is_file_exist = @file_get_contents($filepath);
        //echo $is_file_exist;
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $config['upload_path'] = $path;
            $config['file_name'] = $file;
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', 'Foto gagal diupload');
            } else {
                welcomeRevision($this->input->post('id'));
                json_result('success', 'Foto behasil diupload.');
            }
        }
    }

    /*     * ******************* POST ********************** */

    public function post() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('post_title', 'Nama', 'required|custom');
            $this->form_validation->set_rules('post_type', 'tipe', 'required|numeric');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanPost($data)) {
                    json_result('success', 'Tautan berhasil disimpan.');
                } else {
                    json_result('error', 'Tautan gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-post');
        }
    }

    public function postHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusPost($id)) {
            json_result('success', 'Tautan berhasil dihapus.');
        } else {
            json_result('error', 'Tautan gagal dihapus.');
        }
    }

    public function uploadPost($PNS_NIPBARU = NULL) {
        $path = 'frontend/img/contents';
        postRevision($this->input->post('id'));
        $file = 'post_' . $this->input->post('id') . ".jpg";
        $filepath = base_url($path) . '/' . $file;

        $is_file_exist = @file_get_contents($filepath);
        //echo $is_file_exist;
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $config['upload_path'] = $path;
            $config['file_name'] = $file;
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', $this->upload->display_errors('', ''));
            } else {
                $image_data = $this->upload->data();
                $config_thumb = array(
                    'image_library' => 'GD2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => $path . '/thumbs',
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 150
                );
                $this->load->library('image_lib');
                $this->image_lib->clear(); // added this line
                $this->image_lib->initialize($config_thumb); // added this line
                if (!$this->image_lib->resize()) {
                    json_result('error', $this->image_lib->display_errors('', ''));
                } else {
                    json_result('success', 'Foto behasil diupload.');
                }
            }
        }
    }

    public function getJsonPost($id = NULL) {
        //echo $id;
        $this->load->model('M_back');
        $data = $this->M_back->jsonPost($id);
        echo json_encode($data);
    }

    /*     * ******************* HLMN ********************** */

    public function halaman() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('hlmn_title', 'Nama', 'required|custom');
            $this->form_validation->set_rules('hlmn_type', 'tipe', 'required|numeric');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanHalaman($data)) {
                    json_result('success', 'Tautan berhasil disimpan.');
                } else {
                    json_result('error', 'Tautan gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-halaman');
        }
    }

    public function halamanHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusHalaman($id)) {
            json_result('success', 'Tautan berhasil dihapus.');
        } else {
            json_result('error', 'Tautan gagal dihapus.');
        }
    }

    public function uploadHalaman($PNS_NIPBARU = NULL) {
        $path = 'frontend/img/contents';
        halamanRevision($this->input->post('id'));
        $file = 'hlmn_' . $this->input->post('id') . ".jpg";
        $filepath = base_url($path) . '/' . $file;

        $is_file_exist = @file_get_contents($filepath);
        //echo $is_file_exist;
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $config['upload_path'] = $path;
            $config['file_name'] = $file;
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', $this->upload->display_errors('', ''));
            } else {
                $image_data = $this->upload->data();
                $config_thumb = array(
                    'image_library' => 'GD2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => $path . '/thumbs',
                    'maintain_ratio' => TRUE,
                    'create_thumb' => TRUE,
                    'thumb_marker' => '_thumb',
                    'width' => 150
                );
                $this->load->library('image_lib');
                $this->image_lib->clear(); // added this line
                $this->image_lib->initialize($config_thumb); // added this line
                if (!$this->image_lib->resize()) {
                    json_result('error', $this->image_lib->display_errors('', ''));
                } else {
                    json_result('success', 'Foto behasil diupload.');
                }
            }
        }
    }

    public function getJsonHalaman($id = NULL) {
        //echo $id;
        $this->load->model('M_back');
        $data = $this->M_back->jsonHalaman($id);
        echo json_encode($data);
    }

    /*     * ******************* album ********************** */

    public function album() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('albm_nama', 'Judul', 'required|custom');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanAlbum($data)) {
                    json_result('success', 'Album berhasil disimpan.');
                } else {
                    json_result('error', 'Album gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-album');
        }
    }

    public function albumHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusAlbum($id)) {
            json_result('success', 'Album berhasil dihapus.');
        } else {
            json_result('error', 'Album gagal dihapus.');
        }
    }

    public function galeriAlbumHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusGaleriAlbum($id)) {
            json_result('success', 'Foto berhasil dihapus.');
        } else {
            json_result('error', 'Foto gagal dihapus.');
        }
    }

    public function uploadAlbum($PNS_NIPBARU = NULL) {
        $idAlbum = $this->input->post('id');
        $this->load->model('M_Back');

        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $this->load->library('upload');
            $dataInfo = array();
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            $path = 'frontend/img/galleries';
            $count = 0;
            $this->load->library('upload');
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                $idGaleri = ($idAlbum . '_-_' . round(microtime(true) * 100) );
                $config['upload_path'] = $path;
                $config['file_name'] = ($idGaleri . '.jpg');
                $config['allowed_types'] = 'jpg';
                $config['max_size'] = '0';
                $config['max_width'] = '0';
                $config['max_height'] = '0';

                $this->upload->initialize($config);
                if ($this->upload->do_upload()) {
                    $count++;
                    $dataInfo[] = $this->upload->data();
                    $data['glri_id'] = $idGaleri;
                    $data['glri_album'] = $idAlbum;
                    $this->M_Back->simpanGaleriAlbum($data);


                    $image_data = $this->upload->data();
                    $config_thumb = array(
                        'image_library' => 'GD2',
                        'source_image' => $image_data['full_path'],
                        'new_image' => $path . '/thumbs',
                        'maintain_ratio' => TRUE,
                        'create_thumb' => TRUE,
                        'thumb_marker' => '_thumb',
                        'height' => 150,
                        'width' => 150
                    );
                    $this->load->library('image_lib');
                    $this->image_lib->clear(); // added this line
                    $this->image_lib->initialize($config_thumb); // added this line
                    $this->image_lib->resize();
                }
            }
            if ($cpt != $count) {
                json_result('error', 'Foto gagal diupload.');
            } else {
                json_result('success', 'Foto behasil diupload.');
            }
        }
    }

    public function getJsonAlbum($id = NULL) {
        //echo $id;
        $this->load->model('M_back');
        $data = $this->M_back->jsonAlbum($id);
        echo json_encode($data);
    }

    /*     * ******************* unduhan ********************** */

    public function unduhan() {
        if ($data = $this->input->post()) {
            $this->form_validation->set_rules('undh_title', 'Nama Unduhan', 'required|alpha_numeric_spaces');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_Back');
                if ($this->M_Back->simpanUnduhan($data)) {
                    json_result('success', 'File berhasil disimpan.');
                } else {
                    json_result('error', 'File gagal disimpan.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            $this->load->view('front/b-unduhan');
        }
    }

    public function unduhanHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $ext = $data['ext'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusUnduhan($id, $ext)) {
            json_result('success', 'File berhasil dihapus.');
        } else {
            json_result('error', 'File gagal dihapus.');
        }
    }

    public function uploadUnduhan($PNS_NIPBARU = NULL) {
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $id = $this->input->post('id');
            $ext = $this->input->post('ext');
            $path = 'frontend/unduhan';
            $this->load->model('M_Back');
            $this->M_Back->hapusUnduhan($id, $ext, true);

            $config['file_name'] = ($id);
            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['overwrite'] = false;

            $this->load->library('upload', $config);
            $this->upload->overwrite = true;
            if (!$this->upload->do_upload()) {
                json_result('error', $this->upload->display_errors('', ''));
            } else {

                $this->db->set('undh_file', $_FILES['userfile']['name']);
                $this->db->set('undh_size', $this->upload->data('file_size') * 1024);
                $this->db->set('undh_type', $this->upload->data('file_ext'));
                $this->db->set('undh_uploaded', time('now'));
                $this->db->where('undh_id', $id);
                $this->db->update('front_unduhan');
                json_result('success', 'File berhasil diupload');
            }
        }
    }

    /*     * ******************* layanan ********************** */

    public function layanan() {
        if ($data = $this->input->post()) {
            $this->load->model('M_Back');
            if ($this->M_Back->simpanFrontPengaturan($data)) {
                json_result('success', 'Layanan berhasil disimpan.');
            } else {
                json_result('error', 'Layanan gagal disimpan.');
            };
        } else {

            $this->load->view('front/b-layanan');
        }
    }

    public function layananHapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_Back');
        if ($this->M_Back->hapusLayanan($id)) {
            json_result('success', 'Layanan berhasil dihapus.');
        } else {
            json_result('error', 'Layanan gagal dihapus.');
        }
    }

}
