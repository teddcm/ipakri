<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

    public function unduhan($id) {
        $this->load->helper('download');
        $this->db->where('undh_id', $id);
        $query = $this->db->get('front_unduhan');

        if ($query->num_rows() == 0) {
            return false;
        }

        $path = '';
        $file = '';

        foreach ($query->result_array() as $result) {

            $path .= FCPATH . 'frontend/unduhan/';
            $stored_file_name = $result['undh_id'] . $result['undh_type'];
            // Out puts just example "config.php"
            $original = $result['undh_file'];
        }

        force_download($original, file_get_contents($path . $stored_file_name));
    }

}
