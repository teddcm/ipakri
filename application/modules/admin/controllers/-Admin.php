<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->form_validation->set_error_delimiters('', '');
    }

    public function login() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {
                $this->load->model('M_admin');
                if ($this->M_admin->login($this->input->post('username'), $this->input->post('password'), $this->input->post('ip'))) {
                    json_result('success', 'Selamat datang.');
                } else {
                    json_result('error', 'Username atau password tidak sesuai.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else if (MatchID('d6edfd48787c3c97e35ab74ca12bf625')) {
            $this->load->view('admin/v-login');
        }
    }

    public function logout() {
        $this->load->view('admin/v-logout');
        $this->session->sess_destroy();
        redirect(base_url('admin/login'));
    }

    public function define_pupns() {
        $this->load->model('M_pegawai');
        $this->load->model('M_admin');
        $data['instansi'] = $this->M_pegawai->get_instansi();
        if ($this->input->post()) {
            $kode_unor = $this->input->post('kode_unor');
            $hasil = $this->M_admin->define_pupns($kode_unor);
            if ($hasil) {
                json_result('success', 'Berhasil menghapus PNS Non SKPD.');
            } else {
                json_result('error', 'Gagal menghapus PNS Non SKPD.');
            }
        } else {
            $this->load->view('admin/v-define_pupns', $data);
        }
    }

    public function duplicate_table_pupns() {
        if ($this->input->post()) {
            $kode_unor = $this->input->post('kode_unor');
            $duplicate = duplicate_table_pupns(INSTANSI_KERJA);
            if ($duplicate) {
                json_result('success', 'Berhasil menduplikasi tabel.');
            } else {
                json_result('error', 'Gagal menduplikasi tabel.');
            }
        }
    }

    public function backup() {
        $this->load->dbutil();

        $db_name = 'pdk_backup_' . date("Y-m-d H.i.s") . '_' . get_user_info('user_name');
        $prefs = array(
            'format' => 'zip',
            'filename' => $db_name . '.sql'
        );


        $backup = $this->dbutil->backup($prefs);
        $save = 'backupdb/' . $db_name . '.zip';

        $this->load->helper('file');
        write_file($save, $backup);

        echo "<a href='$save' class='btn btn-flat btn-danger btn-sm'>download backup</a>";
        //$this->load->helper('download');
        //force_download($db_name, $backup);
    }

}
