<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in()) {    
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->form_validation->set_error_delimiters('', '');
    }

    public function index() {
        show_404();
    }

    public function profil() {
        $this->load->view('admin/v-profil');
    }

    public function simpan_profil() {
        $data = $this->input->post();

        $this->form_validation->set_rules('user_full_name', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        if ($this->form_validation->run() == true) {

            if ($data['user_password'] == $data['user_password_ulang'] && $data['user_password'] != '') {
                unset($data['user_password_ulang']);
                $data['user_password'] = $this->bcrypt->hash(get_user_info('user_name') . $data['user_password']);
            } else if ($data['user_password'] != $data['user_password_ulang']) {
                json_result('warn', 'Password tidak sesuai.');
                exit();
            } else {
                unset($data['user_password']);
                unset($data['user_password_ulang']);
            }
            unset($data['user_name']);
            unset($data['level_name']);
            $data['user_name'] = get_user_info('user_name');
            $data['mode'] = 'edit';
            $this->load->model('M_admin');
            $result = $this->M_admin->simpan_user($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function manajemen_user() {
        $this->load->view('admin/v-user_manajemen');
    }

    public function manajemen_level() {
        $this->load->view('admin/v-level_manajemen');
    }

    public function manajemen_unor() {
        $this->load->view('admin/v-unor_manajemen');
    }

    /*public function user_ajax_view() {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'view_admin_user';
        $primaryKey = 'user_id';
        $columns = array(
            array(
                'db' => 'user_id',
                'dt' => 'user_id',
                'formatter' => function( $d ) {
                    return safe_encode($d);
                }),
            array('db' => 'user_name', 'dt' => 'user_name'),
            array('db' => 'user_full_name', 'dt' => 'user_full_name'),
            array('db' => 'user_email', 'dt' => 'user_email'),
            array('db' => 'level_name', 'dt' => 'level_name'),
            array('db' => 'level_id', 'dt' => 'level_id'),
        );
        $where = '';
        if (get_user_info('user_name') != 'teddcm') {
            $where .= ' user_name<>"teddcm"';
        }
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }
*/
    public function level_ajax_view() {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'admin_level';
        $primaryKey = 'level_id';
        $columns = array(
            array(
                'db' => 'level_id',
                'dt' => 'level_id',
                'formatter' => function( $d ) {
                    return safe_encode($d);
                }),
            array('db' => 'level_name', 'dt' => 'level_name'),
            array('db' => 'level_id', 'dt' => 'lv'),
        );
        $where = '';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function unor_ajax_view() {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'kanreg8_unor';
        $primaryKey = 'UNO_ID';
        $columns = array(
            array(
                'db' => 'UNO_ID',
                'dt' => 'UNO_ID',
                'formatter' => function( $d ) {
                    return safe_encode($d);
                }),
            array('db' => 'UNO_NAMUNO', 'dt' => 'UNO_NAMUNO'),
            array('db' => 'UNO_KODUNO', 'dt' => 'UNO_KODUNO'),
            array('db' => 'UNO_NAMJAB', 'dt' => 'UNO_NAMJAB'),
            array(
                'db' => 'UNO_DIATASAN_ID',
                'dt' => 'NAMATASAN',
                'formatter' => function( $d ) {
                    return convert_unor($d);
                }),
            array('db' => 'UNO_DIATASAN_ID', 'dt' => 'UNO_DIATASAN_ID'),
            array('db' => 'UNO_KODESL', 'dt' => 'UNO_KODESL'),
            array(
                'db' => 'UNO_KODESL',
                'dt' => 'ESL',
                'formatter' => function( $d ) {
                    return convert_eselon($d);
                }),
        );
        $where = 'UNO_NAMUNO NOT LIKE "%###DELETED###%"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function tambah_user() {
        if (is_admin()) {
            $this->load->model('M_admin');
            $data['level'] = $this->M_admin->get_level();
            $this->load->view('admin/v-user_select', $data);
        }
    }

    public function tambah_level() {
        if (is_admin()) {
            $this->load->model('M_admin');
            $this->load->view('admin/v-level_select');
        }
    }

    public function tambah_unor() {
        if (is_admin()) {
            $this->load->model('M_pegawai');
            $data['level'] = $this->M_pegawai->get_eselon();
            $this->load->view('admin/v-unor_select', $data);
        }
    }

    public function simpan_user() {
        $data = $this->input->post();
        if ($data['mode'] == 'tambah') {
            $this->form_validation->set_rules('user_name', 'Username', 'required|is_unique[admin_user.user_name]');
            $this->form_validation->set_rules('user_password', 'Password', 'required|matches[user_password_ulang]|min_length[6]|max_length[20]');
            $this->form_validation->set_rules('user_password_ulang', 'Password Confirmation', 'required');
        } else {
            if (($data['user_password'] != '' || $data['user_password_ulang'] != '')) {
                $this->form_validation->set_rules('user_password', 'Password', 'required|matches[user_password_ulang]|min_length[6]|max_length[20]');
                $this->form_validation->set_rules('user_password_ulang', 'Password Confirmation', 'required');
            }
        }
        $this->form_validation->set_rules('user_full_name', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('level_id', 'Level', 'required');
        if ($this->form_validation->run() == true) {
            if (!isset($data['IDUNO'])) {
                $data['IDUNO'] = '';
            }
            $this->load->model('M_admin');
            if ($data['user_password'] != '' && $data['user_password_ulang'] != '') {
                unset($data['user_password_ulang']);
                $data['user_password'] = $this->bcrypt->hash($data['user_name'] . $data['user_password']);
            } else {
                unset($data['user_password']);
                unset($data['user_password_ulang']);
            }
            $data['level_id'] = safe_decode($this->input->post('level_id'));
            $result = $this->M_admin->simpan_user($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function simpan_level() {
        $data = $this->input->post();
        if ($data['mode'] == 'tambah') {
            $this->form_validation->set_rules('level_name', 'Nama Level', 'required|is_unique[admin_level.level_name]');
            unset($data['level_id']);
        } else {
            $level_name_old = $data['level_name_old'];
            unset($data['level_name_old']);
            if ($level_name_old != $data['level_name']) {
                $this->form_validation->set_rules('level_name', 'Nama Level', 'required|is_unique[admin_level.level_name]');
            } else {
                $this->form_validation->set_rules('level_name', 'Nama Level', 'required');
            }
            $data['level_id'] = safe_decode($data['level_id']);
        }
        if ($this->form_validation->run() == true) {
            $this->load->model('M_admin');
            $result = $this->M_admin->simpan_level($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function simpan_unor() {
        $data = $this->input->post();
        $this->load->model('M_admin');
        $this->form_validation->set_rules('UNO_NAMUNO', 'Nama Unor', 'required');
        $this->form_validation->set_rules('UNO_NAMJAB', 'Nama Jabatan Unor', 'required');
        //$this->form_validation->set_rules('UNO_DIATASAN_ID', 'Unor Atasan', 'required');
        $this->form_validation->set_rules('UNO_KODESL', 'Eselon', 'required');
        $this->form_validation->set_rules('UNO_KODUNO', 'Kode Unor', 'required|min_length[10]|max_length[10]');
        if ($this->form_validation->run() == true) {

            if (convert_unor($data['UNO_DIATASAN_ID'], 'UNO_KODESL') >= $data['UNO_KODESL']) {
                json_result('warn', 'Level eselon harus lebih rendah daripada unor atasan');
            } else if ($data['mode'] == 'edit' && $this->M_admin->validasi_status_unor($data['UNO_ID'])['max_kodesl'] <= $data['UNO_KODESL']) {
                json_result('warn', 'Level eselon harus lebih tinggi daripada unor bawahan');
            } else {
                $result = $this->M_admin->simpan_unor($data);
                if ($result) {
                    json_result('success', 'Data berhasil disimpan.');
                } else {
                    json_result('error', 'Data gagal disimpan.');
                }
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function edit_user($id) {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($id);
            $this->load->model('M_admin');
            $data['level'] = $this->M_admin->get_level();
            $data['user'] = $this->M_admin->get_data_user($id);
            $this->load->view('admin/v-user_select', $data);
        }
    }

    public function edit_level($id) {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($id);
            $this->load->model('M_admin');
            $data['level'] = $this->M_admin->get_data_level($id);
            $this->load->view('admin/v-level_select', $data);
        }
    }

    public function edit_unor($id) {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($id);
            $this->load->model('M_pegawai');
            $data['level'] = $this->M_pegawai->get_eselon();
            $this->load->model('M_admin');
            $data['unor'] = $this->M_admin->get_data_unor($id);
            $this->load->view('admin/v-unor_select', $data);
        }
    }

    public function hapus_user() {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($data['id']);
            $this->load->model('M_admin');
            if ($this->M_admin->hapus_user($id)) {
                json_result('success', 'Data berhasil dihapus.');
            } else {
                json_result('error', 'Data gagal dihapus.');
            };
        } else {
            json_result('error', 'Anda tidak memiliki hak untuk menghapus.');
        }
    }

    public function hapus_level() {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($data['id']);
            $this->load->model('M_admin');
            if ($this->M_admin->hapus_level($id)) {
                json_result('success', 'Data berhasil dihapus.');
            } else {
                json_result('error', 'Data gagal dihapus.');
            };
        } else {
            json_result('error', 'Anda tidak memiliki hak untuk menghapus.');
        }
    }

    public function hapus_unor() {
        if (is_admin()) {
            $data = $this->input->post();
            $id = safe_decode($data['id']);
            $this->load->model('M_admin');
            if ($this->M_admin->validasi_status_unor($id)['num_rows'] > 0) {
                json_result('error', 'Masih memiliki unor bawahan.');
            } else {
                if ($this->M_admin->hapus_unor($id)) {
                    json_result('success', 'Data berhasil dihapus.');
                } else {
                    json_result('error', 'Data gagal dihapus.');
                }
            }
        } else {
            json_result('error', 'Anda tidak memiliki hak untuk menghapus.');
        }
    }

}
