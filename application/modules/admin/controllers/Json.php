<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    
    public function index($PNS_NIPBARU = NULL) {
      
        echo 'ok';
    }
    public function json_data_pegawai($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data = $this->M_pegawai->get_data_pegawai($PNS_NIPBARU);
        $data->PNS_TEMLHR_TXT = convert_tempat_lahir($data->PNS_TEMLHR);
        $data->PNS_TGLLHRDT = reverseDate($data->PNS_TGLLHRDT);
        echo json_encode($data);
    }

    public function json_foto_pegawai($PNS_NIPBARU = NULL) {
        header("Cache-Control: no-cache, must-revalidate");
        echo get_foto($PNS_NIPBARU);
    }

    public function json_data_old_kgb($id_now) {
        $this->load->model('M_pegawai');
        if ($data = $this->M_pegawai->get_kgb_sk_lama($id_now)) {
            $data->RWKGB_MKGBARU = str_pad($data->RWKGB_MKGBARU, 4, '0', STR_PAD_LEFT);
            $data->RWKGB_TGLSKBARU = @reverseDate($data->RWKGB_TGLSKBARU);
            $data->RWKGB_TMTSKBARU = @reverseDate($data->RWKGB_TMTSKBARU);
            echo json_encode($data);
        } else {
            json_result('warn', "Tidak ditemukan data SK terakhir dari pegawai ini.\nSilakan input data manual.");
        }
    }

    public function json_pegawai() {
        $this->output->set_content_type('application/json');
        $this->load->model('M_pegawai');
        $hasil = $this->M_pegawai->json_pegawai($this->input->get('q'));
//echo $this->input->get('q');
    }

    public function json_unor() {
        $this->output->set_content_type('application/json');
        $this->load->model('M_pegawai');
        $hasil = $this->M_pegawai->json_unor($this->input->get('q'));
//echo $this->input->get('q');
    }

    public function json_jabfun() {
        $this->output->set_content_type('application/json');
        $this->load->model('M_pegawai');
        $hasil = $this->M_pegawai->json_jabfun($this->input->get('q'));
//echo $this->input->get('q');
    }

    public function json_pendidikan() {
        $this->output->set_content_type('application/json');
        $this->load->model('M_pegawai');
        $hasil = $this->M_pegawai->json_pendidikan($this->input->get('q'));
//echo $this->input->get('q');
    }

    public function json_temlahir($wil_tk) {
        $this->output->set_content_type('application/json');
        $this->load->model('M_pegawai');
        $hasil = $this->M_pegawai->json_temlahir($this->input->get('q'), $wil_tk);
//echo $this->input->get('q');
    }

    public function unor_jqtree() {
        $node = $this->input->get('node');
        if (!isset($node)) {
            $sql = "SELECT COUNT(c.UNO_ID) as endChild,a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
            $sql .= "FROM kanreg8_unor a  ";
            $sql .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
            $sql .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
            $sql .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
            $sql .= "AND b.UNO_ID IS NULL ";
            $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
            $sql .= "GROUP BY a.UNO_ID ";
            $sql .= "ORDER BY a.UNO_KODUNO ";
        } else {
            $sql = "SELECT COUNT(c.UNO_ID) as endChild,a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
            $sql .= "FROM kanreg8_unor a  ";
            $sql .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
            $sql .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
            $sql .= "AND a.UNO_DIATASAN_ID='" . $node . "' ";
            $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
            $sql .= "GROUP BY a.UNO_ID ";
            $sql .= "ORDER BY a.UNO_KODUNO ";
        }
        $query = $this->db->query($sql);
//echo $this->db->last_query();
        $groups = array();
        $return = array();
        foreach ($query->result_array() as $result) {
            $return[] = $result;
        }
        echo json_encode(create_unor_jqtree($return, $node));
    }

    public function pegawai_jqtree($NIP = NULL) {
        $sql = ""
                . "SELECT m.endChild,o.RWJAB_NAMAJAB,n.PNS_NIPBARU,n.PNS_PNSNAM,IF(m.UNO_ID IS NULL,n.PNS_NIPBARU,m.UNO_ID) AS UNO_ID,IF(m.UNO_ID IS NULL,n.PNS_UNOR,m.UNO_DIATASAN_ID) AS UNO_DIATASAN_ID, n.PNS_UNOR,o.RWJAB_NAMUNO "
                . "FROM kanreg8_pupns n "
                . "LEFT JOIN ("
                . "SELECT x.PNS_NIPBARU,x.PNS_PNSNAM as pejabat, x.xpejabat,  COUNT(c.UNO_ID)+1 as endChild,a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL "
                . "FROM kanreg8_unor a "
                . "LEFT JOIN (SELECT  z.PNS_UNOR,GROUP_CONCAT(DISTINCT z.PNS_PNSNAM SEPARATOR ', ') as PNS_PNSNAM, z.PNS_NIPBARU,COUNT(z.PNS_NIPBARU)AS xpejabat FROM kanreg8_pupns z WHERE z.PNS_JNSJAB=1 AND z.PNS_KEDHUK<20 GROUP BY z.PNS_UNOR) x ON x.PNS_UNOR=a.UNO_ID "
                . "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID "
                . "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID "
                . "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' "
                . "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' "
                . "GROUP BY a.UNO_ID "
                . "ORDER BY a.UNO_KODUNO "
                . ") m ON n.PNS_NIPBARU=m.PNS_NIPBARU "
                . "LEFT JOIN (SELECT * FROM z_datarwjabatan GROUP BY RWJAB_NIP ORDER BY RWJAB_TMTJAB DESC ) o ON n.PNS_NIPBARU=o.RWJAB_NIP "
                . "WHERE n.PNS_KEDHUK<20 "
                . "ORDER BY m.endChild";
        $query = $this->db->query($sql);
//echo $this->db->last_query();
        $groups = array();
        $return = array();
        foreach ($query->result_array() as $result) {
            $return[] = $result;
        }
        $jqtree = create_pegawai_jqtree($return);


        $return2 = array();
        $temp = array();
        $sql2 = ""
                . "SELECT * FROM kanreg8_pupns WHERE PNS_NIPBARU NOT IN ('" . implode("','", $jqtree[1]) . "') "
                . "AND PNS_KEDHUK<20 "
                . "";
        $query2 = $this->db->query($sql2);
//        echo $this->db->last_query();
        foreach ($query2->result() as $result2) {
            $temp['nip'] = $result2->PNS_NIPBARU;
            $temp['nama'] = $result2->PNS_PNSNAM;
            $return2[] = $temp;
        }

//     print_r($jqtree[0]);
//        print_r($return2);
        echo json_encode(array($jqtree[0], $return2));
    }
 
    public function unor2_jqtree() {
        $sql = "SELECT x.PNS_PNSNAM as pejabat, x.xpejabat,COUNT(DISTINCT(c.UNO_ID)) + COUNT(DISTINCT(d.UNO_ID)) + COUNT(DISTINCT(e.UNO_ID)) + COUNT(DISTINCT(f.UNO_ID)) + COUNT(DISTINCT(g.UNO_ID)) AS endChild,a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
        $sql .= "FROM kanreg8_unor a  ";
        $sql .= "LEFT JOIN (SELECT  z.PNS_UNOR,GROUP_CONCAT(DISTINCT z.PNS_PNSNAM SEPARATOR ' / ') as PNS_PNSNAM ,COUNT(z.PNS_NIPBARU)AS xpejabat FROM kanreg8_pupns z WHERE z.PNS_JNSJAB=1 AND z.PNS_KEDHUK<20 GROUP BY z.PNS_UNOR) x ON x.PNS_UNOR=a.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor d ON d.UNO_DIATASAN_ID=c.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor e ON e.UNO_DIATASAN_ID=d.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor f ON f.UNO_DIATASAN_ID=e.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor g ON g.UNO_DIATASAN_ID=f.UNO_ID ";
        $sql .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
//            $sql .= "AND b.UNO_ID IS NULL ";
        $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
        $sql .= "GROUP BY a.UNO_ID ";
        $sql .= "ORDER BY a.UNO_KODUNO ";
        $query = $this->db->query($sql);
//echo $this->db->last_query();
        $groups = array();
        $return = array();
        foreach ($query->result_array() as $result) {
            $return[] = $result;
        }
        $jqtree = create_unor2_jqtree($return);

        $return2 = array();
        $temp = array();
        $sql2 = "SELECT a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
        $sql2 .= "FROM kanreg8_unor a  ";
        $sql2 .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
        $sql2 .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
        $sql2 .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
        $sql2 .= "AND a.UNO_ID NOT IN ('" . implode("','", $jqtree[1]) . "') ";
        $sql2 .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
        $sql2 .= "GROUP BY a.UNO_ID ";
        $sql2 .= "ORDER BY a.UNO_KODUNO ";
        $query2 = $this->db->query($sql2);
//        echo $this->db->last_query();
        foreach ($query2->result_array() as $result2) {
//            $temp['pejabat'] = $result2['pejabat'];
//            $temp['xpejabat'] = $result2['xpejabat'];
            $temp['name'] = $result2['UNO_NAMUNO'];
            $temp['id'] = $result2['UNO_ID'];
            $temp['jabatan'] = $result2['UNO_NAMJAB'];
            $temp['eselon'] = $result2['UNO_KODESL'];
            $temp['kode'] = $result2['UNO_KODUNO'];
            $temp['atasan'] = $result2['UNO_DIATASAN_ID'];
            $return2[] = $temp;
        }
        echo json_encode(array($jqtree[0], $return2));
    }

}
