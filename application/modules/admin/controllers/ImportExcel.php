<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//load Spout Library
require_once APPPATH . '/third_party/Spout/src/spout/Autoloader/autoload.php';

//lets Use the Spout Namespaces
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class ImportExcel extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('M_importExcel');
        $this->load->model('M_pegawai');
    }

    public function index() {
        $data = array(); // Buat variabel $data sebagai array

        if ($this->input->post()) { // Jika user menekan tombol Preview pada form
//            set_time_limit(600);
            $filename = $this->input->post('value');
            $upload = $this->M_importExcel->upload_file($filename);
            if ($upload['result'] == "success") { // Jika proses upload sukses
                $data = $upload['file'];

                $result = $this->M_importExcel->simpanData($filename);
                if (is_integer($result)) {
                    $simpandetail['id'] = $filename;
                    $simpandetail['IMP_LASTDATE'] = date("Y-m-d H:i:s");
                    $simpandetail['IMP_NUMROWS'] = $result;
                    $simpandetail['IMP_UPLOADED'] = 1;
                    $simpandetail['IMP_IMPORTED'] = 0;
                    $simpandetail['IMP_VALIDATED'] = 0;
                    $simpandetail['IMP_SIZE'] = $data['file_size'];
                    $simpandetail['IMP_USER'] = $this->session->userdata('identity');
                    $this->M_pegawai->simpan_apapun($simpandetail, 'q_detail_import', 'IMP_NAME');

                    json_result('success', 'Data berhasil diupload | Total Row: ' . $result . ' | Peak Memory: ' . (memory_get_peak_usage(true) / 1024 / 1024) . " MB", $data['file_name']);
                } else {
                    json_result('error', $result);
                }
            } else { // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
                json_result('error', $upload['error']);
            }
        }
        $data['detail'] = $this->M_pegawai->view_apapun('q_detail_import');
        $this->load->view('admin/importExcel/upload', $data);
    }

    public function uploadFile() {
        $data = array(); // Buat variabel $data sebagai array

        if ($this->input->post()) { // Jika user menekan tombol Preview pada form
            $filename = $this->input->post('value');
            $upload = $this->M_importExcel->upload_file($filename);
            if ($upload['result'] == "success") { // Jika proses upload sukses
                $data = $upload['file'];
                $simpandetail['id'] = $filename;
                $simpandetail['IMP_LASTDATE'] = date("Y-m-d H:i:s");
                $simpandetail['IMP_UPLOADED'] = 1;
                $simpandetail['IMP_IMPORTED'] = 0;
                $simpandetail['IMP_VALIDATED'] = 0;
                $this->M_pegawai->simpan_apapun($simpandetail, 'q_detail_import', 'IMP_NAME');
                json_result('success', 'Data berhasil diupload | Peak Memory: ' . (memory_get_peak_usage(true) / 1024 / 1024) . " MB", $data['file_name']);
            } else { // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
                json_result('error', $upload['error']);
            }
        }
        $data['detail'] = $this->M_pegawai->view_apapun('q_detail_import');
        $this->load->view('admin/importExcel/upload', $data);
    }

    public function readExcelFile() {

        try {
            //Lokasi file excel       
            $file_path = "./assets/excels/dataPns.xlsx";
            $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
            $reader->open($file_path); //open the file          

            $i = 0;

            /**
             * Sheets Iterator. Kali aja multiple sheets                  
             * */
            foreach ($reader->getSheetIterator() as $sheet) {

                //Rows iterator                
                foreach ($sheet->getRowIterator() as $row) {

                    print_r($row);
                    echo " " . $i . "<br>";

                    ++$i;
                }
            }

            echo "Total Rows : " . $i;
            $reader->close();


            echo "Peak memory:", (memory_get_peak_usage(true) / 1024 / 1024), " MB";
        } catch (Exception $e) {

            echo $e->getMessage();
            exit;
        }
    }

}
