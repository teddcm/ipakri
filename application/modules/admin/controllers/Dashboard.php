<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in()) {
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->form_validation->set_error_delimiters('', '');
    }

    public function index() {
        $this->utama();
    }

    public function utama() {
        $this->load->model('M_dashboard');
        $data['jumPeg'] = $this->M_dashboard->getJumPeg();
        $data['jumPegStruktural'] = $this->M_dashboard->getJumPegStruktural();
        $data['jumPegJFT'] = $this->M_dashboard->getJumPegJFT();
        $data['jumPegJFU'] = $this->M_dashboard->getJumPegJFU();
        $data['jumPegPria'] = $this->M_dashboard->getJumPegPria();
        $data['jumPegWanita'] = $this->M_dashboard->getJumPegWanita();
        $data['sebaranJenJab'] = $this->M_dashboard->getSebaranJenJab();
        $data['sebaranGolru'] = $this->M_dashboard->getSebaranGolru();
        $data['sebaranPendidikan'] = $this->M_dashboard->getSebaranPendidikan();
        $data['sebaranTMT'] = $this->M_dashboard->getSebaranTMT();
        $data['getKGB'] = $this->M_dashboard->getKGB();
        $this->load->view('dashboard/v-utama', $data);
    }

}
