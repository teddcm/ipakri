<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->form_validation->set_error_delimiters('', '');
    }

    public function index() {
        $this->load->view('pengaturan/p-index');
    }

    public function utama() {
        if ($data = $this->input->post()) {
            $this->load->model('M_pengaturan');
            if ($this->M_pengaturan->simpanPengaturan($data)) {
                json_result('success', 'Pengaturan berhasil disimpan.');
            } else {
                json_result('error', 'Pengaturan gagal disimpan.');
            };
        } else {
            $this->load->view('pengaturan/p-utama');
        }
    }

    public function uploadLogo() {
        if (empty($this->input->post())) {
            json_result('error', 'Foto gagal diupload');
        } else {
            $this->load->library('upload');
            $files = $_FILES;
            $path = 'contents/img';
            $this->load->library('upload');
            $_FILES['userfile']['name'] = $files['userfile']['name'];
            $_FILES['userfile']['type'] = $files['userfile']['type'];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'];
            $_FILES['userfile']['error'] = $files['userfile']['error'];
            $_FILES['userfile']['size'] = $files['userfile']['size'];

            $idGaleri = 'logo_sk';

            $temp = explode("/", $_FILES["userfile"]["type"]);
            $config['upload_path'] = $path;
            $config['file_name'] = ($idGaleri . '.' . $temp[1]);
            $config['allowed_types'] = '*';
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) {
                //var_dump(is_dir($path));
                //echo $config['upload_path'].$config['file_name'];
                //echo $this->upload->display_errors();
                json_result('error', 'Foto gagal diupload.');
            } else {
                $_SESSION['logo_rand'] = rand(0, 9999);
                savePengaturan('utama_logo_sk', $config['file_name']);
                json_result('success', 'Foto behasil diupload.');
            }
        }
    }

    public function kgb() {
        if ($data = $this->input->post()) {
            $this->load->model('M_pengaturan');
            if ($this->M_pengaturan->simpanPengaturan($data)) {
                json_result('success', 'Pengaturan berhasil disimpan.');
            } else {
                json_result('error', 'Pengaturan gagal disimpan.');
            };
        } else {
            $this->load->view('pengaturan/p-kgb');
        }
    }

    public function kp() {
        if ($data = $this->input->post()) {
            $this->load->model('M_pengaturan');
            if ($this->M_pengaturan->simpanPengaturan($data)) {
                json_result('success', 'Pengaturan berhasil disimpan.');
            } else {
                json_result('error', 'Pengaturan gagal disimpan.');
            };
        } else {
            $this->load->view('pengaturan/p-kp');
        }
    }

    public function cuti() {
        if ($data = $this->input->post()) {
            $this->load->model('M_pengaturan');
            if (($this->input->post() != NULL)) {
                if ($this->M_pengaturan->atur_cuti((object) $this->input->post())) {
                    json_result('success', 'Pengaturan berhasil disimpan.');
                } else {
                    json_result('error', 'Pengaturan gagal disimpan.');
                }
            }
        } else {
            $this->load->view('pengaturan/p-cuti');
        }
    }

    public function spesimen($page = NULL) {
        if ($page == 'ajax') {
            $this->load->library('datatables_ssp'); //panggil library datatables
            $sql_details = array(
                'user' => $this->db->username,
                'pass' => $this->db->password,'port' => $this->db->port,
                'db' => $this->db->database,
                'host' => $this->db->hostname
            );
            $table = 'q_spesimen';
            $primaryKey = 'SPES_ID';
            $columns = array(
                array('db' => 'SPES_ID', 'dt' => 'SPES_ID'),
                array('db' => 'SPES_NIPBARU', 'dt' => 'SPES_NIPBARU'),
                array('db' => 'SPES_PNSNAM', 'dt' => 'SPES_PNSNAM'),
                array('db' => 'SPES_NAMAJAB', 'dt' => 'SPES_NAMAJAB'),
                array('db' => 'SPES_FORMAT', 'dt' => 'SPES_FORMAT'),
            );
            $where = '';
            echo json_encode(
                    Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
            );
        } elseif ($page == 'simpan') {
            $this->form_validation->set_rules('SPES_PNSNAM', 'Nama Spesimen', 'required|trim|custom');
            $this->form_validation->set_rules('SPES_NIPBARU', 'NIP', 'required|min_length[18]|max_length[18]|numeric|trim');
            $this->form_validation->set_rules('SPES_NAMAJAB', 'Nama Jabatan', 'required|trim|alpha_numeric_spaces');
            $this->form_validation->set_rules('SPES_FORMAT', 'Format Spesimen', 'required');

            if ($this->form_validation->run() == true) {
                $this->load->model('M_pegawai');
                $data = $this->input->post();
                if ($this->M_pegawai->simpan_apapun($data, 'q_spesimen', 'SPES_ID')) {
                    json_result('success', 'Data berhasil disimpan');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } elseif ($page == 'hapus') {
            $data = $this->input->post();
            $id = $data['id'];
            $this->load->model('M_pegawai');
            if ($this->M_pegawai->hapus_apapun($id, 'q_spesimen', 'SPES_ID')) {
                json_result('success', 'Data berhasil dihapus.');
            } else {
                json_result('error', 'Data gagal dihapus.');
            }
        } else {
            $this->load->view('pengaturan/p-spesimen');
        }
    }

    public function backup() {
        if ($data = $this->input->post()) {
            if ($this->input->post('post') == 'backup') {
                $this->load->dbutil();
                $prefs = array(
                    'tables' => array("admin_pengaturan", "auth_groups", "auth_users", "auth_users_groups", "front_album", "front_cuti_temp", "front_galeri", "front_halaman", "front_linker", "front_pengaturan", "front_peraturan", "front_post", "front_slider", "front_welcome", "kanreg8_agama", "kanreg8_eselon", "kanreg8_gaji_pokok", "kanreg8_golru", "kanreg8_instansi", "kanreg8_jabfun", "kanreg8_jenis_id_dokumen", "kanreg8_jenis_kp", "kanreg8_jenis_pengadaan", "kanreg8_jenis_pensiun", "kanreg8_jenjab", "kanreg8_jenkaw", "kanreg8_jenpeg", "kanreg8_kedhuk", "kanreg8_lokker", "kanreg8_pendik", "kanreg8_pupns", "kanreg8_pupns_pendidikan", "kanreg8_tktpendik", "kanreg8_unor", "q_angkakredit", "q_datarwanak", "q_datarwangkakredit", "q_datarwcuti", "q_datarwcutinew", "q_datarwskp", "q_diklat", "q_gapok", "q_jatah_cuti", "q_jenis_cuti", "q_penangguhan_cuti", "q_spesimen", "q_status_anak", "q_status_kawin", "q_unor_prosedur", "wil_tk1", "wil_tk2", "wil_tk3", "z_datarwdiklat", "z_datarwgolongan", "z_datarwissu", "z_datarwjabatan", "z_datarwkgb", "z_datarwkursus", "z_datarwpendidikan", "z_datarwpmk", "z_datarwpnsunor"), // Array of tables to backup.
                    'ignore' => array(), // List of tables to omit from the backup
                    'format' => 'zip',
                    'filename' => 'my_db_backup.sql',
                    'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
                    'add_insert' => TRUE, // Whether to add INSERT data to backup file
                    'newline' => "\n"// Newline character used in backup file
                );

                $backup = $this->dbutil->backup($prefs);
                if ($backup) {
                    $this->load->helper('file');
                    $db_name = 'backup_' . date("Y-m-d_H-i-s") . '.zip';
                    $save = 'backups/' . $db_name;
                    write_file($save, $backup);
                    json_result('success', 'Data berhasil dibackup.');
                } else {
                    json_result('error', 'Data gagal dibackup.');
                }
            } else {
                $path = (FCPATH.'backups' . DIRECTORY_SEPARATOR . $this->input->post('namefile'));
                if (unlink($path)) {
                    json_result('success', 'Data berhasil dihapus');
                } else {
                    json_result('error', 'Data gagal dihapus');
                }
            }
        } else {
            $files = scandir('backups');
            $results = array();
            foreach ($files as $key => $value) {
                $path = ('backups' . DIRECTORY_SEPARATOR . $value);
                if (!is_dir($path)) {
                    $results[] = ['name' => $value, 'size' => filesize($path)];
                }
            }
            $data['filelist'] = $results;
            $this->load->view('pengaturan/p-backup', $data);
        }
    }

}
