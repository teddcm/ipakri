<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        if (!$this->ion_auth->logged_in()) {
//            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
//            http_response_code(401);
//            exit();
//        }
    }

    public function index() {
        $this->load->view('admin/report/v-report');
    }

    public function duk($FULLDATA = FALSE, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getDukNew($FULLDATA, $UNOR));
        $this->load->view('admin/report/v-report#duk', $data);
    }

    public function nominatif($FULLDATA = FALSE, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getNominatifNew($FULLDATA, $UNOR));
        $this->load->view('admin/report/v-report#nominatif', $data);
    }

    public function jabatan($JNSJAB = FALSE, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getJabatanNew($JNSJAB, $UNOR));
        $this->load->view('admin/report/v-report#jabatan', $data);
    }

    public function kgb($X = NULL, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['data'] = ($this->M_report->getkgb($UNOR, $NIP = NULL));
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $this->load->view('admin/report/v-report#kgb', $data);
    }

    public function cuti($TAHUN = FALSE, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getCuti($TAHUN, $UNOR));
        $this->load->view('admin/report/v-report#cuti', $data);
    }

    public function pensiun($TAHUN = FALSE, $UNOR = NULL) {
        $this->load->model('M_report');
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getPensiun($TAHUN, $UNOR));
        $this->load->view('admin/report/v-report#pensiun', $data);
    }

    public function kp($x = NULL, $UNOR = NULL) {
        $this->load->model('M_report');
        $this->load->model('M_pegawai');
        $data['golru'] = $this->M_pegawai->get_golru();
        $data['jumPeg'] = $this->M_report->getJumPeg();
        $data['data'] = ($this->M_report->getKp($UNOR));
        $this->load->view('admin/report/v-report#kp', $data);
    }

    public function getkgb($LIMIT, $OFFSET) {
        $this->load->model('M_report');
        echo json_encode($this->M_report->getkgb($LIMIT, $OFFSET));
    }

    public function kgb_sk_cetak() {
        $id = $this->input->get('RWKGB_ID');
        $this->load->model('M_pegawai');
        $data['kgb'] = $this->M_pegawai->get_kgb_sk($id);
        $data['kepada'] = $this->input->get('kepada');
        $data['spesimen'] = getSpesimen($this->input->get('spesimen'));
        $data['tembusan'] = $this->input->get('tembusan');
        $this->M_pegawai->update_kgb_spesimen($id, getSpesimen($this->input->get('spesimen'), 'SPES_NAMAJAB'));
        if ($data['kgb']->RWKGB_NOSKBARU == NULL) {
            $no_surat = $this->M_pegawai->update_kgb_nosurat($id);
            $data['kgb']->RWKGB_NOSKBARU = $no_surat;
        }
        $this->load->view('admin/cetak/fpdf-kgb', $data);
    }

    public function profil_cetak() {
        $NIP = $this->input->get('NIP');
        $this->load->model('M_pegawai');
        $data['NIP'] = $NIP;
        $data['pegawai'] = $this->M_pegawai->get_data_pegawai($NIP);
        $data['golongan'] = $this->M_pegawai->riwayat_golongan($NIP);
        $data['pendidikan'] = $this->M_pegawai->riwayat_pendidikan($NIP);
        $data['jabatan'] = $this->M_pegawai->riwayat_jabatan($NIP);
        $data['diklat'] = $this->M_pegawai->riwayat_diklat($NIP);
        $data['kursus'] = $this->M_pegawai->riwayat_kursus($NIP);
        $data['angka_kredit'] = $this->M_pegawai->riwayat_angka_kredit($NIP);
        $data['skp'] = $this->M_pegawai->riwayat_skp($NIP);
        $data['issu'] = $this->M_pegawai->riwayat_keluarga_issu($NIP);
        $data['anak'] = $this->M_pegawai->riwayat_keluarga_anak($NIP);
        $data['barcode'] = base_url("frontpage/barcode/" . $NIP);

        $this->load->view('admin/cetak/fpdf-profil', $data);
    }

}
