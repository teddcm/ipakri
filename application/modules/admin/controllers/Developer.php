<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo UniqueMachineID();
        //print_r($_SERVER);
    }

    public function declare_all($NIP = NULL) {
        if (!$this->input->post()) {
            echo 'Menu ini akan melakukan deklarasi nilai awal untuk menu Golongan, Jabatan, Pendidikan, Diklat Struktural, dan Unit Organisasi';
            echo '<ul>';
            echo '<li> Golongan : Hanya akan menggenerate golongan CPNS, golongan PNS, dan golongan akhir</li>';
            echo '<li> Jabatan : Hanya akan menggenerate Jabatan Struktural dan Jabatan Fungsional Tertentu. Untuk Jabatan Fungsional Umum Hanya akan tercantum N/A. Mohon diperbaiki secara manual.</li>';
            echo '<li> Pendidikan : Hanya akan menggenerate pendidikan terakhir sesuai dengan tingkat pendidikan yang ada. Detail pendidikan dapat dilengkapi kemudian.</li>';
            echo '<li> Diklat Struktural : Hanya akan menggenerate diklat struktural terakhir.</li>';
            echo '<li> Unit Organisasi : Hanya akan menggenerate Unit Organisasi sesuai dengan jabatan terakhir .</li>';
            echo '</ul>';
            echo form_open();
            $data = array(
                'name' => 'username',
                'type' => 'text',
                'placeholder' => 'username',
                'required' => '',
            );
            echo form_input($data);
            $data = array(
                'name' => 'password',
                'type' => 'password',
                'placeholder' => 'password',
                'required' => '',
            );
            echo form_input($data);
            echo form_submit('mysubmit', 'Declare All!');
        } else {

            $remember = 0;

            if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember)) {
                $this->load->model('M_pegawai');
                $this->M_pegawai->declare_all($NIP);
            } else {
                redirect(base_url()); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
    }

    function nip_unor($UNOR = 'A8ACA73FDC703912E040640A040269BB') {
        $getnipunor = get_nip_unor($UNOR);
        $nipunor = implode(',', $getnipunor);
        echo $nipunor;
    }
    function get_bawahan_unor($UNOR = 'A8ACA73FDC703912E040640A040269BB') {
        $getnipunor = get_nip_unor($UNOR,0);
        $nipunor = implode(',', $getnipunor);
        echo $nipunor;
    }

}
