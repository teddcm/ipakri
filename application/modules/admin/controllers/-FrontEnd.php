<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FrontEnd extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->form_validation->set_error_delimiters('', '');
    }

    public function index() {
        $this->load->view('FrontEnd/v-index');
    }

}
