<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in()) {
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->form_validation->set_error_delimiters('', '');
    }

    public function index() {
        $this->listing();
    }

    public function nonaktif() {
        $data['KELOLA'] = $this->ion_auth->is_admin() ? 'SEMUA UNIT (ADMIN)' : (convert_unor($this->ion_auth->user()->row()->IDUNO));
        $this->load->view('pegawai/v-listing', $data);
    }

    private function listing() {
        $data['KELOLA'] = $this->ion_auth->is_admin() ? 'SEMUA UNIT (ADMIN)' : (convert_unor($this->ion_auth->user()->row()->IDUNO));
        $this->load->view('pegawai/v-listing', $data);
    }

    public function hierarki() {
        $data['KELOLA'] = $this->ion_auth->is_admin() ? 'SEMUA UNIT (ADMIN)' : (convert_unor($this->ion_auth->user()->row()->IDUNO));
        $this->load->view('pegawai/v-hierarki', $data);
    }

    public function listing_ajax_view($nonaktif) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'kanreg8_pupns';
        $primaryKey = 'PNS_NIPBARU';
        $columns = array(
            array(
                'db' => 'PNS_NIPBARU',
                'dt' => 'PNS_NIPBARU',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'PNS_PNSNAM', 'dt' => 'PNS_PNSNAM'),
            array('db' => 'PNS_GOLRU', 'dt' => 'PNS_GOLRU',
                'formatter' => function( $d ) {
                    return convert_golongan($d);
                }),
            array('db' => 'PNS_TKTDIK', 'dt' => 'PNS_TKTDIK',
                'formatter' => function( $d ) {
                    return convert_tktpendik($d);
                }),
            array('db' => 'PNS_KEDHUK', 'dt' => 'PNS_KEDHUK',
                'formatter' => function( $d ) {
                    return convert_kedhuk($d);
                }),
            array('db' => 'PNS_UNOR', 'dt' => 'PNS_UNOR',
                'formatter' => function( $d ) {
                    return convert_unor($d);
                }),
            array('db' => 'PNS_NIPBARU', 'dt' => 'PNS_NIPBARU'),
        );

        $unor = $this->ion_auth->user()->row()->IDUNO; //$this->ion_auth->user()->row()->IDUNO;

//        $getnipunor = get_nip_unor($unor, 1, 1);
//        $nipunor = implode(',', $getnipunor);
//        $where = '';
//        $where .= ' PNS_INSKER="' . INSTANSI_KERJA . '" AND ';
//
//        if (!$nonaktif) {
//            $where .= ' PNS_NIPBARU IN (' . $nipunor . ')  ';
//            $where .= ' AND PNS_KEDHUK <20';
//        } else {
//            if ($this->ion_auth->is_admin()) {
//                $where .= ' PNS_NIPBARU NOT IN (' . $nipunor . ') OR  PNS_KEDHUK >=20';
//            } else {
//                $where .= ' PNS_NIPBARU IN (' . $nipunor . ') AND ';
//                $where .= ' PNS_KEDHUK >=20';
//            }
//        }


        $where = '';
        $where .= ' PNS_INSKER="' . INSTANSI_KERJA . '"';
        if ($this->ion_auth->is_admin()) {
            if (!$nonaktif) {
                $where .= ' AND PNS_KEDHUK <20';
            } else {
                $where .= ' AND PNS_KEDHUK >=20';
            }
        } else {
            $getnipunor = get_nip_unor($unor, 1, 1);
            $nipunor = implode(',', $getnipunor);
            if (!$nonaktif) {
                $where .= ' AND PNS_NIPBARU IN (' . $nipunor . ')';
                $where .= ' AND PNS_KEDHUK <20';
            } else {
                $where .= ' AND PNS_NIPBARU IN (' . $nipunor . ')';
                $where .= ' AND PNS_KEDHUK >=20'; 
            }
        }
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function penetapan_nip($PNS_NIPBARU = NULL) {
        if ($this->ion_auth->in_group('data_utama_tambah')) {
            $this->load->model('M_pegawai');
            $data['agama'] = $this->M_pegawai->get_agama();
            $data['kedhuk'] = $this->M_pegawai->get_kedhuk();
            $data['jenpeg'] = $this->M_pegawai->get_jenpeg();
            $data['golru'] = $this->M_pegawai->get_golru();
            $this->load->view('pegawai/v-penetapan_nip', $data);
        } else {
            show_404();
        }
    }

    public function hapus_foto() {
        if ($this->ion_auth->in_group('data_utama_kelola')) {
            $file = ('./contents/foto/' . $this->input->post('file'));
            if (unlink($file)) {
                json_result('success', 'Foto behasil dihapus.');
            } else {
                json_result('error', 'Foto gagal dihapus.');
            }
        } else {
            show_404();
        }
    }

    public function upload_foto($PNS_NIPBARU = NULL) {
        if ($this->ion_auth->in_group('data_utama_kelola')) {
            $this->load->model('M_pegawai');
            $datapeg = $this->M_pegawai->get_data_pegawai($PNS_NIPBARU);

            $path = 'contents/foto';
            $file = $datapeg->PNS_NIPBARU . '_' . str_replace('.', '_', str_replace(' ', '_', $datapeg->PNS_PNSNAM)) . '.jpg';
            $filepath = base_url($path) . '/' . $file;
//echo $filepath;

            $is_file_exist = @file_get_contents($filepath);
//echo $is_file_exist;
            $_SESSION['rand_num'] = rand(1, 1000);
            $_SESSION['nip_last'] = NULL;
            if (empty($this->input->post())) {
                $data['is_file_exist'] = $is_file_exist;
                $data['filepath'] = $filepath;
                $data['file'] = $file;
                $data['path'] = $path;
                $data['pegawai'] = $datapeg;
                $this->load->view('pegawai/v-data_utama#foto', $data);
            } else {
                $config['upload_path'] = $path;
                $config['file_name'] = $file;
                $config['allowed_types'] = 'jpg';
                $config['max_size'] = '0';
                $config['max_width'] = '0';
                $config['max_height'] = '0';

                $this->load->library('upload', $config);
                $this->upload->overwrite = true;
                if (!$this->upload->do_upload()) {
                    json_result('error', 'Foto gagal diupload');
                } else {

                    $image_data = $this->upload->data();
                    $this->load->library('image_lib');
                    $configx['maintain_ratio'] = FALSE;
                    $configx['image_library'] = 'GD2';
                    $configx['source_image'] = $image_data['full_path']; //get original image
                    $configx['height'] = 400;
                    $configx['y_axis'] = (($image_data['image_height'] / 2) - ($configx['height'] / 2));
                    $this->image_lib->clear(); // added this line
                    $this->image_lib->initialize($configx); // added this line
                    if (!$this->image_lib->crop()) {
                        json_result('error', $this->image_lib->display_errors('', ''));
                    } else {
                        json_result('success', 'Foto behasil diupload.');
                    }
                }
            }
        } else {
            show_404();
        }
    }

    public function data_utama($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
//$this->M_pegawai->validasi_all($PNS_NIPBARU);
        $data['agama'] = $this->M_pegawai->get_agama();
        $data['kedhuk'] = $this->M_pegawai->get_kedhuk();
        $data['jenpeg'] = $this->M_pegawai->get_jenpeg();
        $data['tktpendik'] = $this->M_pegawai->get_tktpendik();
//$data['lokker'] = $this->M_pegawai->get_lokker();
//$data['pegawai'] = $this->M_pegawai->get_data_pegawai(($PNS_NIPBARU));
        $data['golru'] = $this->M_pegawai->get_golru();
        $this->load->view('pegawai/v-data_utama', $data);
    }

    public function data_utama_simpan() {
        if ($this->ion_auth->in_group('data_utama_kelola')) {
            $data = $this->input->post();
//VALIDASI NIP
            $tgl_lahir = str_replace('-', '', reverseDate($data['PNS_TGLLHRDT']));
            $sex = $data['PNS_PNSSEX'];
            $tmt_cpns = str_replace('-', '', reverseDate($data['PNS_TMTCPN']));
            $validasi = substr($data['PNS_NIPBARU'], 0, 15) == $tgl_lahir . substr($tmt_cpns, 0, 6) . $sex ? TRUE : FALSE;
//--
//Jika Tambah Pegawai
            if ($data['mode'] == 'tambah') {
                $this->form_validation->set_rules('PNS_NIPBARU', 'NIP', 'required|is_unique[kanreg8_pupns.PNS_NIPBARU]|min_length[18]|max_length[18]|numeric|trim');
                $this->form_validation->set_rules('PNS_PNSNIP', 'NIP Lama', 'trim|alpha_numeric_spaces');
                $this->form_validation->set_rules('PNS_PNSNAM', 'Nama', 'required|trim|alpha_numeric_spaces');
                $this->form_validation->set_rules('PNS_GLRDPN', 'Gelar Depan', 'custom');
                $this->form_validation->set_rules('PNS_GLRBLK', 'Gelar Belakang', 'custom');
                $this->form_validation->set_rules('PNS_TGLLHRDT', 'Tanggal Lahir', 'required|valid_date');
                $this->form_validation->set_rules('PNS_KODAGA', 'Agama', 'numeric');
                $this->form_validation->set_rules('PNS_PNSSEX', 'Jenis Kelamin', 'required|numeric');
                $this->form_validation->set_rules('PNS_KEDHUK', 'Kedudukan PNS', 'required|numeric');
                $this->form_validation->set_rules('PNS_TMTCPN', 'TMT CPNS', 'required|valid_date');
                $this->form_validation->set_rules('PNS_GOLAWL', 'Golongan Awal', 'required|numeric');
                $this->form_validation->set_rules('PNS_KARPEG', 'Karpeg', 'custom');

                $this->form_validation->set_rules('PNS_STCPNS', 'Status Pegawai', 'alpha|required');
                if ($this->input->post('PNS_STCPNS') == 'P') {
                    $this->form_validation->set_rules('PNS_TMTPNS', 'TMT PNS', 'required|valid_date');
                } else {
                    $this->form_validation->set_rules('PNS_TMTPNS', 'TMT PNS', 'valid_date');
                }

//definisikan PNS_GOLRU = PNS_GOLAWL
                $data['PNS_UNOR'] = $this->ion_auth->user()->row()->IDUNO;
                $data['PNS_GOLRU'] = $data['PNS_GOLAWL'];
            } else {
                $nip = $data['nip_lama'];
                if ($nip != $data['PNS_NIPBARU']) {
                    $this->form_validation->set_rules('PNS_NIPBARU', 'NIP', 'required|is_unique[kanreg8_pupns.PNS_NIPBARU]|min_length[18]|max_length[18]|numeric|trim');
                } else {
                    $this->form_validation->set_rules('PNS_NIPBARU', 'NIP', 'required|min_length[18]|max_length[18]|numeric|trim');
                }
                $this->form_validation->set_rules('PNS_PNSNIP', 'NIP Lama', 'trim|alpha_numeric_spaces');
                $this->form_validation->set_rules('PNS_PNSNAM', 'Nama', 'required|trim|alpha_numeric_spaces');
                $this->form_validation->set_rules('PNS_GLRDPN', 'Gelar Depan', 'trim|custom');
                $this->form_validation->set_rules('PNS_GLRBLK', 'Gelar Depan', 'trim|custom');
                $this->form_validation->set_rules('PNS_TGLLHRDT', 'Tanggal Lahir', 'required|valid_date');
                $this->form_validation->set_rules('PNS_EMAIL', 'Email', 'valid_email');
                $this->form_validation->set_rules('PNS_PNSSEX', 'Jenis Kelamin', 'required|numeric');
                $this->form_validation->set_rules('PNS_JENDOK', 'Jenis Dokumen', 'numeric');
                $this->form_validation->set_rules('PNS_NOMDOK', 'Nomor Dokumen', 'alpha_numeric');
                $this->form_validation->set_rules('PNS_ALAMAT', 'Alamat', 'custom');
                $this->form_validation->set_rules('PNS_KODAGA', 'Agama', 'numeric');
                $this->form_validation->set_rules('PNS_NOMHP', 'Nomor HP', 'numeric');
                $this->form_validation->set_rules('PNS_NOMTEL', 'Nomor Telepon', 'numeric');
                $this->form_validation->set_rules('PNS_KEDHUK', 'Kedudukan PNS', 'required|numeric');
                $this->form_validation->set_rules('PNS_STCPNS', 'Status Pegawai', 'alpha');
                $this->form_validation->set_rules('PNS_TMTCPN', 'TMT CPNS', 'required|valid_date');
                $this->form_validation->set_rules('PNS_KARPEG', 'Karpeg', 'custom');
                $this->form_validation->set_rules('PNS_TMTPNS', 'TMT PNS', 'valid_date');
                //$this->form_validation->set_rules('PNS_GOLAWL', 'Golongan Awal', 'required|numeric');
            }

            if ($this->form_validation->run() == true) {
                if ($validasi) {
                    $datapeg = $data;
                    if ($data['mode'] == 'edit') {
//PERSIAPAN VALIDASI FILEFOTO
                        $this->load->model('M_pegawai');
                        $datapeg = $this->M_pegawai->get_data_pegawai($data['nip_lama']);
                        $path = 'contents/foto';
                        $fileold = $datapeg->PNS_NIPBARU . '_' . str_replace(' ', '_', $datapeg->PNS_PNSNAM) . '.jpg';
                        $filenew = $data['PNS_NIPBARU'] . '_' . str_replace(' ', '_', $data['PNS_PNSNAM']) . '.jpg';
                    }
                    $this->load->model('M_pegawai');
                    $result = $this->M_pegawai->simpan_data_utama($data);
                    if ($result) {
                        if ($data['mode'] == 'edit') {
//VALIDASI FILEFOTO
                            @rename(($path . '/' . $fileold), ($path . '/' . $filenew));
                        }
                        json_result('success', 'Data berhasil disimpan.');
                    } else {
                        json_result('error', 'Data gagal disimpan.');
                    }
                } else {
                    json_result('error', 'NIP Tidak sesuai dengan Informasi Tanggal Lahir/TMT Awal/Jenis Kelamin.');
                }
            } else {
                json_result('warn', validation_errors());
            }
        } else {
            show_404();
        }
    }

    public function data_utama_hapus() {
        if ($this->ion_auth->in_group('data_utama_hapus')) {
            $data = $this->input->post();
            $pass = $data['pass'];
            $nip = $data['nip'];
            $username = $this->ion_auth->user()->row()->username;
            $this->load->model('M_pegawai');
            if ($this->ion_auth->login($username, $pass)) {
                if ($this->M_pegawai->hapus_data_utama($nip)) {
                    json_result('success', 'Data berhasil dihapus.');
                } else {
                    json_result('error', 'Data gagal dihapus.');
                }
            } else {
                json_result('error', 'Password yang anda masukkan tidak sesuai');
            }
        } else {
            show_404();
        }
    }

    public function golongan($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['golru'] = $this->M_pegawai->get_golru();
        $data['get_jenis_kp'] = $this->M_pegawai->get_jenis_kp();

        $this->load->view('pegawai/v-golongan', $data);
    }

    public function golongan_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwgolongan';
        $primaryKey = 'RWGOL_ID';
        $columns = array(
            array('db' => 'RWGOL_ID', 'dt' => 'RWGOL_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWGOL_KDGOL', 'dt' => 'RWGOL_KDGOL',
                'formatter' => function( $d ) {
                    return convert_golongan($d);
                }),
            array('db' => 'RWGOL_NOSK', 'dt' => 'RWGOL_NOSK'),
            array('db' => 'RWGOL_TGLSK', 'dt' => 'RWGOL_TGLSK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWGOL_TMTGOL', 'dt' => 'RWGOL_TMTGOL',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWGOL_NOBKN', 'dt' => 'RWGOL_NOBKN'),
            array('db' => 'RWGOL_TGLBKN', 'dt' => 'RWGOL_TGLBKN',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWGOL_MKGTHN', 'dt' => 'RWGOL_MKGTHN'),
            array('db' => 'RWGOL_MKGBLN', 'dt' => 'RWGOL_MKGBLN'),
            array('db' => 'RWGOL_KDJNSKP', 'dt' => 'RWGOL_KDJNSKP',
                'formatter' => function( $d ) {
                    return convert_jenis_kp($d);
                }),
            array('db' => 'RWGOL_NIP', 'dt' => 'RWGOL_NIP'),
        );
        $where = 'RWGOL_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function golongan_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWGOL_KDGOL', 'Golongan Ruang', 'required|numeric');
        $this->form_validation->set_rules('RWGOL_TMTGOL', 'TMT Golongan', 'required|valid_date');
        $this->form_validation->set_rules('RWGOL_MKGTHN', 'Masa Kerja Golongan Tahun', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWGOL_MKGBLN', 'Masa Kerja Golongan Bulan', 'required|alpha_numeric_spaces');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_golongan($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function golongan_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_golongan($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        }
    }

    public function peninjauan_masa_kerja($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');

        $this->load->view('pegawai/v-pmk');
    }

    public function peninjauan_masa_kerja_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwpmk';
        $primaryKey = 'RWPMK_ID';
        $columns = array(
            array('db' => 'RWPMK_ID', 'dt' => 'RWPMK_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWPMK_PERSEN', 'dt' => 'RWPMK_PERSEN',
                'formatter' => function( $d ) {
                    return convert_jenis_pmk($d);
                }),
            array('db' => 'RWPMK_TGLAWL', 'dt' => 'RWPMK_TGLAWL',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWPMK_TGLAKR', 'dt' => 'RWPMK_TGLAKR',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWPMK_TPTKRJ', 'dt' => 'RWPMK_TPTKRJ'),
            array('db' => 'RWPMK_NOSK', 'dt' => 'RWPMK_NOSK'),
            array('db' => 'RWPMK_TGLSK', 'dt' => 'RWPMK_TGLSK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWPMK_NOBKN', 'dt' => 'RWPMK_NOBKN'),
            array('db' => 'RWPMK_TGLBKN', 'dt' => 'RWPMK_TGLBKN',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWPMK_NOBKN', 'dt' => 'RWPMK_NOBKN'),
            array('db' => 'RWPMK_THNKRJ', 'dt' => 'RWPMK_THNKRJ'),
            array('db' => 'RWPMK_BLNKRJ', 'dt' => 'RWPMK_BLNKRJ'),
            array('db' => 'RWPMK_NIP', 'dt' => 'RWPMK_NIP'),
        );
        $where = 'RWPMK_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function peninjauan_masa_kerja_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWPMK_PERSEN', 'Jenis PMK', 'required|numeric');
        $this->form_validation->set_rules('RWPMK_THNKRJ', 'Masa Kerja Tahun', 'required|numeric');
        $this->form_validation->set_rules('RWPMK_BLNKRJ', 'Masa Kerja Bulan', 'required|numeric');

        if ($this->form_validation->run() == true) {
            if ($data['RWPMK_THNKRJ'] == 0 && $data['RWPMK_BLNKRJ'] == 0) {
                json_result('warn', 'Masa Kerja tidak boleh 0 Tahun 0 Bulan.');
//} elseif ($data['RWPMK_THNKRJ'] < 0 || $data['RWPMK_BLNKRJ'] < 0) {
//json_result('warn', 'Masa Kerja tidak boleh bernilai negatif.');
            } else {
                $this->load->model('M_pegawai');
                if ($data['RWPMK_PERSEN'] == 0) {
                    $data['RWPMK_THNKRJ'] = -1 * abs($data['RWPMK_THNKRJ']);
                    $data['RWPMK_BLNKRJ'] = -1 * abs($data['RWPMK_BLNKRJ']);
                }
                $result = $this->M_pegawai->simpan_pmk($data);
                if ($result) {
                    json_result('success', 'Data berhasil disimpan.');
                } else {
                    json_result('error', 'Data gagal disimpan.');
                }
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function peninjauan_masa_kerja_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_pmk($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function pendidikan($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['tktpendik'] = $this->M_pegawai->get_tktpendik();
        $data['pegawai'] = $this->M_pegawai->get_data_pegawai(($PNS_NIPBARU));
        $this->load->view('pegawai/v-pendidikan', $data);
    }

    public function pendidikan_update_gelar() {
        $data = $this->input->post();
        $data['mode'] = 'update_gelar';
        $this->load->model('M_pegawai');
        $result = $this->M_pegawai->update_gelar($data);
        if ($result) {
            json_result('success', 'Gelar berhasil diupdate.');
        } else {
            json_result('error', 'Gelar gagal diupdate.');
        }
    }

    public function pendidikan_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwpendidikan';
        $primaryKey = 'RWDIK_ID';
        $columns = array(
            array('db' => 'RWDIK_ID', 'dt' => 'RWDIK_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWDIK_IDDIK', 'dt' => 'NAMADIK',
                'formatter' => function( $d ) {
                    return convert_nama_dik($d);
                }),
            array('db' => 'RWDIK_TGLLULUS', 'dt' => 'RWDIK_TGLLULUS',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWDIK_THNLULUS', 'dt' => 'RWDIK_THNLULUS'),
            array('db' => 'RWDIK_NOIJZ', 'dt' => 'RWDIK_NOIJZ'),
            array('db' => 'RWDIK_NAMSKLH', 'dt' => 'RWDIK_NAMSKLH'),
            array('db' => 'RWDIK_PERTAMA', 'dt' => 'RWDIK_PERTAMA'),
            array('db' => 'RWDIK_TKTDIK', 'dt' => 'RWDIK_TKTDIK'),
            array('db' => 'RWDIK_GLRDPN', 'dt' => 'RWDIK_GLRDPN'),
            array('db' => 'RWDIK_GLRBLK', 'dt' => 'RWDIK_GLRBLK'),
            array('db' => 'RWDIK_IDDIK', 'dt' => 'RWDIK_IDDIK'),
            array('db' => 'RWDIK_NIP', 'dt' => 'RWDIK_NIP'),
        );
        $where = 'RWDIK_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function pendidikan_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWDIK_IDDIK', 'Pendidikan', 'required|numeric');
        $this->form_validation->set_rules('RWDIK_TKTDIK', 'Tingkat Pendidikan', 'required|numeric');
        $this->form_validation->set_rules('RWDIK_THNLULUS', 'Tahun Lulus', 'required|numeric');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_pendidikan($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function pendidikan_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_pendidikan($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function jabatan($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['jenjab'] = $this->M_pegawai->get_jenjab();
        $data['eselon'] = $this->M_pegawai->get_eselon();

        $this->load->view('pegawai/v-jabatan', $data);
    }

    public function jabatan_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwjabatan';
        $primaryKey = 'RWJAB_ID';
        $columns = array(
            array('db' => 'RWJAB_ID', 'dt' => 'RWJAB_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWJAB_IDJENJAB', 'dt' => 'RWJAB_IDJENJAB',
                'formatter' => function( $d ) {
                    return convert_jenjab($d);
                }),
            array('db' => 'RWJAB_IDUNO', 'dt' => 'RWJAB_IDUNO'),
            array('db' => 'RWJAB_NAMUNO', 'dt' => 'RWJAB_NAMUNO'),
            array('db' => 'RWJAB_IDESL', 'dt' => 'RWJAB_IDESL',
                'formatter' => function( $d ) {
                    return convert_eselon($d);
                }),
            array('db' => 'RWJAB_TMTJAB', 'dt' => 'RWJAB_TMTJAB',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWJAB_TGLSK', 'dt' => 'RWJAB_TGLSK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWJAB_TMTLANTIK', 'dt' => 'RWJAB_TMTLANTIK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWJAB_NOMSK', 'dt' => 'RWJAB_NOMSK'),
            array('db' => 'RWJAB_IDJAB', 'dt' => 'RWJAB_IDJAB'),
            array('db' => 'RWJAB_NAMAJAB', 'dt' => 'RWJAB_NAMAJAB'),
            array('db' => 'RWJAB_NIP', 'dt' => 'RWJAB_NIP'),
            array('db' => 'RWJAB_TMTJAB', 'dt' => 'URUTAN'),
        );
        $where = 'RWJAB_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function jabatan_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWJAB_IDJENJAB', 'Jenis Jabatan', 'required|numeric');
        $this->form_validation->set_rules('RWJAB_IDUNO', 'Unit Organisasi', 'required|alpha_numeric');
        $this->form_validation->set_rules('RWJAB_TMTJAB', 'TMT Jabatan', 'required|valid_date');
        $this->form_validation->set_rules('RWJAB_TGLSK', 'Tanggal SK', 'valid_date');

        if ($this->form_validation->run() == true) {
            if ($data['RWJAB_IDJENJAB'] == 1) {
                $data['RWJAB_NAMAJAB'] = $data['RWJAB_NAMAJAB_JS'];
            } else if ($data['RWJAB_IDJENJAB'] == 2) {
                //$data['RWJAB_IDESL'] = NULL; //SIMPAN KODE ESELON?
                $data['RWJAB_NAMAJAB'] = $data['RWJAB_NAMAJAB_JFT'];
            } else if ($data['RWJAB_IDJENJAB'] == 4) {
                //$data['RWJAB_IDESL'] = NULL; //SIMPAN KODE ESELON?
                $data['RWJAB_NAMAJAB'] = $data['RWJAB_NAMAJAB_JFU'];
            }
            unset($data['RWJAB_NAMAJAB_JS']);
            unset($data['RWJAB_NAMAJAB_JFT']);
            unset($data['RWJAB_NAMAJAB_JFU']);
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_jabatan($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function jabatan_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_jabatan($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function diklat($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['diklat'] = $this->M_pegawai->get_diklat();

        $this->load->view('pegawai/v-diklat', $data);
    }

    public function diklat_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwdiklat';
        $primaryKey = 'RWDLT_ID';
        $columns = array(
            array('db' => 'RWDLT_ID', 'dt' => 'RWDLT_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWDLT_NOM', 'dt' => 'RWDLT_NOM'),
            array('db' => 'RWDLT_TGL', 'dt' => 'RWDLT_TGL',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWDLT_THN', 'dt' => 'RWDLT_THN'),
            array('db' => 'RWDLT_NAMDLT', 'dt' => 'RWDLT_NAMDLT'),
            array('db' => 'RWDLT_NIP', 'dt' => 'RWDLT_NIP'),
        );
        $where = 'RWDLT_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function diklat_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWDLT_IDDLT', 'Nama Diklat Jabatan', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWDLT_THN', 'Tahun Diklat', 'required|numeric');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_diklat($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function diklat_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_diklat($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function keluarga($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['status_kawin'] = $this->M_pegawai->get_status_kawin();
        $data['status_anak'] = $this->M_pegawai->get_status_anak();
        $data['issu'] = $this->M_pegawai->get_issu($PNS_NIPBARU);
        $this->load->view('pegawai/v-keluarga', $data);
    }

    public function keluarga_issu_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwissu';
        $primaryKey = 'RWISU_ID';
        $columns = array(
            array('db' => 'RWISU_ID', 'dt' => 'RWISU_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWISU_NAMISU', 'dt' => 'RWISU_NAMISU'),
            array('db' => 'RWISU_TGLNIKAH', 'dt' => 'RWISU_TGLNIKAH',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWISU_AKTNIKAH', 'dt' => 'RWISU_AKTNIKAH'),
            array('db' => 'RWISU_STATUS', 'dt' => 'RWISU_STATUS',
                'formatter' => function( $d ) {
                    return convert_status_kawin($d);
                }),
            array('db' => 'RWISU_NIP', 'dt' => 'RWISU_NIP'),
        );
        $where = 'RWISU_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function keluarga_issu_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWISU_NAMISU', 'Nama Pasangan', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWISU_STATUS', 'Status', 'required|numeric');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_keluarga_issu($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function keluarga_issu_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($hapus = $this->M_pegawai->hapus_keluarga_issu($id, $nip)) {
            if ($hapus == 'berhasil') {
                json_result('success', 'Data berhasil dihapus.');
            } else {
                json_result('error', $hapus);
            }
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function keluarga_anak_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'q_datarwanak';
        $primaryKey = 'RWANK_ID';
        $columns = array(
            array('db' => 'RWANK_ID', 'dt' => 'RWANK_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWANK_NAMA', 'dt' => 'RWANK_NAMA'),
            array('db' => 'RWANK_ID_ORTU', 'dt' => 'RWANK_ID_ORTU',
                'formatter' => function( $d ) {
                    return convert_ortu($d);
                }),
            array('db' => 'RWANK_JK', 'dt' => 'RWANK_JK',
                'formatter' => function( $d ) {
                    return ($d == 1 ? 'Laki-laki' : 'Perempuan');
                }),
            array('db' => 'RWANK_TGLLHIR', 'dt' => 'RWANK_TGLLHIR',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWANK_STATUS', 'dt' => 'RWANK_STATUS',
                'formatter' => function( $d ) {
                    return convert_status_anak($d);
                }),
            array('db' => 'RWANK_NIP', 'dt' => 'RWANK_NIP'),
        );
        $where = 'RWANK_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function keluarga_anak_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWANK_NAMA', 'Nama Anak', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWANK_ID_ORTU', 'Nama Orang Tua', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWANK_STATUS', 'Status', 'required|numeric');
        $this->form_validation->set_rules('RWANK_JK', 'Jenis Kelamin', 'required|numeric');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_keluarga_anak($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function keluarga_anak_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_keluarga_anak($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

//////////////////////////////////////////////////////////////////////////////// UNIT ORGANISASI
    public function unit_organisasi($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
//$data['unit_organisasi'] = $this->M_pegawai->get_unit_organisasi();
        $this->load->view('pegawai/v-unor');
    }

    public function unit_organisasi_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwpnsunor';
        $primaryKey = 'RWUNOR_ID';
        $columns = array(
            array('db' => 'RWUNOR_ID', 'dt' => 'RWUNOR_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWUNOR_NOSK', 'dt' => 'RWUNOR_NOSK'),
            array('db' => 'RWUNOR_TGLSK', 'dt' => 'RWUNOR_TGLSK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWUNOR_NOSK', 'dt' => 'RWUNOR_NOSK'),
            array('db' => 'RWUNOR_NAMUNORBARU', 'dt' => 'RWUNOR_NAMUNORBARU'),
            array('db' => 'RWUNOR_IDUNORBARU', 'dt' => 'RWUNOR_IDUNORBARU'),
            array('db' => 'RWUNOR_IDASAL', 'dt' => 'RWUNOR_IDASAL'),
            array('db' => 'RWUNOR_PROSEDUR', 'dt' => 'RWUNOR_PROSEDUR',
                'formatter' => function( $d ) {
                    return convert_prosedur_unor($d);
                }),
            array('db' => 'RWUNOR_IDUNORBARU', 'dt' => 'ATASAN',
                'formatter' => function( $d ) {
                    $result = convert_unor(convert_unor($d, 'UNO_DIATASAN_ID'), 'UNO_NAMUNO');
                    return ($result == NULL && $d != SKPD) ? '[INVALID]' : $result;
                }),
            array('db' => 'RWUNOR_NIP', 'dt' => 'RWUNOR_NIP'),
        );
        $where = 'RWUNOR_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function unit_organisasi_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWUNOR_IDUNORBARU', 'Unit Organisasi ', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWUNOR_TGLSK', 'Tanggal SK ', 'required|valid_date');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_unit_organisasi($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function unit_organisasi_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_unit_organisasi($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function kursus($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['instansi'] = $this->M_pegawai->get_instansi();
        $this->load->view('pegawai/v-kursus', $data);
    }

    public function kursus_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwkursus';
        $primaryKey = 'RWKRS_ID';
        $columns = array(
            array('db' => 'RWKRS_ID', 'dt' => 'RWKRS_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWKRS_JNSKRS', 'dt' => 'RWKRS_JNSKRS',
                'formatter' => function( $d ) {
                    return ($d == 1 ? 'Kursus' : 'Sertifikat');
                }),
            array('db' => 'RWKRS_NAMKRS', 'dt' => 'RWKRS_NAMKRS'),
            array('db' => 'RWKRS_KODINS', 'dt' => 'RWKRS_KODINS',
                'formatter' => function( $d ) {
                    return convert_instansi($d);
                }),
            array('db' => 'RWKRS_NOM', 'dt' => 'RWKRS_NOM'),
            array('db' => 'RWKRS_THN', 'dt' => 'RWKRS_THN'),
            array('db' => 'RWKRS_TGL', 'dt' => 'RWKRS_TGL',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWKRS_JAM', 'dt' => 'RWKRS_JAM'),
            array('db' => 'RWKRS_INSTITUSI', 'dt' => 'RWKRS_INSTITUSI'),
            array('db' => 'RWKRS_NIP', 'dt' => 'RWKRS_NIP'),
        );
        $where = 'RWKRS_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function kursus_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWKRS_JNSKRS', 'Jenis Kursus', 'required|numeric');
        $this->form_validation->set_rules('RWKRS_NAMKRS', 'Nama Kursus', 'required|custom');
        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_kursus($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function kursus_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_kursus($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function skp($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['jenjab'] = $this->M_pegawai->get_jenjab();
        $this->load->view('pegawai/v-skp', $data);
    }

    public function skp_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'q_datarwskp';
        $primaryKey = 'RWSKP_ID';
        $columns = array(
            array('db' => 'RWSKP_ID', 'dt' => 'RWSKP_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWSKP_IDJENJAB', 'dt' => 'RWSKP_IDJENJAB',
                'formatter' => function( $d ) {
                    return convert_jenjab($d);
                }),
            array('db' => 'RWSKP_THN', 'dt' => 'RWSKP_THN'),
            array('db' => 'RWSKP_NSKP', 'dt' => 'RWSKP_NSKP'),
            array('db' => 'RWSKP_NPELAYANAN', 'dt' => 'RWSKP_NPELAYANAN'),
            array('db' => 'RWSKP_NKOMITMEN', 'dt' => 'RWSKP_NKOMITMEN'),
            array('db' => 'RWSKP_NKERJASAMA', 'dt' => 'RWSKP_NKERJASAMA'),
            array('db' => 'RWSKP_NINTEGRITAS', 'dt' => 'RWSKP_NINTEGRITAS'),
            array('db' => 'RWSKP_NDISIPLIN', 'dt' => 'RWSKP_NDISIPLIN'),
            array('db' => 'RWSKP_NKEPEMIMPINAN', 'dt' => 'RWSKP_NKEPEMIMPINAN'),
            array('db' => 'RWSKP_NRATA', 'dt' => 'RWSKP_NRATA'),
            array('db' => 'RWSKP_NRATA', 'dt' => 'KETERANGAN',
                'formatter' => function( $d ) {
                    switch (true) {
                        case $d >= 91:
                            $ket = 'sangat baik';
                            break;
                        case $d >= 76:
                            $ket = 'baik';
                            break;
                        case $d >= 61:
                            $ket = 'cukup';
                            break;
                        case $d >= 51:
                            $ket = 'kurang';
                            break;
                        default:
                            $ket = 'buruk';
                    }
                    return ucwords($ket);
                }),
            array('db' => 'RWSKP_NJUMLAH', 'dt' => 'RWSKP_NJUMLAH'),
            array('db' => 'RWSKP_PJBPENILAI', 'dt' => 'RWSKP_PJBPENILAI'),
            array('db' => 'RWSKP_ATASANPJBPENILAI', 'dt' => 'RWSKP_ATASANPJBPENILAI'),
            array('db' => 'RWSKP_NIP', 'dt' => 'RWSKP_NIP'),
        );
        $where = 'RWSKP_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function skp_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWSKP_IDJENJAB', 'Jenis Jabatan', 'required|numeric');
        $this->form_validation->set_rules('RWSKP_THN', 'Tahun SKP', 'required|callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NSKP', 'Nilai SKP', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NPELAYANAN', 'Nilai Orientasi Pelayanan', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NKOMITMEN', 'Nilai Komitmen', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NKERJASAMA', 'Nilai Kerjasama', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NINTEGRITAS', 'Nilai Integritas', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NDISIPLIN', 'Nilai Disiplin', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_NKEPEMIMPINAN', 'Nilai Kepemimpinan', 'callback_valid_nilai');
        $this->form_validation->set_rules('RWSKP_THN', 'Tahun SKP', 'required|numeric');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_skp($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function skp_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_skp($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

//////////////////////////////////////////////////////////////////////////////// ANGKA KREDIT
    public function angka_kredit($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        $data['jabatan'] = $this->M_pegawai->get_jabatan_angka_kredit($PNS_NIPBARU);
        $this->load->view('pegawai/v-angka_kredit', $data);
    }

    public function angka_kredit_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'q_datarwangkakredit';
        $primaryKey = 'RWAKR_ID';
        $columns = array(
            array('db' => 'RWAKR_ID', 'dt' => 'RWAKR_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWAKR_NOSK', 'dt' => 'RWAKR_NOSK'),
            array('db' => 'RWAKR_TGLSK', 'dt' => 'URUTAN'),
            array('db' => 'RWAKR_TGLSK', 'dt' => 'RWAKR_TGLSK',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWAKR_AKRUTAMA', 'dt' => 'RWAKR_AKRUTAMA'),
            array('db' => 'RWAKR_AKRPENUNJANG', 'dt' => 'RWAKR_AKRPENUNJANG'),
            array('db' => 'RWAKR_AKRTOTAL', 'dt' => 'RWAKR_AKRTOTAL'),
            array('db' => 'RWAKR_AKRPERTAMA', 'dt' => 'RWAKR_AKRPERTAMA'),
            array('db' => 'RWAKR_BLNAWL', 'dt' => 'RWAKR_BLNAWL'),
            array('db' => 'RWAKR_THNAWL', 'dt' => 'RWAKR_THNAWL'),
            array('db' => 'RWAKR_BLNAKR', 'dt' => 'RWAKR_BLNAKR'),
            array('db' => 'RWAKR_THNAKR', 'dt' => 'RWAKR_THNAKR'),
            array('db' => 'RWAKR_JAB', 'dt' => 'RWAKR_JAB'),
            array('db' => 'RWAKR_JAB', 'dt' => 'NAMAJAB',
                'formatter' => function( $d ) {
                    return convert_jabfun($d);
                }),
            array('db' => 'RWAKR_NIP', 'dt' => 'RWAKR_NIP'),
        );
        $where = 'RWAKR_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function angka_kredit_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWAKR_AKRUTAMA', 'Angka Kredit Utama ', 'callback_valid_float');
        $this->form_validation->set_rules('RWAKR_AKRPENUNJANG', 'Angka Kredit Penunjang ', 'callback_valid_float');
        $this->form_validation->set_rules('RWAKR_JAB', 'Jabatan ', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWAKR_THNAWL', 'Tahun Awal', 'numeric|min_length[4]|max_length[4]');
        $this->form_validation->set_rules('RWAKR_THNAKR', 'Tahun Akhir ', 'numeric|min_length[4]|max_length[4]');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_angka_kredit($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function angka_kredit_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_angka_kredit($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

// ========================================================================= CUTI


    public function xcuti($PNS_NIPBARU = NULL) {

        $this->load->model('M_pegawai');
        $data['jenis_cuti'] = $this->M_pegawai->get_jenis_cuti();
        $this->load->view('pegawai/v-cuti', $data);
    }

    public function get_temp_cutinew() {
        $this->load->model('M_pegawai');
        if ($result = $this->M_pegawai->get_temp_cuti($this->input->post('id'))) {
            $result = ((array) $result);
            if ($result['nip'] == $this->input->post('nip') && $result['used'] == 0) {
                $result[] = 'success';
                $result[] = 'Data ditemukan.';
                echo json_encode($result);
            } else if ($result['used'] == 1) {
                json_result('error', 'Formulir dengan barcode tersebut sudah pernah disimpan.');
            } else {
                json_result('error', 'Barcode form tersebut milik ' . get_data_pns($result['nip'], 'PNS_PNSNAM') . '.');
            }
        } else {
            json_result('error', 'Data tidak ditemukan.');
        }
    }

    public function cuti($PNS_NIPBARU = NULL) {
        $this->load->model('M_pegawai');
        if (cek_jatah_cutinew() == 0) {
            $data['data_cuti'] = $this->M_pegawai->get_data_cuti();
        }
        $data['jenis_cuti'] = $this->M_pegawai->get_jenis_cuti();
        $this->load->view('pegawai/v-cutinew', $data);
    }

//
//    public function cuti_ajax_view($PNS_NIPBARU = NULL) {
//        $this->load->library('datatables_ssp'); //panggil library datatables
//        $sql_details = array(
//            'user' => $this->db->username,
//            'pass' => $this->db->password, 'port' => $this->db->port,
//            'db' => $this->db->database,
//            'host' => $this->db->hostname
//        );
//        $table = 'q_datarwcuti';
//        $primaryKey = 'RWCUTI_ID';
//        $columns = array(
//            array('db' => 'RWCUTI_ID', 'dt' => 'RWCUTI_ID',
//                'formatter' => function( $d ) {
//                    return $d;
//                }),
//            array('db' => 'RWCUTI_JNS', 'dt' => 'RWCUTI_JNS'),
//            array('db' => 'RWCUTI_JNS', 'dt' => 'RWCUTI_JNSNM',
//                'formatter' => function( $d ) {
//                    return convert_jenis_cuti($d);
//                }),
//            array('db' => 'RWCUTI_TGLAWL', 'dt' => 'RWCUTI_TGLAWL',
//                'formatter' => function( $d ) {
//                    return reverseDate($d);
//                }),
//            array('db' => 'RWCUTI_TGLAKR', 'dt' => 'RWCUTI_TGLAKR',
//                'formatter' => function( $d ) {
//                    return reverseDate($d);
//                }),
//            array('db' => 'RWCUTI_JUM', 'dt' => 'RWCUTI_JUM'),
//            array('db' => 'RWCUTI_THNCUTI', 'dt' => 'RWCUTI_THNCUTI'),
//            array('db' => 'RWCUTI_KET', 'dt' => 'RWCUTI_KET'),
//            array('db' => 'RWCUTI_NIP', 'dt' => 'RWCUTI_NIP'),
//            array('db' => 'RWCUTI_IDFORM', 'dt' => 'RWCUTI_IDFORM'),
//        );
//        $where = 'RWCUTI_NIP="' . $PNS_NIPBARU . '" AND RWCUTI_THNCUTI="' . $this->input->get('tahun') . '"';
//        $newdata = Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where);
//        echo json_encode($newdata);
//    }

    public function cutinew_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'q_datarwcutinew';
        $primaryKey = 'RWCUTI_ID';
        $columns = array(
            array('db' => 'RWCUTI_ID', 'dt' => 'RWCUTI_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWCUTI_JNS', 'dt' => 'RWCUTI_JNS'),
            array('db' => 'RWCUTI_JNS', 'dt' => 'RWCUTI_JNSNM',
                'formatter' => function( $d ) {
                    return convert_jenis_cuti($d);
                }),
            array('db' => 'RWCUTI_TGLAWL', 'dt' => 'RWCUTI_TGLAWL',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWCUTI_TGLAKR', 'dt' => 'RWCUTI_TGLAKR',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWCUTI_JUM', 'dt' => 'RWCUTI_JUM'),
            array('db' => 'RWCUTI_THNCUTI', 'dt' => 'RWCUTI_THNCUTI'),
            array('db' => 'RWCUTI_KET', 'dt' => 'RWCUTI_KET'),
            array('db' => 'RWCUTI_NIP', 'dt' => 'RWCUTI_NIP'),
            array('db' => 'RWCUTI_IDFORM', 'dt' => 'RWCUTI_IDFORM'),
        );
        $where = 'RWCUTI_NIP="' . $PNS_NIPBARU . '" AND RWCUTI_THNCUTI="' . $this->input->get('tahun') . '"';
        $newdata = Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where);
        $nambahdata = array(
            'RWCUTI_ID' => 'null',
            'RWCUTI_JNS' => 0,
            'RWCUTI_JNSNM' => 'Jatah Tahun ' . $this->input->get('tahun'),
            'RWCUTI_TGLAWL' => null,
            'RWCUTI_TGLAKR' => null,
            'RWCUTI_JUM' => cek_jatah_cutinew($this->input->get('tahun')),
            'RWCUTI_THNCUTI' => $this->input->get('tahun'),
            'RWCUTI_KET' => null,
            'RWCUTI_NIP' => null,
        );
        $newdata['recordsFiltered'] = $numrows = (int) $newdata['recordsFiltered'] + 1;
        for ($x = $numrows - 1; $x >= 1; $x--) {
            $newdata['data'][$x] = $newdata['data'][$x - 1];
        }
        $newdata['data'][0] = $nambahdata;
        echo json_encode($newdata);
    }

//    public function cuti_simpan() {
//        $data = $this->input->post();
//        if ($data['mode'] == 'define') {
//            $this->form_validation->set_rules('cuti_sisa', 'Sisa Cuti', 'required|numeric');
//            $this->form_validation->set_rules('cuti_jatah', 'Jatah Cuti', 'required|numeric');
//        } else {
//            if ($data['mode'] == 'tambah') {
//                $this->form_validation->set_rules('RWCUTI_JNS', 'Jenis Cuti', 'required|numeric');
//            }
//            if (isset($data['RWCUTI_JNS'])) {
//                $this->form_validation->set_rules('RWCUTI_JNS', 'Jenis Cuti', 'required|numeric');
//                $this->form_validation->set_rules('RWCUTI_JUM', 'Jumlah Cuti', 'required|numeric');
//            }
//            $this->form_validation->set_rules('RWCUTI_JUM', 'Jumlah Hari', 'required|numeric');
//        }
//        if ($this->form_validation->run() == true) {
//            $this->load->model('M_pegawai');
//            unset($data['potijin']);
//            $result = $this->M_pegawai->simpan_cuti($data);
//            if ($result) {
//                json_result('success', 'Data berhasil disimpan.');
//            } else {
//                json_result('error', 'Data gagal disimpan.');
//            }
//        } else {
//            json_result('warn', validation_errors());
//        }
//    }

    public function cutinew_simpan() {
        $data = $this->input->post();
        if ($data['mode'] == 'define') {
            $this->form_validation->set_rules('RWCUTI_JUM', 'Jumlah Sisa Hak Cuti', 'required|numeric');
        } else {
            if ($data['mode'] == 'tambah') {
                $this->form_validation->set_rules('RWCUTI_JNS', 'Jenis Cuti', 'required|numeric');
            }
            if (isset($data['RWCUTI_JNS'])) {
                $this->form_validation->set_rules('RWCUTI_JNS', 'Jenis Cuti', 'required|numeric');
                $this->form_validation->set_rules('RWCUTI_JUM', 'Jumlah Cuti', 'required|numeric');
            }
            $this->form_validation->set_rules('RWCUTI_JUM', 'Jumlah Hari', 'required|numeric');
        }
        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            unset($data['potijin']);
            $result = $this->M_pegawai->simpan_cuti($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function cutinew_awal_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('jatah[]', 'Jatah Cuti', 'required|numeric|less_than_equal_to[24]|greater_than_equal_to[0]');

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_cuti_awal($data);
            if (TRUE) {
                json_result('success', 'Data cuti berhasil disimpan.');
            } else {
                json_result('error', 'Data cuti gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function cuti_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $nip = $data['nip'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_cuti($id, $nip)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function cuti_validasi() {
        $this->load->model('M_pegawai');
        if ($result = $this->M_pegawai->get_temp_cuti($this->input->post('id'))) {
            $result = ((array) $result);
            if ($result['nip'] == $this->input->post('nip') && $result['used'] == 1) {
                $user = $this->ion_auth->user()->row();
                $result['html'] = "
                <style>
                    table, th, td {
                        border: 1px solid black;
                        font-size: 9px;
                        border-collapse: collapse;
                        background-color: gray;
                        text-align: center;
                    }
                </style>
                <table>
                <tr><td>Validasi Admin Sistem | " . date('d-m-Y H:i:s') . "</td></tr>
                <tr><td><br><br><br></td></tr>
                <tr><td>" . $user->full_name . " (" . $user->username . ")</td></tr>
                </table>
                <script>
                    function cetak() {
                        window.print();
                        window.top.close();
                    }
                    window.onload = cetak();
                </script>
                                        ";
                echo json_encode($result);
            }
        } else {
            json_result('error', 'Data tidak ditemukan.');
        }
    }

//////////////////////////////////////////////////////////////////////////////// KGB
    public function kgb($PNS_NIPBARU = NULL) {
        $this->load->model('M_report');
        $this->load->model('M_pegawai');
        $data['golru'] = $this->M_pegawai->get_golru();
        $data['dataKGB'] = $this->M_report->getkgb(NULL, $PNS_NIPBARU)[0];
        $this->load->view('pegawai/v-kgb', $data);
    }

    public function kgb_sk($PNS_NIPBARU, $id) {
        $this->load->model('M_pegawai');
        $data['golru'] = $this->M_pegawai->get_golru();
        $data['dataKGB'] = $this->M_pegawai->get_kgb_sk($id);
        $data['dataPegawai'] = $this->M_pegawai->get_data_pegawai($PNS_NIPBARU);
        $data['dataSpesimen'] = get_spesimen();
        $this->load->view('pegawai/v-kgb#sk', $data);
    }

    public function kgb_ajax_view($PNS_NIPBARU = NULL) {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'z_datarwkgb';
        $primaryKey = 'RWKGB_ID';
        $columns = array(
            array('db' => 'RWKGB_ID', 'dt' => 'RWKGB_ID',
                'formatter' => function( $d ) {
                    return $d;
                }),
            array('db' => 'RWKGB_NOSKBARU', 'dt' => 'RWKGB_NOSKBARU'),
            array('db' => 'RWKGB_GOLBARU', 'dt' => 'RWKGB_GOLBARU',
                'formatter' => function( $d ) {
                    return convert_golongan($d);
                }),
            array('db' => 'RWKGB_TGLSKBARU', 'dt' => 'RWKGB_TGLSKBARU',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWKGB_TMTSKBARU', 'dt' => 'RWKGB_TMTSKBARU',
                'formatter' => function( $d ) {
                    return reverseDate($d);
                }),
            array('db' => 'RWKGB_MKGBARU', 'dt' => 'RWKGB_MKGBARU',
                'formatter' => function( $d ) {
                    $d = str_pad($d, 4, '0', STR_PAD_LEFT);
                    return substr($d, 0, 2) . " " . substr($d, 2, 2);
                }),
            array('db' => 'RWKGB_GAPOKBARU', 'dt' => 'RWKGB_GAPOKBARU'),
            array('db' => 'RWKGB_NIP', 'dt' => 'RWKGB_NIP'),
        );
        $where = 'RWKGB_NIP="' . $PNS_NIPBARU . '"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function kgb_simpan() {
        $data = $this->input->post();
        $this->form_validation->set_rules('RWKGB_TGLSKBARU', 'Tanggal SK ', 'valid_date');
        $this->form_validation->set_rules('RWKGB_NOSKBARU', 'Nomor SK ', 'custom');
        $this->form_validation->set_rules('RWKGB_TMTSKBARU', 'TMT ', 'required|valid_date');
        $this->form_validation->set_rules('RWKGB_GAPOKBARU', 'Gaji Pokok ', 'required|numeric');
        $this->form_validation->set_rules('RWKGB_MKGBARU', 'Masa Kerja Golongan ', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWKGB_GOLBARU', 'Golongan Ruang ', 'required|numeric');
        $data['RWKGB_MKGBARU'] = str_replace(' ', '', $data['RWKGB_MKGBARU']);
//melalui form SK

        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_kgb($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function kgb_sk_simpan() {
        $data = $this->input->post();

        $data = $this->input->post();
        $this->form_validation->set_rules('RWKGB_TGLSKBARU', 'Tanggal SK ', 'required|valid_date');
//$this->form_validation->set_rules('RWKGB_NOSKBARU', 'Nomor SK ', 'required|custom');
        $this->form_validation->set_rules('RWKGB_TMTSKBARU', 'TMT ', 'required|valid_date');
        $this->form_validation->set_rules('RWKGB_GAPOKBARU', 'Gaji Pokok ', 'required|numeric');
        $this->form_validation->set_rules('RWKGB_MKGBARU', 'Masa Kerja Golongan ', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWKGB_GOLBARU', 'Golongan Ruang ', 'required|numeric');
        $data['RWKGB_MKGBARU'] = str_replace(' ', '', $data['RWKGB_MKGBARU']);


        $this->form_validation->set_rules('RWKGB_TGLSKLAMA', 'Tanggal SK Lama', 'required|valid_date');
        $this->form_validation->set_rules('RWKGB_NOSKLAMA', 'Nomor SK Lama', 'required|custom');
        $this->form_validation->set_rules('RWKGB_TMTSKLAMA', 'TMT Lama', 'required|valid_date');
        $this->form_validation->set_rules('RWKGB_GAPOKLAMA', 'Gaji Pokok Lama', 'required|numeric');
        $this->form_validation->set_rules('RWKGB_MKGLAMA', 'Masa Kerja Golongan Lama', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('RWKGB_GOLLAMA', 'Golongan Ruang Lama', 'required|numeric');
        $this->form_validation->set_rules('RWKGB_JABPJBLAMA', 'Jabatan Pejabat Lama ', 'required|alpha_numeric_spaces');
        $data['RWKGB_MKGLAMA'] = str_replace(' ', '', $data['RWKGB_MKGLAMA']);

        $this->form_validation->set_rules('RWKGB_NAMA', 'Nama Pegawai', 'required|custom');
        $this->form_validation->set_rules('RWKGB_INSKER', 'Unit Kerja Pegawai', 'required|custom');
        $this->form_validation->set_rules('RWKGB_JAB', 'Jabatan Pegawai', 'required|custom');
        $this->form_validation->set_rules('RWKGB_DASARHUK', 'Dasar Hukum', 'required|custom');

        $data['mode'] = 'fullmode';
        if ($this->form_validation->run() == true) {
            $this->load->model('M_pegawai');
            $result = $this->M_pegawai->simpan_kgb($data);
            if ($result) {
                json_result('success', 'Data berhasil disimpan.');
            } else {
                json_result('error', 'Data gagal disimpan.');
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function kgb_hapus() {
        $data = $this->input->post();
        $id = $data['id'];
        $this->load->model('M_pegawai');
        if ($this->M_pegawai->hapus_kgb($id)) {
            json_result('success', 'Data berhasil dihapus.');
        } else {
            json_result('error', 'Data gagal dihapus.');
        };
    }

    public function coba() {
        $this->load->dbutil();

        $db_name = 'pdk_backup_' . date("Y-m-d H.i.s") . '_' . get_user_info('user_name');
        $prefs = array(
            'format' => 'zip',
            'filename' => $db_name . '.sql'
        );


        $backup = $this->dbutil->backup($prefs);
        $save = 'backupdb/' . $db_name . '.zip';

        $this->load->helper('file');
        write_file($save, $backup);

        echo "<a href='$save' class='btn btn-flat btn-danger btn-sm'>download backup</a>";
//$this->load->helper('download');
//force_download($db_name, $backup);
    }

    //FORM VALIDATION
    public function valid_nilai($str) {
        if (is_numeric($str)) {
            if ((float) $str > 100) {
                $this->form_validation->set_message('valid_nilai', 'Kolom {field} tidak boleh lebih dari 100');
                return FALSE;
            } else if ((float) $str < 0) {
                $this->form_validation->set_message('valid_nilai', 'Kolom {field} tidak boleh kurang dari 0');
                return FALSE;
            } else {
                return TRUE;
            }
        } else if ($str == '') {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_nilai', 'Kolom {field} harus bernilai valid');
            return FALSE;
        }
    }

    public function valid_float($str) {
        if (is_numeric($str)) {
            return TRUE;
        } else if ($str == '') {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_float', 'Kolom {field} harus bernilai valid');
            return FALSE;
        }
    }

}
