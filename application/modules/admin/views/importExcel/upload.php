<table class="table table-bordered"><?php
    foreach ($detail as $r) {
        echo '<tr>'
        . '<td>' . $r->IMP_KET . '</td>'
        . '<td><button type="button" class="btn btn-block btn-primary myModal" id="' . $r->IMP_NAME . '" title="' . $r->IMP_KET . '">Upload file</button></td>'
        . '<td>(' . $r->IMP_LASTDATE . ') <i style="color:red">' . get_timeago(strtotime($r->IMP_LASTDATE)) . '</i></td>'
        . '<td>' . number_format(((float) $r->IMP_SIZE) / 1024, 3, '.', '') . ' MB</td>'
        . '</tr>';
    }
    ?>
</table>
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Upload file excel  <span id="keterangan"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="file-loading">
                    <input id="upload" name="userfile" type="file" data-browse-on-zone-click='true'>
                </div>
                <div id="kartik-file-errors"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" title="Your custom upload logic">Save</button>
            </div>
        </div>
    </div>
</div>
<input id="hidden" type="hidden">

<script>
    $('.myModal').on('click', function () {
        $('#exampleModal').modal('show');
        $('#hidden').val($(this).prop('id'));
        $('#keterangan').text($(this).prop('title'));
        //        alert($(this).prop('id'));
    })

    $("#upload").fileinput({
        uploadUrl: "<?php echo base_url(uri_string()); ?>",
        autoReplace: true,
        elErrorContainer: '#kartik-file-errors',
        dropZoneTitle: "Silakan upload file dengan ekstensi .xlsx",
        dropZoneClickTitle: '',
        maxFileCount: 1,
        uploadExtraData: function (previewId, index) {
            var data = {
                value: $("#hidden").val(),
            };
            return data;
        },
        allowedFileExtensions: ["xlsx"],
    }).on('fileuploaded', function (event, data, msg) {
        $.notify(data.response[1], data.response[0]);
//        if (data.response[0] === 'success') {
        //            $('#modal-default').modal('show'); 
        alert('Selesai')
        $('#upload').fileinput('clear');
        $('#exampleModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 1000);

//        }
    });
    $("#exampleModal").on("hidden.bs.modal", function () {
        $('#upload').fileinput('clear');
    })
</script>