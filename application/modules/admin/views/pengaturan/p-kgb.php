<style>
form *{
    text-transform: none!important  ;
}
</style>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Format</th>
                            <th>Keterangan</th>
                            <th>Contoh</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td>%n1%</td><td>nomor urut surat biasa </td><td>1, 10, 100</td></tr>
                        <tr><td>%n2%</td><td>nomor urut surat 2 digit </td><td>01, 10, 100</td></tr>
                        <tr><td>%n3%</td><td>nomor urut surat 3 digit </td><td>001, 010, 100</td></tr>
                        <tr><td>%n4%</td><td>nomor urut surat 4 digit </td><td>0001, 0010, 0100</td></tr>
                        <tr><td>%d%</td><td>tanggal </td><td>01-31</td></tr>
                        <tr><td>%da%</td><td>tanggal </td><td>1-31</td></tr>
                        <tr><td>%m%</td><td>bulan </td><td>01-12</td></tr>
                        <tr><td>%ma%</td><td>bulan </td><td>1-12</td></tr>
                        <tr><td>%mb%</td><td>bulan </td><td>I-XII</td></tr>
                        <tr><td>%y%</td><td>tahun </td><td>2000-2017</td></tr>
                        <tr><td>%ya%</td><td>tahun </td><td>00-17</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btn_sampel').on('click', function () {
        $('#sampel').html('<img src="<?php echo base_url('contents/img/sk_kgb.png'); ?>" class="image img-responsive"/>');
    })
</script>
<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab" data-toggle="tab">KGB</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="tab">

                    <div class="form-group col-lg-6">
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label>No Urut</label>
                                <input class="form-control" name="kgb_no_surat" value="<?= getPengaturan('kgb_no_surat'); ?>">
                            </div>
                            <div class="form-group col-lg-7">
                                <label>Format Nomor Surat</label>
                                <input class="form-control" name="kgb_no_surat_format" value="<?= getPengaturan('kgb_no_surat_format'); ?>">
                            </div>
                            <div class="form-group col-lg-1">
                                <label>&nbsp;</label>
                                <span class="input-group-btn" data-toggle="modal" data-target="#myModal">
                                    <button type="button" class="btn btn-info btn-flat">
                                        <i class="fa fa-question"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-6">
                                <label>Sifat Surat</label>
                                <input class="form-control" name="kgb_sk_sifat" value="<?= getPengaturan('kgb_sk_sifat'); ?>">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Lampiran Surat</label>
                                <input class="form-control" name="kgb_sk_lampiran" value="<?= getPengaturan('kgb_sk_lampiran'); ?>">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-6">
                                <label>Perihal Surat</label>
                                <input class="form-control" name="kgb_sk_perihal" value="<?= getPengaturan('kgb_sk_perihal'); ?>">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Kota Surat</label>
                                <input class="form-control" name="kgb_sk_kota" value="<?= getPengaturan('kgb_sk_kota'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>Header Surat</label>
                                <textarea class="form-control" name="kgb_sk_header" style = 'resize:none;height: 100px;text-align: center;font-weight: bold'><?= getPengaturan('kgb_sk_header'); ?></textarea>
                            </div>
                            <div class="form-group col-lg-12">
                                <label>Tujuan Surat</label>
                                <textarea class="form-control" name="kgb_sk_kepada" style = 'resize:none;height: 100px'><?= getPengaturan('kgb_sk_kepada'); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <hr>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>Dasar Hukum Pemberian KGB</label>
                                <input class="form-control" name="kgb_sk_dasarhuk" value="<?= getPengaturan('kgb_sk_dasarhuk'); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label>Tembusan Surat</label>
                                <textarea class="form-control" name="kgb_sk_tembusan" style = 'resize:none;height: 115px'><?= getPengaturan('kgb_sk_tembusan'); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </form>
</div>
<div class="clearfix"></div>
<script>
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pengaturan/kgb'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('admin/pengaturan/kgb'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>