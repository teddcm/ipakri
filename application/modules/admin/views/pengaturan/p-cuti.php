<style>
    form *{
        text-transform: none!important  ;
    }
</style>

<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab" data-toggle="tab">Cuti</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab">
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label>Tujuan Permohonan</label>
                            <textarea class="form-control" name="cuti_form_kepada" style='resize:none;height: 135px'><?= getPengaturan('cuti_form_kepada'); ?></textarea>
                        </div>
                        <!--div class="form-group col-sm-6">
                            <label>Pejabat Kepegawaian </label>
                            <select class="form-control" name="cuti_pjb_kepegawaian">
                                <!--?php
                                $dataSpesimen = get_spesimen();
                                foreach ($dataSpesimen as $x) {
                                    $select = $x->SPES_NIPBARU == getPengaturan('cuti_pjb_kepegawaian') ? 'selected' : '';
                                    echo '<option '.$select.' value="' . $x->SPES_NIPBARU . '">' . $x->SPES_PNSNAM . '</option>';
                                }
                                ?>
                            </select> 
                        </div-->
                        <div class="form-group col-sm-6">
                            <label>Pejabat yang Berwenang </label>
                            <select class="form-control" name="cuti_pjb_berwenang">
                                <?php
                                $dataSpesimen = get_spesimen();
                                foreach ($dataSpesimen as $x) {
                                    $select = $x->SPES_NIPBARU == getPengaturan('cuti_pjb_berwenang') ? 'selected' : '';
                                    echo '<option '.$select.' value="' . $x->SPES_NIPBARU . '">' . $x->SPES_PNSNAM . '</option>';
                                }
                                ?>
                            </select> 
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="clearfix"></div>
<script>
    $("#maksimal").val(<?php echo getPengaturan('cuti_sisa_maksimal'); ?>);
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pengaturan/cuti'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('admin/pengaturan/cuti'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>