<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Spesimen Penandatangan</h4>
            </div>
            <div class="modal-body">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="SPES_ID" name="id">
                    </div>       
                    <div class="form-group col-lg-4">
                        <label>NIP</label>
                        <input class="form-control" name="SPES_NIPBARU" id="SPES_NIPBARU" maxlength="18" placeholder="Ketik NIP atau Nama">
                    </div>
                    <div class="form-group col-lg-8">
                        <label>Nama Spesimen</label>
                        <input class="form-control" name="SPES_PNSNAM" id="SPES_PNSNAM" style="text-transform: none">
                    </div>     
                    <div class="form-group col-lg-12">
                        <label>Nama Jabatan</label>
                        <input class="form-control" name="SPES_NAMAJAB" id="SPES_NAMAJAB">
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Format Spesimen</label>
                        <textarea class="form-control" name="SPES_FORMAT" id="SPES_FORMAT" style="height: 200px;text-align: center"></textarea>
                    </div>
                    <div class="clearfix"></div>
                </form>           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="simpan">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="pull-right">
            <div> 
                <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>
            </div>
        </li>
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Spesimen Penandatangan</a></li>
    </ul>
    <div class="tab-content n-a">  
        <div class="tab-pane active" id="tab_1">
            <table id="myDataTable" class="table">
                <thead>
                    <tr>
                        <th width='5%'>No</th>
                        <th width='20%'>NIP</th>
                        <th width='25%'>Nama</th>
                        <th>Nama Jabatan</th>
                        <th width='10%'>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>  
            </table>
        </div>
    </div>
</div>


<script>

    var vpegawai = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?= base_url('admin/json/json_pegawai'); ?>?q=y',
        remote: {
            url: '<?= base_url('admin/json/json_pegawai'); ?>?q=%QUERY',
            wildcard: '%QUERY',
            cache: false
        }
    });
    $('#SPES_NIPBARU').typeahead(null, {
        name: 'pegawai',
        display: 'PNS_NIPBARU',
        limit: 15,
        source: vpegawai,
        templates: {
            empty: [
                '<div class="tt-suggestion">',
                'DATA TIDAK DITEMUKAN',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return '<p><strong>' + data.PNS_NIPBARU + '</strong><br><u> ' + data.PNS_PNSNAM + '</u></p>';
            }
        }
    }).on('typeahead:selected', function (event, data) {
        $('#SPES_NIPBARU').val(data.PNS_NIPBARU);
        $('#SPES_PNSNAM').val((data.PNS_GLRDPN == null || data.PNS_GLRDPN == '' ? '' : data.PNS_GLRDPN + ' ') + data.PNS_PNSNAM + (data.PNS_GLRBLK == null || data.PNS_GLRBLK == '' ? '' : ', ' + data.PNS_GLRBLK));
        $('#SPES_NAMAJAB').val(data.RWJAB_NAMAJAB);
        format();
    });

    $("#myForm *").attr("disabled", false);
    $("#tambah").on("click", function () {
        $("#SPES_ID").val('');
        $("#SPES_FORMAT").html('');
        $('#myForm').trigger("reset");
        $("#myModal").modal('show');
    });
    function format() {
        var NIP = $('#SPES_NIPBARU').val();
        var formatNIP = NIP.substr(0, 8) + ' ' + NIP.substr(8, 6) + ' ' + NIP.substr(14, 1) + ' ' + NIP.substr(15, 3);
        $("#SPES_FORMAT").html($('#SPES_NAMAJAB').val() + '\n\n\n' + $('#SPES_PNSNAM').val() + '\nNIP. ' + formatNIP);
    }
    $("#simpan").click(function () {
        $("#myForm").submit();
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    $('#myDataTable').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<"F">t<"F">',
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": "<?php echo site_url('admin/pengaturan/spesimen/ajax'); ?>",
        "iDisplayLength": 100,
        "columns": [
            {"data": "SPES_ID", "class": "text-center", },
            {"data": "SPES_NIPBARU"},
            {"data": "SPES_PNSNAM"},
            {"data": "SPES_NAMAJAB"},
            {"data": "SPES_ID", "class": "text-center"},
        ],
        "order": [[1, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.SPES_ID + '">edit</a>\n\
                        <a onclick="hapus(\'' + data.SPES_ID + '\',\'' + data.SPES_ID + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
            $('td:eq(-1)', row).html(aksi);

        },
    });

    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pengaturan/spesimen/simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    //loadContent('<?php echo base_url(uri_string()); ?>');
                    $('#myDataTable').DataTable().ajax.reload();
                    $('#myModal').modal("hide");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });


    var oTable = $('#myDataTable').DataTable();
    $('#myDataTable tbody').on('click', '.edit', function () {
        var rowData = oTable.row($(this).closest("tr")).data();
        $('#myModal').modal("show");
        $('#myForm').trigger("reset");
        $("#SPES_ID").val(rowData.SPES_ID);
        $("#SPES_NIPBARU").val(rowData.SPES_NIPBARU);
        $("#SPES_PNSNAM").val(rowData.SPES_PNSNAM);
        $("#SPES_NAMAJAB").val(rowData.SPES_NAMAJAB);
        $("#SPES_FORMAT").html(rowData.SPES_FORMAT);
    });
    function hapus(id, nip) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('admin/pengaturan/spesimen/hapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": id, "nip": nip},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $('#myModal').modal("hide");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }
</script>