<style>
    form *{
        text-transform: none!important  ;
    }
</style>

<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">              
                <input id="input-foto" name="userfile" type="file" multiple class="file-loading">
                <div id="kv-error" style="margin-top:10px;display:none"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" id="close">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab" data-toggle="tab">Utama</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">simpan</button></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="tab">

                    <div class="form-group col-lg-6">
                        <label>Nama Organisasi</label>
                        <input class="form-control" name="main_nama_organisasi" value="<?= getPengaturan('main_nama_organisasi'); ?>">
                    </div>
                    <div class="form-group col-lg-5">
                        <label>Nama Organisasi (singkat)</label>
                        <input class="form-control" name="main_nama_organisasi_short" value="<?= getPengaturan('main_nama_organisasi_short'); ?>">
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Nama Aplikasi</label>
                        <input class="form-control" name="main_nama_aplikasi" value="<?= getPengaturan('main_nama_aplikasi'); ?>">
                    </div>
                    <div class="form-group col-lg-5">
                        <label>Nama Aplikasi (singkat)</label>
                        <input class="form-control" name="main_nama_aplikasi_short" value="<?= getPengaturan('main_nama_aplikasi_short'); ?>">
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="form-group col-lg-5 text-c">
                        <label>Logo Organisasi</label>
                        <div class="text-center">
                            <img src="<?php echo base_url('contents/img/'. getPengaturan('utama_logo_sk').'?'. @$_SESSION['logo_rand']); ?>"><br>
                            <button class="btn btn-flat btn-primary ganti-logo" name="logo" type="button">Ganti Logo</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </form>
</div>
<div class="clearfix"></div>
<script>
    $("#close").click(function () {
        $("#myModal").modal('hide');
        //$("#input-foto").fileinput('reset');
    })
    $('.ganti-logo').on('click', function () {
        $('#myModal').modal('show');
        //$('#foto_id').val($(this).attr('id'));
    })
    $("#input-foto").fileinput({
        showCaption: false,
        uploadUrl: "<?= base_url('admin/pengaturan/uploadLogo/'); ?>",
        autoReplace: true,
        maxFileSize: 2000,
        allowedFileTypes: ["image"],
        allowedFileExtensions: ["jpg", "jpeg", "png"],
        disableImageResize: false,
        resizeImage: true,
        maxImageHeight: 200,
        minFileCount: 1,
        maxFileCount: 1,
        elErrorContainer: '#kv-error',
    }).on('filebatchuploadcomplete', function (event, files, extra) {
        console.log('File batch upload complete');
        $('#myModal').modal('hide');
        setTimeout(function () {
            loadContent('<?php echo base_url(uri_string()); ?>');
        }, 500);
    });
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pengaturan/utama'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('admin/pengaturan/utama'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>