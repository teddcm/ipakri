<style>
    form *{
        text-transform: none!important  ;
    }
</style>

<div class="col-md-12">
    <form id="myForm"> 
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab" data-toggle="tab">Backup Database</a></li>
                <li class="pull-right header"><button class="btn btn-success btn-sm btn-flat">backup sekarang</button></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab">
                    <table class="table-bordered table btn-">
                        <tr><th>Waktu Backup</th><th>Ukuran</th><th style="width: 200px">Aksi</th></tr>
                        <?php
                        foreach ($filelist as $file) {
                            $name = explode('_', explode('.', $file['name'])[0]);
                            $date = convertDate(strtotime($name[1]), 'd M Y ');

                            echo "<tr>"
                            . "<td>" . $date . '&nbsp;&nbsp;&nbsp;' . str_replace('-', ':', $name[2]) . "</td>"
                            . "<td>" . floor($file['size'] / 1000) . " KB</td>"
                            . "<td> <a href='" . base_url('backups/' . $file['name']) . "' class='btn btn-xs btn-default'>download</a> <a href='#' onclick='hapusBackup(\"" . $file['name'] . "\")' class='btn btn-xs btn-danger'>hapus</a></td>"
                            . "</tr>";
                        }
                        ?>
                    </table>

                </div>
            </div>
        </div>
    </form>
</div>
<div class="clearfix"></div>
<script>
    $("#maksimal").val(<?php echo getPengaturan('cuti_sisa_maksimal'); ?>);
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pengaturan/backup'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: {post: "backup"},
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('admin/pengaturan/backup'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    function hapusBackup(namafile) {
        var r = confirm("Yakin akan menghapus file " + namafile + "?");
        if (r) {
            $.ajax({
                url: 'admin/pengaturan/backup',
                dataType: "json",
                type: "POST",
                data: {post: "delete", namefile: namafile},
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url('admin/pengaturan/backup'); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
        }
    }
</script>