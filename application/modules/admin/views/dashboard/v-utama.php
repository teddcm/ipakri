<?php
$this->load->view('pegawai/v-listing#search.php');
?>
<div class="box">
    <div class="box-body">
        <?php if (cek_jatah_cutinew(date('Y')) <= 0) { //belum pernah ada definisi cuti
            ?> 
            <div class="tab-pane active" id="tab_1">
                <div class="alert alert-warning col-lg-12">
                    Alokasi cuti untuk seluruh pegawai belum terdefinisi. Mohon <?php echo ($this->ion_auth->is_admin()) ? '' : 'menghubungi admin'; ?> untuk mengalokasikan hak cuti tahun <?php echo date('Y'); ?> <a href="#" onclick="loadContent('<?php echo base_url();?>admin/pegawai/Cuti/b801ad7c9ea047949f352b5fe98dcc9b')" class="text-black text-bold">DISINI</a><br/>
                </div>
            </div>
        <?php }
        if ($getKGB) {
            ?>
            <div class="col-lg-6 col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pegawai yang akan KGB</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover" width="100%">     
                            <tr>
                                <th class='text-center'>No</th>
                                <th class='text-center'>Nama</th>
                                <th class='text-center'>Tanggal KGB</th>
                                <th style="width:10%">Aksi</th>
                            </tr>
                            <?php
                            $n = 1;
                            foreach ($getKGB as $kgb) {
                                echo "<tr>"
                                . "<td class='text-center'>" . $n++ . "</td>"
                                . "<td>" . $kgb->PNS_PNSNAM . "</td>"
                                . "<td class='text-center'>" . $kgb->nextKGB . "</td>"
                                . "<td class='text-center'>"
                                . "<a href='#' onclick='loadContent(\"" . base_url('admin/pegawai/KGB/' . $kgb->PNS_NIPBARU . '/' . ($kgb->isDone ? '' : $kgb->nextKGB . '/' . $kgb->KGBthn)) . "\")'>"
                                . "<i class='fa fa-" . (!$kgb->isDone ? 'edit text-red' : 'check text-blue') . " ' title='" . (!$kgb->isDone ? 'Buat SK' : 'Lihat') . "'></i>"
                                . "</a>"
                                . "</td>"
                                . "</tr>";
                            }
                            ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
<?php } ?>
        <div class="col-lg-6 col-md-6 col-lg-offset-0 col-md-offset-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo $jumPeg; ?></h3>
                    <p>Jumlah Pegawai</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a class="linkmenu small-box-footer" href="#" onclick="loadContent('<?php echo base_url('admin/pegawai'); ?>')">Daftar pegawai <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Jenis Jabatan</h3>
                </div>
                <div class="box-body">

                    <div class="chart">
                        <canvas id="pieChart2" style="height: 10;"></canvas>
                    </div>
                    <!-- /.progress-group -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Jenis Kelamin</h3>
                </div>
                <div class="box-body">

                    <div class="chart">
                        <canvas id="pieChart1" style="height: 10;"></canvas>
                    </div>
                    <!-- /.progress-group -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!--div class="col-md-4">
            <div class="progress-group">
                <span class="progress-text">Jabatan Struktural</span>
                <span class="progress-number"><b><?php echo $jumPegStruktural; ?></b>/<?php echo $jumPeg; ?></span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: <?php echo @floor(($jumPegStruktural / $jumPeg) * 100); ?>%"></div>
                </div>
            </div>
            <div class="progress-group">
                <span class="progress-text">Jabatan Fungsional Tertentu</span>
                <span class="progress-number"><b><?php echo $jumPegJFT; ?></b>/<?php echo $jumPeg; ?></span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width:  <?php echo @floor(($jumPegJFT / $jumPeg) * 100); ?>%"></div>
                </div>
            </div>
            <div class="progress-group">
                <span class="progress-text">Jabatan Fungsional Umum</span>
                <span class="progress-number"><b><?php echo $jumPegJFU; ?></b>/<?php echo $jumPeg; ?></span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width:  <?php echo @floor(($jumPegJFU / $jumPeg) * 100); ?>%"></div>
                </div>
            </div>
        </div-->
        <!--div class="col-md-4">
            <div class="progress-group">
                <span class="progress-text">Pria</span>
                <span class="progress-number"><b><?php echo $jumPegPria; ?></b>/<?php echo $jumPeg; ?></span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-primary" style="width: <?php echo @floor(($jumPegPria / $jumPeg) * 100); ?>%"></div>
                </div>
            </div>
            <div class="progress-group">
                <span class="progress-text">Wanita</span>
                <span class="progress-number"><b><?php echo $jumPegWanita; ?></b>/<?php echo $jumPeg; ?></span>

                <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width:  <?php echo @floor(($jumPegWanita / $jumPeg) * 100); ?>%"></div>
                </div>
            </div>
        </div-->
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Sebaran Golongan</h3>
            </div>
            <div class="box-body">
                <div class="chart">
                    <canvas id="barChartGol" style="height: 10;"></canvas>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Sebaran Pendidikan</h3>
            </div>
            <div class="box-body">
                <div class="chart">
                    <canvas id="barChartDik" style="height: 10;"></canvas>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

</div>
<div class="clearfix"></div>
<script>
    var pieChartCanvas1 = $("#pieChart1").get(0).getContext("2d");
    var pieChart1 = new Chart(pieChartCanvas1);
    var PieData1 = [{
            value: <?php echo $jumPegPria; ?>,
            color: "lightblue",
            label: "Pria"
        }, {
            value: <?php echo $jumPegWanita; ?>,
            color: "pink",
            label: "Wanita"
        }, ];

    var pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    var pieChart2 = new Chart(pieChartCanvas2);
<?php $noDef = $jumPeg - ($jumPegStruktural + $jumPegJFT + $jumPegJFU); ?>
    var PieData2 = [
<?php
if (!empty($sebaranJenJab)) {
    $color = array('grey', '#dd4b39', '#00a65a', '#f39c12', '#00c0ef');
    foreach ($sebaranJenJab as $x) {
        echo""
        . "{"
        . "value:" . $x->jum . ","
        . "color:'" . @$color[$x->PNS_JNSJAB] . "',"
        . "label:'" . ($x->PNS_JNSJAB == 0 ? 'N/A' : convert_jenjab($x->PNS_JNSJAB, TRUE)) . "'"
        . "},";
    }
}
?>];


    var pieOptions = {
        percentageInnerCutout: 0,
        responsive: true,
        maintainAspectRatio: false,
    };
    pieChart1.Doughnut(PieData1, pieOptions);
    pieChart2.Doughnut(PieData2, pieOptions);





    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
    };
    barChartOptions.datasetFill = false;

    //CHART GOLONGAN
<?php
$jumgol = '[';
$labgol = '[';
$coma = '';
if (!empty($sebaranGolru)) {
    foreach ($sebaranGolru as $x) {
        $jumgol .= $coma . '"' . ($x->jum) . '"';
        $labgol .= $coma . '"' . convert_golongan($x->PNS_GOLRU) . '"';
        $coma = ',';
    }
}
$jumgol .= ']';
$labgol .= ']';
?>
    var chartDataGol = {
        labels: <?php echo $labgol; ?>,
        datasets: [
            {
                data: <?php echo $jumgol; ?>,
            }
        ]
    };
    var barChartCanvasGol = $("#barChartGol").get(0).getContext("2d");
    var barChartGol = new Chart(barChartCanvasGol)
    chartDataGol.datasets[0].fillColor = "green";
    chartDataGol.datasets[0].strokeColor = "green";
    chartDataGol.datasets[0].pointColor = "green";
    barChartGol.Bar(chartDataGol, barChartOptions);




    //CHART PENDIDIKAN
<?php
$jumdik = '[';
$labdik = '[';
$coma = '';
if (!empty($sebaranPendidikan)) {
    foreach ($sebaranPendidikan as $x) {
        $jumdik .= $coma . '"' . ($x->jum) . '"';
        $labdik .= $coma . '"' . ($x->PNS_TKTDIK == '' ? 'N/A' : convert_tktpendik($x->PNS_TKTDIK)) . '"';
        $coma = ',';
    }
}
$jumdik .= ']';
$labdik .= ']';
?>
    var chartDataDik = {
        labels: <?php echo $labdik; ?>,
        datasets: [
            {
                data: <?php echo $jumdik; ?>,
            }
        ]
    };
    var barChartCanvasDik = $("#barChartDik").get(0).getContext("2d");
    var barChartDik = new Chart(barChartCanvasDik)
    chartDataDik.datasets[0].fillColor = "red";
    chartDataDik.datasets[0].strokeColor = "red";
    chartDataDik.datasets[0].pointColor = "red";
    barChartDik.Bar(chartDataDik, barChartOptions);


</script>