
<div class="modal fade" id="modal_duk" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content modal-info">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor_duk"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select_duk">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Urut Kepangkatan</h3>
        </div>
        <div class="box-body">
            <input type="hidden" id="IDUNO_duk" value="">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>Data</b></span>
                <select class="form-control" id="FULLDATA_duk">
                    <option value="0">Terakhir</option>
                    <option value="1">Lengkap</option>
                </select>
                <span class="input-group-addon"><b>Unor</b></span>
                <input class="form-control cariLokasi_duk" type="text"  id="NAMUNO_duk" placeholder=". . . . .">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" id="cetak_duk">Cetak</button>
                </span>
            </div>
        </div>
    </div>
</div>

<script>

    $('#pilih_unor_duk').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select_duk').click(function () {
        var node = $('#pilih_unor_duk').tree('getSelectedNode');
        $("#NAMUNO_duk").val(node.name);
        $("#IDUNO_duk").val(node.id);
        $('#modal_duk').modal('hide');
    })
    $('.cariLokasi_duk').on('focusin', function (e) {
        $('#modal_duk').modal('show')
        $("#cariLokasi_duk *").blur();
        e.preventDefault();
    })

    $('#cetak_duk').click(function () {
        if ($("#IDUNO_duk").val().length > 0) {
            window.open('<?php echo base_url('admin/report/duk'); ?>/' + $("#FULLDATA_duk").val() + '/' + $("#IDUNO_duk").val(), 'duk').focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>

<div class="modal fade" id="modal_nominatif" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content modal-warning">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor_nominatif"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select_nominatif">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar Nominatif</h3>
        </div>
        <div class="box-body ">
            <input type="hidden" id="IDUNO_nominatif" value="">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>Data</b></span>
                <select class="form-control" id="FULLDATA_nominatif">
                    <option value="0">Terakhir</option>
                    <option value="1">Lengkap</option>
                </select>
                <span class="input-group-addon"><b>Unor</b></span>
                <input class="form-control cariLokasi_nominatif form" type="text" id="NAMUNO_nominatif" placeholder=". . . . .">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-warning btn-flat" id="cetak_nominatif">Cetak</button>
                </span>
            </div>
        </div>
    </div>
</div>

<script>

    $('#pilih_unor_nominatif').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select_nominatif').click(function () {
        var node = $('#pilih_unor_nominatif').tree('getSelectedNode');
        $("#NAMUNO_nominatif").val(node.name);
        $("#IDUNO_nominatif").val(node.id);
        $('#modal_nominatif').modal('hide');
    })
    $('.cariLokasi_nominatif').on('focusin', function (e) {
        $('#modal_nominatif').modal('show')
        $("#cariLokasi_nominatif *").blur();
        e.preventDefault();
    })
    $('#cetak_nominatif').click(function () {
        if ($("#IDUNO_nominatif").val().length > 0) {
            window.open('<?php echo base_url('admin/report/nominatif'); ?>/' + $("#FULLDATA_nominatif").val() + '/' + $("#IDUNO_nominatif").val(), 'nominatif').focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>


<div class="modal fade" id="modal_jabatan" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content modal-danger">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor_jabatan"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select_jabatan">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar jabatan</h3>
        </div>
        <div class="box-body ">
            <input type="hidden" id="IDUNO_jabatan" value="">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>Jabatan</b></span>
                <select class="form-control" id="FULLDATA_jabatan">
                    <option value="1">Struktural</option>
                    <option value="2">Fungsional Tertentu</option>
                    <option value="4">Fungsional Umum</option>
                </select>
                <span class="input-group-addon"><b>Unor</b></span>
                <input class="form-control cariLokasi_jabatan form" type="text" id="NAMUNO_jabatan" placeholder=". . . . .">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-danger btn-flat" id="cetak_jabatan">Cetak</button>
                </span>
            </div>

        </div>
    </div>
</div>

<script>

    $('#pilih_unor_jabatan').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select_jabatan').click(function () {
        var node = $('#pilih_unor_jabatan').tree('getSelectedNode');
        $("#NAMUNO_jabatan").val(node.name);
        $("#IDUNO_jabatan").val(node.id);
        $('#modal_jabatan').modal('hide');
    })
    $('.cariLokasi_jabatan').on('focusin', function (e) {
        $('#modal_jabatan').modal('show')
        $("#cariLokasi_jabatan *").blur();
        e.preventDefault();
    })
    $('#cetak_jabatan').click(function () {
        if ($("#IDUNO_jabatan").val().length > 0) {
            window.open('<?php echo base_url('admin/report/jabatan'); ?>/' + $("#FULLDATA_jabatan").val() + '/' + $("#IDUNO_jabatan").val(), 'jabatan').focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>



<div class="modal fade" id="modal_kgb" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content modal-success">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor_kgb"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select_kgb">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Daftar pegawai yang akan KGB</h3>
        </div>
        <div class="box-body ">
            <input type="hidden" id="IDUNO_kgb" value="">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>Unor</b></span>
                <input class="form-control cariLokasi_kgb form" type="text" id="NAMUNO_kgb" placeholder=". . . . .">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success btn-flat" id="cetak_kgb">Cetak</button>
                </span>
            </div>

        </div>
    </div>
</div>

<script>

    $('#pilih_unor_kgb').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select_kgb').click(function () {
        var node = $('#pilih_unor_kgb').tree('getSelectedNode');
        $("#NAMUNO_kgb").val(node.name);
        $("#IDUNO_kgb").val(node.id);
        $('#modal_kgb').modal('hide');
    })
    $('.cariLokasi_kgb').on('focusin', function (e) {
        $('#modal_kgb').modal('show')
        $("#cariLokasi_kgb *").blur();
        e.preventDefault();
    })
    $('#cetak_kgb').click(function () {
        if ($("#IDUNO_kgb").val().length > 0) {
            window.open('<?php echo base_url('admin/report/kgb'); ?>/' + $("#IDUNO_kgb").val(), 'kgb').focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>


<div class="modal fade" id="modal_cuti" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content modal-info">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor_cuti"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select_cuti">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">CUTI</h3>
        </div>
        <div class="box-body">
            <input type="hidden" id="IDUNO_cuti" value="">
            <div class="input-group input-group-sm">
                <span class="input-group-addon"><b>Data</b></span>
                <select class="form-control" id="FULLDATA_cuti">
                    <?php
                    for ($x = date('Y'); $x >= date('Y') - 5; $x--) {
                        echo "<option value='$x'>$x</option>";
                    }
                    ?>
                </select>
                <span class="input-group-addon"><b>Unor</b></span>
                <input class="form-control cariLokasi_cuti" type="text"  id="NAMUNO_cuti" placeholder=". . . . .">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" id="cetak_cuti">Cetak</button>
                </span>
            </div>
        </div>
    </div>
</div>

<script>

    $('#pilih_unor_cuti').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select_cuti').click(function () {
        var node = $('#pilih_unor_cuti').tree('getSelectedNode');
        $("#NAMUNO_cuti").val(node.name);
        $("#IDUNO_cuti").val(node.id);
        $('#modal_cuti').modal('hide');
    })
    $('.cariLokasi_cuti').on('focusin', function (e) {
        $('#modal_cuti').modal('show')
        $("#cariLokasi_cuti *").blur();
        e.preventDefault();
    })

    $('#cetak_cuti').click(function () {
        if ($("#IDUNO_cuti").val().length > 0) {
            window.open('<?php echo base_url('admin/report/cuti'); ?>/' + $("#FULLDATA_cuti").val() + '/' + $("#IDUNO_cuti").val(), 'cuti').focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>
<style>
    .jqtree-title{
        color: white!important;
    }
</style>
<div class="clearfix"></div>
