
<div class="modal fade" id="modalx" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Pilih Unit Organisasi</h4>
                </div>
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default btn-sm" id="select">Pilih</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Cetak Report</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-7 ">
                    <div class="form-group has-error">
                        <input type="hidden" id="IDUNO" value="">
                        <label><b>Pilih Unit Organisasi</b></label>
                        <input class="form-control cariLokasi" type="text"  id="NAMUNO" placeholder=". . . . .">
                    </div>
                    <br>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-6">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Nominatif Pegawai</td>
                                <td>:</td>
                                <td>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><b>Data</b></span>
                                        <select class="form-control" id="FULLDATA_nominatif">
                                            <option value="0">Terakhir</option>
                                            <option value="1">Lengkap</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning btn-flat cetak" id="nominatif">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis Jabatan</td>
                                <td>:</td>
                                <td>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><b>Jabatan</b></span>
                                        <select class="form-control" id="FULLDATA_jabatan">
                                            <option value="1">Struktural</option>
                                            <option value="2">Fungsional Tertentu</option>
                                            <option value="4">Fungsional Umum</option>
                                        </select>

                                    </div>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-flat cetak" id="jabatan">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Daftar Urut Kepangkatan</td>
                                <td>:</td>
                                <td>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><b>Data</b></span>
                                        <select class="form-control" id="FULLDATA_duk">
                                            <option value="0">Terakhir</option>
                                            <option value="1">Lengkap</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat cetak" id="duk">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Cuti Pegawai</td>
                                <td>:</td>
                                <td>
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><b>Data</b></span>
                                        <select class="form-control" id="FULLDATA_cuti">
                                            <?php
                                            for ($x = date('Y'); $x >= date('Y') - 5; $x--) {
                                                echo "<option value='$x'>$x</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>                    
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-github btn-flat cetak" id="cuti">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kenaikan Gaji Berkala</td>
                                <td>:</td>
                                <td>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-success btn-flat cetak" id="kgb">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Pensiun</td>
                                <td>:</td>
                                <td>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-bitbucket btn-flat cetak" id="pensiun">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Kenaikan Pangkat</td>
                                <td>:</td>
                                <td>
                                </td>
                                <td>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-vk btn-flat cetak" id="kp">Cetak</button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('#pilih_unor').tree({
        dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
    });
    $('#select').click(function () {
        var node = $('#pilih_unor').tree('getSelectedNode');
        $("#NAMUNO").val(node.name);
        $("#IDUNO").val(node.id);
        $('#modalx').modal('hide');
    })
    $('.cariLokasi').on('focusin', function (e) {
        $('#modalx').modal('show')
        $("#cariLokasi *").blur();
        e.preventDefault();
    })

    $('.cetak').click(function () {
        var did = (this.id);
        if ($("#IDUNO").val().length > 0) {
            window.open('<?php echo base_url('admin/report/'); ?>' + did + '/' + $("#FULLDATA_" + did).val() + '/' + $("#IDUNO").val(), (did + $("#FULLDATA_" + did).val())).focus();
        } else {
            $.notify('Silakan pilih dahulu Unit Organisasi', 'error');
        }
    })
</script>
<div class="clearfix"></div>
