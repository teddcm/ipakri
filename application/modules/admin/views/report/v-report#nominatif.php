<body>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <title>Report Nominatif</title>
    <div class="box">
        <div style="text-align: center">
            <h4 class="center">
                DAFTAR NOMINATIF PEGAWAI<br>
                <?php echo strtoupper(convert_unor(SKPD)); ?><br>
                <?php echo strtoupper(convert_instansi(INSTANSI_KERJA)); ?>
            </h4>
        </div>
        <span class="no-print">
            &nbsp<a class="btn btn-sm btn-success export">EKSPOR</a>
            &nbsp<a onclick="window.print()" class="btn btn-sm btn-danger">CETAK</a>

        </span>
        <div class="clearfix"></div><br>
        <table id="myTable" class="table table-bordered" border="1">
            <thead>
                <tr>
                    <th rowspan="2">No.</th>
                    <th rowspan="2">Nama</th>
                    <th colspan="2">NIP</th>
                    <th rowspan="2">JK</th>
                    <th colspan="2">Lahir</th>
                    <th colspan="2">Pangkat</th>
                    <th colspan="3">Jabatan</th>
                    <th colspan="2">Masa Kerja</th>
                    <th colspan="2">Diklat Jabatan</th>
                    <th colspan="3">Pendidikan</th>
                    <th colspan="2">Catatan Mutasi</th>
                </tr>
                <tr style="background-color: #e3e3e3;">
                    <th>Lama</th>
                    <th>Baru</th>
                    <th>Tempat</th>
                    <th>Tanggal</th>
                    <th>Golru</th>
                    <th>TMT</th>
                    <th>Nama</th>
                    <th>Eselon</th>
                    <th>TMT</th>
                    <th>Tahun</th>
                    <th>Bulan</th>
                    <th>Nama</th>
                    <th>Tahun</th>
                    <th>Nama</th>
                    <th>Lulus</th>
                    <th>Tingkat</th>
                    <th>Unit Organisasi</th>
                    <th>Tahun</th>
                </tr>
                <tr style="background-color: #e3e3e3;">
                    <?php
                    for ($x = 1; $x <= 21; $x++)
                        echo "<th>" . $x . "</th>";
                    ?>

                </tr>
            </thead>
            <tbody>
                <?php
                $bgclr = array('', '#808080', '#808080', '#C0C0C0', '#F8F8F8');
                if (!empty($data))
                    foreach ($data as $x) {
                        echo "<tr>";
                        if (isset($x->CLASS)) {
                            if ($x->CLASS == 1) {
                                $maxrow = $this->uri->segment(4) == 1 ? $x->MAXROW : 1;
                                echo "<td rowspan='" . $maxrow . "'>" . @$x->NO . "</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @$x->PNS_PNSNAM . "</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @$x->PNS_PNSNIP . "</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @$x->PNS_NIPBARU . "&nbsp;</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @($x->PNS_PNSSEX == 1 ? 'Pria' : 'Wanita') . "</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @convert_tempat_lahir($x->PNS_TEMLHR) . "</td>";
                                echo "<td rowspan='" . $maxrow . "'>" . @$x->PNS_TGLLHRDT . "</td>";
                                $span = 0;
                            } else if ($x->CLASS == 99) {
                                echo "<td colspan='21' style='background-color:" . $bgclr[@$x->ESL] . "'>" . @$x->UNOR . "</b></td>";
                                $span = 1;
                            }
                        } else {
                            $span = 0;
                        }
                        if ($span == 0) {
                            echo "<td>" . @$x->RWGOL_KDGOL . "</td>";
                            echo "<td>" . @$x->RWGOL_TMTGOL . "</td>";
                            echo "<td>" . @$x->RWJAB_NAMAJAB . "</td>";
                            echo "<td>" . @$x->RWJAB_IDESL . "</td>";
                            echo "<td>" . @$x->RWJAB_TMTJAB . "</td>";
                            echo "<td>" . @$x->PNS_THNKER . "</td>";
                            echo "<td>" . @$x->PNS_BLNKER . "</td>";
                            echo "<td>" . @$x->RWDLT_NAMDLT . "</td>";
                            echo "<td>" . @$x->RWDLT_THN . "</td>";
                            echo "<td>" . @$x->RWDIK_IDDIK . "</td>";
                            echo "<td>" . @$x->RWDIK_THNLULUS . "</td>";
                            echo "<td>" . @$x->RWDIK_TKTDIK . "</td>";
                            echo "<td>" . @$x->UNO_NAMUNO . "</td>";
                            echo "<td>" . @$x->RWUNOR_TGLSK . "</td>";
                        }

                        echo "</tr>";
                    }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="21" style="text-align: right">
                        dicetak pada : <?php echo strtoupper(date("d-m-Y h:i a")); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <style>
        body {
            zoom: 0.95; /* Other non-webkit browsers */
            zoom: 95%; /* Webkit browsers */ 


        }
        @media print {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
        table{
            -webkit-print-color-adjust: exact;
        }
        th{
            padding: 1px !important;
            font-size: 11;
            vertical-align: middle!important;
            text-align: center;
            background-color: #e3e3e3; 
            border-color: white !important;
        }
        td { 
            padding: 3px !important;
            font-size: 9;

        }
        .mytd{
            white-space: nowrap;
        }
        tr.first td {

            border-top:1.5pt solid black !important    ;
        }
        .ctr{
            text-align: center;
        }@media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
    </style>
    <script>

        $(document).ajaxStart(function () {
            $("#wait").css("display", "block");
        });
        $(document).ajaxStop(function () {
            setTimeout(function () {
                $("#wait").css("display", "none");
            }, 10);
        });

        $(document).ready(function () {
            $(".export").click(function (e) {
                e.preventDefault();

                var tab_text = "<table border='2px'><tr>";
                var textRange;
                var j = 0;
                tab = document.getElementById('myTable'); // id of table

                for (j = 0; j < tab.rows.length; j++)
                {
                    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                    //tab_text=tab_text+"</tr>";
                }

                tab_text = tab_text + "</table>";
                tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // removes input params

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                var a = document.createElement('a');
                a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
                a.download = 'NOMINATIF_<?php echo date("Ymd"); ?>.xls';
                document.body.appendChild(a)
                a.click();
            });
        });
    </script>
    <div id="wait" style=" z-index:1050;display:none;width:100%;height:100%;position:fixed;top:0%;right:0%;background: rgba( 250, 250, 250, 0.5 );cursor: progress;">
        <!--img src='<?php echo base_url('matrix/img/dist/ajaxLoader.gif'); ?>' style="width: 150px;position:fixed;top:5%;right:1%;" /-->
    </div>
</body>