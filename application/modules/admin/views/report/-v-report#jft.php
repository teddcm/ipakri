<?php
$LIMIT = safe_decode($this->uri->segment(4));
$OFFSET = safe_decode($this->uri->segment(5));
?>

<body>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <div class="box">
        <div style="text-align: center">
            <h4 class="center">
                DAFTAR PEGAWAI JFT<br>
                <?php echo strtoupper(convert_unor(SKPD)); ?><br>
                <?php echo strtoupper(convert_instansi(INSTANSI_KERJA)); ?>
            </h4>
        </div>
        <span class="no-print">
            &nbsp<a class="btn btn-sm btn-success export">EKSPOR</a>
            &nbsp<a onclick="window.print()" class="btn btn-sm btn-danger">CETAK</a>
            &nbsp<a href="<?php echo $OFFSET > 0 ? (base_url('admin/report/jft/') . safe_encode($LIMIT) . '/' . safe_encode(0)) : '#'; ?>"  class="<?php echo $OFFSET > 0 ? "" : "disabled"; ?> btn btn-sm btn-primary">< FIRST</a>
            &nbsp<a href="<?php echo $OFFSET > 0 ? (base_url('admin/report/jft/') . safe_encode($LIMIT) . '/' . safe_encode(($OFFSET - $LIMIT))) : '#'; ?>"  class="<?php echo $OFFSET > 0 ? "" : "disabled"; ?> btn btn-sm btn-primary"><<<</a>
            &nbsp<a class="btn btn-sm btn-default disabled"><?php echo ($OFFSET + 1) . "-" . ($LIMIT + $OFFSET < $jumPeg ? $LIMIT + $OFFSET : $jumPeg); ?></a>
            &nbsp<a href="<?php echo $OFFSET + $LIMIT < $jumPeg ? (base_url('admin/report/jft/') . safe_encode($LIMIT) . '/' . safe_encode(($OFFSET + $LIMIT))) : "#"; ?>" class="<?php echo $OFFSET + $LIMIT < $jumPeg ? "" : "disabled"; ?> btn btn-sm btn-primary">>>></a>
            &nbsp<a href="<?php echo $OFFSET + $LIMIT < $jumPeg ? (base_url('admin/report/jft/') . safe_encode($LIMIT) . '/' . safe_encode($LIMIT == 0 ? 0 : (floor($jumPeg / $LIMIT) * $LIMIT))) : "#"; ?>" class="<?php echo $OFFSET + $LIMIT < $jumPeg ? "" : "disabled"; ?> btn btn-sm btn-primary">LAST ></a>
        </span>
        <div class="clearfix"></div><br>
        <table id="myTable" class="table table-bordered" border="1">
            <thead>
                <tr style="background-color: #e3e3e3;  ">
                    <th rowspan="2">No.</th>
                    <th rowspan="2">Nama</th>
                    <th colspan="2">NIP</th>
                    <th rowspan="2">JK</th>
                    <th colspan="2">Lahir</th>
                    <th rowspan="2">Pangkat</th>
                    <th rowspan="2">Golru</th>
                    <th rowspan="2">TMT Golru</th>
                    <th colspan="2">Jabatan</th>
                </tr>
                <tr style="background-color: #e3e3e3;">
                    <th>Lama</th>
                    <th>Baru</th>
                    <th>Tempat</th>
                    <th>Tanggal</th>
                    <th>Nama</th>
                    <th>TMT</th>
                </tr>
                <tr style="background-color: #e3e3e3;">
                    <?php
                    for ($x = 1; $x <= 12; $x++)
                        echo "<th>" . $x . "</th>";
                    ?>

                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <style>
        body {
            zoom: 0.95; /* Other non-webkit browsers */
            zoom: 95%; /* Webkit browsers */ 
        }
        @media print {    
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
        table{
            -webkit-print-color-adjust: exact;
        }
        th{
            padding: 1px !important;
            font-size: 11;
            vertical-align: middle!important;
            text-align: center;
            background-color: #e3e3e3;  
        }
        td { 
            padding: 3px !important;
            font-size: 9;

        }
        .mytd{
            white-space: nowrap;
        }
        tr.first td {

            border-top:1.5pt solid black !important    ;
        }
    </style>
    <script>

        $(document).ajaxStart(function () {
            $("#wait").css("display", "block");
        });
        $(document).ajaxStop(function () {
            setTimeout(function () {
                $("#wait").css("display", "none");
            }, 10);
        });

        $(function () {
            $.getJSON("<?php echo base_url("admin/report/getjft/") . $LIMIT . '/' . $OFFSET; ?>", function (response) {
                var count = 0;
                $.each(response, function (i, item) {
                    count++;
                    var tr = item.CLASS == 1 ? $('<tr class="first">') : $('<tr>');
                    var $tr = tr.addClass(item.CLASS).append(
                            $('<td style="text-align: center;">').text(item.NO),
                            $('<td>').text(item.PNS_PNSNAM),
                            $('<td>').text(item.PNS_PNSNIP),
                            $('<td>').text(item.PNS_NIPBARU),
                            $('<td>').text(item.PNS_PNSSEX == 1 ? 'L' : 'P'),
                            $('<td>').text(item.PNS_TEMLHR),
                            $('<td style="text-align: center;">').text(item.PNS_TGLLHRDT),
                            $('<td>').text(item.PNS_PANGKAT),
                            $('<td style="text-align: center;">').text(item.PNS_GOLRU),
                            $('<td>').text(item.PNS_TMTGOL),
                            $('<td>').text(item.RWJAB_NAMAJAB),
                            $('<td style="text-align: center;">').text(item.RWJAB_TMTJAB)
                            ).appendTo('#myTable tbody');
                    //console.log($tr.wrap('<p>').html());
                });

                $("td").css("white-space", "nowrap");
            });
        });

        $(document).ready(function () {
            $(".export").click(function (e) {
                e.preventDefault();

                var tab_text = "<table border='2px'><tr>";
                var textRange;
                var j = 0;
                tab = document.getElementById('myTable'); // id of table

                for (j = 0; j < tab.rows.length; j++)
                {
                    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                    //tab_text=tab_text+"</tr>";
                }

                tab_text = tab_text + "</table>";
                tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // removes input params

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                var a = document.createElement('a');
                a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
                a.download = 'JFT_<?php echo ($OFFSET + 1) . "-" . ($LIMIT + $OFFSET < $jumPeg ? $LIMIT + $OFFSET : $jumPeg) . '_' . date("Ymd"); ?>.xls';
                document.body.appendChild(a)
                a.click();
            });
        });
    </script>
    <div id="wait" style=" z-index:1050;display:none;width:100%;height:100%;position:fixed;top:0%;right:0%;background: rgba( 250, 250, 250, 0.5 );cursor: progress;">
        <!--img src='<?php echo base_url('matrix/img/dist/ajaxLoader.gif'); ?>' style="width: 150px;position:fixed;top:5%;right:1%;" /-->
    </div>
</body>