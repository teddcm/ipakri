<html moznomarginboxes mozdisallowselectionprint>
    <body>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bootstrap/css/bootstrap.min.css">
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
        <title>Report CUTI</title>
        <div class="box">
            <div style="text-align: center">
                <h4 class="center">
                    DAFTAR CUTI PEGAWAI<br>
                <?php echo strtoupper(convert_unor(SKPD)); ?><br>
                <?php echo strtoupper(convert_instansi(INSTANSI_KERJA)); ?>
                    TAHUN <?php echo $this->uri->segment(4); ?>
                </h4>
            </div>
            <span class="no-print">
                &nbsp<a class="btn btn-sm btn-success export">EKSPOR</a>
                &nbsp<a onclick="window.print()" class="btn btn-sm btn-danger">CETAK</a>

            </span>
            <div class="clearfix"></div><br>
            <table id="myTable" class="table table-bordered" border="1">
                <thead>

                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Nama</th>
                        <th colspan="2">NIP</th>
                        <th colspan="4">Cuti Tahunan</th>
                        <th rowspan="2" width="12%">non Cuti Tahunan</th>
                    </tr>
                    <tr>
                        <th>Lama</th>
                        <th>Baru</th>
                        <th width="12%">Sisa Tahun Lalu</th>
                        <th width="12%">Jatah Tahun Ini</th>
                        <th width="12%">Terpakai</th>
                        <th width="12%">Tersisa</th>
                    </tr>
                    <tr style="background-color: #e3e3e3;">
                        <?php
                        for ($x = 1; $x <= 9; $x++)
                            echo "<th>" . $x . "</th>";
                        ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($data))
                        foreach ($data as $x) {
                            echo "<tr>";
                            echo "<td>" . @$x->NO . "</td>";
                            echo "<td>" . @$x->PNS_PNSNAM . "</td>";
                            echo "<td>" . @$x->PNS_PNSNIP . "</td>";
                            echo "<td>" . @$x->PNS_NIPBARU . "&nbsp;</td>";
                            echo "<td>" . @$x->sisa_tahun_lalu . "</td>";
                            echo "<td>" . (@$x->jatah_tahun_ini + 12) . "</td>";
                            echo "<td>" . @$x->cuti_tertanggung . "</td>";
                            echo "<td>" . (@$x->sisa_cuti_tertanggung + 12) . "</td>";
                            echo "<td>" . @$x->cuti_tak_tertanggung . "</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12" style="text-align: right">
                            dicetak pada : <?php echo strtoupper(date("d-m-Y h:i a")); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <style>
            body {
                zoom: 0.95; /* Other non-webkit browsers */
                zoom: 95%; /* Webkit browsers */ 
            }
            @media print {    
                .no-print, .no-print *
                {
                    display: none !important;
                }
            }
            table{
                -webkit-print-color-adjust: exact;
            }
            th{
                padding: 1px !important;
                font-size: 11;
                vertical-align: middle!important;
                text-align: center;
                background-color: #e3e3e3; 
                border-color: white !important;
            }
            td { 
                padding: 3px !important;
                font-size: 9;

            }
            .mytd{
                white-space: nowrap;
            }
            tr.first td {

                border-top:1.5pt solid black !important    ;
            }
            .ctr{
                text-align: center;
            }@media print {
                @page { margin: 0; }
                body { margin: 1.6cm; }
            }

        </style>

        <script>
            $('table td:not(:first-child)').each(function () {
                if (parseInt($(this).text()) < 0) {
                    $(this).css({'font-weight': 'bold', 'font-size': '12', 'color': 'red'});
                } else if (parseInt($(this).text()) > 0 && parseInt($(this).text()) < 100) {
                    $(this).css({'font-weight': 'bold', 'font-size': '12', 'color': 'green'});
                } else if (parseInt($(this).text()) == 0) {
                    $(this).css({'font-weight': 'bold', 'font-size': '12', 'color': 'blue'});
                }
            });
            $(document).ajaxStart(function () {
                $("#wait").css("display", "block");
            });
            $(document).ajaxStop(function () {
                setTimeout(function () {
                    $("#wait").css("display", "none");
                }, 10);
            });

            $(document).ready(function () {
                $(".export").click(function (e) {
                    e.preventDefault();

                    var tab_text = "<table border='2px'><tr>";
                    var textRange;
                    var j = 0;
                    tab = document.getElementById('myTable'); // id of table

                    for (j = 0; j < tab.rows.length; j++)
                    {
                        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                        //tab_text=tab_text+"</tr>";
                    }

                    tab_text = tab_text + "</table>";
                    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // removes input params

                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");

                    var a = document.createElement('a');
                    a.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text);
                    a.download = 'CUTI_<?php echo $this->uri->segment(4); ?>_<?php echo date("Ymd"); ?>.xls';
                    document.body.appendChild(a)
                    a.click();
                });
            });
        </script>
        <div id="wait" style=" z-index:1050;display:none;width:100%;height:100%;position:fixed;top:0%;right:0%;background: rgba( 250, 250, 250, 0.5 );cursor: progress;">
            <!--img src='<?php echo base_url('matrix/img/dist/ajaxLoader.gif'); ?>' style="width: 150px;position:fixed;top:5%;right:1%;" /-->
        </div>
    </body>
</html>