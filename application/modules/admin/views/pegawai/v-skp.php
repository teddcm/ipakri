<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat SKP</a></li>
        </ul>
        <div class="tab-content n-a">  
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>             
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWSKP_NIP" name="RWSKP_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWSKP_ID" name="RWSKP_ID">
                        <input type="hidden" id="RWSKP_NJUMLAH" name="RWSKP_NJUMLAH">
                        <input type="hidden" id="RWSKP_NRATA" name="RWSKP_NRATA">
                        <input type="hidden" id="mode" name="mode" value="edit" >         
                    </div>       

                    <div class="col-lg-10">
                        <div class="row">

                            <div class="form-group col-lg-6">
                                <label>* Jenis Jabatan</label>
                                <select class="form-control" name="RWSKP_IDJENJAB" id="RWSKP_IDJENJAB">
                                    <?php
                                    foreach ($jenjab as $x) {
                                        if ($x->JJB_JJBKOD != 3)
                                            echo '<option value="' . $x->JJB_JJBKOD . '">' . $x->JJB_JJBNAM . '</option>';
                                    }
                                    ?>
                                </select>  
                            </div>
                            <div class="form-group col-lg-3">
                                <label>* Tahun</label>
                                <input class="form-control" name="RWSKP_THN" id="RWSKP_THN" size="4">
                            </div>
                            <div class="form-group col-lg-3 has-warning">
                                <label>Nilai SKP</label>
                                <div class="input-group">
                                    <input class="form-control nilaiskp" name="RWSKP_NSKP" id="RWSKP_NSKP">
                                    <span class="input-group-addon">60%</span>
                                    <input class="form-control"  id="RWSKP_NSKPX" >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="form-group col-lg-4 has-error">
                                <label>Orientasi Pelayanan</label>
                                <div class="input-group">
                                    <input class="form-control nilai" name="RWSKP_NPELAYANAN" id="RWSKP_NPELAYANAN">
                                    <span class="input-group-addon informasi" style="width:70%" id="RWSKP_NPELAYANANX"></span>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 has-error">
                                <label>Komitmen</label>
                                <div class="input-group">
                                    <input class="form-control nilai" name="RWSKP_NKOMITMEN" id="RWSKP_NKOMITMEN">
                                    <span class="input-group-addon informasi" style="width:70%" id="RWSKP_NKOMITMENX"></span>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 has-error">
                                <label>Kerjasama</label>
                                <div class="input-group">
                                    <input class="form-control nilai" name="RWSKP_NKERJASAMA" id="RWSKP_NKERJASAMA">
                                    <span class="input-group-addon informasi" style="width:70%" id="RWSKP_NKERJASAMAX"></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-4 has-error">
                                <label>Integritas</label>
                                <div class="input-group">
                                    <input class="form-control nilai" name="RWSKP_NINTEGRITAS" id="RWSKP_NINTEGRITAS">
                                    <span class="input-group-addon informasi" style="width:70%" id="RWSKP_NINTEGRITASX"></span>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 has-error">
                                <label>Disiplin</label>
                                <div class="input-group">
                                    <input class="form-control nilai" name="RWSKP_NDISIPLIN" id="RWSKP_NDISIPLIN">
                                    <span class="input-group-addon informasi" style="width:70%" id="RWSKP_NDISIPLINX"></span>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 has-error">
                                <label class="struktural">Kepemimpinan</label>
                                <div class="input-group">
                                    <input class="form-control nilai struktural" name="RWSKP_NKEPEMIMPINAN" id="RWSKP_NKEPEMIMPINAN">
                                    <span class="input-group-addon struktural informasi" style="width:70%" id="RWSKP_NKEPEMIMPINANX"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="form-group col-lg-3 has-warning">
                            <label>Nilai Perilaku Kerja</label>
                            <div class="input-group" >
                                <input class="form-control nilai" id="NPERILAKU">
                                <span class="input-group-addon">40%</span>
                                <input class="form-control" id="NPERILAKUX" >
                            </div>
                            </div>
                            <div class="form-group col-lg-3 has-success">
                                <label>Nilai Prestasi Kerja</label>
                                <div class="input-group">
                                    <input class="form-control nilai" id="NPRESTASI">
                                    <span class="input-group-addon informasi" style="width:60%" id="NPRESTASIX"></span>
                                </div>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-6">
                                <label>Pejabat Penilai</label>
                                <input class="form-control" name="RWSKP_PJBPENILAI" id="RWSKP_PJBPENILAI">
                            </div>
                            <div class="form-group col-lg-6">
                                <label>Atasan Pejabat Penilai</label>
                                <input class="form-control" name="RWSKP_ATASANPJBPENILAI" id="RWSKP_ATASANPJBPENILAI">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div><br>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Tahun</th>
                            <th>Rata-rata</th>
                            <th>Keterangan</th>
                            <th>Jumlah</th>
                            <th>Pejabat Penilai</th>
                            <th>Atasan Pjb. Penilai</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>
    <style>
        .informasi{
            background-color: gray!important;
            color: white!important;
        }
    </style>
    <script>
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/skp_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWSKP_ID", "class": "text-center", },
                {"data": "RWSKP_THN"},
                {"data": "RWSKP_NRATA"},
                {"data": "KETERANGAN"},
                {"data": "RWSKP_NJUMLAH"},
                {"data": "RWSKP_PJBPENILAI"},
                {"data": "RWSKP_ATASANPJBPENILAI"},
                {"data": "RWSKP_NIP", "class": "text-center"},
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWSKP_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWSKP_ID + '\',\'' + data.RWSKP_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/skp_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");

        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myForm').trigger("reset");
            $('#RWSKP_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWSKP_THN").val(rowData.RWSKP_THN);
            $("#RWSKP_IDJENJAB option").filter(function () {
                return $(this).text() == rowData.RWSKP_IDJENJAB;
            }).prop('selected', true)
            $("#RWSKP_PJBPENILAI").val(rowData.RWSKP_PJBPENILAI);
            $("#RWSKP_ATASANPJBPENILAI").val(rowData.RWSKP_ATASANPJBPENILAI);
            $("#RWSKP_NSKP").val(rowData.RWSKP_NSKP);
            $("#RWSKP_NPELAYANAN").val(rowData.RWSKP_NPELAYANAN);
            $("#RWSKP_NKOMITMEN").val(rowData.RWSKP_NKOMITMEN);
            $("#RWSKP_NKERJASAMA").val(rowData.RWSKP_NKERJASAMA);
            $("#RWSKP_NINTEGRITAS").val(rowData.RWSKP_NINTEGRITAS);
            $("#RWSKP_NDISIPLIN").val(rowData.RWSKP_NDISIPLIN);
            $("#RWSKP_NKEPEMIMPINAN").val(rowData.RWSKP_NKEPEMIMPINAN);
            $('#RWSKP_IDJENJAB').trigger("change");
            $('.nilai').trigger('keyup');
        });

        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            $(".informasi").text('');
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $('.nilai').trigger('keyup');
        })


        $(".nilai").mask("9?99", {placeholder: ""});
        $(".struktural").hide();
        $("#RWSKP_NKEPEMIMPINAN").attr('disabled', true);

        $("#RWSKP_IDJENJAB").change(function () {
            //alert($("#RWSKP_IDJENJAB").val())
            $(".struktural").hide();
            $("#RWSKP_IDJENJAB").val() == 1 ? $(".struktural").show() : $(".struktural").hide();
            $("#RWSKP_NKEPEMIMPINAN").attr('disabled', $("#RWSKP_IDJENJAB").val() == 1 ? false : true);
            hitung();
        })
        $('.nilaiskp').keyup(function () {
            hitung();
        })
        function getKet(nilai) {
            var ket = '';
            switch (true) {
                case nilai >= 91:
                    ket = 'sangat baik';
                    break;
                case nilai >= 76:
                    ket = 'baik';
                    break;
                case nilai >= 61:
                    ket = 'cukup';
                    break;
                case nilai >= 51:
                    ket = 'kurang';
                    break;
                default:
                    ket = 'buruk';
            }
            return ket;
        }
        $('.nilai').keyup(function () {
            var nilai = $(this).val();
            hitung();
            $('#' + this.id + 'X').text(getKet(nilai));
        });
        function hitung() {
            var sum = 0;
            var divider = 5;
            $('.nilai:not(:disabled)').each(function () {
                sum += (parseFloat($(this).val() == '' ? 0 : $(this).val()));
            });

            $("#RWSKP_NSKPX").val((parseFloat($(".nilaiskp").val() == '' ? 0 : $(".nilaiskp").val()) / 100 * 60).toFixed(2))

            divider = $("#RWSKP_IDJENJAB").val() == 1 ? 6 : 5;
            $("#NPERILAKU").val(sum / divider);
            $("#NPERILAKUX").val((parseFloat(sum / divider) / 100 * 40).toFixed(2));

            $("#NPRESTASI").val((parseFloat($("#NPERILAKUX").val()) + parseFloat($("#RWSKP_NSKPX").val())).toFixed(2));
            $("#NPRESTASIX").text(getKet($("#NPRESTASI").val()));

            $("#RWSKP_NJUMLAH").val(sum);
            $("#RWSKP_NRATA").val($("#NPRESTASI").val());
        }
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/skp_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }


    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
