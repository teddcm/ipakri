<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="modal fade" id="myModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <form id="newForm">  
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Gelar Depan</label>
                            <input class="form-control"  style="text-transform: none"name="PNS_GLRDPN"  value="<?php echo $pegawai->PNS_GLRDPN; ?>">               
                        </div>
                        <div class="form-group">
                            <label>Gelar Belakang</label>
                            <input class="form-control"  style="text-transform: none"name="PNS_GLRBLK" value="<?php echo $pegawai->PNS_GLRBLK; ?>">               
                        </div>
                    </div>
                    <div class="modal-footer">                        
                        <input type="hidden" name="nip_lama" value="<?php echo $this->uri->segment(4); ?>">               
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="simpan_update">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Pendidikan</a></li>
        </ul>
        <div class="tab-content n-a">
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                            
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWDIK_NIP" name="RWDIK_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWDIK_ID" name="RWDIK_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>          
                    <div class="form-group col-lg-3">
                        <label>* Pendidikan</label>
                        <input class="form-control typeahead" name="NAMADIK" id="NAMADIK" >
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Kode Pendidikan</label>
                        <input class="form-control" disabled name="RWDIK_IDDIK" id="RWDIK_IDDIK" tabindex="-10" size="1" readonly>               
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Tingkat Pendidikan</label>
                        <select class="form-control" name="RWDIK_TKTDIK" id="RWDIK_TKTDIK">
                            <?php
                            foreach ($tktpendik as $x) {
                                echo '<option value="' . $x->DIK_TKTDIK . '">' . $x->DIK_NAMDIK . '</option>';
                            }
                            ?>
                        </select>                    
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Pendidikan Pertama</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  name="RWDIK_PERTAMA" id="RWDIK_PERTAMA" value="V"  tabindex="0">
                                Pendidikan Pengangkatan CPNS
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal Lulus</label>
                        <input class="form-control datepicker" name="RWDIK_TGLLULUS" id="RWDIK_TGLLULUS">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Tahun Lulus</label>
                        <input class="form-control" name="RWDIK_THNLULUS" id="RWDIK_THNLULUS">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor Ijazah</label>
                        <input class="form-control" name="RWDIK_NOIJZ" id="RWDIK_NOIJZ">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Gelar Depan</label>
                        <input class="form-control" style="text-transform: none" name="RWDIK_GLRDPN" id="RWDIK_GLRDPN">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Gelar Belakang</label>
                        <input class="form-control" style="text-transform: none" name="RWDIK_GLRBLK" id="RWDIK_GLRBLK">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Nama Sekolah</label>
                        <input class="form-control" name="RWDIK_NAMSKLH" id="RWDIK_NAMSKLH">
                    </div>  
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>
                            <a class="btn btn-default btn-flat" href="#" id="update">Update Gelar</a>       
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Pendidikan</th>
                            <th>Tahun Lulus</th>
                            <th>Nama Sekolah</th>
                            <th>Pendidikan Pertama</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>

        $('#update').on('click', function (e) {
            $('#myModal').modal('show');
            e.preventDefault();
        })
        $("#simpan_update").click(function () {
            $("#newForm").submit();
        });
        $("#newForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/pendidikan_update_gelar'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#newForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        $('#myModal').modal('hide');
                        //setTimeout(function () {
                        //    loadContent('<?php echo base_url(uri_string()); ?>');
                        //}, 2000);

                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });



        $('#RWDIK_TGLLULUS').on('focusout', function () {
            var dt = $('#RWDIK_TGLLULUS').val().split('-');
            $('#RWDIK_THNLULUS').val(dt != '' ? parseInt(dt[2]) : '');
        })

        var vpendidikan = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '<?= base_url('admin/json/json_pendidikan'); ?>?q=y',
            remote: {
                url: '<?= base_url('admin/json/json_pendidikan'); ?>?q=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $("#RWDIK_IDDIK").keydown(function (e) {
            e.preventDefault();
        });
        $('#NAMADIK').typeahead(null, {
            name: 'pendidikan',
            display: 'DIK_NMDIK',
            limit: 15,
            source: vpendidikan,
            templates: {
                empty: [
                    '<div class="tt-suggestion">',
                    'DATA TIDAK DITEMUKAN',
                    '</div>'
                ].join('\n'),
                suggestion: function (data) {
                    return '<p><strong>' + data.DIK_KODIK + '</strong> – ' + data.DIK_NMDIK + '</p>';
                }
            }
        }).on('typeahead:selected', function (event, data) {
            $('#RWDIK_IDDIK').val(data.DIK_KODIK);
        });


        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/pendidikan_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWDIK_ID", "class": "text-center", },
                {"data": "NAMADIK"},
                {"data": "RWDIK_THNLULUS"},
                {"data": "RWDIK_NAMSKLH"},
                {"data": "RWDIK_PERTAMA"},
                {"data": "RWDIK_NIP", "class": "text-center"},
            ],
            "order": [[3, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWDIK_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWDIK_ID + '\',\'' + data.RWDIK_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/pendidikan_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");

        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            var id = $(this).attr('id');
            $("#myForm").trigger("reset");
            $('#RWDIK_ID').val(id);
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');

            $("#NAMADIK").val(selected.find('td:eq(1)').html());
            $('#RWDIK_IDDIK').val(rowData.RWDIK_IDDIK);
            $('#RWDIK_TKTDIK').val(rowData.RWDIK_TKTDIK);
            $("#RWDIK_PERTAMA").prop('checked', rowData.RWDIK_PERTAMA === 'V' ? true : false);
            $("#RWDIK_TGLLULUS").val(rowData.RWDIK_TGLLULUS);
            $("#RWDIK_THNLULUS").val(rowData.RWDIK_THNLULUS);
            $("#RWDIK_NOIJZ").val(rowData.RWDIK_NOIJZ);
            $("#RWDIK_NAMSKLH").val(rowData.RWDIK_NAMSKLH);
            $('#RWDIK_GLRDPN').val(rowData.RWDIK_GLRDPN);
            $('#RWDIK_GLRBLK').val(rowData.RWDIK_GLRBLK);
        });

        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $(".tt-hint").focus();
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/pendidikan_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
