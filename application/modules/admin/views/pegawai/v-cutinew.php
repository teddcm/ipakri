<?php
if ($this->uri->segment(4) != 'b801ad7c9ea047949f352b5fe98dcc9b') {
    require 'v-listing#search.php';
}
if (true) {
    if (cek_jatah_cutinew(date('Y')) <= 0) { //belum pernah ada definisi cuti
        ?>
        <style>
            .table-hover tbody tr:hover td {
                background-color: greenyellow;
            }
        </style>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Definisikan Cuti</a></li>
            </ul>
            <div class="tab-content n-a">
                <div class="tab-pane active" id="tab_1">
                    <div class="alert alert-warning col-lg-12">
                        Alokasi cuti untuk seluruh pegawai belum terdefinisi. Mohon <?php echo ($this->ion_auth->is_admin()) ? '' : 'menghubungi admin'; ?> untuk mengalokasikan hak cuti tahun <?php echo date('Y'); ?>.<br/>
                    </div>
                </div>
                <?php if ($this->ion_auth->is_admin()) { ?>
                    <p>Adapun ketentuan hak cuti yang diperoleh masing-masing pegawai ditentukan berdasarkan kondisi di bawah ini (Perka BKN Nomor 24 Tahun 2017):</p>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th>No.</th><th>Hak cuti tahun <?php echo date('Y') - 2; ?></th><th>Hak cuti tahun <?php echo date('Y') - 1; ?></th><th>Hak cuti tahun <?php echo date('Y'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2</td><td></td><td>Sisa hak cuti kurang dari 6 (enam) hari</td><td> sisa hak cuti tahun <?php echo date('Y') - 1; ?> + 12 hari</td>
                            </tr>
                            <tr>
                                <td>3</td><td></td><td>Sisa hak cuti lebih dari 6 (enam) hari</td><td> 6 hari + 12 hari</td>
                            </tr>
                            <tr>
                                <td>1</td><td></td><td>Sisa hak cuti lebih dari 6 (enam) hari dan <span class="text-red">ditangguhkan<sup>(*)</sup></span></td><td> sisa hak cuti tahun <?php echo date('Y') - 1; ?> + 12 hari</td>
                            </tr>
                            <tr>
                                <td>4</td><td>Digunakan</td><td>Tidak digunakan</td><td> 6 hari + 12 hari</td>
                            </tr>
                            <tr>
                                <td>5</td><td>Tidak digunakan</td><td>Tidak digunakan</td><td> 12 hari + 12 hari</td>
                            </tr>
                            <tr>
                                <td colspan="4"><span class="text-red">(*) Ditetapkan oleh pejabat yang berwenang</span></td>
                            </tr>
                        </tbody>
                    </table>
            <!--                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th>No.</th><th>Penggunaan hak cuti tahun <?php echo date('Y') - 2; ?></th><th>Penggunaan hak cuti tahun <?php echo date('Y') - 1; ?></th><th>Hak Cuti tahun <?php echo date('Y'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td><td>Menggunakan / tidak menggunakan hak cuti</td><td>Sisa hak cuti ditangguhkan oleh Pejabat yang Berwenang</td><td> 12 hari + sisa hak cuti tahun <?php echo date('Y') - 1; ?></td>
                            </tr>
                            <tr>
                                <td>2</td><td>Menggunakan / tidak menggunakan hak cuti</td><td>Menggunakan hak cuti dengan sisa hak cuti kurang dari 6 (enam) hari</td><td> 12 hari + sisa hak cuti tahun <?php echo date('Y') - 1; ?></td>
                            </tr>
                            <tr>
                                <td>3</td><td>Menggunakan / tidak menggunakan hak cuti</td><td>Menggunakan hak cuti dengan sisa hak cuti lebih dari 6 (enam) hari</td><td> 18 hari</td>
                            </tr>
                            <tr>
                                <td>4</td><td>Menggunakan hak cuti</td><td>Tidak menggunakan hak cuti</td><td> 18 hari</td>
                            </tr>
                            <tr>
                                <td>5</td><td>Tidak menggunakan hak cuti</td><td>Tidak menggunakan hak cuti</td><td> 24 hari</td>
                            </tr>
                        </tbody>
                    </table>-->
                    <form id="myForm1">
                        <table class="table table-bordered table-responsive table-striped table-hover">
                            <tr>
                                <th colspan="2"></th>
                                <th colspan="6">Jatah cuti tahun <?php echo date('Y'); ?> sebanyak <span class='jatahtahunan' style="color:red;font-size: 16px"></span> hari<input type="range" name="jatahtahunan" id="jatahtahunan" min="0" max="24" value="12" ></th>
                                <th colspan="2"><button id="proses" type="button" class="btn btn-primary btn-flat"><i class="fa fa-gavel"></i>&nbsp;&nbsp;Alokasikan cuti </button></th> 
                            </tr>
                            <?php
                            echo ""
                            . "<tr class='label-danger'>"
                            . "<th rowspan=2>NIP</th>"
                            . "<th rowspan=2>NAMA</th>"
                            . "<th colspan=3>Hak Cuti " . (date('Y') - 2) . "</th>"
                            . "<th colspan=3>Hak Cuti " . (date('Y') - 1) . "</th>"
                            . "<th colspan=1>Hak Cuti " . (date('Y') - 0) . "</th>"
                            . "</tr>"
                            . "<tr class='text-center text-bold label-danger'>"
                            . "<th>Hak</th>"
                            . "<th>Pakai</th>"
                            . "<th>Sisa</th>"
                            . "<th>Hak</th>"
                            . "<th>Pakai</th>"
                            . "<th>Sisa</th>"
//                            . "<th>Penangguhan</th>"
                            . "<th>Sisa + Jatah</th>"
                            . "</tr>";
                            echo ""
                            . "<tr style='background-color:black;color:white'>"
                            . "<th>(1)</th>"
                            . "<th>(2)</th>"
                            . "<th>(3)</th>"
                            . "<th>(4)</th>"
                            . "<th>(5)</th>"
                            . "<th>(6)</th>"
                            . "<th>(7)</th>"
                            . "<th>(8)</th>"
                            . "<th>(9)</th>"
//                            . "<th>(10)</th>"
                            . "</tr>";
                            foreach ($data_cuti as $c) {
                                //sesuai Perka BKN Nomor 24 Tahun 2017
                                $jatah2 = ($c->j2 + $c->rest2);
                                $penggunaan2 = ($c->taken2 == NULL ? '-' : $c->taken2);
                                $sisa2 = ($c->j2 + $c->rest2 + $c->taken2);
                                $jatah1 = ($c->j1 + $c->rest1);
                                $penggunaan1 = ($c->taken1 == NULL ? '-' : $c->taken1);
                                $sisa1 = ($c->j1 + $c->rest1 + $c->taken1);
                                if ($penggunaan2 == '-' && $penggunaan1 == '-') {
                                    $jatah0 = 12;
                                } elseif ($penggunaan2 > 0 && $penggunaan1 == '-') {
                                    $jatah0 = 6;
                                } elseif ($sisa1 >= 6) {
                                    $jatah0 = 6;
                                } elseif ($sisa1 < 6) {
                                    $jatah0 = $sisa1;
                                }
                                echo "<tr>"
                                . "<td>$c->PNS_NIPBARU</td>"
                                . "<td>$c->PNS_PNSNAM</td>"
                                . "<td class='aqua'>$jatah2</td>"
                                . "<td class='aqua'>$penggunaan2</td>"
                                . "<td class='aqua'>$sisa2</td>"
                                . "<td class='green'>$jatah1</td>"
                                . "<td class='green'>$penggunaan1</td>"
                                . "<td class='green'>$sisa1</td>"
//                                . "<td class='green'><input type='checkbox' name='penangguhan[$c->PNS_NIPBARU]' value='T' class='tangguh'></td>"
                                . "<td class='text-bold' width='150px'><input class='sisatahunan' required type='number' class='input-sm' style='width:60px' maxlength='2' min='0' max='$sisa1' name='jatah[$c->PNS_NIPBARU]]' value='$jatah0'> + <span class='jatahtahunan' style='color:red;'></span></td>"
                                . "</tr>";
                            }
                            ?>
                        </table>

                    </form>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <script>
            //var colorarray = ['FF0000', 'FF2A00', 'FF5400', 'FF7F00', 'FFAA00', 'FFD400', 'FFFF00', 'D4FF00', 'AAFF00', '7FFF00', '54FF00', '2AFF00', '00FF00', '00FF2A', '00FF55', '00FF7F', '00FFA9', '00FFD4', '00FFFF', '00D4FF', '00A9FF', '007FFF', '0054FF', '002AFF', '0000FF'];
            var colorarray = ['FF0000', 'FF0015', 'FF002A', 'FF003F', 'FF0055', 'FF006A', 'FF007F', 'FF0094', 'FF00AA', 'FF00BF', 'FF00D4', 'FF00E9', 'FF00FF', 'E900FF', 'D400FF', 'BF00FF', 'AA00FF', '9400FF', '7F00FF', '6A00FF', '5400FF', '3F00FF', '2A00FF', '1500FF', '0000FF'];
            //var colorarray = ['FF0000','F60000','EE0000','E50000','DD0000','D40000','CC0000','C30000','BB0000','B20000','AA0000','A10000','990000','900000','880000','7F0000','770000','6E0000','660000','5D0000','550000','4C0000','440000','3B0000','330000'];

            $("input[type='checkbox']").attr('tabindex', -1)
            $('th').addClass('text-center');
            $('.aqua').addClass('text-aqua text-bold text-center');
            $('.green').addClass('text-green text-bold text-center');
            $("input[type='text']").click(function () {
                $(this).select();
            });

            $('.jatahtahunan').html($('#jatahtahunan').val()).css('color', '#' + colorarray[$('#jatahtahunan').val()]);
            $('#jatahtahunan').bind('change keyup input', function () {
                $('.jatahtahunan').html($('#jatahtahunan').val()).css('color', '#' + colorarray[$('#jatahtahunan').val()]);
            });

            $('.sisatahunan').bind('change keyup input', function () {
                $(this).css('color', '#' + colorarray[$(this).val()]);
            });
            $('.sisatahunan').trigger('change');

            $("#proses").click(function (e) {
                if (!confirm('Apakah anda akan mengalokasikan jatah cuti untuk semua pegawai sebanyak ' + $('#jatahtahunan').val() + ' hari ditambah dengan sisa hak cuti tahun lalu?')) {
                    event.preventDefault();
                } else {
                    var url = "<?php echo base_url('admin/pegawai/cutinew_awal_simpan'); ?>";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#myForm1").serialize(),
                        dataType: "json",
                        success: function (result)
                        {
                            $.notify(result[1], result[0]);
                            if (result[0] === 'success') {
                                if ('<?php echo $this->uri->segment(4) ?>' === '') {
                                    loadContent('<?php echo base_url('admin/dashboard'); ?>');
                                } else {
                                    loadContent('<?php echo base_url(uri_string()); ?>');
                                }
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            cekError(XMLHttpRequest, textStatus);
                        },
                    });
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                }
            });
        </script>
        <?php
    } else if (get_data_pns($this->uri->segment(4)) != NULL && cek_sisa_cutinew($this->uri->segment(4)) == NULL) {  //pegawai belum didefinisikan jatah cutinya (biasanya penambahan pegawai baru)
        ?>
        <div class="alert alert-info">
            <i class="icon fa fa-info"></i> Mohon alokasikan sisa hak cuti dari tahun <?php echo date('Y') - 1; ?>  yang dapat dipergunakan untuk tahun <?php echo date('Y'); ?> untuk pegawai bernama <b><?php echo get_data_pns($this->uri->segment(4), 'PNS_PNSNAM'); ?></b>.
        </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Cuti</a></li>
                <li class="pull-right">
                    <div> 
                        <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                    </div>
                </li>
            </ul>
            <div class="tab-content n-a">
                <div class="col-lg-2">
                    <?php echo get_foto($this->uri->segment(4)); ?>
                </div>                              
                <div class="tab-pane active" id="tab_1">
                    <form id="myForm2"> 
                        <div>
                            <input type="submit" style="display: none" name="ok">
                            <input type="hidden" id="RWCUTI_NIP" name="RWCUTI_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                            <input type="hidden" id="RWCUTI_THNCUTI" name="RWCUTI_THNCUTI" value="<?php echo date('Y'); ?>">
                            <input type="hidden" id="mode" name="mode" value="define" >   
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Sisa Hak Cuti pada tahun <?php echo date('Y') - 1; ?></label>
                            <input class="form-control" name="RWCUTI_JUM" id="RWCUTI_JUM" type="text" value="<?php echo cek_sisa_cutinew($this->uri->segment(4), date('Y') - 1); ?>">
                        </div>
                    </form>
                    <div class="form-group col-lg-12">
                        <div class="pull-right">
                            <div>
                                <a class="btn btn-success btn-flat" href="#" id="simpan">simpan</a>   
                            </div>
                        </div>    
                        <div class="clearfix"></div>      
                    </div>       
                    &nbsp;                       
                    <div class="clearfix"></div>      

                </div>
            </div>
        </div>
        <script>

            $("#simpan").click(function () {
                $("#myForm2").submit();
            });
            $("#myForm2").submit(function (e) {
                var url = "<?php echo base_url('admin/pegawai/cutinew_simpan'); ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#myForm2").serialize(),
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //$('#myDataTable').DataTable().ajax.reload();
                            //$('#cancel').trigger("click");
                            loadContent('<?php echo base_url(uri_string()); ?>');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                });
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        </script>
        <?php
    } else if (get_data_pns($this->uri->segment(4)) != NULL) {
        //echo cek_sisa_cutinew($this->uri->segment(4), 2016);
        //echo cek_jatah_cutinew(2019);
        ?>
        <div class="alert alert-danger" style="display: none">
            <i class="icon fa fa-warning"></i> Sisa cuti bernilai negatif. Mohon periksa kembali!
        </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Cuti</a></li>
                <li class="pull-right">
                    <div> 
                        <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                    </div>
                </li>
                <li class="pull-left" style="padding-top: 3px;">
                    <select class="form-control" id="tahun_cuti">
                        <?php
                        for ($x = date('Y'); $x > date('Y') - 3; $x--) {
                            echo "<option value='" . $x . "'>" . $x . "</option>";
                        }
                        ?>
                    </select>
                </li>
                <li class="pull-right">
                    <button type="button" class="btn btn-primary btn-flat  n-a" data-toggle="modal" data-target="#cutiModal">
                        <i class="fa fa-barcode"></i><i class="fa fa-barcode"></i> Scan Barcode
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="cutiModal" tabindex="-1" role="dialog" aria-labelledby="cutiModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="cutiModalLabel">Scan Barcode Form Cuti yang telah disetujui</h5>
                                </div>
                                <div class="modal-body">
                                    <input class="form-control has-error" name="CariBarcode" id="CariBarcode" placeholder="Barcode Form Cuti" style=" text-transform: none!important">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" id="submitCuti">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                    </div>
                </li>

            </ul>
            <div class="tab-content n-a">
                <div class="col-lg-2">
                    <?php echo get_foto($this->uri->segment(4)); ?>
                </div>                              
                <div class="tab-pane active" id="tab_1">
                    <form id="myForm3"> 
                        <div>
                            <input type="submit" style="display: none" name="ok">
                            <input type="hidden" id="RWCUTI_NIP" name="RWCUTI_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                            <input type="hidden" id="RWCUTI_ID" name="RWCUTI_ID">
                            <input type="hidden" id="RWCUTI_IDFORM" name="RWCUTI_IDFORM">
                            <input type="hidden" id="RWCUTI_THNCUTI" name="RWCUTI_THNCUTI" value="<?php echo date('Y'); ?>">
                            <input type="hidden" id="mode" name="mode" value="edit" >   
                        </div>
                        <div class="form-group col-lg-10" style="color: red">
                            Sisa Cuti Tahunan yang dapat diambil <b id="sisa-cuti"></b> 
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Jenis Cuti</label>
                            <select class="form-control" name="RWCUTI_JNS" id="RWCUTI_JNS">
                                <option value="">-</option>
                                <?php
                                foreach ($jenis_cuti as $x) {
                                    echo '<option value="' . $x->JNSCT_ID . '">' . $x->JNSCT_NAMA . '</option>';
                                }
                                ?>
                            </select>                 
                        </div>
                        <div class="form-group col-lg-2">
                            <label>Tanggal Awal</label>
                            <input class="form-control datepicker" name="RWCUTI_TGLAWL" id="RWCUTI_TGLAWL">
                        </div>
                        <div class="form-group col-lg-2">
                            <label>Tanggal Akhir</label>
                            <input class="form-control datepicker" name="RWCUTI_TGLAKR" id="RWCUTI_TGLAKR">
                        </div>
                        <div class="form-group col-lg-2">
                            <label>* Jumlah Hari</label>
                            <input class="form-control" name="RWCUTI_JUM" id="RWCUTI_JUM" type="text">
                        </div>
                        <div class="form-group col-lg-4" disabled="disabled">
                            <label>&nbsp;</label>
                            <div class="checkbox">
                                <label disabled="disabled">
                                    <input id="potijin" name="potijin" type="checkbox">
                                    Cek jika ijin potong cuti tahunan
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <label>Keterangan</label>
                            <textarea class="form-control textarea" name="RWCUTI_KET" id="RWCUTI_KET" style="resize: none;height:35px"></textarea>
                        </div>
                    </form>
                    <div class="form-group col-lg-12">
                        <div class="pull-right">
                            <div>
                                <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                                <a class="btn btn-default btn-flat" href="#" id="notambah" disabled style="display: none">tambah</a>   
                                <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                                <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                            </div>
                        </div>    
                        <div class="clearfix"></div>      
                    </div>       
                    &nbsp;
                    <table id="myDataTable" class="table">
                        <thead>
                            <tr>
                                <th width='5%'>No</th>
                                <th>Jenis Cuti</th>
                                <th>Jenis Cuti</th>
                                <th>Tgl Awal</th>
                                <th>Tgl Akhir</th>
                                <th>Jumlah Hari</th>
                                <th>Tahun Cuti</th>
                                <th>Keterangan</th>
                                <th width='10%'>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>  
                    </table>
                </div>
            </div>
        </div>
        <script>
            $(function () {
                $('#cutiModal').on('shown.bs.modal', function () {
                    $("#CariBarcode").focus();
                })
                var e = $.Event("keypress", {which: 13});
                $('#submitCuti').on('click', function () {
                    $('#CariBarcode').trigger(e);
                });
                $("#CariBarcode").keypress(function (e) {
                    if (e.which == 13) {

                        playSound();
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('admin/pegawai/get_temp_cutinew'); ?>",
                            data: {"id": $("#CariBarcode").val(), "nip": '<?php echo $this->uri->segment(4); ?>'},
                            dataType: "json",
                            success: function (result)
                            {
                                $("#CariBarcode").val('');
                                $.notify(result[1], result[0]);
                                if (result[0] === 'success') {
                                    $("#tambah").trigger('click');
                                    $("#RWCUTI_TGLAWL").val(result.date_awal).attr("readonly", true);
                                    $("#RWCUTI_TGLAKR").val(result.date_akhir).attr("readonly", true);
                                    $("#RWCUTI_IDFORM").val(result.id);
                                    $("#RWCUTI_JUM").val(Math.abs(parseFloat(result.kuantitas))).attr("readonly", true);
                                    $('#RWCUTI_JNS').val(1);
                                    $('#RWCUTI_KET').val("Barcode :" + result.id).attr("readonly", true);
                                    $('#RWCUTI_JNS').trigger("change");
                                    $('#RWCUTI_KET').focus("Barcode :" + result.id);
                                    $("#potijin").attr("disabled", true)
                                    $("#RWCUTI_JNS").attr("disabled", true)
                                    $('#cutiModal').modal('hide');
                                    setTimeout(function () {
                                        $.notify('Data otomatis tersimpan', 'warn');
                                    }, 1000);
                                    setTimeout(function () {
                                        $("#simpan").trigger('click');
                                    }, 2000);
                                } else {
                                    $("#CariBarcode").focus();
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                cekError(XMLHttpRequest, textStatus);
                            },
                        });
                    }
                });
                $('#tahun_cuti').on('change', function () {
                    $("#RWCUTI_THNCUTI").val($('#tahun_cuti :selected').val());
                    $("#cancel").trigger('click');
                    count = 0;
                    $('#myDataTable').DataTable().ajax.reload();
                });
                $("#potijin").change(function () {
                    if (this.checked) {
                        $("#RWCUTI_KET").val("IJIN POTONG CUTI");
                        $("#RWCUTI_KET").attr("readonly", true);
                    } else {
                        $("#RWCUTI_KET").attr("readonly", false);
                        $("#RWCUTI_KET").val("");
                    }
                });
                $('#RWCUTI_JNS').on('change', function (e) {
                    if (this.value == 1) {
                        $("#potijin").attr("disabled", false);
                    } else {
                        $("#potijin").prop('checked', false);
                        $("#potijin").attr("disabled", true);
                        $("#RWCUTI_KET").val("");
                        $("#RWCUTI_KET").attr("readonly", false);
                    }
                });
                $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };
                var count = 0;
                $('#myDataTable').dataTable({
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "sDom": '<"F">t<"F">',
                    "processing": true,
                    "serverSide": true,
                    "autoWidth": false,
                    "ajax": {
                        "url": "<?php echo site_url('admin/pegawai/cutinew_ajax_view/') . $this->uri->segment(4); ?>/",
                        "data": function (d) {
                            d.tahun = $('#tahun_cuti :selected').val();
                        },
                    },
                    "iDisplayLength": 100,
                    "columnDefs": [
                        {
                            "targets": [2],
                            "visible": false,
                            "searchable": false
                        },
                    ],
                    "columns": [
                        {"data": "RWCUTI_ID", "class": "text-center", },
                        {"data": "RWCUTI_JNSNM"},
                        {"data": "RWCUTI_JNS"},
                        {"data": "RWCUTI_TGLAWL"},
                        {"data": "RWCUTI_TGLAKR"},
                        {"data": "RWCUTI_JUM", "class": "text-center", },
                        {"data": "RWCUTI_THNCUTI"},
                        {"data": "RWCUTI_KET"},
                        {"data": "RWCUTI_NIP", "class": "text-center"},
                    ],
                    "order": [[2, 'asc']],
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var ref = '<?= uri_string(); ?>';
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        var id = $('td:eq(0)', row).text();
                        $('td:eq(0)', row).html(index);
                        var edit = ' <a class="edit btn btn-xs btn-info" id="' + data.RWCUTI_ID + '" ">edit</a>'
                        var validasi = ' <a class="validasi btn btn-xs btn-success" id="' + data.RWCUTI_ID + '" " title="cetak validasi"><i class="fa  fa-check-square-o"></i></a>'
                        var hapus = ' <a class="hapus btn btn-xs btn-danger">hapus</a>';
                        $('td:eq(-1)', row).html(((data.RWCUTI_JNS <= 0 && data.RWCUTI_JNS >= -1) ? '' : hapus) + ((data.RWCUTI_JNS == 0 || !isNaN(parseInt(data.RWCUTI_IDFORM))) ? '' : edit) + (!isNaN(parseInt(data.RWCUTI_IDFORM)) ? validasi : ''));
        //                        alert(!isNaN(parseInt(data.RWCUTI_IDFORM))) //apakah IDFORM (isi melalui formulir) ada isinya. 
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };
                        if (data.RWCUTI_JNS <= 1 && data.RWCUTI_JNS >= -1) {
                            if (index == 1) {
                                count = 0;
                            }
                            count = count + intVal(data.RWCUTI_JUM);
                            $('#sisa-cuti').html('tersisa ' + count + ' hari');
                            if (info.iEnd == index) {
                                if (count < 0) {
                                    $('.alert-danger').show();
                                } else {
                                    $('.alert-danger').hide();
                                }
                            }
                        }
                    },
                });



                $("#myForm3").submit(function (e) {
                    var url = "<?php echo base_url('admin/pegawai/cutinew_simpan'); ?>";
                    $("#RWCUTI_JNS").attr("disabled", false);
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#myForm3").serialize(),
                        dataType: "json",
                        success: function (result)
                        {
                            $.notify(result[1], result[0]);
                            if (result[0] === 'success') {
                                $('#myDataTable').DataTable().ajax.reload();
                                $('#cancel').trigger("click");
                                //loadContent('<?php echo base_url(uri_string()); ?>');
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            cekError(XMLHttpRequest, textStatus);
                        },
                    });
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                });
                $("#myForm3 *").attr("disabled", true);
                $('#myForm3').trigger("reset");
                $("#simpan").click(function () {
                    $("#myForm3").submit();
                });

                var oTable = $('#myDataTable').DataTable();
                $('#myDataTable tbody').on('click', '.edit', function () {
                    var rowData = oTable.row($(this).closest("tr")).data();
                    $('#myForm3').trigger("reset");
                    $('#RWCUTI_ID').val($(this).attr('id'));
                    $("#myForm3 * [name]").attr("disabled", false);
                    if (rowData.RWCUTI_JNS <= 0) {
                        $("#RWCUTI_JNS").attr("disabled", true)
                        $("#RWCUTI_TGLAWL").attr("disabled", true)
                        $("#RWCUTI_TGLAKR").attr("disabled", true)
                        $("#RWCUTI_KET").attr("disabled", true)
                        $("#potijin").attr("disabled", true)
                    }
                    $("option").attr("disabled", false);
                    $("#cancel").show();
                    $("#simpan").show();
                    $("#tambah").hide();
                    $("#mode").val('edit');
                    $("#mode").attr("disabled", false);
                    var selected = $(this).parents('tr');
                    $("#RWCUTI_JNS option").filter(function () {
                        return $(this).text() == selected.find('td:eq(1)').html();
                    }).prop('selected', true)

                    $("#RWCUTI_TGLAWL").val(rowData.RWCUTI_TGLAWL);
                    $("#RWCUTI_TGLAKR").val(rowData.RWCUTI_TGLAKR);
                    $("#RWCUTI_JUM").val(Math.abs(parseFloat(rowData.RWCUTI_JUM)));
                    $("#RWCUTI_KET").val(rowData.RWCUTI_KET);
                    $("#RWCUTI_KET").attr("readonly", rowData.RWCUTI_KET == 'IJIN POTONG CUTI' ? true : false);
                    $("#potijin").prop('checked', rowData.RWCUTI_KET == 'IJIN POTONG CUTI' ? true : false);
                    $('#RWCUTI_JNS').trigger("change");

                });
                $('#myDataTable tbody').on('click', '.hapus', function () {
                    var rowData = oTable.row($(this).closest("tr")).data();
                    var e = confirm('Apakah data ini akan dihapus?');
                    if (e) {
                        count = 0;
                        var url = "<?php echo base_url('admin/pegawai/cuti_hapus'); ?>"; // the script where you handle the form input.
                        $.ajax({
                            type: "POST",
                            url: url,
                            async: false,
                            data: {"id": rowData.RWCUTI_ID, "nip": rowData.RWCUTI_NIP},
                            dataType: "json",
                            success: function (result)
                            {
                                $.notify(result[1], result[0]);
                                if (result[0] === 'success') {
                                    $('#myDataTable').DataTable().ajax.reload();
                                    $('#cancel').trigger("click");
                                    //loadContent('<?php echo base_url(uri_string()); ?>');
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                cekError(XMLHttpRequest, textStatus);
                            },
                        })
                    }
                });


                $('#myDataTable tbody').on('click', '.validasi', function () {
        //                    $.post('<?php echo base_url(); ?>admin/pegawai/cuti_validasi', function (data) {
        //                        var w = window.open('about:blank');
        //                        w.document.open();
        //                        w.document.write(data);
        //                        w.document.close();
        //                    });
                    var rowData = oTable.row($(this).closest("tr")).data();
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url(); ?>admin/pegawai/cuti_validasi',
                        data: {"id": rowData.RWCUTI_IDFORM, "nip": rowData.RWCUTI_NIP},
                        dataType: "json",
                        success: function (data) {
                            var win = window.open();
                            var table="";
                            win.document.write(data.html);
                        }
                    })
                });
                $("#cancel").click(function () {
                    $("#myForm3 *").attr("disabled", true);
                    $('#myForm3').trigger("reset");
                    $("#cancel").hide();
                    $("#simpan").hide();
                    $("#tambah").show();
                    //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
                })
                $("#tambah").click(function () {
                    $("#myForm3 * [name]").attr("disabled", false);
                    $("option").attr("disabled", false);
                    $("#mode").attr("disabled", false);
                    $("#mode").val('tambah');
                    $("#cancel").show();
                    $("#simpan").show();
                    $("#tambah").hide();
                })

                $('#RWCUTI_TGLAWL').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    format: 'DD-MM-YYYY',
                    daysOfWeekDisabled: [0, 6]
                });
                $('#RWCUTI_TGLAKR').datetimepicker({
                    useCurrent: false, //Important! See issue #1075
                    format: 'DD-MM-YYYY',
                    daysOfWeekDisabled: [0, 6]
                });
                $("#RWCUTI_TGLAWL").on("dp.change", function (e) {
                    $('#RWCUTI_TGLAKR').data("DateTimePicker").minDate(e.date);
                    countDiff();
                });
                $("#RWCUTI_TGLAKR").on("dp.change", function (e) {
                    $('#RWCUTI_TGLAWL').data("DateTimePicker").maxDate(e.date);
                    countDiff();
                });
                function countDiff() {
                    var iWeeks, iDateDiff, iAdjust = 0;
                    var tglAwl = $('#RWCUTI_TGLAWL').val().split('-');
                    var tglAkr = $('#RWCUTI_TGLAKR').val().split('-');
                    var dDate1 = new Date(tglAwl[2] + '-' + tglAwl[1] + '-' + tglAwl[0]);
                    var dDate2 = new Date(tglAkr[2] + '-' + tglAkr[1] + '-' + tglAkr[0]);
                    //alert(dDate1);
                    var iWeeks, iDateDiff, iAdjust = 0;
                    if (dDate2 < dDate1)
                        return -1; // error code if dates transposed
                    var iWeekday1 = dDate1.getDay(); // day of week
                    var iWeekday2 = dDate2.getDay();
                    iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
                    iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
                    if ((iWeekday1 == 6 && iWeekday2 == 6) || (iWeekday1 == 7 && iWeekday2 == 7)) {
                        iAdjust = 1; // adjustment if both days on weekend
                    } else if (iWeekday1 == 6 && iWeekday2 == 7) {
                        iAdjust = 2;
                    }
                    // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
                    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

                    if (iWeekday1 <= iWeekday2) {
                        iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
                    } else {
                        iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
                    }

                    iDateDiff -= iAdjust // take into account both days on weekend
                    $('#RWCUTI_JUM').val(iDateDiff + 1); // add 1 because dates are inclusive
                }
            });
        </script>
        <?php
    }
} else {
    ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
