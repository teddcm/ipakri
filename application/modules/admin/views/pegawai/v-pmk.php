<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat PMK</a></li>
        </ul>
        <div class="tab-content n-a"> 
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                    
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWPMK_NIP" name="RWPMK_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWPMK_ID" name="RWPMK_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>          
                    <div class="form-group col-lg-3">
                        <label>* Jenis PMK</label>
                        <select class="form-control" id="RWPMK_PERSEN" name="RWPMK_PERSEN">
                            <option value=""></option>  
                            <option value="0">Pengurangan</option>  
                            <option value="100">Negeri</option>  
                            <option value="50">Swasta</option>  
                        </select>    
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Instansi / Perusahaan</label>
                        <input class="form-control" name="RWPMK_TPTKRJ" id="RWPMK_TPTKRJ">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal Awal Kerja</label>
                        <input class="form-control datepicker" name="RWPMK_TGLAWL" id="RWPMK_TGLAWL">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal Akhir Kerja</label>
                        <input class="form-control datepicker" name="RWPMK_TGLAKR" id="RWPMK_TGLAKR">
                    </div>
                    <div class="form-group col-lg-3">
                        <label>* Masa Kerja</label>
                        <div class="input-group">
                            <span class="input-group-addon">Tahun</span>
                            <input class="form-control" name="RWPMK_THNKRJ" id="RWPMK_THNKRJ">
                            <span class="input-group-addon">Bulan</span>
                            <input class="form-control" name="RWPMK_BLNKRJ" id="RWPMK_BLNKRJ">
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Nomor SK</label>
                        <input class="form-control" name="RWPMK_NOSK" id="RWPMK_NOSK">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal SK</label>
                        <input class="form-control datepicker" name="RWPMK_TGLSK" id="RWPMK_TGLSK">
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Nomor BKN</label>
                        <input class="form-control" name="RWPMK_NOBKN" id="RWPMK_NOBKN">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal BKN</label>
                        <input class="form-control datepicker" name="RWPMK_TGLBKN" id="RWPMK_TGLBKN">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Jenis PMK</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                            <th>Instansi / Perusahaan</th>
                            <th>Tahun Kerja</th>
                            <th>Bulan Kerja</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/peninjauan_masa_kerja_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWPMK_ID", "class": "text-center", },
                {"data": "RWPMK_PERSEN"},
                {"data": "RWPMK_TGLAWL"},
                {"data": "RWPMK_TGLAKR"},
                {"data": "RWPMK_TPTKRJ"},
                {"data": "RWPMK_THNKRJ"},
                {"data": "RWPMK_BLNKRJ"},
                {"data": "RWPMK_NIP", "class": "text-center"},
            ],
            "order": [],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWPMK_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWPMK_ID + '\',\'' + data.RWPMK_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $('#RWPMK_TGLAWL,#RWPMK_TGLAKR').on('focusout', function () {
            dateDiff();
        })

        $('#RWPMK_PERSEN').on('change', function () {
            dateDiff();
        })
        function dateDiff() {
            var dtawal = $('#RWPMK_TGLAWL').val().split('-');
            var dtakhir = $('#RWPMK_TGLAKR').val().split('-');
            var totmonth = ((parseInt(dtakhir[2]) * 12) + parseInt(dtakhir[1])) - ((parseInt(dtawal[2]) * 12) + parseInt(dtawal[1]));
            totmonth = dtawal[0] <= dtakhir[0] ? totmonth : totmonth - 1;

            var persen = $('#RWPMK_PERSEN').val();
            if (persen == 50) {
                totmonth = parseFloat(totmonth / 2);
            } else if (persen == 100) {
                totmonth = totmonth;
            } else {
                totmonth = 0;
            }
            //alert(totmonth);
            var year = Math.floor(parseInt(totmonth / 12))
            var month = Math.floor(parseFloat(totmonth % 12));
            month = (isNaN(month) || month === 0) ? 0 : month;
            year = (isNaN(year) || year === 0) ? 0 : year;
            //alert(month + ' ' + year);
            $('#RWPMK_THNKRJ').val(year);
            $('#RWPMK_BLNKRJ').val(month);
        }

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/peninjauan_masa_kerja_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            var id = $(this).attr('id');
            $("#myForm").trigger("reset");
            $('#RWPMK_ID').val(id);
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWPMK_PERSEN option").filter(function () {
                return $(this).text() == rowData.RWPMK_PERSEN;
            }).prop('selected', true)
            $("#RWPMK_TGLAWL").val(rowData.RWPMK_TGLAWL);
            $("#RWPMK_TGLAKR").val(rowData.RWPMK_TGLAKR);
            $("#RWPMK_TPTKRJ").val(rowData.RWPMK_TPTKRJ);
            $("#RWPMK_NOSK").val(rowData.RWPMK_NOSK);
            $("#RWPMK_TGLSK").val(rowData.RWPMK_TGLSK);
            $("#RWPMK_NOBKN").val(rowData.RWPMK_NOBKN);
            $("#RWPMK_TGLBKN").val(rowData.RWPMK_TGLBKN);
            $("#RWPMK_THNKRJ").val(rowData.RWPMK_THNKRJ);
            $("#RWPMK_BLNKRJ").val(rowData.RWPMK_BLNKRJ);
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/peninjauan_masa_kerja_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
