<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="modal fade" id="myModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <form id="newForm">  
                <div class="modal-content">
                    <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                            <div id="pilih_angka_kredit"></div>
                        </div>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Angka kredit</a></li>
        </ul>
        <div class="tab-content n-a"> 
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                           
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWAKR_NIP" name="RWAKR_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWAKR_ID" name="RWAKR_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>          
                    <div class="form-group col-lg-4">
                        <label>Nomor SK PAK</label>
                        <input class="form-control typeahead"  name="RWAKR_NOSK" id="RWAKR_NOSK">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal SK</label>
                        <input class="form-control datepicker" name="RWAKR_TGLSK" id="RWAKR_TGLSK">
                    </div>
                    <div class="form-group col-lg-3" disabled="disabled">
                        <label disabled="disabled">Kredit Pertama</label>
                        <div class="checkbox" disabled="disabled">
                            <label disabled="disabled">
                                <input name="RWAKR_AKRPERTAMA" id="RWAKR_AKRPERTAMA" value="V" tabindex="0" type="checkbox">
                                Penetapan Angka Kredit Pertama
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-3">
                        <label class="struktural">Mulai Penilaian</label>
                        <div class="input-group">
                            <select class="form-control" style="width: 180%" name="RWAKR_BLNAWL" id="RWAKR_BLNAWL">
                                <?php
                                echo "<option></option>";
                                for ($i = 1; $i <= 12; $i++) {
                                    $label = array("", "Januari", "Pebruari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
                                    $value = $i;
                                    echo "<option value='$value'>$label[$i]</option>";
                                }
                                ?>
                            </select>
                            <span class="input-group-addon"  style="width: 30%"></span>
                            <input  style="width: 100%"class="form-control" name="RWAKR_THNAWL" id="RWAKR_THNAWL" placeholder="Tahun" maxlength="4">
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label class="struktural">Selesai Penilaian</label>
                        <div class="input-group">
                            <select class="form-control" style="width: 180%" name="RWAKR_BLNAKR" id="RWAKR_BLNAKR">
                                <?php
                                echo "<option></option>";
                                for ($i = 1; $i <= 12; $i++) {
                                    $label = array("", "Januari", "Pebruari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
                                    $value = $i;
                                    echo "<option value='$value'>$label[$i]</option>";
                                }
                                ?>
                            </select>
                            <span class="input-group-addon"  style="width: 30%"></span>
                            <input  style="width: 100%"class="form-control" name="RWAKR_THNAKR" id="RWAKR_THNAKR" placeholder="Tahun" maxlength="4">
                        </div>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>* Jabatan (Berdasarkan riwayat jabatan)</label>
                        <select class="form-control" id="RWAKR_JAB" name="RWAKR_JAB">  
                            <?php
                            foreach ($jabatan as $x) {
                                echo '<option value="' . $x->RWJAB_IDJAB . '">' . $x->RWJAB_NAMAJAB . '</option>';
                            }
                            ?>
                        </select>    
                    </div>
                    <div class="form-group col-lg-2 has-error">
                        <label>* Kredit Utama</label>
                        <input class="form-control nilai" name="RWAKR_AKRUTAMA" id="RWAKR_AKRUTAMA">
                    </div>
                    <div class="form-group col-lg-2 has-error">
                        <label>* Kredit Penunjang</label>
                        <input class="form-control nilai" name="RWAKR_AKRPENUNJANG" id="RWAKR_AKRPENUNJANG">
                    </div>
                    <div class="form-group col-lg-2 has-warning">
                        <label>Total Kredit</label>
                        <input class="form-control" name="RWAKR_AKRTOTAL" id="RWAKR_AKRTOTAL" readonly="">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-left">
                        <br>
                        <div class="input-group has-success" disabled="disabled" style="height: 80%">
                            <span class="input-group-addon label-success" style="width: 70%;text-align: left">Total Kredit Utama</span>
                            <input class="form-control input-sm" id="TOTUTAMA" disabled style="text-align: right">
                        </div>
                        <div class="input-group has-success" disabled="disabled">
                            <span class="input-group-addon label-success" style="width: 70%;text-align: left">Total Kredit Penunjang</span>
                            <input class="form-control input-sm" id="TOTPENUNJANG" disabled style="text-align: right">
                        </div>
                    </div>
                    <div class="pull-right">
                        <div> 
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   

                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Nomor SK</th>
                            <th>Tanggal SK</th>
                            <th>Kredit Utama</th>
                            <th>Kredit Penunjang</th>
                            <th>Total Kredit</th>
                            <th>Jabatan</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <script>

        $('.nilai').keyup(function () {
            hitung();
        })
        function hitung() {
            var sum = 0;
            var divider = 5;
            $('.nilai:not(:disabled)').each(function () {
                sum += (parseFloat($(this).val() == '' ? 0 : $(this).val()));
            });
            $("#RWAKR_AKRTOTAL").val(sum.toFixed(3));
        }
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        var xutama = 0;
        var xpenunjang = 0;
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/angka_kredit_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columnDefs": [
                {
                    "targets": [7],
                    "visible": false,
                    "searchable": false
                },
            ],
            "columns": [
                {"data": "RWAKR_ID", "class": "text-center", },
                {"data": "RWAKR_NOSK"},
                {"data": "RWAKR_TGLSK"},
                {"data": "RWAKR_AKRUTAMA"},
                {"data": "RWAKR_AKRPENUNJANG"},
                {"data": "RWAKR_AKRTOTAL"},
                {"data": "NAMAJAB"},
                {"data": "URUTAN"},
                {"data": "RWAKR_NIP", "class": "text-center"},
            ],
            "order": [[7, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var edit = ' <a class="edit btn btn-xs btn-info" id="' + data.RWAKR_ID + '">edit</a>'
                var hapus = ' <a onclick="hapus(\'' + data.RWAKR_ID + '\',\'' + data.RWAKR_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(edit + hapus);

                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };
                if (index == 1) {
                    xutama = 0;
                    xpenunjang = 0;
                }
                xutama = xutama + intVal(data.RWAKR_AKRUTAMA);
                xpenunjang = xpenunjang + intVal(data.RWAKR_AKRPENUNJANG);
                if (info.iEnd == index) {
                    $('#TOTUTAMA').val(xutama.toFixed(3));
                    $('#TOTPENUNJANG').val(xpenunjang.toFixed(3));
                }

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/angka_kredit_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#simpan").click(function () {
            $("#myForm").submit();
        });

        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myForm').trigger("reset");
            $('#RWAKR_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWAKR_NOSK").val(rowData.RWAKR_NOSK);
            $("#RWAKR_TGLSK").val(rowData.RWAKR_TGLSK);
            $("#RWAKR_AKRUTAMA").val(rowData.RWAKR_AKRUTAMA);
            $("#RWAKR_AKRPENUNJANG").val(rowData.RWAKR_AKRPENUNJANG);
            $("#RWAKR_AKRTOTAL").val(rowData.RWAKR_AKRTOTAL);
            $("#RWAKR_BLNAWL").val(rowData.RWAKR_BLNAWL);
            $("#RWAKR_THNAWL").val(rowData.RWAKR_THNAWL);
            $("#RWAKR_BLNAKR").val(rowData.RWAKR_BLNAKR);
            $("#RWAKR_THNAKR").val(rowData.RWAKR_THNAKR);
            $("#RWAKR_AKRPERTAMA").prop('checked', rowData.RWAKR_AKRPERTAMA === 'V' ? true : false);
            $("#RWAKR_JAB").val(rowData.RWAKR_JAB);
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/angka_kredit_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
