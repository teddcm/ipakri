<?php
require 'v-listing#search.php';
//$this->output->cache(1);
//$this->output->cache(60);
$NIP = $this->uri->segment(4);
$pnssex = get_data_pns($NIP, 'PNS_PNSSEX');
if ($pnssex != NULL) {
    ?>
    <div class="modal fade" id="hapusModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    Yakin akan menghapus pegawai ini? Pegawai yang sudah dihapus <b>TIDAK DAPAT</b> dikembalikan lagi.
                    <div class="clearfix"></div>
                    <br>
                    <div class="form-group">
                        <label>Masukkan Password untuk melanjutkan :</label>
                        <input type="password" id="passwordHapus" class="form-control input-sm">
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                    <button class="btn btn-danger btn-flat" onclick="hapus()"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <input type="text" id="find_tk2" class="form-control input-sm" placeholder="Cari Kabupaten/Kota . . .">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-flat">Pilih</button>
                </div>
            </div>
        </div>
    </div>

    <form id="myForm"> 
        <input type="hidden" id="mode" name="mode" value="edit" >
        <input type="hidden" id="nip_lama" name="nip_lama" value="<?php echo $NIP; ?>" >
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php $maintab = $this->session->userdata('maintab'); ?>
                <li id="lt1"><a id="t1" href="#tab_1" data-toggle="tab" aria-expanded="true" class="maintab">Data Pribadi</a></li>
                <li id="lt2"><a id="t2" href="#tab_2" data-toggle="tab" aria-expanded="true" class="maintab">Posisi & Jabatan</a></li>
                <li class="pull-right">
                    <div> 
                        <a class="btn btn-success btn-flat n-a" href="#" id="simpan"style="display: none">simpan</a> 
                        <a class="btn btn-danger btn-flat n-a" href="#" id="cancel"style="display: none">cancel</a> 
                        <a class="btn btn-info btn-flat n-a" href="#" id="edit">edit</a> 
                        <a class="btn btn-flat btn-default n-a" href="<?php echo base_url(); ?>/admin/report/profil_cetak?NIP=<?php echo $NIP; ?>" target="_BLANK"><i class="fa fa-print"></i></a>
                        <?php if ($this->ion_auth->in_group('data_utama_hapus')) { ?>
                            <a class="btn btn-danger btn-flat n-a" href="#" id="hapus"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                        <a class="btn btn-warning btn-flat" href="#" id="backPage"><i class="fa fa-backward"></i> data pegawai</a>

                    </div>
                </li>
            </ul>
            <div class="tab-content n-a"> 
                <div class="col-lg-2">
                    <div id="getFoto"></div>
                </div>
                <div class="tab-pane" id="tab_1">
                    <div class="form-group col-lg-3">
                        <label>* NIP Baru</label>
                        <input class="form-control" id="PNS_NIPBARU" name="PNS_NIPBARU" disabled maxlength="18">
                    </div>
                    <div class="form-group col-lg-3">
                        <label>NIP Lama</label>
                        <input class="form-control" name="PNS_PNSNIP" id="PNS_PNSNIP" disabled>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>* Nama</label>
                        <input class="form-control" name="PNS_PNSNAM" id="PNS_PNSNAM" disabled>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Gelar Depan</label>
                        <input class="form-control" style="text-transform: none" name="PNS_GLRDPN" id="PNS_GLRDPN">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Gelar Belakang</label>
                        <input class="form-control" style="text-transform: none" name="PNS_GLRBLK" id="PNS_GLRBLK">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Tempat Lahir</label>
                        <div class="input-group" id="cariLokasi">
                            <input id="PNS_TEMLHR_TXT" class="form-control" >
                            <input type="hidden" id="PNS_TEMLHR" name="PNS_TEMLHR">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary"><i class="fa fa-search"></i></button>
                            </span>

                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Tanggal Lahir</label>
                        <input class="form-control datepicker" id="PNS_TGLLHRDT" name="PNS_TGLLHRDT" disabled>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Jenis Kelamin</label>
                        <select class="form-control" id="PNS_PNSSEX" name="PNS_PNSSEX" disabled >
                            <option value="">-</option>
                            <option value="1">Pria</option>
                            <option value="2">Wanita</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Agama</label>
                        <select class="form-control" id="PNS_KODAGA" name="PNS_KODAGA">
                            <option value="">-</option>
                            <?php
                            foreach ($agama as $ag) {
                                echo '<option value="' . $ag->AGA_KODAGA . '">' . $ag->AGA_NAMAGA . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Email</label>
                        <input type="email" style="font-size: 12px;text-transform: none" class="form-control" id="PNS_EMAIL" name="PNS_EMAIL">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Jenis Dokumen</label>
                        <select class="form-control" id="PNS_JENDOK" name="PNS_JENDOK" >
                            <option value="">-</option>
                            <option value="1">KTP</option>
                            <option value="2">Passport</option>
                            <option value="3">SIM</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Nomor Dokumen</label>
                        <input class="form-control" id="PNS_NOMDOK" name="PNS_NOMDOK">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-8">
                        <label>Alamat</label>
                        <input class="form-control" style="font-size: 12px;" id="PNS_ALAMAT" name="PNS_ALAMAT">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor HP</label>
                        <input class="form-control" id="PNS_NOMHP" name="PNS_NOMHP">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor Telepon</label>
                        <input class="form-control" id="PNS_NOMTEL" name="PNS_NOMTEL">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-6">
                        <label>* Kedudukan PNS</label>
                        <select class="form-control" id="PNS_KEDHUK" name="PNS_KEDHUK">
                            <?php
                            foreach ($kedhuk as $x) {
                                echo '<option value="' . $x->KED_KEDKOD . '">' . $x->KED_KEDNAM . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* TMT CPNS/Awal</label>
                        <input class="form-control datepicker" id="PNS_TMTCPN" name="PNS_TMTCPN">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>TMT PNS</label>
                        <input class="form-control datepicker" id="PNS_TMTPNS" name="PNS_TMTPNS" disabled>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Status Pegawai</label>
                        <select class="form-control" id="PNS_STCPNS" name="PNS_STCPNS" >
                            <option value="">-</option>
                            <option value="C">CPNS</option>
                            <option value="P">PNS</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-2">
                        <label>Tingkat Pendidikan</label>
                        <select class="form-control" id="PNS_TKTDIK" disabled>
                            <option value="">-</option>
                            <?php
                            foreach ($tktpendik as $x) {
                                echo '<option value="' . $x->DIK_TKTDIK . '">' . $x->DIK_NAMDIK . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Diklat Struktural</label>
                        <input class="form-control" disabled id="PNS_LATSTR">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-2">
                        <label>Pendidikan Terakhir</label>
                        <input class="form-control" disabled id="PEN_PENKOD">
                    </div>
                    <div class="form-group col-lg-8">
                        <label>&nbsp;</label>
                        <input class="form-control" disabled id="DIK_NMDIK">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tahun Lulus</label>
                        <input class="form-control" disabled id="PEN_TAHLUL">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-3">
                        <label>Status Kawin</label>
                        <select class="form-control" id="PNS_STSWIN" name="PNS_STSWIN" disabled >
                            <option value="">-</option>
                            <option value="1">Menikah</option>
                            <option value="2">Cerai</option>
                            <option value="3">Janda/Duda</option>
                            <option value="4">Belum Menikah</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Jumlah Keluarga</label>
                        <div class="input-group" disabled="disabled">
                            <span class="input-group-addon" disabled="disabled"><?php echo $pnssex == 1 ? 'Istri' : 'Suami'; ?></span>
                            <input class="form-control" disabled="disabled" id="PNS_JMLIST">
                            <span class="input-group-addon" disabled="disabled">Anak</span>
                            <input class="form-control" disabled="disabled" id="PNS_JMLANK">
                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>No. Seri Karpeg</label>
                        <input class="form-control" id="PNS_KARPEG" name="PNS_KARPEG">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <div class="form-group col-lg-10">
                        <label>Instansi Kerja </label>
                        <div class="input-group">
                            <input class="form-control" disabled id='PNS_INSKER' size="1">
                            <span class="input-group-addon"></span>
                            <input class="form-control" disabled id='INS_NAMINS'>
                        </div>
                    </div>
                    <div class="form-group col-lg-10">
                        <label>Unit Organisasi</label>
                        <div class="input-group">
                            <input class="form-control" disabled id='UNO_KODUNO' size="1">
                            <span class="input-group-addon"></span>
                            <input class="form-control" disabled id='UNO_NAMUNO'>
                        </div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Jenis Jabatan</label>
                        <select class="form-control" id="RWJAB_IDJENJAB">
                            <option value="">-</option>
                            <option value="1">Struktural</option>
                            <option value="2">Fungsional Tertentu</option>
                            <option value="3">Rangkap</option>
                            <option value="4">Fungsional Umum</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-5">
                        <label>Nama Jabatan</label>
                        <input class="form-control" disabled id="RWJAB_NAMAJAB">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>TMT Jabatan</label>
                        <input class="form-control" disabled id="RWJAB_TMTJAB">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Golru Awal</label>
                        <select class="form-control" id="PNS_GOLAWL">
                            <option value="">-</option>
                            <?php
                            foreach ($golru as $x) {
                                echo '<option value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-2">
                        <label>Golru Terakhir</label>
                        <select class="form-control" disabled id="PNS_GOLRU">
                            <option value="">-</option>
                            <?php
                            foreach ($golru as $x) {
                                echo '<option value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>TMT Golongan</label>
                        <input class="form-control" disabled id="PNS_TMTGOL">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Masa Kerja</label>
                        <div class="input-group">
                            <span class="input-group-addon">Tahun</span>
                            <input class="form-control" disabled id="PNS_THNKER">
                            <span class="input-group-addon">Bulan</span>
                            <input class="form-control" disabled id="PNS_BLNKER">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
    </form>

    <script>
        $(".maintab").click(function () {
            localStorage['maintab'] = $(this).attr('id');
        })
        if (localStorage['maintab'] != 't2') {
            $("#lt1").addClass('active');
            $("#tab_1").addClass('active');
        } else {
            $("#lt2").addClass('active');
            $("#tab_2").addClass('active');
        }
        $(document).ready(function () {
            $.getJSON("<?php echo base_url('admin/json/json_data_pegawai/' . $NIP); ?>", function (result) {
                $.each(result, function (i, field) {
                    if (field != null) {
                        var date = ['PNS_TMTPNS', 'PNS_TMTCPN', 'PNS_TMTGOL'];
                        var select = ['PNS_PNSSEX', 'PNS_JENDOK', 'PNS_KODAGA', 'PNS_KEDHUK', 'PNS_STCPNS', 'PNS_TKTDIK', 'PNS_GOLAWL', 'PNS_GOLRU', 'RWJAB_IDJENJAB'];
                        if (jQuery.inArray(i, date) >= 0) {
                            //alert(field.substr(8, 2)+'-'+field.substr(5, 2)+'-'+field.substr(0, 4));
                            $("#" + i).val(field.substr(8, 2) + '-' + field.substr(5, 2) + '-' + field.substr(0, 4));
                        } else {
                            $("#" + i).val(field);
                        }
                    }
                });
            });

            $('#backPage').click(function () {
                var nonaktif = $('#PNS_KEDHUK').find(":selected").val() > 20 ? 'nonaktif' : '';
                loadContent('admin/pegawai/' + nonaktif);
            })
        });
        $('#PNS_NIPBARU').on('focusout', function () {
            var nip = $('#PNS_NIPBARU').val();
            $('#PNS_TGLLHRDT').val(nip != '' ? (nip.substring(6, 8) + '-' + nip.substring(4, 6) + '-' + nip.substring(0, 4)) : '');
            $('#PNS_TMTCPN').val(nip != '' ? ('01-' + nip.substring(12, 14) + '-' + nip.substring(8, 12)) : '');
            $('#PNS_PNSSEX').val(nip != '' ? nip.substring(14, 15) : '');
        })

        jQuery(function ($) {
            $("input[name=PNS_NIPBARU]").mask("999999999999999999", {placeholder: ""});
            $("input[name=PNS_PNSNIP]").mask("999999999", {placeholder: ""});
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $(".numeric").mask("9999999?999999", {placeholder: ""});

        });
        var find_tk2 = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                cache: false,
                url: '<?= base_url('admin/json/json_temlahir'); ?>/2?q=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('#find_tk2').typeahead(null, {
            name: 'best-pictures',
            display: 'name',
            source: find_tk2,
            templates: {
                suggestion: function (data) {
                    return '<p>' + data.ket + ' – <strong>' + data.name + '</strong></p>';
                },
            }
        }).on('typeahead:selected', function (evt, item) {
            $('#PNS_TEMLHR_TXT').val(item.name);
            $('#PNS_TEMLHR').val((item.id + "0000000000").slice(0, 10));
            $('#myModal').modal('hide')

        });


        $("#cariLokasi *").attr("disabled", true);
        $('#cariLokasi').on('focusin', function (e) {
            $('#myModal').modal('show')
            $("#find_tk2").parent('span').show();
            $("#kecamatan").attr("checked", false)
            $("#cariLokasi *").blur();
            e.preventDefault();
        })
        $('#myModal').on('shown.bs.modal', function () {
            $("#find_tk2").focus();
        })

        $(".tab-content *").attr("disabled", true);
        $("#edit").click(function () {
            $(".tab-content * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#edit").hide();
            $("#cariLokasi *").attr("disabled", false);
        })
        $("#hapus").click(function () {
            $('#hapusModal').modal('show')
        })
        $("#cancel").click(function () {
            loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/data_utama_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url('admin/pegawai/data_utama/'); ?>' + $("input[name='PNS_NIPBARU']").val());
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        function hapus() {
            if ($("#passwordHapus").val() == '') {
                alert('Masukkan Password');
            } else {
                var e = confirm('Data Pegawai ini akan dihapus?');
                var pass = $("#passwordHapus").val();
                var nip = '<?php echo $this->uri->segment(4); ?>';
                if (e) {
                    var url = "<?php echo base_url('admin/pegawai/data_utama_hapus'); ?>"; // the script where you handle the form input.
                    $.ajax({
                        type: "POST",
                        url: url,
                        async: false,
                        data: {"nip": nip, "pass": pass},
                        dataType: "json",
                        success: function (result)
                        {
                            $.notify(result[1], result[0]);
                            if (result[0] === 'success') {
                                $('#hapusModal').modal('hide');
                                setTimeout(function () {
                                    loadContent('<?php echo base_url('admin/pegawai'); ?>');
                                }, 2000);
                            } else {
                                $("#passwordHapus").val('')
                                $('#hapusModal').modal('hide');
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            cekError(XMLHttpRequest, textStatus);
                        },
                    })
                }
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
