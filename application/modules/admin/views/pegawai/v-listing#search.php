<?php
$CTRL = $this->uri->segment(2);
$PAGE = $this->uri->segment(3);
$NIP = $this->uri->segment(4);
$this->session->set_userdata('nip_now', $NIP);

//echo convert_tempat_lahir(3300000000);
//print_r(get_nip_unor($this->ion_auth->user()->row()->IDUNO));
$IDUNO = $this->ion_auth->user()->row()->IDUNO; //dapatkan data IDUNO dari user saat ini
$getNIPUnor = get_nip_unor($IDUNO); //dapatkan array nip yang termasuk dalam Unor tersebut
//print_r($getNIPUnor);
$freeAccess = ($PAGE == '' || $PAGE == 'nonaktif' || $NIP == '' || (get_data_pns($NIP, 'PNS_KEDHUK')>=20&&$PAGE=='data_utama')) ? TRUE : FALSE; //apakah termasuk halaman free akses
$nipUnor = searchForId($NIP, $getNIPUnor); //cari apakah NIP ini terdapat dalam Unor tersebut
$hasUnor = convert_unor(get_data_pns($NIP, 'PNS_UNOR')) != NULL;
$hasJabatan = get_data_pns($NIP, 'PNS_JNSJAB') != NULL;
$noAccess = !$nipUnor && !$freeAccess && $hasUnor && $hasJabatan;
$nipExist = get_data_pns($NIP);
$listManajemenGroups = array('data_utama', 'golongan', 'jabatan', 'peninjauan_masa_kerja', 'diklat', 'kursus', 'pendidikan', 'unit_organisasi', 'skp', 'angka_kredit', 'kgb', 'keluarga', 'cuti', 'xxx', 'xxx', 'xxx', 'xxx');
$noKelola = in_array(strtolower($PAGE), $listManajemenGroups) && !$this->ion_auth->in_group(strtolower($PAGE) . '_kelola');
//echo $freeAccess;
?>
<style>
<?php if ($noAccess || $noKelola) { ?>                                                                                                                                                                   
        .n-a{
            pointer-events: none;
        }
        .tab-content{
            background-color: #e9e9e9!important;
        }  
<?php } ?>
    .dropdown-menu {
        margin-top: 0; 
    }
    .dropdown-menu:hover {
        cursor: pointer
    }
    .dropdown-menu > li > a{
        margin-top: 1px; 
        padding: 10px; 
        color: white !important;
        background-color: #337ab7;
    }
    .dropdown-menu > li > a:hover{
        /*color: black !important;*/
        background-color: black;
    }
</style>
<?php if ($noAccess) { ?>
    <div class="alert alert-warning">
        <i class="icon fa fa-warning"></i> Anda tidak memiliki akses untuk mengelola data pegawai ini.
    </div>

    <?php
} else if ($noKelola) {
    ?>
    <div class="alert alert-warning">
        <i class="icon fa fa-warning"></i> Anda tidak memiliki akses untuk mengelola <?php echo strtoupper(str_replace('_', ' ', $PAGE)); ?>.
    </div>

    <?php
}
if (!$hasJabatan && !$freeAccess && $nipExist) {
    ?>
    <div class="alert alert-warning">
        <i class="icon fa fa-warning"></i> Pegawai ini belum terdefinisi Jabatannya. Mohon dilengkapi pada menu <b class="h5 bg-blue-active" style="cursor: pointer" onclick="loadContent('<?php echo base_url('admin/pegawai/jabatan/' . $NIP); ?>')">&nbsp;jabatan&nbsp;</b>
    </div>
<?php } else if (!$hasUnor && !$freeAccess && $nipExist) { ?>
    <div class="alert alert-warning">
        <i class="icon fa fa-warning"></i> Pegawai ini belum terdefinisi Unit Organisasinya. Mohon dilengkapi pada menu Unor.
    </div>

<?php } ?>
<?php if ($PAGE != 'nonaktif') { ?>
    <form class="form-inline pull-left" id="cari_nip">
        <div class="form-group">
            <input class="form-control" style="width: 255px;" type="text" id="NIP" value="<?php echo $NIP != '' ? $NIP : ''; ?>" placeholder="Cari NIP / Nama . . . ">
        </div>
        <div class="form-group">
            <input class="form-control" style="width: 255px; font-size: 13px;"  type="text" disabled value="<?php echo $NIP != '' ? get_data_pns($NIP) : ''; ?>">
        </div>
    </form>
<?php } ?>

<div class="pull-right">
    <?php
    if ($PAGE == '') {
        if ($CTRL == 'pegawai' && $this->ion_auth->in_group('data_utama_tambah')) {
            ?>
            <div class="btn-group pull-right">
                <a onclick="loadContent('<?php echo base_url('admin/pegawai/penetapan_nip'); ?>')" class="btn btn-info btn-flat">Tambah Pegawai</a>
            </div>
            <?php
        }
    } else if ($PAGE == 'nonaktif') {
        ?>
        <div class="btn-group pull-right">
        </div>
    <?php } else { ?>
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-sm btn-flat btn-microsoft dropdown-toggle drop" data-toggle="dropdown" style="width:210px">
                <i class="fa fa-fw fa-navicon"></i> <b class="text-right"><?php echo ucwords(str_replace('_', ' ', $this->uri->segment(3))); ?></b>
            </button>
            <button type="button" class="btn btn-sm btn-flat btn-default dropdown-toggle drop" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu drop drop-menu" style="margin-right: 210px;width: 210px"> 
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/data_utama/') . $this->uri->segment(4); ?>')" title="Data Utama"><i class="fa fa-fw fa-database"></i>Data Utama</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/jabatan/') . $this->uri->segment(4); ?>')" title="Mutasi Jabatan / Pengangkatan"><i class="fa fa-fw fa-briefcase"></i>Jabatan</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/diklat/') . $this->uri->segment(4); ?>')" title="Diklat"><i class="fa fa-fw fa-book"></i>Diklat</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/unit_organisasi/') . $this->uri->segment(4); ?>')" title="Unit Organisasi"><i class="fa fa-fw fa-university"></i>Unit Organisasi</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/kursus/') . $this->uri->segment(4); ?>')" title="Kursus"><i class="fa fa-fw fa-certificate"></i>Kursus</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/angka_kredit/') . $this->uri->segment(4); ?>')" title="Angka Kredit"><i class="fa fa-fw fa-gg"></i>Angka Kredit</a></li>
            </ul>
            <ul class="dropdown-menu drop drop-menu" style="margin-right: 5px;width: 210px"> 
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/golongan/') . $this->uri->segment(4); ?>')" title="Golongan / Kenaikan Pangkat"><i class="fa fa-fw  fa-line-chart"></i>Golongan / KP</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/peninjauan_masa_kerja/') . $this->uri->segment(4); ?>')" title="Peninjauan Masa Kerja"><i class="fa fa-fw fa-newspaper-o"></i>Peninjauan Masa Kerja</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/pendidikan/') . $this->uri->segment(4); ?>')" title="Pendidikan"><i class="fa fa-fw fa-mortar-board"></i>Pendidikan</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/KGB/') . $this->uri->segment(4); ?>')" title="KGB"><i class="fa fa-fw fa-money"></i>KGB</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/SKP/') . $this->uri->segment(4); ?>')" title="SKP"><i class="fa fa-fw fa-percent"></i>SKP</a></li>
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/keluarga/') . $this->uri->segment(4); ?>')" title="Keluarga"><i class="fa fa-fw fa-sitemap"></i>Keluarga</a></li>
                <!--li><a onclick="loadContent('<?php echo base_url('admin/pegawai/Cuti/') . $this->uri->segment(4); ?>')" title="Cuti"><i class="fa fa-fw fa-calendar"></i>Cuti</a></li-->
                <li><a onclick="loadContent('<?php echo base_url('admin/pegawai/Cuti/') . $this->uri->segment(4); ?>')" title="Cuti"><i class="fa fa-fw fa-calendar"></i>Cuti</a></li>
            </ul>
        </div>
    <?php }; ?>
</div>
<div class="clearfix"></div>
<br>
<script>
<?php if (isset($NIP)) { ?>
        $('#getFoto').append('<img src="<?php echo base_url(); ?>/contents/foto/nofoto.jpg?x" class="img-rounded" width="100%" style="opacity: 0;">');
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('admin/json/json_foto_pegawai/' . $NIP); ?>",
            dataType: "html",
            success: function (img) {
                $('#getFoto').html(img);
            },
            error: function (error, txtStatus) {
                console.log(txtStatus);
                console.log('error');
            }
        });
<?php } ?>
    var vpendidikan = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '<?= base_url('admin/json/json_pegawai'); ?>?q=y',
        remote: {
            url: '<?= base_url('admin/json/json_pegawai'); ?>?q=%QUERY',
            wildcard: '%QUERY',
            cache: false
        }
    });
    $('#NIP').typeahead(null, {
        name: 'pendidikan',
        display: 'PNS_NIPBARU',
        limit: 15,
        source: vpendidikan,
        templates: {
            empty: [
                '<div class="tt-suggestion">',
                'DATA TIDAK DITEMUKAN',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return '<p><strong>' + data.PNS_NIPBARU + '</strong><br><u> ' + data.PNS_PNSNAM + '</u></p>';
            }
        }
    }).on('typeahead:selected', function (event, data) {
        $('#NIP').val(data.PNS_NIPBARU);
        loadContent('<?php echo base_url('admin/pegawai') . '/' . ($PAGE == '' ? 'data_utama' : $PAGE) . '/'; ?>' + $('#NIP').val());
    });


    //$('#NIP').mask('999999999999999999', {placeholder: ""});
    $('#NIP').on("focus", function () {
        $('#NIP').select();
    })
    $('.drop').mouseenter(function () {
        $('.drop-menu').show();
    })
    $('.drop').mouseleave(function () {
        $('.drop-menu').hide();
    })
    $('#NIP').keypress(function (e) {
        var key = e.which;
        if (key == 13)
        {
            loadContent('<?php echo base_url('admin/pegawai') . '/' . ($PAGE == '' ? 'data_utama' : $PAGE) . '/'; ?>' + $('#NIP').val());
        }
    });

    $('label').each(function () {
        var html = $(this).html().replace(/\*/g, "<span class=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })
</script>
