<?php
//$this->output->cache(30);
$nonaktif = 0;
$tableName = 'HTable';
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/js/jquery.treegrid.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/css/jquery.treegrid.css">

<script type="text/javascript">
    function eselon(eselon) {
        var kodesl = {'99': 'NON',
            '11': 'I.a',
            '12': 'I.b',
            '21': 'II.a',
            '22': 'II.b',
            '31': 'III.a',
            '32': 'III.b',
            '41': 'IV.a',
            '42': 'IV.b',
            '51': 'V.a',
            '52': 'V.b'};
        return kodesl[eselon];
    }


    $(function () {
        function ucword(str) {
            var news = str.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });
            news = news.replace("Bkn", "BKN");
            news = news.replace("Viii", "VIII");
            return news;
        }
        $.getJSON("<?php echo base_url(); ?>admin/json/pegawai_jqtree", function (result) {
            var no = 1;
            $.each(result[0], function (i, field) {
                var $tr = $('<tr>').addClass('treegrid-' + field.id).addClass(field.parent == 0 ? '' : 'treegrid-parent-' + field.atasan).append(
                        $('<td>').text(ucword(field.jabatan)),
                        $('<td>').text(ucword(field.nama)),
                        $('<td>').html(ucword(field.unor)),
                        $('<td>').html('<a onclick="view(\'' + field.nip + '\')" class="edit btn btn-xs btn-info">detail</a> <a onclick="vieworg(\'' + field.nip + '\')" class="edit btn btn-xs btn-warning">unor</a>'),
                        $('<td>').html((no++)).addClass('text-center'),
                        ).appendTo('.tree');
            });

            no = 1;
            $.each(result[1], function (i, field) {
                $('#treex').show();
                var $tr = $('<tr>').append(
                        $('<td>').text(ucword(field.nama)),
                        $('<td>').html(ucword(field.nip)),
                        $('<td>').html('<a onclick="view(\'' + field.nip + '\')" class="edit btn btn-xs btn-info">detail</a> <a onclick="vieworg(\'' + field.nip + '\')" class="edit btn btn-xs btn-warning">unor</a>'),
                        $('<td>').html((no++)).addClass('text-center text-red'),
                        ).appendTo('.treex');
            });
            $('.tree').treegrid();

            $('a#pagetree-expand-all').on('click', function (e) {
                $('.tree').treegrid('expandAll');
            });
            $('a#pagetree-collapse-all').on('click', function (e) {
                $('.tree').treegrid('collapseAll');
            });
            $('span[class=treegrid-expander]').after('<span class="treegrid-expander file"></span>');
            $('.treegrid-expander-expanded,.treegrid-expander-collapsed').before('<span class="treegrid-indent buka"></span>');
        });
    });
</script>
<style>
    .treegrid-indent{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -176px 0;
    }
    .treegrid-indent.buka{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -192px 0;
    }
    .treegrid-expander{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -192px 0; 
    }
    .treegrid-expander.file{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -240px 0;
    }
    .treegrid-expander.treegrid-expander-expanded{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -224px 0;
    }
    .treegrid-expander.treegrid-expander-collapsed{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png');?>) no-repeat -208px 0;
    }
</style>
<div class="clearfix"></div>
<div class="box"> 
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-map"></i> Hirearki Pegawai</h3>
        <div class="pull-right">
            <a href="#" class="btn btn-default btn-flat" id="pagetree-collapse-all">Collapse all</a>        
            <a href="#" class="btn btn-default btn-flat" id="pagetree-expand-all">Expand all</a>        
        </div>
    </div>
    <div class="box-body">
        <table class="tree table table-bordered  table-hover" style="font-size: 11px;">
            <thead>
                <tr>
                    <th>Jabatan</th>
                    <th>Nama Lengkap</th>
                    <th>Nama Organisasi</th>
                    <th width="10%">Aksi</th>
                    <th width="5%">No</th>
                </tr>
            </thead>
        </table>
        <span id="treex" style="display: none">
            <br>
            <br>
            <br>       
            <h3 class="text-red"><i class="fa fa-exclamation"></i> Hierarki Pegawai Tidak Valid</h3>
            Hierarki pegawai berstatus tidak valid apabila :
            <ul>
                <li>Unor pegawai tidak terdefinisi</li>
                <li>Unor pegawai terdefinisi, namun unor tersebut tidak berelasi dengan <?php echo convert_unor(SKPD); ?></li>
                <li>Unor pegawai terdefinisi dan telah berelasi dengan <?php echo convert_unor(SKPD); ?>, namun tidak memiliki pejabat struktural</li>
            </ul>
            <span class="text-red">tips: lengkapi relasi dan pejabat setiap unor pada halaman <a href="#" onclick="loadContent('<?php echo base_url('manage/unor'); ?>')">unor</a></span>
            <table class="treex table table-bordered  table-hover" style="font-size: 11px;">
                <thead>
                    <tr>
                        <th>Nama Lengkap</th>
                        <th>NIP</th>
                        <th width="10%">Aksi</th>
                        <th width="5%">No</th>
                    </tr>
                </thead>
            </table>
        </span>
    </div>
</div>
<style>
    button.dt-button, div.dt-button, a.dt-button{
        background-image:none;
        background-color:#00acd6;
        color: white;
    }
    .dt-button:hover{
        background-image:none;
        background-color:white;
        color: #00acd6;
    }
</style>
<script>
    function view(id) {
        loadContent('<?php echo base_url('admin/pegawai/data_utama/'); ?>' + id);
    }
    function vieworg(id) {
        loadContent('<?php echo base_url('admin/pegawai/unit_organisasi/'); ?>' + id);
    }
</script>