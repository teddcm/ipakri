<?php
require 'v-listing#search.php';
//$this->output->cache(30);
$nonaktif = $this->uri->segment(3) != '' ? 1 : 0;
$tableName = $nonaktif == 1 ? 'NATable' : 'ATable';
?>
<div class="clearfix"></div>
<div class="box">
    <div class="box-body">
        <span class="text-bold text-aqua">UNIT KELOLA : <?php echo $KELOLA; ?></span><div class="clearfix"></div><br>
        <table id="<?php echo $tableName; ?>" class="table table-bordered  table-hover" style="font-size: 10px;color: <?php echo $nonaktif ? 'red' : 'black'; ?>">
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th>NIP</th>
                    <th width='30%'>Nama Lengkap</th>
                    <th>Gol</th>
                    <th><?php echo $nonaktif ? 'Status' : 'Pendidikan'; ?></th>
                    <th width='30%'>Unit Kerja</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>  
        </table>
    </div>
</div>
<style>
    button.dt-button, div.dt-button, a.dt-button{
        background-image:none;
        background-color:#00acd6;
        color: white;
    }
    .dt-button:hover{
        background-image:none;
        background-color:white;
        color: #00acd6;
    }
</style>
<script>
    var table = $('#<?php echo $tableName; ?>').DataTable({
        "language": {
            "infoFiltered": ""
        },
        "bStateSave": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "processing": false,
        "serverSide": true,
        "autoWidth": false,
        "ajax": "<?php echo site_url('admin/pegawai/listing_ajax_view/' . $nonaktif); ?>",
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columns": [
            {"data": "PNS_NIPBARU", "orderable": false, "class": "text-center", },
            {"data": "PNS_NIPBARU"},
            {"data": "PNS_PNSNAM"},
            {"data": "PNS_GOLRU"},
            {"data": "<?php echo $nonaktif ? 'PNS_KEDHUK' : 'PNS_TKTDIK'; ?>"},
            {"data": "PNS_UNOR"},
            {"data": "PNS_PNSNAM"},
        ],
        "stateDuration": 60 * 60,
        "order": [[3, 'desc'], [1, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            var aksi = '\
                <a href="#" onclick="view(\'' + id + '\')" class="btn btn-xs btn-flat btn-success">kelola <i class="fa fa-search" title="kelola pegawai"></i></a>\n\
                <a href="<?php echo base_url(); ?>/admin/report/profil_cetak?NIP=' + id + '" target="_BLANK" class="btn btn-xs btn-flat btn-default" title="cetak biodata"><i class="fa fa-print"></i></a>\n\
                ';
            $('td:eq(-1)', row).html(aksi);

        },
        dom: 'lBfrtip',
        buttons: [
            {
                className: 'btn btn-flat btn-primary btn-xs',
                text: 'Reset State Tabel',
                action: function (e, dt, node, config) {
                    table.state.clear();
                    loadContent('<?php echo base_url('admin/pegawai' . ($nonaktif ? '/nonaktif' : '')); ?>');
                }
            }
        ]
    });
    $('.dt-button').removeClass('dt-button');

    function view(id) {
        loadContent('<?php echo base_url('admin/pegawai/data_utama/'); ?>' + id);
    }

    $(document).ready(function () {
        var table = $('#<?php echo $tableName; ?>').DataTable();
        $('#<?php echo $tableName; ?> tbody').on('click', '.edit', function () {
            var data = table.row(this.parrent).data();
            view(data.PNS_NIPBARU);
        });
    });
</script>