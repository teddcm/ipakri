<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Kursus</a></li>
        </ul>
        <div class="tab-content n-a">     
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                       
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWKRS_NIP" name="RWKRS_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWKRS_ID" name="RWKRS_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>                 
                    <div class="form-group col-lg-4">
                        <label>* Jenis Kursus</label>
                        <select class="form-control" id="RWKRS_JNSKRS" name="RWKRS_JNSKRS"> 
                            <option value="1">Kursus</option>
                            <option value="2">Sertifikat</option>
                        </select>    
                    </div>         
                    <div class="form-group col-lg-6">
                        <label>* Nama Kursus</label>
                        <input class="form-control" name="RWKRS_NAMKRS" id="RWKRS_NAMKRS">
                    </div>     
                    <div class="form-group col-lg-2">
                        <label>Lama Kursus (Jam)</label>
                        <input class="form-control" name="RWKRS_JAM" id="RWKRS_JAM">
                    </div>     
                    <div class="form-group col-lg-2">
                        <label>Tanggal</label>
                        <input class="form-control datepicker" name="RWKRS_TGL" id="RWKRS_TGL">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tahun</label>
                        <input class="form-control" name="RWKRS_THN" id="RWKRS_THN">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Nomor</label>
                        <input class="form-control" name="RWKRS_NOM" id="RWKRS_NOM">
                    </div>
                    <div class="form-group col-lg-5">
                        <label>Instansi</label>
                        <select class="form-control" id="RWKRS_KODINS" name="RWKRS_KODINS">  
                            <?php
                            foreach ($instansi as $x) {
                                echo '<option value="' . $x->INS_KODINS . '">' . $x->INS_NAMINS . '</option>';
                            }
                            ?>
                        </select>    
                    </div> 
                    <div class="form-group col-lg-5">
                        <label>Institusi Penyelenggara</label>
                        <input class="form-control" name="RWKRS_INSTITUSI" id="RWKRS_INSTITUSI">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Tipe</th>
                            <th>Instansi</th>
                            <th>Nama Kursus</th>
                            <th>Tahun</th>
                            <th>Jam</th>
                            <th>Institusi Penyelenggara</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $("#RWKRS_THN").mask("9999", {placeholder: "_"});
            $("#RWKRS_JAM").mask("?9999", {placeholder: ""});

        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/kursus_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWKRS_ID", "class": "text-center", },
                {"data": "RWKRS_JNSKRS"},
                {"data": "RWKRS_KODINS"},
                {"data": "RWKRS_NAMKRS"},
                {"data": "RWKRS_THN"},
                {"data": "RWKRS_JAM"},
                {"data": "RWKRS_INSTITUSI"},
                {"data": "RWKRS_NIP", "class": "text-center"},
            ],
            "order": [[3, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWKRS_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWKRS_ID + '\',\'' + data.RWKRS_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/kursus_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $('#RWKRS_TGL').on('focusout', function () {
            var dt = $('#RWKRS_TGL').val().split('-');
            $("#RWKRS_THN").val(dt[2]);
        })

        $("#RWKRS_IDKRS").change(function () {
            $("#RWKRS_NAMKRS").val($("#RWKRS_IDKRS").find("option:selected").text());
        })

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#simpan").click(function () {
            $("#myForm").submit();
        });

        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myForm').trigger("reset");
            $('#RWKRS_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWKRS_JNSKRS option").filter(function () {
                return $(this).text() == rowData.RWKRS_JNSKRS;
            }).prop('selected', true)
            $("#RWKRS_KODINS option").filter(function () {
                return $(this).text() == rowData.RWKRS_KODINS;
            }).prop('selected', true)
            $("#RWKRS_NAMKRS").val(rowData.RWKRS_NAMKRS);
            $("#RWKRS_NOM").val(rowData.RWKRS_NOM);
            $("#RWKRS_TGL").val(rowData.RWKRS_TGL);
            $("#RWKRS_THN").val(rowData.RWKRS_THN);
            $("#RWKRS_JAM").val(rowData.RWKRS_JAM);
            $("#RWKRS_INSTITUSI").val(rowData.RWKRS_INSTITUSI);
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/kursus_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
