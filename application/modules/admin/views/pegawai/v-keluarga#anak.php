<div class="tab-pane" id="tab_2">
    <div id="anak">
        <div>

            <form id="myForm"> 
                <div>
                    <input type="submit" style="display: none" name="ok">
                    <input type="hidden" id="RWANK_NIP" name="RWANK_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                    <input type="hidden" id="RWANK_ID" name="RWANK_ID">
                    <input type="hidden" id="mode" name="mode" value="edit" > 
                </div>          
                <div class="form-group col-lg-4">
                    <label>* Nama</label>
                    <input class="form-control" name="RWANK_NAMA" id="RWANK_NAMA">
                </div>
                <div class="form-group col-lg-4">
                    <label>* Orang Tua</label>
                    <select class="form-control" id="RWANK_ID_ORTU" name="RWANK_ID_ORTU">  
                        <?php
                        foreach ($issu as $x) {
                            echo '<option value="' . $x->RWISU_ID . '">' . $x->RWISU_NAMISU . '</option>';
                        }
                        ?>
                    </select>    
                </div>
                <div class="form-group col-lg-2">
                    <label>* Status</label>
                    <select class="form-control" id="RWANK_STATUS" name="RWANK_STATUS">  
                        <?php
                        foreach ($status_anak as $x) {
                            echo '<option value="' . $x->STA_ID . '">' . $x->STA_NAMA . '</option>';
                        }
                        ?>
                    </select>    
                </div>
                <div class="form-group col-lg-2">
                    <label>* Jenis Kelamin</label>
                    <select class="form-control" name="RWANK_JK" id="RWANK_JK" disabled >
                        <option value="1">Laki-laki</option>
                        <option value="2">Perempuan</option>
                    </select>
                </div>
                <div class="form-group col-lg-2">
                    <label>Tanggal Lahir</label>
                    <input class="form-control datepicker" name="RWANK_TGLLHIR" id="RWANK_TGLLHIR">
                </div>
            </form>
            <div class="form-group col-lg-12">
                <div class="pull-right">
                    <div>
                        <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                        <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                        <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                    </div>
                </div>      
            </div>       
            <div class="clearfix"></div>
            &nbsp;
            <table id="myDataTable" class="table">
                <thead>
                    <tr>
                        <th width='5%'>No</th>
                        <th>Nama</th>
                        <th>Orang Tua</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Status</th>
                        <th width='10%'>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>  
            </table>
        </div>
    </div>
</div>


<script>
    jQuery(function ($) {
        $("#anak * .datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
        $("#anak * .datepicker").datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    $("#anak * #myDataTable").dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<"F">t<"F">',
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": "<?php echo site_url('admin/pegawai/keluarga_anak_ajax_view/') . $this->uri->segment(4); ?>",
        "iDisplayLength": 100,
        "columns": [
            {"data": "RWANK_ID", "class": "text-center", },
            {"data": "RWANK_NAMA"},
            {"data": "RWANK_ID_ORTU"},
            {"data": "RWANK_TGLLHIR"},
            {"data": "RWANK_JK"},
            {"data": "RWANK_STATUS"},
            {"data": "RWANK_NIP", "class": "text-center"},
        ],
        "order": [[3, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWANK_ID + '">edit</a>\n\
                        <a onclick="hapus(\'' + data.RWANK_ID + '\',\'' + data.RWANK_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
            $('td:eq(-1)', row).html(aksi);

        },
    });

    $("#anak * #myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pegawai/keluarga_anak_simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#anak * #myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    //loadContent('<?php echo base_url(uri_string()); ?>');
                    $('#myDataTable').DataTable().ajax.reload();
                    $("#anak * #cancel").trigger('click');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#anak * #myForm *").attr("disabled", true);
    $("#anak * #myForm").trigger("reset");
    $("#anak * #simpan").click(function () {
        $("#anak * #myForm").submit();
    });

    $("#anak * #myDataTable tbody").on('click', '.edit', function () {
        $("#anak * #myForm").trigger("reset");
        $("#anak * #RWANK_ID").val($(this).attr('id'));
        $("#anak * #myForm * [name]").attr("disabled", false);
        $("#anak * option").attr("disabled", false);
        $("#anak * #cancel").show();
        $("#anak * #simpan").show();
        $("#anak * #tambah").hide();
        $("#anak * #mode").val('edit');
        $("#anak * #mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("#anak * #RWANK_NAMA").val(selected.find('td:eq(1)').html());
        $("#anak * #RWANK_ID_ORTU option").filter(function () {
            return $(this).text() == selected.find('td:eq(2)').html();
        }).prop('selected', true)
        $("#anak * #RWANK_TGLLHIR").val(selected.find('td:eq(3)').html());
        $("#anak * #RWANK_JK option").filter(function () {
            return $(this).text() == selected.find('td:eq(4)').html();
        }).prop('selected', true)
        $("#anak * #RWANK_STATUS option").filter(function () {
            return $(this).text() == selected.find('td:eq(5)').html();
        }).prop('selected', true)
    });
    $("#anak * #cancel").click(function () {
        $("#anak * #myForm *").attr("disabled", true);
        $("#anak * #myForm").trigger("reset");
        $("#anak * #cancel").hide();
        $("#anak * #simpan").hide();
        $("#anak * #tambah").show();
        //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
    })
    $("#anak * #tambah").click(function () {
        $("#anak * #myForm * [name]").attr("disabled", false);
        $("#anak * option").attr("disabled", false);
        $("#anak * #mode").attr("disabled", false);
        $("#anak * #mode").val('tambah');
        $("#anak * #cancel").show();
        $("#anak * #simpan").show();
        $("#anak * #tambah").hide();
    })
    function hapus(id, nip) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('admin/pegawai/keluarga_anak_hapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": id, "nip": nip},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#anak * #cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }
</script>