<?php
require 'v-listing#search.php';

$tab = $this->uri->segment(5);
echo $tab;
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>
    <style>
        .arsir{
            background-color: #f1f1f1;
            background-image: linear-gradient(transparent 5%, rgba(200,255,255,1) 50%);
            background-size: 3px 3px;
        }
    </style>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="arsir maintab" id="t1"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Data Istri / Suami</a></li>
            <li class="arsir maintab" id="t2"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Data Anak</a></li>
        </ul>
        <div class="tab-content n-a"> 
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                        
            <?php require 'v-keluarga#issu.php'; ?>
    <?php require 'v-keluarga#anak.php'; ?>
        </div>
    </div>
    <script>
        $(".maintab").click(function () {
            localStorage['keluarga'] = $(this).attr('id');
        })
        if (localStorage['keluarga'] != 't2') {
            $("#t1").addClass('active');
            $("#tab_1").addClass('active');
        } else {
            $("#t2").addClass('active');
            $("#tab_2").addClass('active');
        }
    </script>
    <?php
} else {
    ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
