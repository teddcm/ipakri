<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Diklat</a></li>
        </ul>
        <div class="tab-content n-a">     
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                       
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWDLT_NIP" name="RWDLT_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWDLT_ID" name="RWDLT_ID">
                        <input type="hidden" name="RWDLT_NAMDLT" id="RWDLT_NAMDLT">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>                    
                    <div class="form-group col-lg-4">
                        <label>* Nama Diklat Struktural</label>
                        <select class="form-control" id="RWDLT_IDDLT" name="RWDLT_IDDLT">  
                            <?php
                            foreach ($diklat as $x) {
                                echo '<option value="' . $x->DLT_ID . '">' . $x->DLT_NAME . '</option>';
                            }
                            ?>
                        </select>    
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal</label>
                        <input class="form-control datepicker" name="RWDLT_TGL" id="RWDLT_TGL">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor</label>
                        <input class="form-control" name="RWDLT_NOM" id="RWDLT_NOM">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Tahun</label>
                        <input class="form-control" name="RWDLT_THN" id="RWDLT_THN">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Tahun</th>
                            <th>Nama Diklat Struktural</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $("#RWDLT_THN").mask("9999", {placeholder: "_"});

        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/diklat_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWDLT_ID", "class": "text-center", },
                {"data": "RWDLT_NOM"},
                {"data": "RWDLT_TGL"},
                {"data": "RWDLT_THN"},
                {"data": "RWDLT_NAMDLT"},
                {"data": "RWDLT_NIP", "class": "text-center"},
            ],
            "order": [[3, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWDLT_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWDLT_ID + '\',\'' + data.RWDLT_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/diklat_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $('#RWDLT_TGL').on('focusout', function () {
            var dt = $('#RWDLT_TGL').val().split('-');
            $("#RWDLT_THN").val(dt[2]);
        })

        $("#RWDLT_IDDLT").change(function () {
            $("#RWDLT_NAMDLT").val($("#RWDLT_IDDLT").find("option:selected").text());
        })

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#simpan").click(function () {
            $("#myForm").submit();
        });

        $('#myDataTable tbody').on('click', '.edit', function () {
            $('#myForm').trigger("reset");
            $('#RWDLT_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWDLT_NOM").val(selected.find('td:eq(1)').html());
            $("#RWDLT_TGL").val(selected.find('td:eq(2)').html());
            $("#RWDLT_THN").val(selected.find('td:eq(3)').html());
            $("#RWDLT_IDDLT option").filter(function () {
                return $(this).text() == selected.find('td:eq(4)').html();
            }).prop('selected', true)
            $('#RWDLT_IDDLT').trigger("change");
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/diklat_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
