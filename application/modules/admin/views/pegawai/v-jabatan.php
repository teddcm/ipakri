<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>
    <div class="modal fade" id="myModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <form id="newForm">  
                <div class="modal-content">
                    <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                            <div id="pilih_unor"></div>
                        </div>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Jabatan</a></li>
        </ul>
        <div class="tab-content n-a">  
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>             
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWJAB_NIP" name="RWJAB_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWJAB_ID" name="RWJAB_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >         
                    </div>                    
                    <div class="form-group col-lg-6">
                        <label>* Jenis Jabatan</label>
                        <select class="form-control" name="RWJAB_IDJENJAB" id="RWJAB_IDJENJAB">
                            <?php
                            foreach ($jenjab as $x) {
                                if ($x->JJB_JJBKOD != 3)
                                    echo '<option value="' . $x->JJB_JJBKOD . '">' . $x->JJB_JJBNAM . '</option>';
                            }
                            ?>
                        </select>                               
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* TMT Jabatan</label>
                        <input class="form-control datepicker" name="RWJAB_TMTJAB" id="RWJAB_TMTJAB">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal SK</label>
                        <input class="form-control datepicker" name="RWJAB_TGLSK" id="RWJAB_TGLSK">
                    </div>
                    <div class="form-group col-lg-6" id="cariLokasi">
                        <label>* Unit Organisasi</label>
                        <input class="form-control typeahead"  name="RWJAB_NAMUNO" id="RWJAB_NAMUNO">
                        <input type="hidden" name="RWJAB_IDUNO" id="RWJAB_IDUNO">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor SK</label>
                        <input class="form-control" name="RWJAB_NOMSK" id="RWJAB_NOMSK">
                    </div>
                    <div class="form-group col-lg-6"  id="gjs">
                        <label>Jabatan Struktural</label>
                        <div class="input-group">
                            <input class="form-control" name="RWJAB_NAMAJAB_JS" id="RWJAB_NAMAJAB_JS">
                            <span class="input-group-addon" ></span>
                            <select class="form-control" name="RWJAB_IDESL" id="RWJAB_IDESL" size="1">
                                <?php
                                foreach ($eselon as $x) {
                                    echo '<option value="' . $x->ESE_KODESL . '">' . $x->ESE_NAMESL . '</option>';
                                }
                                ?>
                            </select>  
                        </div>
                    </div>
                    <div class="form-group col-lg-6" id="gjft">
                        <label>Jabatan Fungsional Tertentu</label>
                        <input type="hidden" name="RWJAB_IDJAB" id="RWJAB_IDJAB" >
                        <input class="form-control typeahead" name="RWJAB_NAMAJAB_JFT" id="RWJAB_NAMAJAB_JFT">
                    </div>
                    <div class="form-group col-lg-6" id="gjfu">
                        <label>Jabatan Fungsional Umum</label>
                        <input class="form-control" name="RWJAB_NAMAJAB_JFU" id="RWJAB_NAMAJAB_JFU">
                    </div>  
                    <div class="form-group col-lg-2" id="tmtltk">
                        <label>TMT Pelantikan</label>
                        <input class="form-control datepicker" name="RWJAB_TMTLANTIK" id="RWJAB_TMTLANTIK">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Jenis Jab</th>
                            <th>Nama Jab</th>
                            <th>Organisasi</th>
                            <th>Eselon</th>
                            <th>TMT Jab</th>
                            <th>-</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>
        $('#pilih_unor').tree({
            dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
        });
        $('#select').click(function () {
            var node = $('#pilih_unor').tree('getSelectedNode');
            $("#RWJAB_NAMUNO").val(node.name);
            $("#RWJAB_IDUNO").val(node.id);
            $("#RWJAB_NAMAJAB_JS").val(node.pejabat);
            $("#RWJAB_IDESL").val(node.eselon);
            $('#myModal').modal('hide');
            //alert(node.name);
        })
        $("#cariLokasi *").attr("disabled", true);
        $('#cariLokasi').on('focusin', function (e) {
            $('#myModal').modal('show')
            $("#cariLokasi *").blur();
            e.preventDefault();
        })
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/jabatan_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columnDefs": [
                {
                    "targets": [6],
                    "visible": false,
                    "searchable": false
                },
            ],
            "columns": [
                {"data": "RWJAB_ID", "class": "text-center", },
                {"data": "RWJAB_IDJENJAB"},
                {"data": "RWJAB_NAMAJAB"},
                {"data": "RWJAB_NAMUNO"},
                {"data": "RWJAB_IDESL"},
                {"data": "RWJAB_TMTJAB"},
                {"data": "URUTAN"},
                {"data": "RWJAB_NIP", "class": "text-center"},
            ],
            "order": [[6, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWJAB_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWJAB_ID + '\',\'' + data.RWJAB_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);
                var eselon = data.RWJAB_IDJENJAB=='Jabatan Struktural'?data.RWJAB_IDESL:'';
                $('td:eq(-3)', row).html(eselon);

            },
        });

        var vjft = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '<?= base_url('admin/json/json_jabfun'); ?>?q=y',
            remote: {
                url: '<?= base_url('admin/json/json_jabfun'); ?>?q=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('#RWJAB_NAMAJAB_JFT').typeahead(null, {
            name: 'pendidikan',
            display: 'JBF_NAMJAB',
            limit: 15,
            source: vjft,
            templates: {
                empty: [
                    '<div class="tt-suggestion">',
                    'DATA TIDAK DITEMUKAN',
                    '</div>'
                ].join('\n'),
                suggestion: function (data) {
                    return '<p><strong>' + data.JBF_KODJAB + '</strong> – ' + data.JBF_NAMJAB + '</p>';
                }
            }
        }).on('typeahead:selected', function (event, data) {
            $('#RWJAB_IDJAB').val(data.JBF_KODJAB);
        });


        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/jabatan_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
        function hidejab() {
            $("#gjs").hide();
            $("#gjft").hide();
            $("#gjfu").hide();
            $("#tmtltk").hide();
        }
        hidejab();
        $("#RWJAB_IDJENJAB").change(function () {
            var selected = $("#RWJAB_IDJENJAB").val();
            //alert(selected);
            if (selected == 1) {
                $("#gjs").show();
                $("#gjft").hide();
                $("#gjfu").hide();
                $("#tmtltk").show();
            } else if (selected == 2) {
                $("#gjs").hide();
                $("#gjft").show();
                $("#gjfu").hide();
                $("#tmtltk").hide();
            } else if (selected == 4) {
                $("#gjs").hide();
                $("#gjft").hide();
                $("#gjfu").show();
                $("#tmtltk").hide();
            }
        });


        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");

        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myForm').trigger("reset");
            $('#RWJAB_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $('#RWJAB_IDUNO').val(rowData.RWJAB_IDUNO);
            $('#RWJAB_IDJAB').val(rowData.RWJAB_IDJAB);
            $("#RWJAB_IDJENJAB option").filter(function () {
                return $(this).text() == rowData.RWJAB_IDJENJAB;
            }).prop('selected', true)
            $("#RWJAB_NAMUNO").val(rowData.RWJAB_NAMUNO);
            $("#RWJAB_IDESL option").filter(function () {
                return $(this).text() == rowData.RWJAB_IDESL;
            }).prop('selected', true)
            $("#RWJAB_TMTJAB").val(rowData.RWJAB_TMTJAB);
            $("#RWJAB_TGLSK").val(rowData.RWJAB_TGLSK);
            $("#RWJAB_TMTLANTIK").val(rowData.RWJAB_TMTLANTIK);
            $("#RWJAB_NOMSK").val(rowData.RWJAB_NOMSK);

            $('#RWJAB_IDJENJAB').trigger("change");
            if ($('#RWJAB_IDJENJAB').val() == 1) {
                $("#RWJAB_NAMAJAB_JS").val(rowData.RWJAB_NAMAJAB);
            } else if ($('#RWJAB_IDJENJAB').val() == 2) {
                $("#RWJAB_NAMAJAB_JFT").val(rowData.RWJAB_NAMAJAB);
            } else {
                $("#RWJAB_NAMAJAB_JFU").val(rowData.RWJAB_NAMAJAB);
            }
        });

        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            hidejab();

            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/jabatan_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }


    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
