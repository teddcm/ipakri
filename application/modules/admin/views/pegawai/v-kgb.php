<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    $tglKGB = $this->uri->segment(5);
    $KGBthn = $this->uri->segment(6);
    if (get_data_pns($this->uri->segment(4), 'PNS_KEDHUK') <= 20) {
        $tahun = floor($dataKGB->Mrmn / 12);
        $bulan = $dataKGB->Mrmn % 12;
    }

    //print_r($dataKGB);
    ?>          

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Kenaikan Gaji Berkala</a></li>

            <?php
            if (get_data_pns($this->uri->segment(4), 'PNS_KEDHUK') <= 20) {
                ?>
                <li class="pull-right">
                    <div class="h6">
                        <span><?php echo "<strong>KGB Terdekat</strong>   Tanggal : " . $dataKGB->nextKGB . "   |   Masa Kerja : " . str_pad($dataKGB->KGBthn, 2, '0', STR_PAD_LEFT) . " tahun" ?></span>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <div class = "tab-content n-a">
            <div class = "col-lg-2">
                <div id="getFoto"></div>
            </div>                           
            <div class="tab-pane active" id="tab_1">
                <input type="hidden" id="GOLRU" value="<?php echo @convert_golongan($dataKGB->PNS_GOLRU); ?>">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWKGB_NIP" name="RWKGB_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWKGB_ID" name="RWKGB_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* TMT</label>
                        <input class="form-control datepicker" name="RWKGB_TMTSKBARU" id="RWKGB_TMTSKBARU">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* MKG (YY MM)</label>
                        <input class="form-control mkg" name="RWKGB_MKGBARU" id="RWKGB_MKGBARU">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Golru :</label>
                        <select class="form-control" name="RWKGB_GOLBARU" id="RWKGB_GOLBARU">
                            <option value="">-</option>
                            <?php
                            foreach ($golru as $x) {
                                echo '<option value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                            }
                            ?>
                        </select> 
                    </div>
                    <div class="form-group col-lg-3">
                        <label>* Gaji Pokok </label>
                        <input class="form-control" name="RWKGB_GAPOKBARU" id="RWKGB_GAPOKBARU" type="number">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal SK</label>
                        <input class="form-control datepicker" name="RWKGB_TGLSKBARU" id="RWKGB_TGLSKBARU">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Nomor SK</label>
                        <input class="form-control" name="RWKGB_NOSKBARU" id="RWKGB_NOSKBARU">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>TMT</th>
                            <th>MKG</th>
                            <th>Gaji Pokok</th>
                            <th>Tanggal SK</th>
                            <th>Nomor SK</th>
                            <th>Golru</th>
                            <th width='15%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>

        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $(".mkg").mask("99 99", {placeholder: "_"});
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/kgb_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWKGB_ID", "class": "text-center", },
                {"data": "RWKGB_TMTSKBARU"},
                {"data": "RWKGB_MKGBARU"},
                {"data": "RWKGB_GAPOKBARU"},
                {"data": "RWKGB_TGLSKBARU"},
                {"data": "RWKGB_NOSKBARU"},
                {"data": "RWKGB_GOLBARU"},
                {"data": "RWKGB_NIP", "class": "text-center"},
            ],
            "order": [[1, 'desc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var edit = ' <a class="edit btn btn-xs btn-info btn-flat" id="' + data.RWKGB_ID + '">edit</a>'
                var SK = ' <a onclick="sk(\'' + data.RWKGB_NIP + '\',\'' + data.RWKGB_ID + '\')" class="hapus btn btn-xs btn-warning btn-flat">cetak sk</a>';
                var hapus = ' <a onclick="hapus(\'' + data.RWKGB_ID + '\')" class="hapus btn btn-xs btn-danger btn-flat">hapus</a>';
                $('td:eq(-1)', row).html(edit + SK + hapus);

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/kgb_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");

        $("#simpan").click(function () {
            $("#myForm").submit();
        });

        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myForm').trigger("reset");
            $('#RWKGB_ID').val($(this).attr('id'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWKGB_TMTSKBARU").val(rowData.RWKGB_TMTSKBARU);
            $("#RWKGB_MKGBARU").val(rowData.RWKGB_MKGBARU);
            $("#RWKGB_GAPOKBARU").val(rowData.RWKGB_GAPOKBARU);
            $("#RWKGB_TGLSKBARU").val(rowData.RWKGB_TGLSKBARU);
            $("#RWKGB_NOSKBARU").val(rowData.RWKGB_NOSKBARU);
            $("#RWKGB_GOLBARU option").filter(function () {
                return $(this).text() == rowData.RWKGB_GOLBARU;
            }).prop('selected', true)
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })


    <?php if (($tglKGB != '') AND ( $KGBthn != '')) { ?>
            $("#tambah").trigger("click");
            $("#RWKGB_TMTSKBARU").val('<?php echo $tglKGB; ?>');
            $("#RWKGB_MKGBARU").val('<?php echo str_pad($dataKGB->KGBthn, 2, '0', STR_PAD_LEFT); ?> 00');
            $("#RWKGB_GAPOKBARU").val('<?php echo get_gapok(convert_golongan($dataKGB->PNS_GOLRU), $KGBthn); ?>');
            $("#RWKGB_TGLSKBARU").val('<?php echo date('d-m-Y'); ?>');
            $("#RWKGB_GOLBARU").val('<?php echo convert_golongan($dataKGB->PNS_GOLRU); ?>');
            $("#RWKGB_NOSKBARU").attr("disabled", true);
            $("#RWKGB_NOSKBARU").attr("placeholder", 'OTOMATIS');
    <?php } ?>
        function hapus(id) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/kgb_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }

        function sk(nip, id) {
            loadContent('<?php echo base_url('admin/pegawai/kgb_sk/'); ?>' + nip + '/' + id);
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
