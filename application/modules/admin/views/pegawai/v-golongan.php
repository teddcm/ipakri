<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>             
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Golongan</a></li>
        </ul>
        <div class="tab-content n-a">  
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>      
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWGOL_NIP" name="RWGOL_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWGOL_ID" name="RWGOL_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>                    
                    <div class="form-group col-lg-4">
                        <label>* Golongan</label>
                        <div class="input-group">
                            <select class="form-control" id="RWGOL_KDGOL" name="RWGOL_KDGOL">
                                <?php
                                foreach ($golru as $x) {
                                    echo '<option data-pangkat="' . $x->GOL_PKTNAM . '" value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                                }
                                ?>
                            </select>    

                            <span class="input-group-addon"></span>
                            <input class="form-control" disabled id="PANGKAT">
                        </div>
                    </div>
                    <div class="form-group col-lg-5">
                        <label>* Masa Kerja Golongan</label>
                        <div class="input-group">
                            <span class="input-group-addon">Tahun</span>
                            <input class="form-control" name="RWGOL_MKGTHN" id="RWGOL_MKGTHN">
                            <span class="input-group-addon">Bulan</span>
                            <input class="form-control" name="RWGOL_MKGBLN" id="RWGOL_MKGBLN">
                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* TMT Golongan</label>
                        <input class="form-control datepicker" name="RWGOL_TMTGOL" id="RWGOL_TMTGOL">
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Nomor SK</label>
                        <input class="form-control" name="RWGOL_NOSK" id="RWGOL_NOSK">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal SK</label>
                        <input class="form-control datepicker" name="RWGOL_TGLSK" id="RWGOL_TGLSK">
                    </div>
                    <div class="form-group col-lg-3">
                        <label>Nomor BKN</label>
                        <input class="form-control" name="RWGOL_NOBKN" id="RWGOL_NOBKN">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal BKN</label>
                        <input class="form-control datepicker" name="RWGOL_TGLBKN" id="RWGOL_TGLBKN">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Jenis KP</label>
                        <select class="form-control" name="RWGOL_KDJNSKP" id="RWGOL_KDJNSKP">
                            <?php
                            foreach ($get_jenis_kp as $x) {
                                echo '<option value="' . $x->JKP_JPNKOD . '">' . $x->JKP_JPNNAMA . '</option>';
                            }
                            ?>
                        </select>                    
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Gol</th>
                            <th>Nomor SK</th>
                            <th>Tanggal SK</th>
                            <th>Jenis KP</th>
                            <th>MKG Tahun</th>
                            <th>MKG Bulan</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/golongan_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWGOL_ID", "class": "text-center", },
                {"data": "RWGOL_KDGOL"},
                {"data": "RWGOL_NOSK"},
                {"data": "RWGOL_TGLSK"},
                {"data": "RWGOL_TMTGOL"},
                {"data": "RWGOL_KDJNSKP"},
                {"data": "RWGOL_MKGTHN"},
                {"data": "RWGOL_MKGBLN"},
                {"data": "RWGOL_NIP", "class": "text-center"},
            ],
            "order": [[1, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWGOL_ID + '">edit</a>\n\
                            <a onclick="hapus(\'' + data.RWGOL_ID + '\',\'' + data.RWGOL_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html(aksi);

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/golongan_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $('#RWGOL_KDGOL').change(function () {
            $('#PANGKAT').val($('#RWGOL_KDGOL option:selected').data('pangkat'));
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");

        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        var oTable = $('#myDataTable').DataTable();
        $('#myDataTable tbody').on('click', '.edit', function () {
            var rowData = oTable.row($(this).closest("tr")).data();
            var id = $(this).attr('id');
            $("#myForm").trigger("reset");
            $('#RWGOL_ID').val(id);
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWGOL_KDGOL option").filter(function () {
                return $(this).text() == selected.find('td:eq(1)').html();
            }).prop('selected', true)
            $("#RWGOL_KDGOL").trigger("change");
            $("#RWGOL_MKGTHN").val(rowData.RWGOL_MKGTHN);
            $("#RWGOL_MKGBLN").val(rowData.RWGOL_MKGBLN);
            $("#RWGOL_NOSK").val(rowData.RWGOL_NOSK);
            $("#RWGOL_TGLSK").val(rowData.RWGOL_TGLSK);
            $("#RWGOL_TMTGOL").val(rowData.RWGOL_TMTGOL);
            $("#RWGOL_NOBKN").val(rowData.RWGOL_NOBKN);
            $("#RWGOL_TGLBKN").val(rowData.RWGOL_TGLBKN);
            $("#RWGOL_KDJNSKP option").filter(function () {
                return $(this).text() == rowData.RWGOL_KDJNSKP;
            }).prop('selected', true)
        });

        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/golongan_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
