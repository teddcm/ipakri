<?php
if (get_data_pns($this->uri->segment(4)) != NULL) {
    //print_r($dataKGB);
    //print_r($dataPegawai);
    ?>          
    <style>
        form *{
            text-transform: none!important  ;
        }
    </style>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="sampel">

                </div>
            </div>
        </div>
    </div>
    <script>
        $('#btn_sampel').on('click', function () {
            $('#sampel').html('<img src="<?php echo base_url('contents/img/sk_kgb.png'); ?>" class="image img-responsive"/>');
        })
    </script>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <b>Cetak KGB atas nama : <?= get_data_pns($this->uri->segment(4)) ?></b>
                </div>
                <form id="cetakForm" target="cetak_sk" action="<?php echo base_url('admin/report/kgb_sk_cetak'); ?>" method="get">
                    <div class="modal-body">
                        <input type="hidden" name="RWKGB_ID" value="<?= $dataKGB->RWKGB_ID; ?>"/>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-3">Spesimen </label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="spesimen">
                                        <?php
                                        foreach ($dataSpesimen as $x) {
                                            echo '<option value="' . $x->SPES_NIPBARU . '">' . $x->SPES_PNSNAM . '</option>';
                                        }
                                        ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Kepada </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="kepada" style = 'resize:none;height: 95px'><?= getPengaturan('kgb_sk_kepada'); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Tembusan </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="tembusan" style='resize:none;height: 115px'><?= getPengaturan('kgb_sk_tembusan'); ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-flickr btn-flat" type="submit" id="cetak" value="Cetak"> 
                    </div>
                </form>

            </div>
        </div>
    </div>
    <script>
        $("#cetak").click(function () {
            $('#myModal2').modal('hide');
            setTimeout(function () {
                loadContent('<?= base_url(uri_string()); ?>');
            }, 1000);
        });

    </script>
    <form id="myForm" role="form" action="#" method="post" autocomplete="off" >
        <input type="hidden" name="RWKGB_ID" value="<?= $dataKGB->RWKGB_ID; ?>"/>
        <input type="hidden" name="mode" value="fullmode"/>
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <div class="pull-left">
                            <h4>Cetak SK KGB</h4>
                        </div>
                        <div class="pull-right">
                            <span class="btn btn-info btn-flat" data-toggle="modal" data-target="#myModal" id="btn_sampel"><i class="fa fa-question"></i></span>
                            <a class="btn btn-success btn-flat" href="#" id="simpan">simpan</a>  
                            <?php if ($dataKGB->RWKGB_NAMA != NULL): ?><span class="btn btn-flickr btn-flat" data-toggle="modal" data-target="#myModal2">cetak</span><?php endif; ?>
                            <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url('admin/pegawai/KGB/' . $this->uri->segment(4)); ?>')"><i class="fa fa-backward"></i> kembali</a>
                        </div>  
                    </div>
                    <div class="box-body">      
                        <div class="form-horizontal">
                            <div class="col-md-6">
                                <input type="hidden" id="GOLRU" value="<?php echo convert_golongan($dataKGB->RWKGB_GOLBARU); ?>">
                                <div class="form-group form-group-sm">
                                    <label class="control-label col-sm-4">NIP :</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="RWKGB_NIP" name="RWKGB_NIP" value="<?= $dataKGB->RWKGB_NIP; ?>" readonly="">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="control-label col-sm-4">Nama :</label>
                                    <div class="col-sm-8">
                                        <input style="text-transform: none" class="form-control" id="RWKGB_NAMA" name="RWKGB_NAMA" value="<?= $dataKGB->RWKGB_NAMA == NULL ? (($dataPegawai->PNS_GLRDPN != NULL ? $dataPegawai->PNS_GLRDPN . ' ' : '') . $dataPegawai->PNS_PNSNAM . ' ' . $dataPegawai->PNS_GLRBLK ) : $dataKGB->RWKGB_NAMA; ?>">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="control-label col-sm-4">Jabatan:</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="RWKGB_JAB" name="RWKGB_JAB" value="<?= ucwords_strtolower($dataKGB->RWKGB_JAB == NULL ? get_jab_pegawai($dataPegawai->PNS_NIPBARU) : ($dataKGB->RWKGB_JAB)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-sm">
                                    <label class="control-label col-sm-4">Unit Kerja :</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="RWKGB_INSKER" name="RWKGB_INSKER" value="DINAS PEKERJAAN UMUM DAN PENATAAN RUANG">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label class="control-label col-sm-4">Dasar Hukum SK :</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="RWKGB_DASARHUK" name="RWKGB_DASARHUK" value="<?= $dataKGB->RWKGB_DASARHUK != NULL ? $dataKGB->RWKGB_DASARHUK : getPengaturan('kgb_sk_dasarhuk'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="pull-left">
                            <h4>Data SK Lama</h4>
                        </div>
                        <div class="pull-right">
                            Tampilkan informasi SK terakhir <span class="btn btn-info btn-flat btn-xs" id="getOldKGB">Klik <i class="fa fa-download"></i></span>  
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Tgl SK :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date'>    
                                        <input type='text' class="form-control datepicker" id="RWKGB_TGLSKLAMA" class="form-control" name="RWKGB_TGLSKLAMA" value="<?= reverseDate($dataKGB->RWKGB_TGLSKLAMA); ?>" />
                                        <span class="input-group-addon input-sm">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Nomor SK :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="RWKGB_NOSKLAMA" name="RWKGB_NOSKLAMA" value="<?= $dataKGB->RWKGB_NOSKLAMA; ?>">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">TMT SK :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date'>    
                                        <input type='text' class="form-control datepicker" id="RWKGB_TMTSKLAMA" class="form-control" name="RWKGB_TMTSKLAMA" value="<?= reverseDate($dataKGB->RWKGB_TMTSKLAMA); ?>" />
                                        <span class="input-group-addon input-sm">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Gapok :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date'>    
                                        <span class="input-group-addon input-sm">
                                            Rp.
                                        </span>                          
                                        <input class="form-control" type="number" id="RWKGB_GAPOKLAMA" name="RWKGB_GAPOKLAMA" value="<?= $dataKGB->RWKGB_GAPOKLAMA; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">MKG :</label>
                                <div class="col-sm-8">
                                    <input class="form-control mkg" id="RWKGB_MKGLAMA" name="RWKGB_MKGLAMA" value="<?= str_pad($dataKGB->RWKGB_MKGLAMA, 4, '0', STR_PAD_LEFT); ?>">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Golru :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="RWKGB_GOLLAMA" id="RWKGB_GOLLAMA">
                                        <option value="">-</option>
                                        <?php
                                        foreach ($golru as $x) {
                                            $selected = $dataKGB->{'RWKGB_GOLLAMA'} == $x->GOL_KODGOL ? 'selected' : '';
                                            echo '<option ' . $selected . ' value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                                        }
                                        ?>
                                    </select>                                 
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Jabatan Penandatangan :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="RWKGB_JABPJBLAMA" name="RWKGB_JABPJBLAMA" value="<?= $dataKGB->RWKGB_JABPJBLAMA; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h4>Data SK Baru</h4>
                    </div>
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Tgl SK :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date'>    
                                        <input type='text' class="form-control datepicker" id="RWKGB_TGLSKBARU" class="form-control" name="RWKGB_TGLSKBARU" value="<?= reverseDate($dataKGB->RWKGB_TGLSKBARU); ?>" />
                                        <span class="input-group-addon input-sm">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Nomor SK :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" style="color: red;" id="RWKGB_NOSKBARU" name="RWKGB_NOSKBARU" <?php echo $dataKGB->RWKGB_NOSKBARU == NULL ? 'disabled' : ''; ?> value="<?= $dataKGB->RWKGB_NOSKBARU == NULL ? 'OTOMATIS' : $dataKGB->RWKGB_NOSKBARU; ?>">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">TMT SK :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date' >    
                                        <input type='text' class="form-control datepicker" id="RWKGB_TMTSKBARU" class="form-control" name="RWKGB_TMTSKBARU" value="<?= reverseDate($dataKGB->RWKGB_TMTSKBARU); ?>" />
                                        <span class="input-group-addon input-sm">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Gapok :</label>
                                <div class="col-sm-8">
                                    <div class='input-group date'>    
                                        <span class="input-group-addon input-sm">
                                            Rp.
                                        </span>                          
                                        <input class="form-control" type="number" id="RWKGB_GOLBARU" name="RWKGB_GAPOKBARU" value="<?= $dataKGB->RWKGB_GAPOKBARU; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">MKG :</label>
                                <div class="col-sm-8">
                                    <input class="form-control mkg" id="RWKGB_MKGBARU" name="RWKGB_MKGBARU" value="<?= str_pad($dataKGB->RWKGB_MKGBARU, 4, '0', STR_PAD_LEFT); ?>">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Golru :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="RWKGB_GOLBARU">
                                        <option value="">-</option>
                                        <?php
                                        $gol = $dataKGB->{'RWKGB_GOLBARU'} != NULL ? $dataKGB->{'RWKGB_GOLBARU'} : $dataPegawai->PNS_GOLRU;
                                        foreach ($golru as $x) {
                                            $selected = $gol == $x->GOL_KODGOL ? 'selected' : '';
                                            echo '<option ' . $selected . ' value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                                        }
                                        ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="control-label col-sm-4">Jabatan Penandatangan :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="RWKGB_JABPJBBARU" name="RWKGB_JABPJBBARU" value="<?= $dataKGB->RWKGB_JABPJBBARU; ?>" readonly="" placeholder="OTOMATIS SESUAI SPESIMEN SAAT DICETAK">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <!-- /.box -->
    </form>
    <div class="clearfix"></div>


    <script>
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
            $(".mkg").mask("99 99", {placeholder: " "});
            $(".nip").mask("999999999999999999", {placeholder: ""});
        });

        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/kgb_sk_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#getOldKGB").click(function (e) {
            $.getJSON("<?php echo base_url('admin/json/json_data_old_kgb/' . $dataKGB->RWKGB_ID); ?>", function (result) {
                if (result[0] == 'warn') {
                    $.notify(result[1], result[0]);
                } else {
                    $.each(result, function (i, field) {
                        i = i.replace('BARU', 'LAMA');
                        if (field != null) {
                            var date = ['RWKGB_TGLSKBARU', 'RWKGB_TMRSKBARU'];
                            var select = ['RWKGB_GOLBARU'];
                            if (jQuery.inArray(i, date) >= 0) {
                                //alert(field.substr(8, 2)+'-'+field.substr(5, 2)+'-'+field.substr(0, 4));
                                $("#" + i).val(field.substr(8, 2) + '-' + field.substr(5, 2) + '-' + field.substr(0, 4));
                            } else {
                                $("#" + i).val(field);
                            }
                        }
                    });
                }
            });
        });

    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
