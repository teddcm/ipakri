<?php
require 'v-listing#search.php';
?>
<div class="clearfix"></div>
<div class="box">
    <div class="box-body">
        <style>
            th,td{
                text-align: center;
            }
        </style>
        <table id="tabelCuti"class="table table-bordered table-responsive table-hover">
            <thead>
                <tr>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">NIP</th>
                    <th colspan="4">Cuti Tahunan</th>
                    <th rowspan="2" width="12%">non Cuti Tahunan</th>
                </tr>
                <tr>
                    <th width="12%">Sisa Tahun Lalu</th>
                    <th width="12%">Jatah Tahun Ini</th>
                    <th width="12%">Terpakai</th>
                    <th width="12%">Tersisa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $datacuti = getDataCuti();
                foreach ($datacuti as $dc) {
                    ?>
                    <tr>
                        <td style="text-align: left;"><?php echo $dc->nama; ?></td>
                        <td><?php echo $dc->nip; ?></td>
                        <td><?php echo $dc->sisa_tahun_lalu; ?></td>
                        <td><?php echo $dc->jatah_tahun_ini; ?></td>
                        <td><?php echo $dc->cuti_tertanggung; ?></td>
                        <td><?php echo $dc->sisa_cuti_tertanggung; ?></td>
                        <td><?php echo $dc->cuti_tak_tertanggung; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $('td').each(function () {
        if (parseInt($(this).text()) < 0) {
            $(this).addClass('text-red').css('font-weight', 'bold');
        }
        else if (parseInt($(this).text()) > 0 && parseInt($(this).text()) < 100) {
            $(this).addClass('text-blue').css('font-weight', 'bold');
        }
    });
    function view(id) {
        loadContent('<?php echo base_url('admin/pegawai/data_utama/'); ?>' + id);
    }

</script>