<?php
require 'v-listing#search.php';
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>          
    <div class="modal fade" id="myModal" tabindex="-1">
        <div class="modal-dialog" role="document">
            <form id="newForm">  
                <div class="modal-content">
                    <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                            <div id="pilih_unor"></div>
                        </div>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Unit Organisasi</a></li>
        </ul>
        <div class="tab-content n-a"> 
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                           
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" name="RWUNOR_IDUNORBARU" id="RWUNOR_IDUNORBARU">
                        <input type="hidden" id="RWUNOR_NIP" name="RWUNOR_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWUNOR_ID" name="RWUNOR_ID">
                        <input type="hidden" id="mode" name="mode" value="edit" >
                    </div>          
                    <div class="form-group col-lg-4" id="cariLokasi">
                        <label>* Unit Organisasi</label>
                        <input class="form-control typeahead"  name="RWUNOR_NAMUNORBARU" id="RWUNOR_NAMUNORBARU">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Tanggal SK</label>
                        <input class="form-control datepicker" name="RWUNOR_TGLSK" id="RWUNOR_TGLSK">
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Nomor SK</label>
                        <input class="form-control" name="RWUNOR_NOSK" id="RWUNOR_NOSK">
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Unit Organisasi</th>
                            <th>Unor Atasan</th>
                            <th>Nomor SK</th>
                            <th>Tanggal SK</th>
                            <th>Asal Prosedur</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>  
                </table>
            </div>
        </div>
    </div>


    <script>

        $('#pilih_unor').tree({
            dataUrl: '<?php echo base_url(); ?>admin/json/unor_jqtree/',
        });
        $('#select').click(function () {
            var node = $('#pilih_unor').tree('getSelectedNode');
            $("#RWUNOR_NAMUNORBARU").val(node.name);
            $("#RWUNOR_IDUNORBARU").val(node.id);
            $('#myModal').modal('hide');
            //alert(node.name);
        })
        $("#cariLokasi *").attr("disabled", true);
        $('#cariLokasi').on('focusin', function (e) {
            $('#myModal').modal('show')
            $("#cariLokasi *").blur();
            e.preventDefault();
        })
        jQuery(function ($) {
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $('.datepicker').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        $('#myDataTable').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"F">t<"F">',
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax": "<?php echo site_url('admin/pegawai/unit_organisasi_ajax_view/') . $this->uri->segment(4); ?>",
            "iDisplayLength": 100,
            "columns": [
                {"data": "RWUNOR_ID", "class": "text-center", },
                {"data": "RWUNOR_NAMUNORBARU"},
                {"data": "ATASAN"},
                {"data": "RWUNOR_NOSK"},
                {"data": "RWUNOR_TGLSK"},
                {"data": "RWUNOR_PROSEDUR"},
                {"data": "RWUNOR_NIP", "class": "text-center"},
            ],
            "order": [[3, 'asc']],
            "rowCallback": function (row, data, iDisplayIndex) {
                var ref = '<?= uri_string(); ?>';
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var id = $('td:eq(0)', row).text();
                $('td:eq(0)', row).html(index);
                var edit = ' <a class="edit btn btn-xs btn-info" id="' + data.RWUNOR_ID + '" RWUNOR_IDUNORBARU="' + data.RWUNOR_IDUNORBARU + '">edit</a>'
                var hapus = ' <a onclick="hapus(\'' + data.RWUNOR_ID + '\',\'' + data.RWUNOR_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                $('td:eq(-1)', row).html((data.RWUNOR_PROSEDUR !== 'MUTASI PNS UNOR' ? '' : edit + hapus));

            },
        });

        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/unit_organisasi_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        //loadContent('<?php echo base_url(uri_string()); ?>');
                        $('#myDataTable').DataTable().ajax.reload();
                        $("#cancel").trigger('click');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#myForm *").attr("disabled", true);
        $('#myForm').trigger("reset");
        $("#simpan").click(function () {
            $("#myForm").submit();
        });

        $('#myDataTable tbody').on('click', '.edit', function () {
            $('#myForm').trigger("reset");
            $('#RWUNOR_ID').val($(this).attr('id'));
            $('#RWUNOR_IDUNORBARU').val($(this).attr('RWUNOR_IDUNORBARU'));
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
            $("#mode").val('edit');
            $("#mode").attr("disabled", false);
            var selected = $(this).parents('tr');
            $("#RWUNOR_NAMUNORBARU").val(selected.find('td:eq(1)').html());
            $("#RWUNOR_NOSK").val(selected.find('td:eq(2)').html());
            $("#RWUNOR_TGLSK").val(selected.find('td:eq(3)').html());
        });
        $("#cancel").click(function () {
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#cancel").hide();
            $("#simpan").hide();
            $("#tambah").show();
            //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
        })
        $("#tambah").click(function () {
            $("#myForm * [name]").attr("disabled", false);
            $("option").attr("disabled", false);
            $("#mode").attr("disabled", false);
            $("#mode").val('tambah');
            $("#cancel").show();
            $("#simpan").show();
            $("#tambah").hide();
        })
        function hapus(id, nip) {
            var e = confirm('Apakah data ini akan dihapus?');
            if (e) {
                var url = "<?php echo base_url('admin/pegawai/unit_organisasi_hapus'); ?>"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    async: false,
                    data: {"id": id, "nip": nip},
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                            $('#myDataTable').DataTable().ajax.reload();
                            $("#cancel").trigger('click');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                })
            }
        }
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
