<?php
$CUTI = cek_cuti_this_year($this->uri->segment(4));
//print_r($CUTI);
require 'v-listing#search.php';
if (FALSE) {
    if ($this->ion_auth->is_admin()) {
        ?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="pull-right">
                    <div> 
                        <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                    </div>
                </li>
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Definisikan Cuti</a></li>
            </ul>
            <div class="tab-content n-a">         
                <div class="tab-pane active" id="tab_1">
                    <div class="alert alert-info col-lg-12">
                        Cuti untuk semua pegawai belum terdefinisi. Mohon <?php echo ($this->ion_auth->is_admin()) ? '<div class="btn btn-flat btn-default" onclick="loadContent(\'' . base_url('admin/pengaturan/cuti') . '\')">klik disini</div>' : 'hubungi admin'; ?> untuk mengalokasikan cuti untuk tahun <?php echo date('Y'); ?>.<br/>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <?php
    } else {
        echo 'oh no';
    }
} else if ($CUTI != 'TERDEFINISI' && get_data_pns($this->uri->segment(4)) != NULL) {
    //print_r($CUTI);
    ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Definisikan Cuti</a></li>
        </ul>
        <div class="tab-content n-a">   
            <div class="col-lg-2">
                <div id="getFoto"></div>
            </div>                          
            <div class="tab-pane active" id="tab_1">
                <form id="myForm">
                    <input type="hidden" id="RWCUTI_NIP" name="RWCUTI_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                    <input type="hidden" id="mode" name="mode" value="define" >    
                    <div class="alert alert-info col-lg-10" >
                        Cuti untuk pegawai ini belum terdefinisi. Mohon alokasikan cuti untuk tahun <?php echo date('Y'); ?>.<br/>
                        Sisa cuti Tahun <?php echo date('Y') - 1; ?> sebanyak <b><?php echo $CUTI['sisa_tahun_lalu']; ?></b> hari. Dapat diambil <b id="cuti_sisa_tip"><?php echo $CUTI['sisa_tahun_lalu'] >= 6 ? 6 : $CUTI['sisa_tahun_lalu']; ?></b> hari

                        <?php if ($CUTI['sisa_tahun_lalu'] >= 6) { ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="penangguhan"> Terdapat Surat Penangguhan Cuti
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="box-body col-lg-10">
                        <div class="form-group">
                            <label class="col-sm-7">Jatah cuti tahun <?php echo date('Y'); ?> (dikurangi cuti bersama)</label>
                            <div class="col-sm-5">
                                <input class="form-control hitung" name="cuti_jatah" id="cuti_jatah" type="number" min="12"  max="12" value="12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-7">Sisa cuti tahun <?php echo date('Y') - 1; ?></label>
                            <div class="col-sm-5">
                                <input class="form-control hitung" name="cuti_sisa" id="cuti_sisa" type="number" min="0"  max="24" value="<?php echo $CUTI['sisa_tahun_lalu'] >= 6 ? 6 : $CUTI['sisa_tahun_lalu']; ?>">
                            </div>
                        </div>
                        <!--div class="form-group">
                            <label class="col-sm-8">PP No.11/2017 Pasal 312</label>
                            <div class="col-sm-4">
                                <input class="form-control hitung" name="cuti_312" id="cuti_312" type="number" min="0"  max="12" value="0" >
                            </div>
                        </div-->
                        <div class="form-group">
                            <label class="col-sm-7" style="color: red">Total cuti pada tahun <?php echo date('Y'); ?> </label>
                            <div class="col-sm-5">
                                <input class="form-control" id="cuti_total" readonly >
                            </div>
                        </div>
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-success btn-flat" href="#" id="simpan">simpan</a> 
                        </div>
                    </div>      
                </div>       
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <script>

        $('#penangguhan').on('change', function () {
    <?php if ($CUTI['sisa_tahun_lalu'] >= 6) { ?>
                if ($('#penangguhan').is(':checked')) {
                    $('#cuti_sisa').val('<?php echo $CUTI['sisa_tahun_lalu'] <= 12 ? $CUTI['sisa_tahun_lalu'] : 12; ?>');
                    $('#cuti_sisa_tip').html('<?php echo $CUTI['sisa_tahun_lalu'] <= 12 ? $CUTI['sisa_tahun_lalu'] : 12; ?>');
                } else {
                    $('#cuti_sisa').val(6);
                    $('#cuti_sisa_tip').html(6);
                }
                hitung();
    <?php } ?>
        })
        $(document).ready(function () {
            $('#penangguhan').trigger('change');
            hitung();
        })

        $('.hitung').bind('keyup mouseup', function () {
            hitung();
        })
        function hitung() {
            var sum = 0;
            $('.hitung').each(function () {
                sum += parseFloat(this.value); // Or this.innerHTML, this.innerText
            });
            $('#cuti_total').val(sum);
        }


        $("#simpan").click(function () {
            $("#myForm").submit();
        });
        $("#myForm").submit(function (e) {
            var url = "<?php echo base_url('admin/pegawai/cuti_simpan'); ?>";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#myForm").serialize(),
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            });
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });</script>
    <?php
} else if (get_data_pns($this->uri->segment(4)) != NULL) {
    $sisa_cuti = cek_sisa_cuti($this->uri->segment(4));
    ?>   
    <div class="alert alert-danger" style="display: none">
        <i class="icon fa fa-warning"></i> Sisa cuti bernilai negatif. Mohon periksa kembali!
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Riwayat Cuti</a></li>
            <li class="pull-right">
                <div> 
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url(); ?>admin/pegawai/data_utama/<?php echo $this->uri->segment(4); ?>')"><i class="fa fa-backward"></i> data utama</a>
                </div>
            </li>
            <li class="pull-left" style="padding-top: 3px;">
                <select class="form-control" id="tahun_cuti">
                    <?php
                    for ($x = date('Y'); $x > date('Y') - 5; $x--) {
                        echo "<option value='" . $x . "'>" . $x . "</option>";
                    }
                    ?>
                </select>
            </li>

        </ul>
        <div class="tab-content n-a">
            <div class="col-lg-2">
                <?php echo get_foto($this->uri->segment(4)); ?>
            </div>                              
            <div class="tab-pane active" id="tab_1">
                <form id="myForm"> 
                    <div>
                        <input type="submit" style="display: none" name="ok">
                        <input type="hidden" id="RWCUTI_NIP" name="RWCUTI_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                        <input type="hidden" id="RWCUTI_ID" name="RWCUTI_ID">
                        <input type="hidden" id="RWCUTI_THNCUTI" name="RWCUTI_THNCUTI" value="<?php echo date('Y'); ?>">
                        <input type="hidden" id="mode" name="mode" value="edit" >   
                    </div>
                    <div class="form-group col-lg-10" style="color: red">
                        Sisa Cuti Tahunan yang dapat diambil <b id="sisa-cuti"></b> 
                    </div>
                    <div class="form-group col-lg-4">
                        <label>Jenis Cuti</label>
                        <select class="form-control" name="RWCUTI_JNS" id="RWCUTI_JNS">
                            <option value="">-</option>
                            <?php
                            foreach ($jenis_cuti as $x) {
                                echo '<option value="' . $x->JNSCT_ID . '">' . $x->JNSCT_NAMA . '</option>';
                            }
                            ?>
                        </select>                 
                    </div>
                    <div class="form-group col-lg-5" disabled="disabled">
                        <label>Ijin Potong Cuti Tahunan</label>
                        <div class="checkbox">
                            <label disabled="disabled">
                                <input id="potijin" name="potijin" type="checkbox">
                                Beri cek apabila ijin potong cuti tahunan
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal Awal</label>
                        <input class="form-control datepicker" name="RWCUTI_TGLAWL" id="RWCUTI_TGLAWL">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Tanggal Akhir</label>
                        <input class="form-control datepicker" name="RWCUTI_TGLAKR" id="RWCUTI_TGLAKR">
                    </div>
                    <div class="form-group col-lg-2">
                        <label>* Jumlah Hari</label>
                        <input class="form-control" name="RWCUTI_JUM" id="RWCUTI_JUM" type="text">
                    </div>
                    <div class="form-group col-lg-8">
                        <label>Keterangan</label>
                        <textarea class="form-control textarea" name="RWCUTI_KET" id="RWCUTI_KET" style="resize: none;height:35px"></textarea>
                    </div>
                </form>
                <div class="form-group col-lg-12">
                    <div class="pull-right">
                        <div>
                            <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                            <a class="btn btn-default btn-flat" href="#" id="notambah" disabled style="display: none">tambah</a>   
                            <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                            <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                        </div>
                    </div>    
                    <div class="clearfix"></div>      
                </div>       
                &nbsp;
                <table id="myDataTable" class="table">
                    <thead>
                        <tr>
                            <th width='5%'>No</th>
                            <th>Jenis Cuti</th>
                            <th>Jenis Cuti</th>
                            <th>Tgl Awal</th>
                            <th>Tgl Akhir</th>
                            <th>Jumlah Hari</th>
                            <th>Tahun Cuti</th>
                            <th>Keterangan</th>
                            <th width='10%'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>  
                </table>
            </div>
        </div>
    </div>
    <script>
        $(function () {

            $('#tahun_cuti').on('change', function () {
                $("#RWCUTI_THNCUTI").val($('#tahun_cuti :selected').val());
                $("#cancel").trigger('click');
                count = 0;
                $('#myDataTable').DataTable().ajax.reload();
            });
            $("#potijin").change(function () {
                if (this.checked) {
                    $("#RWCUTI_KET").val("IJIN POTONG CUTI");
                    $("#RWCUTI_KET").attr("readonly", true);
                } else {
                    $("#RWCUTI_KET").attr("readonly", false);
                    $("#RWCUTI_KET").val("");
                }
            });
            $('#RWCUTI_JNS').on('change', function (e) {
                if (this.value == 1) {
                    $("#potijin").attr("disabled", false);
                } else {
                    $("#potijin").prop('checked', false);
                    $("#potijin").attr("disabled", true);
                    $("#RWCUTI_KET").val("");
                    $("#RWCUTI_KET").attr("readonly", false);
                }
            });
            $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
            $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };
            var count = 0;
            $('#myDataTable').dataTable({
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "sDom": '<"F">t<"F">',
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                "ajax": {
                    "url": "<?php echo site_url('admin/pegawai/cuti_ajax_view/') . $this->uri->segment(4); ?>/",
                    "data": function (d) {
                        d.tahun = $('#tahun_cuti :selected').val();
                    },
                },
                "iDisplayLength": 100,
                "columnDefs": [
                    {
                        "targets": [2],
                        "visible": false,
                        "searchable": false
                    },
                ],
                "columns": [
                    {"data": "RWCUTI_ID", "class": "text-center", },
                    {"data": "RWCUTI_JNSNM"},
                    {"data": "RWCUTI_JNS"},
                    {"data": "RWCUTI_TGLAWL"},
                    {"data": "RWCUTI_TGLAKR"},
                    {"data": "RWCUTI_JUM", "class": "text-center", },
                    {"data": "RWCUTI_THNCUTI"},
                    {"data": "RWCUTI_KET"},
                    {"data": "RWCUTI_NIP", "class": "text-center"},
                ],
                "order": [[2, 'asc']],
                "rowCallback": function (row, data, iDisplayIndex) {
                    var ref = '<?= uri_string(); ?>';
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    var id = $('td:eq(0)', row).text();
                    $('td:eq(0)', row).html(index);
                    var edit = ' <a class="edit btn btn-xs btn-info" id="' + data.RWCUTI_ID + '" ">edit</a>'
                    var hapus = ' <a onclick="hapus(\'' + data.RWCUTI_ID + '\',\'' + data.RWCUTI_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
                    $('td:eq(-1)', row).html((data.RWCUTI_JNS <= 0 && data.RWCUTI_JNS > -2) ? edit : edit + hapus);

                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };
                    if (data.RWCUTI_JNS <= 1 && data.RWCUTI_JNS > -2) {
                        if (index == 1) {
                            count = 0;
                        }
                        count = count + intVal(data.RWCUTI_JUM);
                        $('#sisa-cuti').html('tersisa ' + count + ' hari');
                        if (info.iEnd == index) {
                            if (count < 0) {
                                $('.alert-danger').show();
                            } else {
                                $('.alert-danger').hide();
                            }
                        }
                    }
                },
            });

            $("#myForm").submit(function (e) {
                var url = "<?php echo base_url('admin/pegawai/cuti_simpan'); ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#myForm").serialize(),
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success') {
                            $('#myDataTable').DataTable().ajax.reload();
                            $('#cancel').trigger("click");
                            //loadContent('<?php echo base_url(uri_string()); ?>');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                });
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
            $("#myForm *").attr("disabled", true);
            $('#myForm').trigger("reset");
            $("#simpan").click(function () {
                $("#myForm").submit();
            });
            $('#myDataTable tbody').on('click', '.edit', function () {
                $('#myForm').trigger("reset");
                $('#RWCUTI_ID').val($(this).attr('id'));
                $("#myForm * [name]").attr("disabled", false);
                $("option").attr("disabled", false);
                $("#cancel").show();
                $("#simpan").show();
                $("#tambah").hide();
                $("#mode").val('edit');
                $("#mode").attr("disabled", false);
                var selected = $(this).parents('tr');
                $("#RWCUTI_JNS option").filter(function () {
                    return $(this).text() == selected.find('td:eq(1)').html();
                }).prop('selected', true)

                $("#RWCUTI_TGLAWL").val(selected.find('td:eq(2)').html());
                $("#RWCUTI_TGLAKR").val(selected.find('td:eq(3)').html());
                $("#RWCUTI_JUM").val(Math.abs(parseFloat(selected.find('td:eq(4)').html())));
                $("#RWCUTI_KET").val(selected.find('td:eq(6)').html());
                $("#RWCUTI_KET").attr("readonly", selected.find('td:eq(6)').html() == 'IJIN POTONG CUTI' ? true : false);
                $("#potijin").prop('checked', selected.find('td:eq(6)').html() == 'IJIN POTONG CUTI' ? true : false);
                $('#RWCUTI_JNS').trigger("change");
                if (selected.find('td:eq(1)').html() === 'Jatah Tahun Ini' || selected.find('td:eq(1)').html() === 'Sisa Tahun Lalu') {
                    $("#RWCUTI_JNS").attr("disabled", true);
                    $("#RWCUTI_TGLAWL").attr("disabled", true);
                    $("#RWCUTI_TGLAKR").attr("disabled", true);
                    $("#RWCUTI_KET").attr("disabled", true);
                    $("#potijin").attr("disabled", true);
                }
            });
            $("#cancel").click(function () {
                $("#myForm *").attr("disabled", true);
                $('#myForm').trigger("reset");
                $("#cancel").hide();
                $("#simpan").hide();
                $("#tambah").show();
                //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
            })
            $("#tambah").click(function () {
                $("#myForm * [name]").attr("disabled", false);
                $("option").attr("disabled", false);
                $("#mode").attr("disabled", false);
                $("#mode").val('tambah');
                $("#cancel").show();
                $("#simpan").show();
                $("#tambah").hide();
            })
            function hapus(id, nip) {
                var e = confirm('Apakah data ini akan dihapus?');
                if (e) {
                    count = 0;
                    var url = "<?php echo base_url('admin/pegawai/cuti_hapus'); ?>"; // the script where you handle the form input.
                    $.ajax({
                        type: "POST",
                        url: url,
                        async: false,
                        data: {"id": id, "nip": nip},
                        dataType: "json",
                        success: function (result)
                        {
                            $.notify(result[1], result[0]);
                            if (result[0] === 'success') {
                                $('#myDataTable').DataTable().ajax.reload();
                                $('#cancel').trigger("click");
                                //loadContent('<?php echo base_url(uri_string()); ?>');
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            cekError(XMLHttpRequest, textStatus);
                        },
                    })
                }
            }

            $('#RWCUTI_TGLAWL').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                daysOfWeekDisabled: [0, 6]
            });
            $('#RWCUTI_TGLAKR').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD-MM-YYYY',
                daysOfWeekDisabled: [0, 6]
            });
            $("#RWCUTI_TGLAWL").on("dp.change", function (e) {
                $('#RWCUTI_TGLAKR').data("DateTimePicker").minDate(e.date);
                countDiff();
            });
            $("#RWCUTI_TGLAKR").on("dp.change", function (e) {
                $('#RWCUTI_TGLAWL').data("DateTimePicker").maxDate(e.date);
                countDiff();
            });
            function countDiff() {
                var iWeeks, iDateDiff, iAdjust = 0;
                var tglAwl = $('#RWCUTI_TGLAWL').val().split('-');
                var tglAkr = $('#RWCUTI_TGLAKR').val().split('-');
                var dDate1 = new Date(tglAwl[2] + '-' + tglAwl[1] + '-' + tglAwl[0]);
                var dDate2 = new Date(tglAkr[2] + '-' + tglAkr[1] + '-' + tglAkr[0]);
                //alert(dDate1);
                var iWeeks, iDateDiff, iAdjust = 0;
                if (dDate2 < dDate1)
                    return -1; // error code if dates transposed
                var iWeekday1 = dDate1.getDay(); // day of week
                var iWeekday2 = dDate2.getDay();
                iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
                iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
                if ((iWeekday1 == 6 && iWeekday2 == 6) || (iWeekday1 == 7 && iWeekday2 == 7)) {
                    iAdjust = 1; // adjustment if both days on weekend
                } else if (iWeekday1 == 6 && iWeekday2 == 7) {
                    iAdjust = 2;
                }
                // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
                iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

                if (iWeekday1 <= iWeekday2) {
                    iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
                } else {
                    iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
                }

                iDateDiff -= iAdjust // take into account both days on weekend
                $('#RWCUTI_JUM').val(iDateDiff + 1); // add 1 because dates are inclusive
            }
        });
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
