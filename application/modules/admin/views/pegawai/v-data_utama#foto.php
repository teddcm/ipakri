<?php $edit = isset($pegawai) ? true : false; ?>
<?php
require 'v-listing#search.php';
//$this->output->cache(60);
if (get_data_pns($this->uri->segment(4)) != NULL) {
    ?>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Upload Foto</a></li>
            <li class="pull-right">
                <div>           
                    <a class="btn btn-warning btn-flat" href="#" onclick="loadContent('<?php echo base_url('admin/pegawai/data_utama/' . $this->uri->segment(4)); ?>')"><i class="fa fa-backward"></i> kembali</a>
                </div>
            </li>
        </ul>
        <div class="tab-content"> 
            <div class="tab-pane active" id="tab_1">
                <div class="clearfix"></div>
                <div class="row input-group-sm">
                    <div class="col-md-6 col-lg-4">
                        <input id="input-foto" name="userfile" type="file" class="file-loading">
                    </div>
                    <?php if ($is_file_exist) { ?>
                        <div class="col-md-4 col-lg-2">
                            <div id="getFoto"></div>
                            <div class="clearfix">&nbsp;</div>
                            <button class="btn btn-danger btn-block" onclick="HapusFoto('<?= $file; ?>')">Hapus Foto</button>
                        </div>
                        <div class="col-md-6 col-lg-4">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>

    <script>
        function HapusFoto(file) //PEGAWAI_TAMPIL
        {
            var x = confirm("Foto akan dihapus?");
            if (x) {
                $.ajax({
                    url: '<?php echo base_url('admin/pegawai/hapus_foto'); ?>',
                    type: 'POST',
                    data: {file: file},
                    success: function (result) {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                });
            }
        }
        $("#input-foto").fileinput({
            showCaption: false,
            uploadUrl: "<?= base_url('admin/pegawai/upload_foto/' . $this->uri->segment(4)); ?>",
            autoReplace: true,
            dropZoneTitle: "Mohon upload foto ukuran 2x3 atau 4x6",
            maxFileCount: 1,
            maxImageWidth: 300,
            allowedFileExtensions: ["jpg", "jpeg"],
            resizeImage: true,
        }).on('fileuploaded', function (event, data, msg) {
            $.notify(data.response[1], data.response[0]);
            if (data.response[0] === 'success') {
                loadContent('<?php echo base_url(uri_string()); ?>');
            }
        });
    </script>
<?php } else { ?>
    <script>
        $.notify('Halaman Tidak Ditemukan', 'error');
    </script>
<?php } ?>
