<div class="tab-pane" id="tab_1">
    <div id="issu">
        <div>
            <form id="myForm_issu"> 
                <div>
                    <input type="submit" style="display: none" name="ok">
                    <input type="hidden" id="RWISU_NIP" name="RWISU_NIP" value="<?php echo $this->uri->segment(4); ?>" >
                    <input type="hidden" id="RWISU_ID" name="RWISU_ID">
                    <input type="hidden" id="mode" name="mode" value="edit" >    
                </div>                    
                <div class="form-group col-lg-4">
                    <label>* Nama</label>
                    <input class="form-control" name="RWISU_NAMISU" id="RWISU_NAMISU">
                </div>
                <div class="form-group col-lg-2">
                    <label>* Status</label>
                    <select class="form-control" id="RWISU_STATUS" name="RWISU_STATUS">  
                        <?php
                        foreach ($status_kawin as $x) {
                            echo '<option value="' . $x->STS_ID . '">' . $x->STS_NAMA . '</option>';
                        }
                        ?>
                    </select>    
                </div>
                <div class="form-group col-lg-2">
                    <label>Tanggal Menikah</label>
                    <input class="form-control datepicker" name="RWISU_TGLNIKAH" id="RWISU_TGLNIKAH">
                </div>
                <div class="form-group col-lg-2">
                    <label>Akta  Menikah</label>
                    <input class="form-control" name="RWISU_AKTNIKAH" id="RWISU_AKTNIKAH">
                </div>
            </form>
            <div class="form-group col-lg-12">
                <div class="pull-right">
                    <div>
                        <a class="btn btn-info btn-flat" href="#" id="tambah">tambah</a>   
                        <a class="btn btn-success btn-flat" href="#" id="simpan"style="display: none">simpan</a>  
                        <a class="btn btn-danger btn-flat" href="#" id="cancel"style="display: none">batal</a>   
                    </div>
                </div>      
            </div>       
            <div class="clearfix"></div>
            &nbsp;
            <table id="myDataTable_issu" class="table">
                <thead>
                    <tr>
                        <th width='5%'>No</th>
                        <th>Nama</th>
                        <th>Tanggal Menikah</th>
                        <th>Akta Menikah</th>
                        <th>Status</th>
                        <th width='10%'>Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>  
            </table>
        </div>
    </div>
</div>


<script>
    jQuery(function ($) {
        $("#issu * .datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
        $("#issu * .datepicker").datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    $("#issu * #myDataTable_issu").dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<"F">t<"F">',
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax": "<?php echo site_url('admin/pegawai/keluarga_issu_ajax_view/') . $this->uri->segment(4); ?>",
        "iDisplayLength": 100,
        "columns": [
            {"data": "RWISU_ID", "class": "text-center", },
            {"data": "RWISU_NAMISU"},
            {"data": "RWISU_TGLNIKAH"},
            {"data": "RWISU_AKTNIKAH"},
            {"data": "RWISU_STATUS"},
            {"data": "RWISU_NIP", "class": "text-center"},
        ],
        "order": [[3, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            var aksi = '<a class="edit btn btn-xs btn-info" id="' + data.RWISU_ID + '">edit</a>\n\
                        <a onclick="hapus_issu(\'' + data.RWISU_ID + '\',\'' + data.RWISU_NIP + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
            $('td:eq(-1)', row).html(aksi);

        },
    });

    $("#issu * #myForm_issu").submit(function (e) {
        var url = "<?php echo base_url('admin/pegawai/keluarga_issu_simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#issu * #myForm_issu").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url(uri_string()); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

    $("#issu * #myForm_issu *").attr("disabled", true);
    $("#issu * #myForm_issu").trigger("reset");
    $("#issu * #simpan").click(function () {
        $("#issu * #myForm_issu").submit();
    });

    $("#issu * #myDataTable_issu tbody").on('click', '.edit', function () {
        $("#issu * #myForm_issu").trigger("reset");
        $("#issu * #RWISU_ID").val($(this).attr('id'));
        $("#issu * #myForm_issu * [name]").attr("disabled", false);
        $("#issu * option").attr("disabled", false);
        $("#issu * #cancel").show();
        $("#issu * #simpan").show();
        $("#issu * #tambah").hide();
        $("#issu * #mode").val('edit');
        $("#issu * #mode").attr("disabled", false);
        var selected = $(this).parents('tr');
        $("#issu * #RWISU_NAMISU").val(selected.find('td:eq(1)').html());
        $("#issu * #RWISU_TGLNIKAH").val(selected.find('td:eq(2)').html());
        $("#issu * #RWISU_AKTNIKAH").val(selected.find('td:eq(3)').html());
        $("#issu * #RWISU_STATUS option").filter(function () {
            return $(this).text() == selected.find('td:eq(4)').html();
        }).prop('selected', true)
    });
    $("#issu * #cancel").click(function () {
        $("#issu * #myForm_issu *").attr("disabled", true);
        $("#issu * #myForm_issu").trigger("reset");
        $("#issu * #cancel").hide();
        $("#issu * #simpan").hide();
        $("#issu * #tambah").show();
        //loadContent('<?php echo base_url($this->uri->uri_string()); ?>');
    })
    $("#issu * #tambah").click(function () {
        $("#issu * #myForm_issu * [name]").attr("disabled", false);
        $("#issu * option").attr("disabled", false);
        $("#issu * #mode").attr("disabled", false);
        $("#issu * #mode").val('tambah');
        $("#issu * #cancel").show();
        $("#issu * #simpan").show();
        $("#issu * #tambah").hide();
    })
    function hapus_issu(id, nip) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            var url = "<?php echo base_url('admin/pegawai/keluarga_issu_hapus'); ?>"; // the script where you handle the form input.
            $.ajax({
                type: "POST",
                url: url,
                async: false,
                data: {"id": id, "nip": nip},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        loadContent('<?php echo base_url(uri_string()); ?>');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }
</script>