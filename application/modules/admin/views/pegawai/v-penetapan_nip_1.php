<?php $edit = isset($pegawai) ? true : false; ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="kecamatan"> Kecamatan
                    </label>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" id="find_tk2" class="form-control input-sm" placeholder="Cari Kabupaten/Kota . . .">
                    <input type="text" id="find_tk3" class="form-control input-sm" placeholder="Cari Kecamatan . . .">                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<form id="myForm">        
    <input type="hidden" id="mode" name="mode" value="<?php echo $edit ? 'edit' : 'tambah'; ?>" >
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Data Pribadi</a></li>
            <li class="pull-right">
                <div>
                    <a class="btn btn-success btn-flat" href="#" id="simpan">simpan</a> 
                    <a class="btn btn-danger btn-flat" onclick="loadContent('<?php echo base_url('admin/pegawai'); ?>')"><b>cancel</b></a>
                </div>
            </li>
        </ul>
        <div class="tab-content n-a">        
            <div class="tab-pane active" id="tab_1">
                <div class="form-group col-lg-3">
                    <label>NIP Baru *</label>
                    <input class="form-control" name="PNS_NIPBARU" id="PNS_NIPBARU" value="<?php echo $edit ? $pegawai->PNS_NIPBARU : ''; ?>" maxlength="18">
                </div>
                <div class="form-group col-lg-2">
                    <label>NIP Lama</label>
                    <input class="form-control" name="PNS_PNSNIP" value="<?php echo $edit ? $pegawai->PNS_PNSNIP : ''; ?>">
                </div>
                <div class="form-group col-lg-2">
                    <label>Gelar Depan</label>
                    <input class="form-control" style="text-transform: none" name="PNS_GLRDPN" value="<?php echo $edit ? $pegawai->PNS_GLRDPN : ''; ?>">
                </div>
                <div class="form-group col-lg-3">
                    <label>Nama</label>
                    <input class="form-control" name="PNS_PNSNAM" value="<?php echo $edit ? $pegawai->PNS_PNSNAM : ''; ?>">
                </div>
                <div class="form-group col-lg-2">
                    <label>Gelar Belakang</label>
                    <input class="form-control" style="text-transform: none" name="PNS_GLRBLK" value="<?php echo $edit ? $pegawai->PNS_GLRBLK : ''; ?>">
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-lg-3">
                    <label>Tempat Lahir</label>
                    <div class="input-group" id="cariLokasi">
                        <input id="PNS_TEMLHR_TXT" class="form-control" value="<?php echo $edit ? convert_tempat_lahir($pegawai->PNS_TEMLHR) : ''; ?>">
                        <input type="hidden" id="PNS_TEMLHR"  name="PNS_TEMLHR" value="<?php echo $edit ? $pegawai->PNS_TEMLHR : ''; ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary"><i class="fa fa-search"></i></button>
                        </span>

                    </div>
                </div>
                <div class="form-group col-lg-2">
                    <label>Tanggal Lahir</label>
                    <input class="form-control datepicker" name="PNS_TGLLHRDT" id="PNS_TGLLHRDT" value="<?php echo $edit ? (($pegawai->PNS_TGLLHRDT != NULL && $edit) ? date('d-m-Y', strtotime($pegawai->PNS_TGLLHRDT)) : '') : ''; ?>">
                </div>
                <div class="form-group col-lg-2">
                    <label>Agama</label>
                    <select class="form-control" name="PNS_KODAGA">
                        <option value="">-</option>
                        <?php
                        foreach ($agama as $ag) {
                            $slct = $pegawai->PNS_KODAGA == $ag->AGA_KODAGA ? 'selected' : '';
                            echo '<option ' . $slct . ' value="' . $ag->AGA_KODAGA . '">' . $ag->AGA_NAMAGA . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-lg-2">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" id="PNS_PNSSEX" name="PNS_PNSSEX">
                        <option value="">-</option>
                        <option <?php echo @$pegawai->PNS_PNSSEX == 1 ? 'selected' : ''; ?> value="1">Pria</option>
                        <option <?php echo @$pegawai->PNS_PNSSEX == 2 ? 'selected' : ''; ?> value="2">Wanita</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-lg-3">
                    <label>Kedudukan PNS</label>
                    <select class="form-control" name="PNS_KEDHUK">
                        <option value="">-</option>
                        <?php
                        foreach ($kedhuk as $x) {
                            $slct = ($pegawai->PNS_KEDHUK == $x->KED_KEDKOD) ? 'selected' : '';
                            echo '<option ' . $slct . ' value="' . $x->KED_KEDKOD . '">' . $x->KED_KEDNAM . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Status Pegawai</label>
                    <select class="form-control" name="PNS_STCPNS" >
                        <option value="">-</option>
                        <option <?php echo @$pegawai->PNS_STCPNS == 'C' ? 'selected' : ''; ?> value="C">CPNS</option>
                        <option <?php echo @$pegawai->PNS_STCPNS == 'P' ? 'selected' : ''; ?> value="P">PNS</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-lg-2">
                    <label>No. Seri Karpeg</label>
                    <input class="form-control" name="PNS_KARPEG" value="<?php echo $edit ? $pegawai->PNS_KARPEG : ''; ?>">
                </div>
                <div class="form-group col-lg-2">
                    <label>TMT CPNS/Awal</label>
                    <input class="form-control datepicker" name="PNS_TMTCPN" id="PNS_TMTCPN" value="<?php echo $edit ? (($pegawai->PNS_TMTCPN != NULL && $edit) ? date('d-m-Y', strtotime($pegawai->PNS_TMTCPN)) : '') : ''; ?>">
                </div>
                <div class="form-group col-lg-2">
                    <label>Golru Awal</label>
                    <select class="form-control" name="PNS_GOLAWL">
                        <option value="">-</option>
                        <?php
                        foreach ($golru as $x) {
                            echo '<option  value="' . $x->GOL_KODGOL . '">' . $x->GOL_GOLNAM . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-lg-2">
                    <label>TMT PNS</label>
                    <input class="form-control datepicker" name="PNS_TMTPNS" value="<?php echo $edit ? (($pegawai->PNS_TMTPNS != NULL && $edit) ? date('d-m-Y', strtotime($pegawai->PNS_TMTPNS)) : '') : ''; ?>">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
</form>
</form>

<script>
    $('form').each(function () {
        var html = $(this).html().replace(/\*/g, "<spanclass=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })
    $('#PNS_NIPBARU').on('focusout', function () {
        var nip = $('#PNS_NIPBARU').val();
        $('#PNS_TGLLHRDT').val(nip != '' ? (nip.substring(6, 8) + '-' + nip.substring(4, 6) + '-' + nip.substring(0, 4)) : '');
        $('#PNS_TMTCPN').val(nip != '' ? ('01-' + nip.substring(12, 14) + '-' + nip.substring(8, 12)) : '');
        $('#PNS_PNSSEX').val(nip != '' ? nip.substring(14, 15) : '');
    })
    jQuery(function ($) {
        $("input[name=PNS_NIPBARU]").mask("999999999999999999", {placeholder: ""});
        $("input[name=PNS_PNSNIP]").mask("999999999", {placeholder: ""});
        $(".datepicker").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });
    $("#kecamatan").on('change', function () {
        if ($(this).is(':checked')) {
            $("#find_tk2").parent('span').hide();
            $("#find_tk3").parent('span').show();
            $("#find_tk3").focus();
        } else {
            $("#find_tk3").parent('span').hide();
            $("#find_tk2").parent('span').show();
            $("#find_tk2").focus();
        }
    })
    var find_tk2 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?= base_url('admin/json/json_temlahir'); ?>/2?q=%QUERY',
            wildcard: '%QUERY'
        }
    });
    var find_tk3 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?= base_url('admin/json/json_temlahir'); ?>/3?q=%QUERY',
            wildcard: '%QUERY'
        }
    });
    $('#find_tk2').typeahead(null, {
        name: 'best-pictures',
        display: 'name',
        source: find_tk2,
        templates: {
            empty: [
                '<div class="empty-message" style="font-size: 12px;line-height: 16px;margin: 0;padding: 3px 5px;">',
                'TIDAK DITEMUKAN',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return '<p>' + data.ket + ' – <strong>' + data.name + '</strong></p>';
            }
        }
    }).on('typeahead:selected', function (evt, item) {
        $('#PNS_TEMLHR_TXT').val(item.name);
        $('#PNS_TEMLHR').val((item.id + "0000000000").slice(0, 10));
        $('#myModal').modal('hide');
        $('#PNS_TGLLHRDT').get(0).focus();

    });
    $('#find_tk3').typeahead(null, {
        name: 'best-pictures',
        display: 'name',
        source: find_tk3,
        templates: {
            empty: [
                '<div class="empty-message" style="font-size: 12px;line-height: 16px;margin: 0;padding: 3px 5px;">',
                'TIDAK DITEMUKAN',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return '<p>' + data.ket + ' – <strong>' + data.name + '</strong></p>';
            }
        }
    }).on('typeahead:selected', function (evt, item) {
        $('#PNS_TEMLHR_TXT').val(item.name);
        $('#PNS_TEMLHR').val((item.id + "0000000000").slice(0, 10));
        $('#myModal').modal('hide');
        $('#PNS_TGLLHRDT').get(0).focus();



    });


    $('#cariLokasi').on('focusin', function (e) {
        $("#cariLokasi *").blur();
        $('#myModal').modal('show')
        $("#find_tk3").parent('span').hide();
        $("#find_tk2").parent('span').show();
        $("#kecamatan").attr("checked", false)
        $('#myModal').on('shown.bs.modal', function () {
            $('#find_tk2').get(0).focus();
        })
        e.preventDefault();
    })



    $("#simpan").click(function () {
        $("#myForm").submit();
    });
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('admin/pegawai/data_utama_simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('admin/pegawai/data_utama/'); ?>' + $("input[name='PNS_NIPBARU']").val());
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>