<?php

//print_r($kgb);
/*  Surat Pemberitahuan Kenaikan Gaji Berkala (KGB)
 *  Badan Kepegawaian Negara
 *  Creator : Teddy Cahyo Munanto
 *  Email   : teddcm@gmail.com
 */
include ('assets/plugins/fpdf/fpdf.php');

// Instanciation of inherited class
class PDF extends FPDF {

    function Footer() {
        $this->SetFont('Arial', '', 9);
        $this->Ln(320);
        $this->Cell(5);
        $this->Cell(0, 70, 'Tembusan :', 0, 0, 'L');


        $kepada = explode(PHP_EOL, getPengaturan('main_nama_organisasi'));
        $n = 80;
        foreach ($kepada as $line) {
            $this->Ln(0);
            $this->Cell(5);
            $this->Cell(0, $n, $line, 0, 0, 'L');
            $n = $n + 10;
        }
    }

    function create_table($xstart, $ystart, $xend, $yend, $title, $line = FALSE) {
        $nowY = $this->getY();
//-----------------------------------------------------------------------------
        $this->SetLineWidth(4);
        $this->Line($xstart + 2, $ystart, $xend - 2, $ystart); // 50mm from each edge

        $this->SetLineWidth(0);

        $this->Line($xstart, $yend, $xend, $yend); // 50mm from each edge
        $this->Line($xstart, $ystart, $xstart, $yend); // 50mm from each edge
        $this->Line($xend, $ystart, $xend, $yend); // 50mm from each edge

        if ($line) {
            $this->Line($xstart, $ystart + 7, $xend, $ystart + 7); // 50mm from each edge
        }
        $this->SetTextColor(255, 255, 255);
        $this->SetFont('Arial', '', 8);

        $this->SetY(0);
        $this->SetY(0);
        $this->Cell($xstart - 9);
        $this->Cell(0, $ystart * 2, $title, 0, 0, 'L');
        $this->SetTextColor(0, 0, 0);
        $this->SetY($nowY);
//-----------------------------------------------------------------------------
    }

}

$pdf = new PDF('P', 'mm', array(210, 330));
$pdf->SetTitle('Profil PNS');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(0);
$pdf->AddFont('ArialNarrow', '', 'arialn.php');
$pdf->AddFont('ArialNarrow', 'B', 'arialnb.php');
$pdf->AddFont('ArialNarrow', 'BI', 'arialnbi.php');
$pdf->AddFont('ArialNarrow', 'I', 'arialni.php');
$font = 8;

$pdf->SetFont('Arial', 'B', 11);
$pdf->Cell(200, 10, 'PROFIL PEGAWAI NEGERI SIPIL', 0, 0, 'C');
$pdf->Line(80, 17, 210 - 70, 17); // 50mm from each edge

$logo = 'contents/img/' . getPengaturan('utama_logo_sk');
$pdf->Image($logo, 20, 8, null, 15);

$path = base_url('contents/foto');
$filename = get_data_pns($NIP, 'PNS_NIPBARU') . '_' . str_replace('.', '_', str_replace(' ', '_', get_data_pns($NIP, 'PNS_PNSNAM'))) . '.jpg';
$img = $path . '/' . (@file_get_contents($path . '/' . $filename) ? $filename : 'nofoto.jpg');

$pdf->Image($img, 210 - 28, 5, 18, NULL);


$pdf->SetFont('Arial', 'B', 9);
$pdf->Ln(10);
$pdf->Cell(14, 15, getPengaturan('main_nama_organisasi_short'), 0, 0, 'L');

$pdf->create_table(10, 32, 210 - 10, 110, 'DATA PRIBADI');
$pdf->SetFont('Arial', '', $font);
$pdf->Ln(12);

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 10, 'Nama', 0, 0, 'L');
$pdf->Cell(1, 10, ':   ' . get_nama_pns($pegawai->PNS_NIPBARU), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 20, 'NIP Lama / NIP Baru', 0, 0, 'L');
$pdf->Cell(1, 20, ':   ' . ($pegawai->PNS_PNSNIP == NULL ? '-' : $pegawai->PNS_PNSNIP) . ' / ' . $pegawai->PNS_NIPBARU, 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 30, 'Pangkat / Gol. Ruang', 0, 0, 'L');
$pdf->Cell(1, 30, ':   ' . convert_pangkat($pegawai->PNS_GOLRU) . ' , ' . convert_golongan($pegawai->PNS_GOLRU) . ' / ' . convertDate(strtotime($pegawai->PNS_TMTGOL)), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 40, 'Jabatan Terakhir', 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 50, '- Jabatan Struktural / TMT', 0, 0, 'L');
$pdf->Cell(1, 50, ':   ' . ($pegawai->RWJAB_IDJENJAB == 1 ? ($pegawai->RWJAB_NAMAJAB . ' / ' . ($pegawai->RWJAB_TMTJAB != NULL ? convertDate(strtotime($pegawai->RWJAB_TMTJAB)) : '-')) : ''), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 60, '- Jabatan Fungsional / TMT', 0, 0, 'L');
$pdf->Cell(1, 60, ':   ' . ($pegawai->RWJAB_IDJENJAB != 1 ? ($pegawai->RWJAB_NAMAJAB . ' / ' . ($pegawai->RWJAB_TMTJAB != NULL ? convertDate(strtotime($pegawai->RWJAB_TMTJAB)) : '-')) : ''), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 70, 'TMT CPNS / PNS', 0, 0, 'L');
$pdf->Cell(1, 70, ':   ' . convertDate(strtotime($pegawai->PNS_TMTCPN)) . ' / ' . convertDate(strtotime($pegawai->PNS_TMTPNS)), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 80, 'Tempat / Tanggal Lahir', 0, 0, 'L');
$pdf->Cell(1, 80, ':   ' . convert_tempat_lahir($pegawai->PNS_TEMLHR) . ' / ' . convertDate(strtotime($pegawai->PNS_TGLLHRDT)), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 90, 'Jenis Kelamin', 0, 0, 'L');
$pdf->Cell(1, 90, ':   ' . convert_jk($pegawai->PNS_PNSSEX), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 100, 'Agama', 0, 0, 'L');
$pdf->Cell(1, 100, ':   ' . convert_agama($pegawai->PNS_KODAGA), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 110, 'Status Perkawinan', 0, 0, 'L');
$pdf->Cell(1, 110, ':   ' . convert_status_kawin($pegawai->PNS_STSWIN), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 120, 'Unit Kerja', 0, 0, 'L');
$pdf->Cell(1, 120, ':   ' . $pegawai->UNO_NAMUNO, 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 130, '', 0, 0, 'L');
$pdf->Cell(1, 130, '    ' . convert_unor(get_induk_unor($pegawai->PNS_UNOR)->UNO_ID), 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 140, 'Instansi Induk', 0, 0, 'L');
$pdf->Cell(1, 140, ':   ' . $pegawai->INS_NAMINS, 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(1);
$pdf->Cell(50, 150, 'Alamat Rumah', 0, 0, 'L');
$pdf->Cell(1, 150, ':   ' . $pegawai->PNS_ALAMAT, 0, 0, 'L');



//-----------------------------------------------------------------------------
$pdf->create_table(10, 115, 104, 155, 'RIWAYAT GOLONGAN', 1);
$start = 165;
$pdf->Cell(80, $start += 10, 'GOLONGAN/PANGKAT', 0, 0, 'L');
$pdf->Cell(0, $start, 'TMT', 0, 0, 'L');
foreach ($golongan as $g) {
    $pdf->Ln(0);
    $pdf->Cell(10, $start += 10, ($g->GOL_GOLNAM), 0, 0, 'L');
    $pdf->Cell(65, $start, ($g->GOL_PKTNAM), 0, 0, 'L');
    $pdf->Cell(0, $start, @reverseDate($g->RWGOL_TMTGOL), 0, 0, 'L');
}

//-----------------------------------------------------------------------------
$pdf->create_table(106, 115, 200, 155, 'RIWAYAT JABATAN', 1);
$start = 165;
$pdf->Cell(96);
$pdf->Cell(80, $start += 10, 'GOLONGAN/PANGKAT', 0, 0, 'L');
$pdf->Cell(0, $start, 'TMT', 0, 0, 'L');
foreach ($jabatan as $g) {
    $pdf->Ln(0);
    $pdf->Cell(96);
    $pdf->Cell(75, $start += 10, ($g->RWJAB_NAMAJAB), 0, 0, 'L');
    $pdf->Cell(0, $start, @reverseDate($g->RWJAB_TMTJAB), 0, 0, 'L');
    $pdf->Ln(0);
}

//-----------------------------------------------------------------------------
$pdf->create_table(10, 160, 104, 200, 'RIWAYAT DIKLAT PIMPINAN', 1);
$start = 255;
$pdf->Cell(80, $start += 10, 'NAMA DIKLAT', 0, 0, 'L');
$pdf->Cell(0, $start, 'TAHUN', 0, 0, 'L');
foreach ($diklat as $g) {
    $pdf->Ln(0);
    $pdf->Cell(80, $start += 10, ($g->RWDLT_NAMDLT), 0, 0, 'L');
    $pdf->Cell(0, $start, ($g->RWDLT_THN), 0, 0, 'L');
}


//-----------------------------------------------------------------------------
$pdf->create_table(106, 160, 200, 200, 'RIWAYAT PENDIDIKAN', 1);
$start = 255;
$pdf->Cell(96);
$pdf->Cell(80, $start += 10, 'PENDIDIKAN', 0, 0, 'L');
$pdf->Cell(0, $start, 'TAHUN', 0, 0, 'L');
foreach ($pendidikan as $g) {
    $pdf->Ln(0);
    $pdf->Cell(96);
    $pdf->Cell(81, $start += 10, ($g->DIK_NMDIK), 0, 0, 'L');
    $pdf->Cell(0, $start, ($g->RWDIK_THNLULUS), 0, 0, 'L');
}

//-----------------------------------------------------------------------------
$pdf->create_table(10, 205, 104, 245, 'RIWAYAT KELUARGA', 1);
$start = 345;
$pdf->Cell(25, $start += 10, 'KETERANGAN', 0, 0, 'L');
$pdf->Cell(10, $start, 'NO', 0, 0, 'L');
$pdf->Cell(0, $start, 'NAMA', 0, 0, 'L');
$n = 0;
$m = 0;
$anakx = $anak;
foreach ($issu as $g) {
    $anakx = $anak;
    $pdf->Ln(0);
    $pdf->Cell(25, $start += 10, 'Istri/Suami', 0, 0, 'L');
    $pdf->Cell(10, $start, $n += 1, 0, 0, 'L');
    $pdf->Cell(0, $start, ($g->RWISU_NAMISU), 0, 0, 'L');
    foreach ($anakx as $h) {
        if ($h->RWANK_ID_ORTU == $g->RWISU_ID) {
            $pdf->Ln(0);
            $pdf->Cell(25, $start += 10, 'Anak', 0, 0, 'L');
            $pdf->Cell(10, $start, $m += 1, 0, 0, 'L');
            $pdf->Cell(0, $start, ($h->RWANK_NAMA), 0, 0, 'L');
        }
    }
    $m = 0;
}
//-----------------------------------------------------------------------------
$pdf->create_table(106, 205, 200, 245, 'RIWAYAT KURSUS', 1);
$start = 345;
$pdf->Cell(96);
$pdf->Cell(80, $start += 10, 'NAMA', 0, 0, 'L');
$pdf->Cell(0, $start, 'TAHUN', 0, 0, 'L');
foreach ($kursus as $g) {
    $pdf->Ln(0);
    $pdf->Cell(96);
    $pdf->Cell(81, $start += 10, ($g->RWKRS_NAMKRS), 0, 0, 'L');
    $pdf->Cell(0, $start, ($g->RWKRS_THN), 0, 0, 'L');
}

//-----------------------------------------------------------------------------
$pdf->create_table(10, 250, 104, 290, 'RIWAYAT ANGKA KREDIT', 1);
$start = 435;
$pdf->Cell(25, $start += 10, 'Tanggal SK', 0, 0, 'L');
$pdf->Cell(23, $start, 'AK UTAMA', 0, 0, 'L');
$pdf->Cell(28, $start, 'AK PENUNJANG', 0, 0, 'L');
$pdf->Cell(25, $start, 'AK TOTAL', 0, 0, 'L');
foreach ($angka_kredit as $g) {
    $pdf->Ln(0);
    $pdf->Cell(15, $start += 10, @reverseDate($g->RWAKR_TGLSK), 0, 0, 'L');
    $pdf->Cell(25, $start, ($g->RWAKR_AKRUTAMA), 0, 0, 'R');
    $pdf->Cell(25, $start, ($g->RWAKR_AKRPENUNJANG), 0, 0, 'R');
    $pdf->Cell(25, $start, ($g->RWAKR_AKRTOTAL), 0, 0, 'R');
}

//-----------------------------------------------------------------------------
$pdf->create_table(106, 250, 200, 290, 'RIWAYAT SKP', 1);
$start = 435;
$pdf->Cell(96);
$pdf->Cell(80, $start += 10, 'TAHUN', 0, 0, 'L');
$pdf->Cell(0, $start, 'NILAI', 0, 0, 'L');
foreach ($skp as $g) {
    $pdf->Ln(0);
    $pdf->Cell(96);
    $pdf->Cell(81, $start += 10, ($g->RWSKP_THN), 0, 0, 'L');
    $pdf->Cell(0, $start, ($g->RWSKP_NRATA), 0, 0, 'L');
}
//$pdf->Image($barcode, 210 - 28, 310, 18, NULL);


$nama_file = 'Profil_' . $pegawai->PNS_NIPBARU . '__' . $pegawai->PNS_PNSNAM . '.pdf';
$pdf->Output($nama_file, 'I');
?>
