<?php

//print_r($kgb);
/*  Surat Pemberitahuan Kenaikan Gaji Berkala (KGB)
 *  Badan Kepegawaian Negara
 *  Creator : Teddy Cahyo Munanto
 *  Email   : teddcm@gmail.com
 */
include ('assets/plugins/fpdf/fpdf.php');

// Instanciation of inherited class
class PDF extends FPDF {

    function Footer() {
        $this->SetFont('ArialNarrow', '', 9);
        $this->Ln(10);
        $this->Cell(5);
        $this->Cell(0, 70, 'Tembusan :', 0, 0, 'L');


        $kepada = explode(PHP_EOL, $_GET['tembusan']);
        $n = 80;
        foreach ($kepada as $line) {
            $this->Ln(0);
            $this->Cell(5);
            $this->Cell(0, $n, $line, 0, 0, 'L');
            $n = $n + 10;
        }
    }

}

$pdf = new PDF('P', 'mm', array(210, 330));
$pdf->SetTitle('Kenaikan Gaji Berkala');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(0);
$pdf->AddFont('ArialNarrow', '', 'arialn.php');
$pdf->AddFont('ArialNarrow', 'B', 'arialnb.php');
$pdf->AddFont('ArialNarrow', 'BI', 'arialnbi.php');
$pdf->AddFont('ArialNarrow', 'I', 'arialni.php');
$font = 10;

// VARIABEL
$sifat = getPengaturan('kgb_sk_sifat');
$lampiran = getPengaturan('kgb_sk_lampiran');
$perihal = 'Kenaikan Gaji Berkala';
$kota = getPengaturan('kgb_sk_kota');
$header = getPengaturan('kgb_sk_header');
$nama = $kgb->RWKGB_NAMA;
$nip = split_nip($kgb->RWKGB_NIP); //ok
$unit_kerja = $kgb->RWKGB_INSKER; //ok

$tgl_sk_lama = convertDate(strtotime($kgb->RWKGB_TGLSKLAMA));
$no_sk_lama = $kgb->RWKGB_NOSKLAMA;
$tgl_mulai_sk_lama = convertDate(strtotime($kgb->RWKGB_TMTSKLAMA));
$gapok_lama = $kgb->RWKGB_GAPOKLAMA;
$mkg_lama = str_pad($kgb->RWKGB_MKGLAMA, 4, '0', STR_PAD_LEFT);
$mkg_sk_lama = substr($mkg_lama, 0, 2) . " Tahun " . substr($mkg_lama, 2, 2) . " Bulan";
$golru_lama = $kgb->RWKGB_GOLLAMA;
$nama_jabatan = $kgb->RWKGB_JAB;
$jabatan_pejabat_lama = $kgb->RWKGB_JABPJBLAMA;


$tgl_sk_baru = convertDate(strtotime($kgb->RWKGB_TGLSKBARU));
$no_sk_baru = $kgb->RWKGB_NOSKBARU;
$tgl_mulai_sk_baru = convertDate(strtotime($kgb->RWKGB_TMTSKBARU));
$gapok_baru = $kgb->RWKGB_GAPOKBARU;
$mkg_baru = str_pad($kgb->RWKGB_MKGBARU, 4, '0', STR_PAD_LEFT);
$mkg_sk_baru = substr($mkg_baru, 0, 2) . " Tahun " . substr($mkg_baru, 2, 2) . " Bulan";
$golru_baru = $kgb->RWKGB_GOLBARU;
$dasar_hukum = $kgb->RWKGB_DASARHUK;

$pdf->SetFont('Times', '', 10);
$img = 'contents/img/' . getPengaturan('utama_logo_sk');

$pdf->Image($img, 95, 8, null, 20);
$n = 48;

$header = explode(PHP_EOL, $header);
foreach ($header as $line) {
    $pdf->Ln(0);
    $pdf->Cell(5);
    $pdf->Cell(180, $n, $line, 0, 0, 'C');
    $n = $n + 10;
}


$pdf->SetFont('ArialNarrow', '', $font);
$pdf->Ln(40);
$pdf->Cell(5);
$pdf->Cell(15, 10, 'Nomor', 0, 0, 'L');
$pdf->Cell(1, 10, ': ' . $no_sk_baru, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell(5);
$pdf->Cell(15, 20, 'Sifat', 0, 0, 'L');
$pdf->Cell(1, 20, ': ' . $sifat, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell(5);
$pdf->Cell(15, 30, 'Lampiran', 0, 0, 'L');
$pdf->Cell(1, 30, ': ' . $lampiran, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell(5);
$pdf->Cell(15, 40, 'Perihal', 0, 0, 'L');
$pdf->Cell(1, 40, ': ' . $perihal, 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(140);
$pdf->Cell(1, 10, $kota . ', ' . $tgl_sk_baru, 0, 0, 'L');

$pdf->Ln(0);
$pdf->Cell(110);
$pdf->Cell(15, 40, 'Kepada:', 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell(102);
$pdf->Cell(15, 50, 'Yth.', 0, 0, 'L');

$kepada = explode(PHP_EOL, $kepada);
$n = 50;
foreach ($kepada as $line) {
    $pdf->Ln(0);
    $pdf->Cell(110);
    $pdf->Cell(15, $n, $line, 0, 0, 'L');
    $n = $n + 10;
}

$padding = 20;
$pdf->Ln(45);
$pdf->Cell($padding);
$pdf->MultiCell(150, 5, '           Dengan ini diberitahukan bahwa berhubung dengan telah dipenuhinya masa kerja dan syarat-syarat lainnya kepada : ');
$pdf->Ln(5);
$pdf->Cell($padding);
$pdf->Cell(5, 0, '1.', 0, 0, 'L');
$pdf->Cell(50, 0, 'Nama', 0, 0, 'L');
$pdf->Cell(0, 0, ': ' . $nama, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 10, '2.', 0, 0, 'L');
$pdf->Cell(50, 10, 'NIP', 0, 0, 'L');
$pdf->Cell(0, 10, ': ' . $nip, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 20, '3.', 0, 0, 'L');
$pdf->Cell(50, 20, 'Pangkat / golongan / jabatan', 0, 0, 'L');
$pdf->Cell(2, 20, ': ', 0, 0, 'L');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();
$pdf->SetXY($current_x, $current_y + 8);
$pdf->MultiCell(90, 5, convert_pangkat($golru_lama) . ', ' . convert_golongan($golru_lama) . ($nama_jabatan != '' ? ', ' . ucwords_strtolower($nama_jabatan) : ''), 0, 'L');

$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 5, '4.', 0, 0, 'L');
$pdf->Cell(50, 5, 'Unit kerja', 0, 0, 'L');
$pdf->Cell(0, 5, ': ' . $unit_kerja, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 15, '5.', 0, 0, 'L');
$pdf->Cell(50, 15, 'Gaji pokok lama', 0, 0, 'L');
$pdf->Cell(0, 15, ': ' . angka_rupiah($gapok_lama), 0, 0, 'L');
$pdf->Ln(10);
$pdf->Cell($padding + 57);
$pdf->SetFont('ArialNarrow', 'I', $font);
$pdf->MultiCell(90, 5, '(' . terbilang_rupiah($gapok_lama) . ')');
$pdf->SetFont('ArialNarrow', '', $font);

$pdf->Ln(2);
$pdf->Cell($padding + 5);
$pdf->MultiCell(165, 5, '(atas dasar surat keputusan terakhir tentang gaji/pangkat yang ditetapkan)');
$pdf->Ln(5);
$pdf->Cell($padding + 10);
$pdf->Cell(5, 0, 'a.', 0, 0, 'L');
$pdf->Cell(40, 0, 'oleh pejabat', 0, 0, 'L');
$pdf->Cell(2, 0, ': ', 0, 0, 'L');

$current_y = $pdf->GetY();
$current_x = $pdf->GetX();
$pdf->SetXY($current_x, $current_y - 2.5);

$pdf->MultiCell(70, 5, $jabatan_pejabat_lama, 0, 'L');
$pdf->Cell($padding + 10);
$pdf->Cell(5, 5, 'b.', 0, 0, 'L');
$pdf->Cell(40, 5, 'tanggal', 0, 0, 'L');
$pdf->Cell(0, 5, ': ' . $tgl_sk_lama, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding + 15);
$pdf->Cell(40, 15, 'nomor', 0, 0, 'L');
$pdf->Cell(0, 15, ': ' . $no_sk_lama, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding + 10);
$pdf->Cell(5, 25, 'c.', 0, 0, 'L');
$pdf->Cell(0, 25, 'tanggal mulai', 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding + 15);
$pdf->Cell(40, 35, 'berlakunya gaji tersebut', 0, 0, 'L');
$pdf->Cell(0, 35, ': ' . $tgl_mulai_sk_lama, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding + 10);
$pdf->Cell(5, 45, 'd.', 0, 0, 'L');
$pdf->Cell(0, 45, 'masa kerja golongan', 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding + 15);
$pdf->Cell(40, 55, 'pada tanggal tersebut', 0, 0, 'L');
$pdf->Cell(0, 55, ': ' . $mkg_sk_lama, 0, 0, 'L');


$pdf->Ln(33);
$pdf->Cell($padding);
$pdf->SetFont('ArialNarrow', 'BU', $font);
$pdf->MultiCell(165, 5, 'diberikan kenaikan gaji berkala hingga memperoleh :');
$pdf->SetFont('ArialNarrow', '', $font);
$pdf->Ln(5);
$pdf->Cell($padding);
$pdf->Cell(5, 0, '6.', 0, 0, 'L');
$pdf->Cell(50, 0, 'Gaji pokok baru', 0, 0, 'L');
$pdf->Cell(0, 0, ': ' . angka_rupiah($gapok_baru) . '', 0, 0, 'L');

$pdf->Ln(2);
$pdf->Cell($padding + 57);
$pdf->SetFont('ArialNarrow', 'I', $font);
$pdf->MultiCell(90, 5, '(' . terbilang_rupiah($gapok_baru) . ')');
$pdf->SetFont('ArialNarrow', '', $font);
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 5, '7.', 0, 0, 'L');
$pdf->Cell(50, 5, 'Berdasarkan masa kerja', 0, 0, 'L');
$pdf->Cell(0, 5, ': ' . $mkg_sk_baru, 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 15, '8.', 0, 0, 'L');
$pdf->Cell(50, 15, 'Dalam golongan ruang', 0, 0, 'L');
$pdf->Cell(0, 15, ': ' . convert_golongan($golru_baru), 0, 0, 'L');
$pdf->Ln(0);
$pdf->Cell($padding);
$pdf->Cell(5, 25, '9.', 0, 0, 'L');
$pdf->Cell(50, 25, 'Mulai tanggal', 0, 0, 'L');
$pdf->Cell(0, 25, ': ' . $tgl_mulai_sk_baru, 0, 0, 'L');


$pdf->Ln(17);
$pdf->Cell($padding);
$pdf->MultiCell(160, 5, '           Diharap agar sesuai dengan ' . $dasar_hukum . ', kepada pegawai tersebut dapat dibayarkan penghasilannya berdasarkan gaji pokoknya yang baru. ');
$pdf->Ln(5);

$pdf->SetFont('ArialNarrow', '', $font);


$kepada = explode(PHP_EOL, $spesimen);
$n = 0;
foreach ($kepada as $line) {
    $pdf->Cell(120);
    $pdf->Cell(0, $n, $line, 0, 0, 'C');
    $pdf->Ln(0);
    $n = $n + 10;
}
$nama_file = $nip . '_' . $kgb->RWKGB_TMTSKBARU . '_' . $nama . '.pdf';
$pdf->Output($nama_file, 'I');
?>
