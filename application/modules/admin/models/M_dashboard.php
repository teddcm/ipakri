<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dashboard extends CI_Model {

    public $allNIP = array();

    public function __construct() {
        parent::__construct();
//        $this->allNIP = get_nip_unor(SKPD);
    }

    function getSebaranTMT() {
        $this->db->select('COUNT(PNS_KEDHUK) as jum,YEAR(PNS_TMTCPN) as TMT');
        $this->db->group_by('PNS_TMTCPN');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    function getSebaranUsia() {
        $this->db->select('COUNT(PNS_KEDHUK) as jum,YEAR(PNS_TGLLHRDT) as tahun,YEAR(NOW())-YEAR(PNS_TGLLHRDT) as usia');
        $this->db->group_by('YEAR(PNS_TGLLHRDT)');
        $this->db->order_by('YEAR(PNS_TGLLHRDT)', 'DESC');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        }
        return $return;
    }

    function getSebaranPendidikan() {
        $this->db->select('COUNT(PNS_KEDHUK) as jum,LPAD(`PNS_TKTDIK`, 2, 0) as PNS_TKTDIK');
        $this->db->group_by('PNS_TKTDIK');
        $this->db->order_by('PNS_TKTDIK', 'ASC');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    function getSebaranGolru() {
        $this->db->select('COUNT(PNS_KEDHUK) as jum,PNS_GOLRU');
        $this->db->group_by('PNS_GOLRU');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    function getSebaranGender() {
        $this->db->select('COUNT(PNS_KEDHUK) as jum, PNS_PNSSEX');
        $this->db->group_by('PNS_PNSSEX');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    function getSebaranJenJab() {
        $this->db->select('PNS_JNSJAB, COUNT(PNS_KEDHUK) AS jum');
        $this->db->group_by('PNS_JNSJAB');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    function getJumPeg() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getJumPegStruktural() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_JNSJAB', 1);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getJumPegJFT() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_JNSJAB', 2);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getJumPegJFU() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_JNSJAB', 4);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getJumPegPria() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_PNSSEX', 1);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getJumPegWanita() {
        $this->db->select('COUNT(PNS_KEDHUK) AS jum');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        //$this->db->where_in('PNS_NIPBARU', $this->allNIP);
        $this->db->where('PNS_PNSSEX', 2);
        $this->db->where('PNS_KEDHUK <', 20);
        $query = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        return $query->row()->jum;
    }

    function getKGB() {
        $return = false;
//        $this->load->model('M_Report');
//        $data = $this->M_Report->getKGB($this->ion_auth->user()->row()->IDUNO, NULL);
//        if ($data) {
//            foreach ($data as $result) {
//                if (strtotime($result->nextKGB) < strtotime('+2 months')) {
//                    $return[] = $result;
//                }
//            }
//        }
        return $return;
    }

}