<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pengaturan extends CI_Model {

    function simpanPengaturan($data) {

        $this->db->trans_begin();
        foreach ($data as $x => $value) {
            $this->db->set('value_pengaturan', $value);
            $this->db->where('nama_pengaturan', $x);
            $this->db->update('admin_pengaturan');
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function atur_cuti($dataPost) {
        $this->db->trans_begin();
        /*if ($dataPost->jatah != getPengaturan('cuti_jatah') || $dataPost->maksimal != getPengaturan('cuti_sisa_maksimal')) {
            $this->db->where('RWCUTI_THNCUTI', date('Y'));
            $this->db->where('RWCUTI_JNS<=', '0', FALSE);
            $this->db->delete('q_datarwcuti');
            echo $this->db->last_query();

            $data = $this->generateDataCuti($dataPost->jatah, $dataPost->maksimal);
            $this->db->insert_batch('q_datarwcuti', $data);
            savePengaturan('cuti_jatah', $dataPost->jatah);
            savePengaturan('cuti_tahun', date('Y'));
            savePengaturan('cuti_sisa_maksimal', $dataPost->maksimal);
        }*/
        savePengaturan('cuti_form_kepada', $dataPost->cuti_form_kepada);
        savePengaturan('cuti_pjb_berwenang', $dataPost->cuti_pjb_berwenang);
        savePengaturan('cuti_pjb_berwenang', $dataPost->cuti_pjb_berwenang);
        //print_r($data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function generateDataCuti($jatah, $maksimal) {
        $sql = "
SELECT b.PNS_NIPBARU AS RWCUTI_NIP,IFNULL(a.RWCUTI_JUM,0) AS RWCUTI_JUM,-1 AS RWCUTI_JNS,YEAR(NOW()) AS RWCUTI_THNCUTI FROM(SELECT a.RWCUTI_NIP,SUM(a.RWCUTI_JUM) AS RWCUTI_JUM 
FROM q_datarwcuti a
WHERE a.RWCUTI_THNCUTI=" . (date('Y') - 1) . " AND a.RWCUTI_JNS<=1
GROUP BY a.RWCUTI_NIP
) a
RIGHT JOIN `kanreg8_pupns` b
ON a.RWCUTI_NIP=b.PNS_NIPBARU AND b.PNS_KEDHUK<=20
WHERE b.PNS_KEDHUK<=20
ORDER BY b.PNS_NIPBARU             
";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $d) {
                $d->RWCUTI_ID = uuid();
                $d->RWCUTI_JUM = $d->RWCUTI_JUM > $maksimal ? $maksimal : $d->RWCUTI_JUM;
                $data[] = json_decode(json_encode($d), True);

                $e = (object) array();
                $e->RWCUTI_ID = uuid();
                $e->RWCUTI_THNCUTI = date('Y');
                $e->RWCUTI_NIP = $d->RWCUTI_NIP;
                $e->RWCUTI_JUM = $jatah;
                $e->RWCUTI_JNS = 0;
                $data[] = json_decode(json_encode($e), True);
            }
            return $data;
        } else {
            return NULL;
        }
    }

}
