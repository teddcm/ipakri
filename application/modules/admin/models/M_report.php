<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report extends CI_Model {

    function getJumPeg($where_col = NULL, $where_val = NULL) {
        $this->db->where('PNS_KEDHUK <', 20);
        if ($where_col != NULL && $where_val != NULL) {
            $this->db->where($where_col, $where_val);
        }
        $query = $this->db->get('kanreg8_pupns');
        return $query->num_rows();
    }

    function getDukNew($FULLDATA, $UNOR) {
        $nipunor = implode(',', get_nip_unor($UNOR));
        // DATA UTAMA
        $this->db->order_by('PNS_GOLRU', 'DESC'); //PANGKAT
        $this->db->order_by('PNS_TMTGOL', 'ASC'); //TMT PANGKAT
        $this->db->order_by('RWJAB_IDESL', 'ASC'); //ESELON JAB
        $this->db->order_by('RWJAB_TMTJAB', 'ASC'); //TMT JAB
        $this->db->order_by('PNS_THNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('PNS_BLNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('RWDLT_IDDLT', 'DESC'); //PELATIHAN
        $this->db->order_by('PNS_TKTDIK', 'DESC'); //PENDIDIKAN
        $this->db->order_by('PNS_TGLLHR', 'DESC'); //UMUR
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->select('PNS_PNSNAM,PNS_PNSNIP,PNS_NIPBARU,PNS_GLRDPN,PNS_GLRBLK,PNS_THNKER,PNS_BLNKER,PNS_TGLLHRDT,PNS_TEMLHR,PNS_TMTCPN,PNS_PNSSEX,'
                . '(SELECT COUNT(*) FROM z_datarwgolongan t WHERE t.RWGOL_NIP = PNS_NIPBARU) AS GOLCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwjabatan t WHERE t.RWJAB_NIP = PNS_NIPBARU) AS JABCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwdiklat t WHERE t.RWDLT_NIP = PNS_NIPBARU) AS DLTCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwpendidikan t WHERE t.RWDIK_NIP = PNS_NIPBARU) AS DIKCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwpnsunor t WHERE t.RWUNOR_NIP = PNS_NIPBARU) AS UNORCOUNTER'
                . '');
        $this->db->join('(SELECT MAX(RWJAB_TMTJAB) AS RWJAB_TMTJAB,RWJAB_IDESL,RWJAB_NIP FROM z_datarwjabatan GROUP BY RWJAB_NIP) b', 'a.PNS_NIPBARU=b.RWJAB_NIP', 'left');
        $this->db->join('(SELECT MAX(RWDLT_IDDLT) AS RWDLT_IDDLT,RWDLT_NIP FROM z_datarwdiklat GROUP BY RWDLT_NIP) c', 'a.PNS_NIPBARU=c.RWDLT_NIP', 'left');
        $query0 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        $a1 = array();
        $a2 = array();
        $a3 = array();
        $a4 = array();
        $a5 = array();
// GOLONGAN
        $this->db->order_by('RWGOL_TMTGOL', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->join('z_datarwgolongan b', 'a.PNS_NIPBARU=b.RWGOL_NIP', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWGOL_KDGOL,b.RWGOL_TMTGOL,'
                . '(SELECT COUNT(*) FROM z_datarwgolongan t WHERE t.RWGOL_NIP = a.PNS_NIPBARU) AS GOLCOUNTER'
                . '');
        $query1 = $this->db->get('kanreg8_pupns a');
        if ($query1->num_rows() > 0) {
            foreach ($query1->result_array() as $a1x) {
                $a1[] = $a1x;
            }
        }

// JABATAN
        $this->db->order_by('RWJAB_TGLSK', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->join('z_datarwjabatan b', 'a.PNS_NIPBARU=b.RWJAB_NIP', 'left');
        $this->db->join('kanreg8_eselon c', 'c.ESE_KODESL=b.RWJAB_IDESL', 'left');
        $this->db->select('a.PNS_NIPBARU,RWJAB_NAMAJAB,ESE_NAMESL,RWJAB_TMTJAB,RWJAB_IDJENJAB');
        $query2 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result_array() as $a2x) {
                if ($a2x['RWJAB_IDJENJAB'] != 1) {
                    $a2x['ESE_NAMESL'] = '';
                }
                $a2[] = $a2x;
            }
        }

//DIKLAT   
        $this->db->order_by('RWDLT_TGL', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->join('z_datarwdiklat b', 'a.PNS_NIPBARU=b.RWDLT_NIP', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWDLT_NAMDLT,b.RWDLT_THN');
        $query3 = $this->db->get('kanreg8_pupns a');
        if ($query3->num_rows() > 0) {
            foreach ($query3->result_array() as $a3x) {
                $a3[] = $a3x;
            }
        }

//PENDIDIKAN   
        $this->db->order_by('RWDIK_THNLULUS', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->join('z_datarwpendidikan b', 'a.PNS_NIPBARU=b.RWDIK_NIP', 'left');
        $this->db->join('kanreg8_tktpendik c', 'c.DIK_TKTDIK=b.RWDIK_TKTDIK', 'left');
        $this->db->join('kanreg8_pendik d', 'd.DIK_KODIK=b.RWDIK_IDDIK', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWDIK_NAMSKLH,b.RWDIK_THNLULUS,c.DIK_NAMDIK,d.DIK_NMDIK');
        $query4 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query4->num_rows() > 0) {
            foreach ($query4->result_array() as $a4x) {
                $a4[] = $a4x;
            }
        }

//UNOR   
        $this->db->order_by('b.RWUNOR_TGLSK', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->join('z_datarwpnsunor b', 'a.PNS_NIPBARU=b.RWUNOR_NIP', 'left');
        $this->db->join('kanreg8_unor c', 'b.RWUNOR_IDUNORBARU=c.UNO_ID');
        $this->db->select('a.PNS_NIPBARU,c.UNO_NAMUNO,b.RWUNOR_TGLSK');
        $query5 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query5->num_rows() > 0) {
            foreach ($query5->result_array() as $a5x) {
                $a5[] = $a5x;
            }
        }

        $return = array();
        $no = 0;
        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $all = $a0;
                $all->CLASS = 1;

                $glrDpn = $all->PNS_GLRDPN == '' ? '' : ($all->PNS_GLRDPN . ' ');
                $glrBlk = $all->PNS_GLRBLK == '' ? '' : (' ' . $all->PNS_GLRBLK);
                unset($all->PNS_GLRDPN);
                unset($all->PNS_GLRBLK);
                $all->PNS_PNSNAM = $glrDpn . ucwords(strtolower($all->PNS_PNSNAM)) . $glrBlk;

                $all->PNS_TGLLHRDT = ($all->PNS_TGLLHRDT != NULL) ? reverseDate($all->PNS_TGLLHRDT) : '';
                $NIP = $all->PNS_NIPBARU;
                //$all->PNS_NIPBARU = split_nip($all->PNS_NIPBARU);

                $date1 = new DateTime($all->PNS_TMTCPN);
                $date2 = new DateTime(date("Y-m-d"));
                $interval = date_diff($date1, $date2);

                $all->PNS_THNKER = $interval->y;
                $all->PNS_BLNKER = $interval->m;

                $no++;
                $all->NO = $no;
                $temp = $all->PNS_NIPBARU;
                $maxrow = max(ARRAY($a0->GOLCOUNTER, $a0->JABCOUNTER, $a0->DLTCOUNTER, $a0->DIKCOUNTER, $a0->UNORCOUNTER,));
                $all->MAXROW = $maxrow;
                $GOLCOUNTER = $a0->GOLCOUNTER;
                $JABCOUNTER = $a0->JABCOUNTER;
                $DLTCOUNTER = $a0->DLTCOUNTER;
                $DIKCOUNTER = $a0->DIKCOUNTER;
                $UNORCOUNTER = $a0->UNORCOUNTER;
                unset($a0->GOLCOUNTER);
                unset($a0->JABCOUNTER);
                unset($a0->DLTCOUNTER);
                unset($a0->DIKCOUNTER);
                unset($a0->UNORCOUNTER);
                unset($a0->PNS_TMTCPN);
                if ($maxrow > 0) {
                    while ($maxrow > 0) {
                        if ($GOLCOUNTER > 0) {
                            $key = searchForId($temp, $a1, 'PNS_NIPBARU');
                            $all->RWGOL_KDGOL = @convert_golongan($a1[$key]['RWGOL_KDGOL']);
                            $all->RWGOL_TMTGOL = @reverseDate($a1[$key]['RWGOL_TMTGOL']);
                            unset($a1[$key]);
                            $GOLCOUNTER--;
                        }
                        if ($JABCOUNTER > 0) {
                            $key = searchForId($temp, $a2, 'PNS_NIPBARU');
                            $all->RWJAB_TMTJAB = @reverseDate($a2[$key]['RWJAB_TMTJAB']);
                            $all->RWJAB_IDESL = ($a2[$key]['ESE_NAMESL']);
                            $all->RWJAB_NAMAJAB = @$a2[$key]['RWJAB_NAMAJAB'];
                            unset($a2[$key]);
                            $JABCOUNTER--;
                        }
                        if ($DLTCOUNTER > 0) {
                            $key = searchForId($temp, $a3, 'PNS_NIPBARU');
                            $all->RWDLT_THN = @$a3[$key]['RWDLT_THN'];
                            $all->RWDLT_NAMDLT = @$a3[$key]['RWDLT_NAMDLT'];
                            unset($a3[$key]);
                            $DLTCOUNTER--;
                        }
                        if ($DIKCOUNTER > 0) {
                            $key = searchForId($temp, $a4, 'PNS_NIPBARU');
                            $all->RWDIK_THNLULUS = @$a4[$key]['RWDIK_THNLULUS'];
                            $all->RWDIK_NAMSKLH = @$a4[$key]['RWDIK_NAMSKLH'];
                            $all->RWDIK_TKTDIK = @$a4[$key]['DIK_NAMDIK'];
                            $all->RWDIK_IDDIK = @$a4[$key]['DIK_NMDIK'];
                            unset($a4[$key]);
                            $DIKCOUNTER--;
                        }
                        if ($UNORCOUNTER > 0) {
                            $key = searchForId($temp, $a5, 'PNS_NIPBARU');
                            $all->RWUNOR_TGLSK = @reverseDate($a5[$key]['RWUNOR_TGLSK']);
                            $all->UNO_NAMUNO = @$a5[$key]['UNO_NAMUNO'];
                            unset($a5[$key]);
                            $UNORCOUNTER--;
                        }
                        if ($FULLDATA) {
                            $return[] = $all;
                            $all = new stdClass;
                        }
                        $maxrow--;
                    }
                    if (!$FULLDATA) {
                        $return[] = $all;
                        $all = new stdClass;
                    }
                } else {
                    $return[] = $all;
                    $all = new stdClass;
                }
            }
        }

        return ($return);
    }

    function getNominatifNew($FULLDATA, $UNOR) {
        // DATA UTAMA
        $this->db->order_by('PNS_GOLRU', 'DESC'); //PANGKAT
        //$this->db->order_by('RWJAB_IDESL', 'ASC');
        $this->db->order_by('PNS_THNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('PNS_BLNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('RWDLT_IDDLT', 'DESC'); //PELATIHAN
        $this->db->order_by('PNS_TKTDIK', 'DESC'); //PENDIDIKAN
        $this->db->order_by('PNS_TGLLHR', 'DESC'); //UMUR
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->select('PNS_PNSNAM,PNS_PNSNIP,PNS_NIPBARU,PNS_GLRDPN,PNS_GLRBLK,PNS_THNKER,PNS_BLNKER,PNS_TGLLHRDT,PNS_TEMLHR,PNS_TMTCPN,PNS_PNSSEX,'
                . '(SELECT COUNT(*) FROM z_datarwgolongan t WHERE t.RWGOL_NIP = PNS_NIPBARU) AS GOLCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwjabatan t WHERE t.RWJAB_NIP = PNS_NIPBARU) AS JABCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwdiklat t WHERE t.RWDLT_NIP = PNS_NIPBARU) AS DLTCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwpendidikan t WHERE t.RWDIK_NIP = PNS_NIPBARU) AS DIKCOUNTER,'
                . '(SELECT COUNT(*) FROM z_datarwpnsunor t WHERE t.RWUNOR_NIP = PNS_NIPBARU) AS UNORCOUNTER'
                . '');
        $this->db->join('(SELECT MAX(RWJAB_TMTJAB) AS RWJAB_TMTJAB,RWJAB_IDESL,RWJAB_NIP FROM z_datarwjabatan GROUP BY RWJAB_NIP) b', 'a.PNS_NIPBARU=b.RWJAB_NIP', 'left');
        $this->db->join('(SELECT MAX(RWDLT_IDDLT) AS RWDLT_IDDLT,RWDLT_NIP FROM z_datarwdiklat GROUP BY RWDLT_NIP) c', 'a.PNS_NIPBARU=c.RWDLT_NIP', 'left');
        $query0 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        $a1 = array();
        $a2 = array();
        $a3 = array();
        $a4 = array();
        $a5 = array();
// GOLONGAN
        $this->db->order_by('RWGOL_TMTGOL', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->join('z_datarwgolongan b', 'a.PNS_NIPBARU=b.RWGOL_NIP', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWGOL_KDGOL,b.RWGOL_TMTGOL,'
                . '(SELECT COUNT(*) FROM z_datarwgolongan t WHERE t.RWGOL_NIP = a.PNS_NIPBARU) AS GOLCOUNTER'
                . '');
        $query1 = $this->db->get('kanreg8_pupns a');
        if ($query1->num_rows() > 0) {
            foreach ($query1->result_array() as $a1x) {
                $a1[] = $a1x;
            }
        }

// JABATAN
        $this->db->order_by('RWJAB_TGLSK', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->join('z_datarwjabatan b', 'a.PNS_NIPBARU=b.RWJAB_NIP', 'left');
        $this->db->join('kanreg8_eselon c', 'c.ESE_KODESL=b.RWJAB_IDESL', 'left');
        $this->db->select('a.PNS_NIPBARU,RWJAB_NAMAJAB,ESE_NAMESL,RWJAB_TMTJAB');
        $query2 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result_array() as $a2x) {
                $a2[] = $a2x;
            }
        }

//DIKLAT   
        $this->db->order_by('RWDLT_TGL', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->join('z_datarwdiklat b', 'a.PNS_NIPBARU=b.RWDLT_NIP', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWDLT_NAMDLT,b.RWDLT_THN');
        $query3 = $this->db->get('kanreg8_pupns a');
        if ($query3->num_rows() > 0) {
            foreach ($query3->result_array() as $a3x) {
                $a3[] = $a3x;
            }
        }

//PENDIDIKAN   
        $this->db->order_by('RWDIK_THNLULUS', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->join('z_datarwpendidikan b', 'a.PNS_NIPBARU=b.RWDIK_NIP', 'left');
        $this->db->join('kanreg8_tktpendik c', 'c.DIK_TKTDIK=b.RWDIK_TKTDIK', 'left');
        $this->db->join('kanreg8_pendik d', 'd.DIK_KODIK=b.RWDIK_IDDIK', 'left');
        $this->db->select('a.PNS_NIPBARU,b.RWDIK_NAMSKLH,b.RWDIK_THNLULUS,c.DIK_NAMDIK,d.DIK_NMDIK');
        $query4 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query4->num_rows() > 0) {
            foreach ($query4->result_array() as $a4x) {
                $a4[] = $a4x;
            }
        }

//UNOR   
        $this->db->order_by('b.RWUNOR_TGLSK', 'ASC');
        $this->db->where('PNS_KEDHUK <', 20);
        $this->db->where('PNS_UNOR', $UNOR);
        $this->db->join('z_datarwpnsunor b', 'a.PNS_NIPBARU=b.RWUNOR_NIP', 'left');
        $this->db->join('kanreg8_unor c', 'b.RWUNOR_IDUNORBARU=c.UNO_ID');
        $this->db->select('a.PNS_NIPBARU,c.UNO_NAMUNO,b.RWUNOR_TGLSK');
        $query5 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();
        if ($query5->num_rows() > 0) {
            foreach ($query5->result_array() as $a5x) {
                $a5[] = $a5x;
            }
        }

        $return = array();
        $no = 0;
        $all = new stdClass;
        $all->CLASS = 99;
        $all->UNOR = convert_unor($UNOR);
        $all->ESL = substr(convert_unor($UNOR, 'UNO_KODESL'), 0, 1);
        $return[] = $all;

        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $all = $a0;
                $all->CLASS = 1;

                $glrDpn = $all->PNS_GLRDPN == '' ? '' : ($all->PNS_GLRDPN . ' ');
                $glrBlk = $all->PNS_GLRBLK == '' ? '' : (' ' . $all->PNS_GLRBLK);
                unset($all->PNS_GLRDPN);
                unset($all->PNS_GLRBLK);
                $all->PNS_PNSNAM = $glrDpn . ucwords(strtolower($all->PNS_PNSNAM)) . $glrBlk;

                $all->PNS_TGLLHRDT = ($all->PNS_TGLLHRDT != NULL) ? reverseDate($all->PNS_TGLLHRDT) : '';
                $NIP = $all->PNS_NIPBARU;
                //$all->PNS_NIPBARU = split_nip($all->PNS_NIPBARU);

                $date1 = new DateTime($all->PNS_TMTCPN);
                $date2 = new DateTime(date("Y-m-d"));
                $interval = date_diff($date1, $date2);

                $all->PNS_THNKER = $interval->y;
                $all->PNS_BLNKER = $interval->m;

                $no++;
                $all->NO = $no;
                $temp = $all->PNS_NIPBARU;
                $maxrow = max(ARRAY($a0->GOLCOUNTER, $a0->JABCOUNTER, $a0->DLTCOUNTER, $a0->DIKCOUNTER, $a0->UNORCOUNTER,));
                $all->MAXROW = $maxrow;
                $GOLCOUNTER = $a0->GOLCOUNTER;
                $JABCOUNTER = $a0->JABCOUNTER;
                $DLTCOUNTER = $a0->DLTCOUNTER;
                $DIKCOUNTER = $a0->DIKCOUNTER;
                $UNORCOUNTER = $a0->UNORCOUNTER;
                unset($a0->GOLCOUNTER);
                unset($a0->JABCOUNTER);
                unset($a0->DLTCOUNTER);
                unset($a0->DIKCOUNTER);
                unset($a0->UNORCOUNTER);
                unset($a0->PNS_TMTCPN);
                if ($maxrow > 0) {
                    while ($maxrow > 0) {
                        if ($GOLCOUNTER > 0) {
                            $key = searchForId($temp, $a1, 'PNS_NIPBARU');
                            $all->RWGOL_KDGOL = @convert_golongan($a1[$key]['RWGOL_KDGOL']);
                            $all->RWGOL_TMTGOL = @reverseDate($a1[$key]['RWGOL_TMTGOL']);
                            unset($a1[$key]);
                            $GOLCOUNTER--;
                        }
                        if ($JABCOUNTER > 0) {
                            $key = searchForId($temp, $a2, 'PNS_NIPBARU');
                            $all->RWJAB_TMTJAB = @reverseDate($a2[$key]['RWJAB_TMTJAB']);
                            $all->RWJAB_IDESL = ($a2[$key]['ESE_NAMESL']);
                            $all->RWJAB_NAMAJAB = @$a2[$key]['RWJAB_NAMAJAB'];
                            unset($a2[$key]);
                            $JABCOUNTER--;
                        }
                        if ($DLTCOUNTER > 0) {
                            $key = searchForId($temp, $a3, 'PNS_NIPBARU');
                            $all->RWDLT_THN = @$a3[$key]['RWDLT_THN'];
                            $all->RWDLT_NAMDLT = @$a3[$key]['RWDLT_NAMDLT'];
                            unset($a3[$key]);
                            $DLTCOUNTER--;
                        }
                        if ($DIKCOUNTER > 0) {
                            $key = searchForId($temp, $a4, 'PNS_NIPBARU');
                            $all->RWDIK_THNLULUS = @$a4[$key]['RWDIK_THNLULUS'];
                            $all->RWDIK_NAMSKLH = @$a4[$key]['RWDIK_NAMSKLH'];
                            $all->RWDIK_TKTDIK = @$a4[$key]['DIK_NAMDIK'];
                            $all->RWDIK_IDDIK = @$a4[$key]['DIK_NMDIK'];
                            unset($a4[$key]);
                            $DIKCOUNTER--;
                        }
                        if ($UNORCOUNTER > 0) {
                            $key = searchForId($temp, $a5, 'PNS_NIPBARU');
                            $all->RWUNOR_TGLSK = @reverseDate($a5[$key]['RWUNOR_TGLSK']);
                            $all->UNO_NAMUNO = @$a5[$key]['UNO_NAMUNO'];
                            unset($a5[$key]);
                            $UNORCOUNTER--;
                        }
                        if ($FULLDATA) {
                            $return[] = $all;
                            $all = new stdClass;
                        }
                        $maxrow--;
                    }
                    if (!$FULLDATA) {
                        $return[] = $all;
                        $all = new stdClass;
                    }
                } else {
                    $return[] = $all;
                    $all = new stdClass;
                }
            }
        }
        $this->db->trans_begin();
        $sql2 = "SELECT UNO_NAMUNO,UNO_ID"
                . " FROM kanreg8_unor where UNO_DIATASAN_ID='" . $UNOR . "' AND UNO_NAMUNO NOT LIKE '%###DELETED###%'  order by UNO_KODUNO,UNO_KODESL ";
        $query2 = $this->db->query($sql2);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        //echo $this->db->last_query();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result() as $q) {
                $return = array_merge($return, $this->getNominatifNew($FULLDATA, $q->UNO_ID));
            }
        }

        return ($return);
    }

    function getJabatanNew($JNSJAB, $UNOR) {
        $getnipunor = get_nip_unor($UNOR);
        $nipunor = implode(',', $getnipunor);
        // DATA UTAMA
        $this->db->order_by('PNS_GOLRU', 'DESC'); //PANGKAT
        //$this->db->order_by('RWJAB_IDESL', 'ASC');
        $this->db->order_by('PNS_THNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('PNS_BLNKER', 'DESC'); //MASA KERJA
        $this->db->order_by('RWDLT_IDDLT', 'DESC'); //PELATIHAN
        $this->db->order_by('PNS_TKTDIK', 'DESC'); //PENDIDIKAN
        $this->db->order_by('PNS_TGLLHR', 'DESC'); //UMUR
        $this->db->where('PNS_KEDHUK <', 20);
        if ($UNOR != NULL) {
            $this->db->where_in('PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->where('PNS_JNSJAB', $JNSJAB);
        $this->db->select('PNS_PNSNAM,PNS_PNSNIP,PNS_NIPBARU,PNS_GLRDPN,PNS_GLRBLK,'
                . 'PNS_THNKER,PNS_BLNKER,PNS_TGLLHRDT,PNS_TEMLHR,PNS_TMTCPN,'
                . 'PNS_GOLRU,PNS_TMTGOL,PNS_PNSSEX,RWJAB_NAMAJAB,RWJAB_IDESL,RWJAB_TMTJAB'
                . '');
        $this->db->join('(SELECT MAX(RWJAB_TMTJAB) AS RWJAB_TMTJAB,RWJAB_IDESL,RWJAB_NAMAJAB,RWJAB_NIP FROM z_datarwjabatan GROUP BY RWJAB_NIP) b', 'a.PNS_NIPBARU=b.RWJAB_NIP', 'left');
        $this->db->join('(SELECT MAX(RWDLT_IDDLT) AS RWDLT_IDDLT,RWDLT_NIP FROM z_datarwdiklat GROUP BY RWDLT_NIP) c', 'a.PNS_NIPBARU=c.RWDLT_NIP', 'left');
        $query0 = $this->db->get('kanreg8_pupns a');
        //echo $this->db->last_query();


        $return = array();
        $no = 0;
        $all = new stdClass;

        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $all = $a0;

                $glrDpn = $all->PNS_GLRDPN == '' ? '' : ($all->PNS_GLRDPN . ' ');
                $glrBlk = $all->PNS_GLRBLK == '' ? '' : (' ' . $all->PNS_GLRBLK);
                unset($all->PNS_GLRDPN);
                unset($all->PNS_GLRBLK);
                $all->PNS_PNSNAM = $glrDpn . ucwords(strtolower($all->PNS_PNSNAM)) . $glrBlk;

                $date1 = new DateTime($all->PNS_TMTCPN);
                $date2 = new DateTime(date("Y-m-d"));
                $interval = date_diff($date1, $date2);

                $all->PNS_THNKER = $interval->y;
                $all->PNS_BLNKER = $interval->m;

                $all->PNS_GOLRU = convert_golongan($all->PNS_GOLRU);
                $all->PNS_TMTGOL = ($all->PNS_TMTGOL != NULL) ? reverseDate($all->PNS_TMTGOL) : '';
                $all->PNS_TGLLHRDT = ($all->PNS_TGLLHRDT != NULL) ? reverseDate($all->PNS_TGLLHRDT) : '';

                $all->RWJAB_TMTJAB = ($all->RWJAB_TMTJAB != NULL) ? reverseDate($all->RWJAB_TMTJAB) : '';
                $all->RWJAB_IDESL = convert_eselon($all->RWJAB_IDESL);
                $NIP = $all->PNS_NIPBARU;
                //$all->PNS_NIPBARU = split_nip($all->PNS_NIPBARU);


                $return[] = $all;
                $all = new stdClass;
            }
        }

        return ($return);
    }

    function getKGB($UNOR = NULL, $NIP = NULL) {
        if ($UNOR != NULL) {
            $getnipunor = get_nip_unor($UNOR);
            $getnipunor[] = '010101010101010101';
            $nipunor = implode(',', $getnipunor);
        }
        $whereUNOR = $UNOR == NULL ? '' : ' AND PNS_NIPBARU IN (' . $nipunor . ')';
        $whereNIP = $NIP == NULL ? '' : ' AND PNS_NIPBARU="' . $NIP . '"';
        $q = "
SELECT 
a.PNS_PNSNIP,
a.PNS_NIPBARU,
a.PNS_GLRDPN,
a.PNS_GLRBLK,
a.PNS_PNSSEX,
a.PNS_GOLRU,
a.PNS_PNSNAM,
a.PNS_TMTCPN,
a.PNS_GOLAWL,
a.PNS_GOLRU,
IFNULL(b.RWPMK_THNKRJ,0) AS RWPMK_THNKRJ,
IFNULL(b.RWPMK_BLNKRJ,0) AS RWPMK_BLNKRJ,
@PMK :=IFNULL(b.bulan,0) AS PMK, /*penambahan masa kerja*/
@MKS :=TIMESTAMPDIFF(MONTH, PNS_TMTCPN, NOW()) AS MKS,  /*masa kerja seluruhnya = TMT CPNS - Sekarang*/
@MKFpls :=IF(a.PNS_GOLAWL IN (12,13,14,22,23,24),3*12,0) AS MKFpls,  /*Penambahan Masa Kerja */
@MKFmin :=  /*Pengurangan Masa Kerja */
IF(SUBSTRING(a.PNS_GOLAWL,1,1)=1 AND SUBSTRING(a.PNS_GOLRU,1,1)=2,-6*12,
IF(SUBSTRING(a.PNS_GOLAWL,1,1)=1 AND SUBSTRING(a.PNS_GOLRU,1,1) IN (3,4),-11*12,
IF(SUBSTRING(a.PNS_GOLAWL,1,1)=2 AND SUBSTRING(a.PNS_GOLRU,1,1) IN (3,4),-5*12,0))) AS MKFmin,

@MKG :=@PMK+@MKS+@MKFpls+@MKFmin AS MKG, /*Masa Kerja Golongan */
FLOOR(@MKG/12) AS MKGthn,
@MKG%12 AS MKGbln,
@Mrmn:= 
/* 24-(((@MKS+@PMK))%24) Month Remain -> Sisa Bulan untuk KGB*/
(((IF(SUBSTRING(a.PNS_GOLRU,1,2)=21 AND @MKG<=12,12-@MKG,IF(SUBSTRING(a.PNS_GOLRU,1,2) IN (12,13,14,21,22,23,24),24-((@MKG-12)%24),24-(@MKG%24)))+23)%24)+1) AS Mrmn,
@nextMonth:=DATE(NOW()+INTERVAL @Mrmn MONTH) nextMonth,
FLOOR((@MKG+@mrmn)/12) AS KGBthn,
CONCAT(YEAR(@nextMonth),'-',LPAD(MONTH(@nextMonth), 2, '0'),'-01') AS nextKGB,
IF(c.RWKGB_JABPJBBARU<>NULL,1,0) AS isPrint,
IF(YEAR(c.RWKGB_TMTSKBARU)=YEAR(@nextMonth),1,0) AS isDone


FROM kanreg8_pupns a
LEFT JOIN (SELECT RWPMK_NIP,RWPMK_BLNKRJ,RWPMK_THNKRJ,SUM(RWPMK_BLNKRJ+(RWPMK_THNKRJ*12)) AS bulan FROM z_datarwpmk GROUP BY RWPMK_NIP) b
ON a.PNS_NIPBARU=b.RWPMK_NIP
LEFT JOIN (SELECT RWKGB_JABPJBBARU,RWKGB_NIP, MAX(RWKGB_TMTSKBARU) AS RWKGB_TMTSKBARU FROM z_datarwkgb GROUP BY RWKGB_NIP) c
ON a.PNS_NIPBARU=c.RWKGB_NIP
WHERE a.PNS_KEDHUK<20 " . $whereNIP . " " . $whereUNOR . "
ORDER BY nextKGB ASC,a.PNS_GOLRU DESC,a.PNS_NIPBARU ASC";
        /*
          SELECT a.PNS_PNSNIP, a.PNS_NIPBARU, a.PNS_GLRDPN, a.PNS_GLRBLK, a.PNS_PNSSEX, a.PNS_GOLRU, a.PNS_PNSNAM, a.PNS_TMTCPN, a.PNS_GOLAWL, a.PNS_GOLRU,
          IFNULL(b.RWPMK_THNKRJ,0) AS RWPMK_THNKRJ,
          IFNULL(b.RWPMK_BLNKRJ,0) AS RWPMK_BLNKRJ,
          @PMK :=IFNULL(b.bulan,0) AS PMK, -- penambahan masa kerja
          @MKS :=TIMESTAMPDIFF(MONTH,
          PNS_TMTCPN, NOW()) AS MKS, -- masa kerja seluruhnya = TMT CPNS - Sekarang
          @MKFpls :=IF(a.PNS_GOLAWL IN (12,13,14,22,23,24),3*12,0) AS MKFpls, -- Penambahan Masa Kerja
          @MKFmin := -- Pengurangan Masa Kerja
          IF(SUBSTRING(a.PNS_GOLAWL,1,1)=1 AND
          SUBSTRING(a.PNS_GOLRU,1,1)=2,-6*12,
          IF(SUBSTRING(a.PNS_GOLAWL,1,1)=1 AND SUBSTRING(a.PNS_GOLRU,1,1) IN (3,4),-11*12,
          IF(SUBSTRING(a.PNS_GOLAWL,1,1)=2 AND SUBSTRING(a.PNS_GOLRU,1,1) IN (3,4),-5*12,0))) AS MKFmin,

          @MKG :=@PMK+@MKS+@MKFpls+@MKFmin AS MKG, -- Masa Kerja Golongan
          FLOOR(@MKG/12) AS MKGthn, @MKG%12 AS MKGbln, @Mrmn:= --  24-(((@MKS+@PMK))%24) Month Remain -> Sisa Bulan untuk KGB
          (((IF(SUBSTRING(a.PNS_GOLRU,1,2)=21 AND @MKG<=12,12-@MKG,IF(SUBSTRING(a.PNS_GOLRU,1,2) IN (12,13,14,21,22,23,24),24-((@MKG-12)%24),24-(@MKG%24)))+23)%24)+1) AS Mrmn,
          @nextMonth:=DATE(NOW()+INTERVAL @Mrmn MONTH) nextMonth,
          FLOOR((@MKG+@mrmn)/12) AS KGBthn,
          CONCAT(YEAR(@nextMonth),'-',LPAD(MONTH(@nextMonth), 2, '0'),'-01') AS nextKGB,
          IF(YEAR(c.RWKGB_TMTSKBARU)=YEAR(@nextMonth),1,0) AS isDone,
          c.RWKGB_TMTSKBARU
          FROM kanreg8_pupns a
          LEFT JOIN (SELECT RWPMK_NIP,RWPMK_BLNKRJ,RWPMK_THNKRJ,SUM(RWPMK_BLNKRJ+(RWPMK_THNKRJ*12)) AS bulan FROM z_datarwpmk GROUP BY RWPMK_NIP) b
          ON a.PNS_NIPBARU=b.RWPMK_NIP
          LEFT JOIN (SELECT RWKGB_NIP, MAX(RWKGB_TMTSKBARU) AS RWKGB_TMTSKBARU FROM z_datarwkgb GROUP BY RWKGB_NIP) c
          ON a.PNS_NIPBARU=c.RWKGB_NIP
          WHERE a.PNS_KEDHUK<20
          ORDER BY nextKGB ASC,a.PNS_GOLRU DESC
         */
        $query = $this->db->query($q);
        //echo $this->db->last_query();
        $xy = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $xy = $xy + 1;
                $result->NO = $xy;
                $maxrow[] = 1;
                $NIP = $result->PNS_NIPBARU;
                $glrDpn = $result->PNS_GLRDPN == '' ? '' : ($result->PNS_GLRDPN . ' ');
                $glrBlk = $result->PNS_GLRBLK == '' ? '' : (' ' . $result->PNS_GLRBLK);
                unset($result->PNS_GLRDPN);
                unset($result->PNS_GLRBLK);
                $result->PNS_PNSNAM = $glrDpn . ucwords(strtolower($result->PNS_PNSNAM)) . $glrBlk;
                $result->PNS_NIPBARU = $result->PNS_NIPBARU;
                $result->PNS_PANGKAT = convert_pangkat($result->PNS_GOLRU);
                $result->PNS_GOLRU = convert_golongan($result->PNS_GOLRU);
                $result->nextKGB = $result->nextKGB != NULL ? reverseDate($result->nextKGB) : '-';
                $result->KGBthn = $result->KGBthn != NULL ? $result->KGBthn : 0;
                $result->PNS_TMTCPN = ($result->PNS_TMTCPN != NULL) ? reverseDate($result->PNS_TMTCPN) : '';
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function getCuti($TAHUN, $UNOR) {
        $getnipunor = get_nip_unor($UNOR);
        $nipunor = implode(',', $getnipunor);
        // DATA UTAMA

        $this->db->order_by('b.PNS_PNSNAM', 'ASC');
        $this->db->group_by('a.RWCUTI_NIP');
        $this->db->where('b.PNS_KEDHUK <', 20);
        $this->db->where('a.RWCUTI_THNCUTI', $TAHUN);
        if ($UNOR != NULL) {
            $this->db->where_in('b.PNS_NIPBARU', $nipunor, FALSE);
        }
        $this->db->select(''
                . 'b.PNS_NIPBARU,b.PNS_PNSNAM,b.PNS_PNSNIP,'
                . 'SUM(IF(a.RWCUTI_JNS=-1,a.RWCUTI_JUM,0)) AS sisa_tahun_lalu, '
                . 'SUM(IF(a.RWCUTI_JNS=0,a.RWCUTI_JUM,0)) AS jatah_tahun_ini,'
                . 'SUM(IF(a.RWCUTI_JNS=1,a.RWCUTI_JUM,0)) AS cuti_tertanggung,'
                . 'SUM(IF(a.RWCUTI_JNS>1,a.RWCUTI_JUM,0)) AS cuti_tak_tertanggung,'
                . 'SUM(IF(a.RWCUTI_JNS<=1,a.RWCUTI_JUM,0)) AS sisa_cuti_tertanggung');
        $this->db->join('q_datarwcutinew a', 'b.PNS_NIPBARU=a.RWCUTI_NIP', 'left');
        $query0 = $this->db->get('kanreg8_pupns b');
        //echo $this->db->last_query();


        $return = array();
        $no = 0;
        $all = new stdClass;

        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $no++;
                $all = $a0;
                $all->NO = $no;

                $return[] = $all;
                $all = new stdClass;
            }
        }

        return ($return);
    }

    function getPensiun($TAHUN, $UNOR) {
        $getnipunor = get_nip_unor($UNOR);
        $nipunor = implode(',', $getnipunor);
        // DATA UTAMA
        $this->db->order_by('`a.tanggal_pensiun`', 'ASC');
        if ($UNOR != NULL) {
            $this->db->where_in('a.PNS_NIPBARU', $nipunor, FALSE);
        }
        //$this->db->join('q_datarwcuti a', 'b.PNS_NIPBARU=a.RWCUTI_NIP', 'left');
        $query0 = $this->db->get('(SELECT '
                . '@usia_pensiun := IF(a.PNS_JNSJAB=2,'
                . 'CASE '
                . 'WHEN (a.PNS_GOLRU >43 AND a.PNS_GOLRU <=45) THEN 65 '
                . 'WHEN (a.PNS_GOLRU >34 AND a.PNS_GOLRU <=43) THEN 60 '
                . 'WHEN (a.PNS_GOLRU >00 AND a.PNS_GOLRU <=34) THEN 58 '
                . 'END,IF(a.PNS_JNSJAB=1,'
                . 'CASE '
                . 'WHEN (a.PNS_KODESL)<=21 THEN 60 '
                . 'WHEN (a.PNS_KODESL)>21 THEN 58 '
                . 'END,'
                . 'IF(a.PNS_JNSJAB=4,58,""))) AS usia_pensiun,'
                . 'DATE_FORMAT(DATE_ADD(a.PNS_TGLLHRDT, INTERVAL (@usia_pensiun*12+1) MONTH), "%Y-%m-01") as tanggal_pensiun, '
                . 'a.PNS_NIPBARU,a.PNS_PNSSEX,a.PNS_PNSNAM,a.PNS_TGLLHRDT,a.PNS_PNSNIP,a.PNS_GOLRU,a.PNS_JNSJAB,a.PNS_KODESL '
                . 'FROM kanreg8_pupns a '
                . 'WHERE a.PNS_KEDHUK<20'
                . ') a');
        //echo $this->db->last_query();

        $return = array();
        $no = 0;
        $all = new stdClass;

        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $no++;
                $all = $a0;
                $all->NO = $no;

                $return[] = $all;
                $all = new stdClass;
            }
        }
        return ($return);
    }

    function getKp($UNOR) {
        $getnipunor = get_nip_unor($UNOR);
        $nipunor = implode(',', $getnipunor);
        // DATA UTAMA
        $this->db->order_by('`nextkpreguler`', 'ASC');
        $this->db->order_by('`nextkpjft`', 'ASC');
        if ($UNOR != NULL) {
            $this->db->where_in('a.PNS_NIPBARU', $nipunor, FALSE);
        }
        //$this->db->where('ALLOW_KP','1');
        //$this->db->where('nextkpreguler<','DATE_ADD(nextkpreguler,INTERVAL 2 YEAR)',FALSE);
        $query0 = $this->db->get("(
SELECT 
`a`.PNS_PNSNIP,
`a`.PNS_NIPBARU,
`a`.PNS_PNSNAM,
`a`.PNS_PNSSEX,
`g`.`GOL_GOLNAM` AS PNS_GOLRU,
`g`.`GOL_PKTNAM` AS PNS_PANGKAT,
`a`.PNS_TMTCPN,
`b`.DIK_NAMDIK,
`a`.PNS_JNSJAB,
`h`.RWJAB_NAMAJAB,
IF(`a`.PNS_JNSJAB=1,`a`.PNS_KODESL,'') AS STR_KODESL,
@maxgolstr:= IF(`a`.`PNS_JNSJAB`=1,`c`.`ESE_HIGRNK`,'') AS MAXGOL_STR,
`b`.`DIK_MAXGOL` AS MAXGOL_DIK,
@maxgoljft:= IF(`a`.PNS_JNSJAB=2,IF(`a`.PNS_TKTDIK<35,34,45),'') AS MAXGOL_JFT,
@akrkp:= IF(`a`.PNS_JNSJAB=2,`f`.AKR_KP,'') AS AKR_KP,
@akrnow:= IF(`a`.PNS_JNSJAB=2,`e`.RWAKR_JUM,'') AS AKR_NOW,
@maxgol:= GREATEST(@maxgolstr,`b`.`DIK_MAXGOL`,@maxgoljft) AS MAXGOL,
@allowkp:= IF(@maxgol>`a`.PNS_GOLRU,IF(`a`.PNS_JNSJAB=2,IF(((@akrkp+0)<=(@akrnow+0) AND NOW()>DATE_ADD(`d`.KP_TERAKHIR,INTERVAL 2 YEAR)),'1','0'),'1'),IF(@maxgol=`a`.PNS_GOLRU,'0','-1')) AS ALLOW_KP,
`d`.KP_TERAKHIR,
`d`.GOL_TERAKHIR,
IF(`a`.PNS_JNSJAB<>2,IF(@allowkp>0,DATE_ADD(`d`.KP_TERAKHIR,INTERVAL 4 YEAR),''),'') AS nextkpreguler,
IF(`a`.PNS_JNSJAB=2,IF(@allowkp>0,DATE_ADD(`d`.KP_TERAKHIR,INTERVAL 2 YEAR),''),'') AS nextkpjft
FROM kanreg8_pupns a 
LEFT JOIN kanreg8_tktpendik b 
ON `a`.PNS_TKTDIK=`b`.DIK_TKTDIK
LEFT JOIN kanreg8_eselon c
ON `a`.PNS_KODESL=`c`.ESE_KODESL
LEFT JOIN (SELECT  `a`.RWGOL_NIP,MAX(`a`.RWGOL_TMTGOL) AS KP_TERAKHIR,MAX(a.RWGOL_KDGOL) as GOL_TERAKHIR FROM( SELECT RWGOL_KDGOL,RWGOL_NIP,MIN(RWGOL_TMTGOL)AS RWGOL_TMTGOL FROM z_datarwgolongan GROUP BY RWGOL_NIP,RWGOL_KDGOL) a GROUP BY  RWGOL_NIP) d
ON `d`.RWGOL_NIP=`a`.PNS_NIPBARU
LEFT JOIN (SELECT RWAKR_NIP,RWAKR_JAB,(SUM(RWAKR_AKRUTAMA)+SUM(RWAKR_AKRPENUNJANG)) AS RWAKR_JUM FROM q_datarwangkakredit GROUP BY RWAKR_NIP,RWAKR_JAB) e
ON `a`.PNS_NIPBARU=`e`.RWAKR_NIP AND `a`.PNS_JABFUN=`e`.RWAKR_JAB
LEFT JOIN q_angkakredit f
ON `a`.PNS_GOLRU=`f`.AKR_GOL
LEFT JOIN kanreg8_golru g 
ON `a`.PNS_GOLRU=g.GOL_KODGOL
LEFT JOIN (SELECT a.RWJAB_NIP, a.RWJAB_NAMAJAB,a.`RWJAB_TMTJAB` FROM z_datarwjabatan a JOIN(SELECT RWJAB_NIP,MAX(RWJAB_TMTJAB) AS RWJAB_TMTJAB FROM z_datarwjabatan GROUP BY RWJAB_NIP) b ON a.`RWJAB_NIP`=b.RWJAB_NIP AND (a.RWJAB_TMTJAB =b.RWJAB_TMTJAB )) h
ON `a`.PNS_NIPBARU=h.RWJAB_NIP
WHERE `a`.PNS_KEDHUK<=20
) a
",FALSE);
        //echo $this->db->last_query();
        $return = array();
        $no = 0;
        $all = new stdClass;

        if ($query0->num_rows() > 0) {
            foreach ($query0->result() as $a0) {
                $all = new stdClass;
                $no++;
                $all = $a0;
                $all->NO = $no;

                $return[] = $all;
                $all = new stdClass;
            }
        }
        return ($return);
    }

}
