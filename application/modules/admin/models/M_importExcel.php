<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//load Spout Library
require_once APPPATH . '/third_party/Spout/src/spout/Autoloader/autoload.php';

//lets Use the Spout Namespaces
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class M_importExcel extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->model('M_pegawai');
    }

    public function upload_file($filename) {
        $this->load->library('upload'); // Load librari upload
        $config['upload_path'] = 'assets/excels';
        $config['allowed_types'] = '*';
        $config['max_size'] = '204800';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if ($this->upload->do_upload('userfile')) { // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors('', ''));
            return $return;
        }
    }

    public function simpanData($filename) {

        try {
            $file_path = "./assets/excels/" . $filename . ".xlsx";
            $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
            $reader->setShouldPreserveEmptyRows(true);
            $reader->open($file_path); //open the file  
            $validasi = $this->validasiData($filename);
            $startpoint = $validasi[0];
            $row1 = $validasi[1];
            $col1 = $validasi[2];
            $val1 = $validasi[3];
            $row2 = $validasi[4];
            $col2 = $validasi[5];
            $val2 = $validasi[6];
            $sheetx = $validasi[7];
            $this->db->trans_begin();
            foreach ($reader->getSheetIterator() as $sheet) {
                if ($sheet->getIndex() === $sheetx) { // index is 0-based
                    $x = 0; //index data
                    $i = 0; //index row
                    foreach ($sheet->getRowIterator() as $row) {
                        if ($i >= $startpoint) {
                            $x++;
                            $this->simpanRow($filename, $row);
                            echo ' ';
//                            echo $i;
//                            print_r($row);
//                            echo"<br>";
                        } else if (($row1 == $i && @$row[$col1] != $val1) || ($row2 == $i && @$row[$col2] != $val2)) { //validasi kesesuaian nilai pada suatu kolom tertentu
//                            echo $validasi[1], $i, $row[$validasi[2]], $validasi[3];
                            return 'File yang diupload tidak valid atau format tidak sesuai';
                            break;
                        }
                        ++$i;
                    }
//                    break; // no need to read more sheets
                }
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return 0;
            } else {
                $this->db->trans_commit();
                $reader->close();
                return $x;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    private function validasiData($filename) {
        $validasi['master'] = ['index start', 'row1', 'col1', 'val1', 'row2', 'col2', 'val2', 'sheet'];
        $validasi['dataPns'] = [7, 3, 8, 'TEMPAT_LAHIR_NAMA', 3, 10, 'JENIS_KELAMIN', 0];
        $validasi['refLokasi'] = [1, 0, 6, 'JENIS_KABUPATEN', 0, 7, 'JENIS_DESA', 0];
        $validasi['refInstansi'] = [1, 0, 4, 'KODE_5_DIGIT', 0, 6, 'INSTANSI_ID', 1];
        $validasi['refKanreg'] = [1, 0, 2, 'KOTA', 0, 3, 'NAMA', 2];
        $validasi['refUnor'] = [5, 4, 2, 'NAMA_UNOR', 4, 3, 'ESELON_ID', 0];
        return $validasi[$filename];
    }

    public function simpanRow($filename, $data, $sheet = null) {
        if ($filename == 'dataPns') {
            $newdata['ORANG_ID'] = $data[1];
            $newdata['PNS_NIPBARU'] = $data[2];
            $newdata['PNS_PNSNIP'] = $data[3];
            $newdata['PNS_PNSNAM'] = $data[4];
            $newdata['PNS_GLRDPN'] = $data[5];
            $newdata['PNS_GLRBLK'] = $data[6];
            $newdata['PNS_TEMLHR_ID'] = $data[7];
//            $newdata['PNS_TEMLHR'] = $data[];
            $newdata['PNS_TGLLHR'] = str_replace('-', '', $data[9]);
            $newdata['PNS_TGLLHRDT'] = reverseDate($data[9]) . ' 00:00:00';
            $newdata['PNS_PNSSEX'] = $data[10] == 'M' ? 1 : 2;
            $newdata['PNS_KODAGA'] = $data[11];
            $newdata['PNS_STSWIN'] = $data[13];
            $newdata['PNS_JENDOK'] = 1;
            $newdata['PNS_NOMDOK'] = $data[15];
            $newdata['PNS_NOMHP'] = $data[16];
            $newdata['PNS_EMAIL'] = $data[17];
            $newdata['PNS_ALAMAT'] = $data[18];
            $newdata['PNSX_NPWP'] = $data[19];
            $newdata['PNSX_BPJS'] = $data[20];
            $newdata['PNS_JENPEG'] = $data[21];
            $newdata['PNS_KEDHUK'] = $data[23];
            $newdata['PNS_STCPNS'] = $data[25];
            $newdata['PNS_KARPEG'] = $data[26];
            $newdata['PNSX_NO_SK_CPN'] = $data[27];
            $newdata['PNSX_TGL_SK_CPN'] = reverseDate($data[28]);
            $newdata['PNS_TMTCPN'] = reverseDate($data[29]);
            $newdata['PNSX_NO_SK_PNS'] = $data[30];
            $newdata['PNSX_TGL_SK_PNS'] = reverseDate($data[31]);
            $newdata['PNS_TMTPNS'] = reverseDate($data[32]);
            $newdata['PNS_GOLAWL'] = $data[33];
            $newdata['PNS_GOLRU'] = $data[35];
            $newdata['PNS_TMTGOL'] = reverseDate($data[37]);
            $newdata['PNS_THNKER'] = $data[38];
            $newdata['PNS_BLNKER'] = $data[39];
            $newdata['PNS_JNSJAB'] = $data[40];
            $newdata['PNSX_IDJAB'] = $data[42];
            $newdata['PNSX_NAMAJAB'] = $data[43];
            $newdata['PNSX_TMTJAB'] = reverseDate($data[44]);
            $newdata['PNS_TKTDIK'] = $data[45];
            $newdata['PNSX_PENDIDIKAN_ID'] = $data[47];
            $newdata['PNSX_THN_LULUS'] = $data[49];
            $newdata['PNSX_KPKN_ID'] = $data[50];
            $newdata['PNS_TEMKRJ'] = $data[52];
            $newdata['PNS_UNOR'] = $data[54];
            $newdata['PNSX_UNOR_INDUK'] = $data[56];
            $newdata['PNSX_INSDUK_ID'] = $data[58];
            $newdata['PNSX_INSKER_ID'] = $data[60];
            $newdata['PNSX_SATKERDUK'] = $data[62];
            $newdata['PNSX_SATKER_KERJA'] = $data[64];
//            $newdata['PNS_UNITOR'] = $data[xxx];
//            $newdata['PNS_KODESL'] = $data[xxx];
//            $newdata['PNS_JABFUN'] = $data[xxx];
//            $newdata['PNS_TMTFUN'] = $data[xxx];
//            $newdata['PNS_JMLIST'] = $data[xxx];
//            $newdata['PNS_JMLANK'] = $data[xxx];
//            $newdata['PNS_RUMAH'] = $data[xxx];
//            $newdata['PNS_TAPRUM'] = $data[xxx];
//            $newdata['PNS_GUNRUM'] = $data[xxx];
//            $newdata['PNS_KODPOS'] = $data[xxx];
//            $newdata['PNS_NOMTEL'] = $data[xxx];
//            $newdata['PNS_KANREG'] = $data[xxx];
//            $newdata['PNS_LATSTR'] = $data[xxx];
//            $newdata['SK_KONV_NOMOR'] = $data[xxx];
//            $newdata['SK_KONV_TANGGAL'] = $data[xxx];
//            $newdata['SK_KONV_URUT'] = $data[xxx];

            $this->db->replace('kanreg8_pupns', $newdata);
        } else if ($filename == 'refLokasi') {
            $newdata['LOK_ID'] = $data[1];
            $newdata['LOK_KANREG'] = $data[2];
            $newdata['LOK_NAMA'] = $data[3];
            $newdata['LOK_KODE'] = $data[4];
            $newdata['LOK_JNS'] = $data[5];
            $newdata['LOK_JNS_KAB'] = $data[6];
            $newdata['LOK_JNS_DESA'] = $data[7];
            $newdata['LOK_IBUKOTA'] = $data[8];
            $this->db->replace('q_lokasi', $newdata);
        } else if ($filename == 'refInstansi') {
            $newdata['INSX_KANREG'] = $data[1];
            $newdata['INSX_JNS'] = $data[2];
            $newdata['INS_KODINS'] = $data[3];
            $newdata['INSX_KODINS_5DIGIT'] = $data[4];
            $newdata['INS_NAMINS'] = $data[5];
            $newdata['INSX_ID'] = $data[6];
            $newdata['INSX_SATKER'] = $data[7];
            $newdata['INSX_SATKER_ID'] = $data[8];
            $this->db->replace('kanreg8_instansi', $newdata);
        } else if ($filename == 'refKanreg') {
            $newdata['KNR_NO'] = $data[0];
            $newdata['KNR_ID'] = $data[1];
            $newdata['KNR_KOTA'] = $data[2];
            $newdata['KNR_NAMA'] = $data[3];
            $this->db->replace('q_kanreg', $newdata);
        } else if ($filename == 'refUnor') {
            $newdata['UNO_ID'] = $data[1];
            $newdata['UNO_NAMUNO'] = trim($data[2]);
            $newdata['UNO_KODESL'] = $data[3] == '00' ? '99' : $data[3];
            $newdata['UNO_KODUNO'] = $data[4];
            $newdata['UNO_NAMJAB'] = $data[5];
            $newdata['UNO_NAMPEJABAT'] = $data[6];
            $newdata['UNO_DIATASAN_ID'] = $data[7];
            $newdata['UNO_INSTAN_ID'] = $data[8];
            $newdata['UNO_INSTAN'] = $this->M_pegawai->get_apapun($data[8], 'kanreg8_instansi', 'INSX_ID', 'INS_KODINS');
            $newdata['UNO_PEMIMPIN_NON_PNS_ID'] = $data[9];
            $newdata['UNO_PEMIMPIN_PNS_ID'] = $data[10];
            $newdata['UNO_JNS'] = $data[11];
            $newdata['UNO_INDUK'] = $data[12];
            $newdata['UNO_JML_STAF'] = $data[13];
            $this->db->replace('kanreg8_unor', $newdata);
        }
    }

}
