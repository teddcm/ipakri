<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pegawai extends CI_Model {

    public function view_apapun($table) {
        $this->db->trans_begin();
        $query = $this->db->get($table);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            if ($query->num_rows() > 0) {
                return $return;
            } else {
                return false;
            }
        }
    }

    public function get_apapun($ID, $table, $primary_column, $return_colum = NULL) {
        $this->db->trans_begin();
        if ($return_colum != NULL) {
            $query = $this->db->select($return_colum);
        }
        $this->db->where($primary_column, $ID);
        $query = $this->db->get($table);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            if ($return_colum == NULL) {
                foreach ($query->result() as $result) {
                    $return[] = $result;
                }
                return $return;
            } else {
                return $query->row()->$return_colum;
            }
        }
    }

    public function simpan_apapun($data, $table, $primary_column) {
        $this->db->trans_begin();
        if ($data['id'] == '') {
            unset($data['id']);
            $ID = uuid();
            $data[$primary_column] = $ID;
            $query = $this->db->insert($table, $data);
        } else {
            $ID = $data['id'];
            unset($data['id']);
            $this->db->where($primary_column, $ID);
            $query = $this->db->update($table, $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function hapus_apapun($where_value, $table, $where_column) {
        $this->db->trans_begin();
        $this->db->where($where_column, $where_value);
        $query = $this->db->delete($table);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function json_jabfun($like) {
        $this->db->select('*, CHAR_LENGTH(JBF_NAMJAB) AS panjang');
        $this->db->like('JBF_NAMJAB', $like);
        $this->db->order_by('panjang', 'asc');
        $this->db->order_by('JBF_KODJAB', 'asc');
        $this->db->limit(10);
        $get = $this->db->get('kanreg8_jabfun');
        //echo $this->db->last_query();
        if ($get->num_rows() > 0) {
            $coma = '';
            foreach ($get->result() as $result) {
                $return[] = $result;
            }
            $json = json_encode($return);
            print_r($json);
        } else {
            return false;
        }
    }

    function json_pegawai($like) {
        $this->db->select('*, CHAR_LENGTH(PNS_PNSNAM) AS panjang');
        $this->db->where('PNS_KEDHUK <=', '20');
        $this->db->where('PNS_INSKER', INSTANSI_KERJA);
        $this->db->like('PNS_PNSNAM', $like);
        $this->db->or_like('PNS_NIPBARU', $like);
        $this->db->order_by('PNS_PNSNAM', 'asc');
        $this->db->order_by('panjang', 'asc');
        $this->db->order_by('PNS_NIPBARU', 'asc');
        $this->db->limit(5);
        $get = $this->db->get('kanreg8_pupns');
        //echo $this->db->last_query();
        if ($get->num_rows() > 0) {
            $coma = '';
            foreach ($get->result() as $result) {
                $result->RWJAB_NAMAJAB = get_jab_pegawai($result->PNS_NIPBARU);
                $return[] = $result;
            }
            $json = json_encode($return);
            print_r($json);
        } else {
            return false;
        }
    }

    function json_pendidikan($like) {
        $this->db->select('*, CHAR_LENGTH(DIK_NMDIK) AS panjang');
        $this->db->like('DIK_NMDIK', $like);
        $this->db->not_like('DIK_NMDIK', 'delete');
        $this->db->order_by('panjang', 'asc');
        $this->db->order_by('DIK_KODIK', 'asc');
        $this->db->limit(5);
        $get = $this->db->get('kanreg8_pendik');
        //echo $this->db->last_query();
        if ($get->num_rows() > 0) {
            $coma = '';
            foreach ($get->result() as $result) {
                $return[] = $result;
            }
            $json = json_encode($return);
            print_r($json);
        } else {
            return false;
        }
    }

    function json_temlahir($like, $wil_tk = 2) {
        $this->db->select('a.id,a.name,b.name as ket');
        $this->db->like('a.name', $like);
        $this->db->order_by('a.id');
        $this->db->join('wil_tk1 b', 'a.wil_tk1_id=b.id');

        //$this->db->limit(10);
        $get = $this->db->get('wil_tk2 a');
        //echo $this->db->last_query();
        if ($get->num_rows() > 0) {
            $coma = '';
            foreach ($get->result() as $result) {
                $return[] = $result;
            }
            $json = json_encode($return);
            print_r($json);
        } else {
            return false;
        }
    }

    function get_unor($ins = NULL) {
        $sql = "SELECT a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL,b.UNO_NAMUNO as NAMATASAN ";
        $sql .= "FROM kanreg8_unor a  ";
        $sql .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
        $sql .= "WHERE a.UNO_INSTAN='" . $ins . "' ";
        $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";

        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $groups = array();
        foreach ($query->result_array() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    public function get_data_cuti() {
        $j0 = cek_jatah_cutinew(date('Y') - 0);
        $j0 = $j0 == NULL ? 0 : $j0;
        $j1 = cek_jatah_cutinew(date('Y') - 1);
        $j2 = cek_jatah_cutinew(date('Y') - 2);

        $sql = "
            
SELECT a.`PNS_NIPBARU`,a.`PNS_PNSNAM`,b.taken1,bb.rest1,c.taken2,cc.rest2,$j0 AS j0,$j1 AS j1, $j2 AS j2 FROM kanreg8_pupns a 
LEFT JOIN (SELECT RWCUTI_NIP,SUM(`RWCUTI_JUM`) AS taken1 FROM q_datarwcutinew WHERE `RWCUTI_THNCUTI`=" . (date('Y') - 1) . " AND `RWCUTI_JNS`=1 GROUP BY `RWCUTI_NIP`) b
ON a.`PNS_NIPBARU`=b.`RWCUTI_NIP`
LEFT JOIN (SELECT RWCUTI_NIP,SUM(`RWCUTI_JUM`) AS rest1 FROM q_datarwcutinew WHERE `RWCUTI_THNCUTI`=" . (date('Y') - 1) . " AND `RWCUTI_JNS`=-1 GROUP BY `RWCUTI_NIP`) bb
ON a.`PNS_NIPBARU`=bb.`RWCUTI_NIP`
LEFT JOIN (SELECT RWCUTI_NIP,SUM(`RWCUTI_JUM`) AS taken2 FROM q_datarwcutinew WHERE `RWCUTI_THNCUTI`=" . (date('Y') - 2) . " AND `RWCUTI_JNS`=1 GROUP BY `RWCUTI_NIP`) c
ON a.`PNS_NIPBARU`=c.`RWCUTI_NIP`
LEFT JOIN (SELECT RWCUTI_NIP,SUM(`RWCUTI_JUM`) AS rest2 FROM q_datarwcutinew WHERE `RWCUTI_THNCUTI`=" . (date('Y') - 2) . " AND `RWCUTI_JNS`=-1 GROUP BY `RWCUTI_NIP`) cc
ON a.`PNS_NIPBARU`=cc.`RWCUTI_NIP`
WHERE PNS_KEDHUK<=20
ORDER BY a.`PNS_PNSNAM`
             ";

        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return NULL;
        }
    }

    function get_data_pegawai($PNS_NIPBARU) {
        $sql = "
 SELECT A.*,
       C.`PEN_PENKOD`,
       C.`PEN_TAHLUL`,
       D.`DIK_NMDIK`,
       E.*,
       F.`UNO_NAMUNO`,
       F.`UNO_KODUNO`,
       F.`UNO_ID`,
       G.`RWJAB_NAMAJAB`,
       G.`RWJAB_TMTJAB`,
       G.`RWJAB_IDJENJAB`
FROM   kanreg8_pupns A
       LEFT JOIN (SELECT *
                  FROM   kanreg8_pupns_pendidikan
                  WHERE  `PNS_NIPBARU` = '" . $PNS_NIPBARU . "'
                  ORDER  BY `PEN_TAHLUL` DESC
                  LIMIT  1) C
              ON A.`PNS_NIPBARU` = C.`PNS_NIPBARU`
       LEFT JOIN kanreg8_pendik D
              ON C.`PEN_PENKOD` = D.`DIK_KODIK`
       LEFT JOIN kanreg8_instansi E
              ON A.`PNS_INSKER` = E.`INS_KODINS`
       LEFT JOIN kanreg8_unor F
              ON A.`PNS_UNOR` = F.`UNO_ID`
       LEFT JOIN (SELECT *
                  FROM   z_datarwjabatan
                  WHERE  RWJAB_NIP = '" . $PNS_NIPBARU . "'
                  ORDER  BY `RWJAB_TMTJAB` DESC
                  LIMIT  1) G
              ON A.`PNS_NIPBARU` = G.`RWJAB_NIP`
WHERE  A.`PNS_NIPBARU` = '" . $PNS_NIPBARU . "'   
";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    }

    function get_golru() {
        $query = $this->db->get('kanreg8_golru');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_eselon() {
        $query = $this->db->get('kanreg8_eselon');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_jenis_kp() {
        $query = $this->db->get('kanreg8_jenis_kp');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_jenis_cuti() {
        $this->db->where('JNSCT_ID >', 0);
        $query = $this->db->get('q_jenis_cuti');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_jenjab() {
        $query = $this->db->get('kanreg8_jenjab');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_status_kawin() {
        $query = $this->db->get('q_status_kawin');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_jabatan_angka_kredit($PNS_NIPBARU) {
        $this->db->where('RWJAB_NIP', $PNS_NIPBARU);
        $this->db->where('RWJAB_IDJENJAB', 2);
        $query = $this->db->get('z_datarwjabatan');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_status_anak() {
        $query = $this->db->get('q_status_anak');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_issu($NIP) {
        $this->db->where('RWISU_NIP', $NIP);
        $query = $this->db->get('z_datarwissu');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_diklat() {
        $query = $this->db->get('q_diklat');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_agama() {
        $query = $this->db->get('kanreg8_agama');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_lokker() {
        $query = $this->db->get('kanreg8_lokker');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_kedhuk() {
        $this->db->order_by('KED_KEDKOD', 'asc');
        $query = $this->db->get('kanreg8_kedhuk');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_jenpeg() {
        $query = $this->db->get('kanreg8_jenpeg');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_instansi() {
        $this->db->order_by('INS_KODINS');
        $query = $this->db->get('kanreg8_instansi');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function get_tktpendik() {
        $this->db->order_by('DIK_TKTDIK', 'asc');
        $query = $this->db->get('kanreg8_tktpendik');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $result) {
                $return[] = $result;
            }
            return $return;
        } else {
            return false;
        }
    }

    function update_gelar($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $nip = $data['nip_lama'];
        unset($data['mode']);
        unset($data['nip_lama']);
        $this->db->trans_begin();

        $this->db->where('PNS_NIPBARU', $nip);
        $query = $this->db->update('kanreg8_pupns', $data);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// DATA UTAMA
    function simpan_data_utama($newdata) {
        $PNS_GLRDPN = $newdata['PNS_GLRDPN'];
        $PNS_GLRBLK = $newdata['PNS_GLRBLK'];
        $data = arraytoupper($newdata);
        $data['PNS_GLRDPN'] = $PNS_GLRDPN;
        $data['PNS_GLRBLK'] = $PNS_GLRBLK;
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        @$this->db->set('PNS_TGLLHR', $data['PNS_TGLLHRDT'] != '' ? (substr($data['PNS_TGLLHRDT'], 0, 2) . substr($data['PNS_TGLLHRDT'], 3, 2) . substr($data['PNS_TGLLHRDT'], 6, 4)) : NULL);
        @$this->db->set('PNS_TGLLHRDT', $data['PNS_TGLLHRDT'] != '' ? reverseDate($data['PNS_TGLLHRDT']) : NULL);
        @$this->db->set('PNS_TMTPNS', $data['PNS_TMTPNS'] != '' ? reverseDate($data['PNS_TMTPNS']) : NULL);
        @$this->db->set('PNS_TMTCPN', $data['PNS_TMTCPN'] != '' ? reverseDate($data['PNS_TMTCPN']) : NULL);

        unset($data['PNS_TMTCPN']);
        unset($data['PNS_TGLLHRDT']);
        unset($data['PNS_TMTPNS']);

        $this->db->set('PNS_INSKER', INSTANSI_KERJA);
        if ($mode == 'TAMBAH') {
            $data['ORANG_ID'] = uuid();
            $query = $this->db->insert('kanreg8_pupns', $data);
            //SIMPAN z_datarwgolongan CPNS
            $datagolongan['mode'] = 'TAMBAH';
            $datagolongan['RWGOL_NIP'] = $newdata['PNS_NIPBARU'];
            $datagolongan['RWGOL_KDJNSKP'] = 211;
            $datagolongan['RWGOL_TMTGOL'] = $newdata['PNS_TMTCPN'];
            $datagolongan['RWGOL_KDGOL'] = $newdata['PNS_GOLAWL'];
            $nemkg = get_mkg($newdata['PNS_GOLAWL'], $newdata['PNS_GOLAWL'], reverseDate($newdata['PNS_TMTCPN']), 0, reverseDate($newdata['PNS_TMTCPN']));
            //PENAMBAHAN MASA KERJA FIKTIF
            $datagolongan['RWGOL_MKGTHN'] = $nemkg->tahun;
            $datagolongan['RWGOL_MKGBLN'] = $nemkg->bulan;
            $this->simpan_golongan($datagolongan);
            //--SIMPAN z_datarwgolongan CPNS
            if (isset($newdata['PNS_TMTPNS'])) {
                //SIMPAN z_datarwgolongan PNS
                $datagolonganpns['mode'] = 'TAMBAH';
                $datagolonganpns['RWGOL_NIP'] = $newdata['PNS_NIPBARU'];
                $datagolonganpns['RWGOL_KDJNSKP'] = 211;
                $datagolonganpns['RWGOL_TMTGOL'] = $newdata['PNS_TMTPNS'];
                $datagolonganpns['RWGOL_KDGOL'] = $newdata['PNS_GOLAWL'];
                $nemkgpns = get_mkg($newdata['PNS_GOLAWL'], $newdata['PNS_GOLAWL'], reverseDate($newdata['PNS_TMTCPN']), 0, reverseDate($newdata['PNS_TMTPNS']));
                //PENAMBAHAN MASA KERJA FIKTIF
                $datagolonganpns['RWGOL_MKGTHN'] = $nemkgpns->tahun;
                $datagolonganpns['RWGOL_MKGBLN'] = $nemkgpns->bulan;
                $this->simpan_golongan($datagolonganpns);
                //--SIMPAN z_datarwgolongan PNS
                //echo 'ok';
            }
        } else if ($mode == 'EDIT') {
            $niplama = $data['nip_lama'];
            $nipbaru = $data['PNS_NIPBARU'];
            unset($data['nip_lama']);
            if ($niplama != $nipbaru) {
                $this->ganti_nip($niplama, $nipbaru);
            }
            $this->db->where('PNS_NIPBARU', $niplama);
            $query = $this->db->update('kanreg8_pupns', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_data_utama($NIP) {
        $this->db->trans_begin();
        $this->db->where('PNS_NIPBARU', $NIP);
        $this->db->delete('kanreg8_pupns');

        $this->db->where('PNS_NIPBARU', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('kanreg8_pupns_pendidikan');

        $this->db->where('RWANK_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('q_datarwanak');

        $this->db->where('RWDLT_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwdiklat');

        $this->db->where('RWGOL_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwgolongan');

        $this->db->where('RWISU_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwissu');

        $this->db->where('RWJAB_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwjabatan');

        $this->db->where('RWDIK_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwpendidikan');

        $this->db->where('RWPMK_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwpmk');

        $this->db->where('RWUNOR_IDPNS', get_data_pns($NIP, 'ORANG_ID'));
        $this->db->delete('z_datarwpnsunor');



        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function ganti_nip($NIPLAMA, $NIPBARU) {
        $this->db->trans_begin();
        $ID_ORANG = get_data_pns($NIPLAMA, 'ORANG_ID');

        $this->db->where('PNS_NIPBARU', $ID_ORANG);
        $this->db->set('PNS_NIPBARU', $NIPBARU);
        $this->db->delete('kanreg8_pupns_pendidikan');

        $this->db->where('RWANK_IDPNS', $ID_ORANG);
        $this->db->set('RWANK_NIP', $NIPBARU);
        $this->db->update('q_datarwanak');

        $this->db->where('RWDLT_IDPNS', $ID_ORANG);
        $this->db->set('RWDLT_NIP', $NIPBARU);
        $this->db->update('z_datarwdiklat');

        $this->db->where('RWGOL_IDPNS', $ID_ORANG);
        $this->db->set('RWGOL_NIP', $NIPBARU);
        $this->db->update('z_datarwgolongan');

        $this->db->where('RWISU_IDPNS', $ID_ORANG);
        $this->db->set('RWISU_NIP', $NIPBARU);
        $this->db->update('z_datarwissu');

        $this->db->where('RWJAB_IDPNS', $ID_ORANG);
        $this->db->set('RWJAB_NIP', $NIPBARU);
        $this->db->update('z_datarwjabatan');

        $this->db->where('RWDIK_IDPNS', $ID_ORANG);
        $this->db->set('RWDIK_NIP', $NIPBARU);
        $this->db->update('z_datarwpendidikan');

        $this->db->where('RWPMK_IDPNS', $ID_ORANG);
        $this->db->set('RWPMK_NIP', $NIPBARU);
        $this->db->update('z_datarwpmk');

        $this->db->where('RWUNOR_IDPNS', $ID_ORANG);
        $this->db->set('RWUNOR_NIP', $NIPBARU);
        $this->db->update('z_datarwpnsunor');



        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// GOLONGAN

    function riwayat_golongan($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwgolongan a "
                . "LEFT JOIN kanreg8_golru b "
                . "ON a.RWGOL_KDGOL = b.GOL_KODGOL "
                . "where a.RWGOL_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWGOL_TMTGOL DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_golongan($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWGOL_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWGOL_TGLSK', @$data['RWGOL_TGLSK'] != '' ? reverseDate($data['RWGOL_TGLSK']) : NULL);
        $this->db->set('RWGOL_TMTGOL', @$data['RWGOL_TMTGOL'] != '' ? reverseDate($data['RWGOL_TMTGOL']) : NULL);
        $this->db->set('RWGOL_TGLBKN', @$data['RWGOL_TGLBKN'] != '' ? reverseDate($data['RWGOL_TGLBKN']) : NULL);

        unset($data['RWGOL_TGLSK']);
        unset($data['RWGOL_TMTGOL']);
        unset($data['RWGOL_TGLBKN']);

        if ($mode == 'TAMBAH') {
            $data['RWGOL_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $data['RWGOL_ID'] = uuid();
            $query = $this->db->insert('z_datarwgolongan', $data);
        } else if ($mode == 'EDIT') {
            $RWGOL_ID = $data['RWGOL_ID'];
            unset($data['RWGOL_ID']);
            $this->db->where('RWGOL_ID', $RWGOL_ID);
            $query = $this->db->update('z_datarwgolongan', $data);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            //echo $this->db->last_query();
            $this->validasi_golru($PNS_NIPBARU);
            return true;
        }
    }

    function hapus_golongan($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWGOL_ID', $ID);
        $this->db->where('RWGOL_IDPNS', get_data_pns($PNS_NIPBARU, 'ORANG_ID'));
        $this->db->delete('z_datarwgolongan');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $this->validasi_golru($PNS_NIPBARU);
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// PMK
    function simpan_pmk($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWPMK_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWPMK_TGLAWL', $data['RWPMK_TGLAWL'] != '' ? reverseDate($data['RWPMK_TGLAWL']) : NULL);
        $this->db->set('RWPMK_TGLAKR', $data['RWPMK_TGLAKR'] != '' ? reverseDate($data['RWPMK_TGLAKR']) : NULL);
        $this->db->set('RWPMK_TGLSK', $data['RWPMK_TGLSK'] != '' ? reverseDate($data['RWPMK_TGLSK']) : NULL);
        $this->db->set('RWPMK_TGLBKN', $data['RWPMK_TGLBKN'] != '' ? reverseDate($data['RWPMK_TGLBKN']) : NULL);

        unset($data['RWPMK_TGLAWL']);
        unset($data['RWPMK_TGLAKR']);
        unset($data['RWPMK_TGLSK']);
        unset($data['RWPMK_TGLBKN']);

        if ($mode == 'TAMBAH') {
            $data['RWPMK_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $data['RWPMK_ID'] = uuid();
            $query = $this->db->insert('z_datarwpmk', $data);
        } else if ($mode == 'EDIT') {
            $RWPMK_ID = $data['RWPMK_ID'];
            unset($data['RWPMK_ID']);
            $this->db->where('RWPMK_ID', $RWPMK_ID);
            $query = $this->db->update('z_datarwpmk', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            //$this->validasi_golru($PNS_NIPBARU);
            return true;
        }
    }

    function hapus_pmk($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWPMK_ID', $ID);
        $this->db->where('RWPMK_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwpmk');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            //$this->validasi_golru($PNS_NIPBARU);
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// PENDIDIKAN
    function riwayat_pendidikan($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwpendidikan a "
                . "LEFT JOIN kanreg8_pendik b "
                . "ON a.RWDIK_IDDIK = b.DIK_KODIK "
                . "where a.RWDIK_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWDIK_THNLULUS DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_pendidikan($newdata) {
        $PNS_GLRDPN = $newdata['RWDIK_GLRDPN'];
        $PNS_GLRBLK = $newdata['RWDIK_GLRBLK'];
        $data = arraytoupper($newdata);
        $data['RWDIK_GLRDPN'] = $PNS_GLRDPN;
        $data['RWDIK_GLRBLK'] = $PNS_GLRBLK;

        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWDIK_NIP'];
        unset($data['mode']);
        unset($data['NAMADIK']);
        $this->db->trans_begin();

        $this->db->set('RWDIK_TGLLULUS', @$data['RWDIK_TGLLULUS'] != '' ? reverseDate($data['RWDIK_TGLLULUS']) : NULL);

        unset($data['RWDIK_TGLLULUS']);

        if ($mode == 'TAMBAH') {
            $data['RWDIK_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $RWDIK_ID = uuid();
            $data['RWDIK_ID'] = $RWDIK_ID;
            $query = $this->db->insert('z_datarwpendidikan', $data);
        } else if ($mode == 'EDIT') {
            $this->db->set('RWDIK_PERTAMA', isset($data['RWDIK_PERTAMA']) ? 'V' : NULL);
            $RWDIK_ID = $data['RWDIK_ID'];
            unset($data['RWDIK_ID']);
            $this->db->where('RWDIK_ID', $RWDIK_ID);
            $query = $this->db->update('z_datarwpendidikan', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            if (isset($data['RWDIK_PERTAMA'])) {
                $this->validasi_pendidikan($PNS_NIPBARU, $RWDIK_ID);
            } else {
                $this->validasi_pendidikan($PNS_NIPBARU);
            }
            return true;
        }
    }

    function hapus_pendidikan($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWDIK_ID', $ID);
        $this->db->where('RWDIK_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwpendidikan');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $this->validasi_pendidikan($PNS_NIPBARU);
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// JABATAN
    function riwayat_jabatan($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwjabatan a "
                . "where a.RWJAB_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWJAB_TMTJAB DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function riwayat_unor($PNS_NIPBARU = NULL) {
        $sql = "SELECT a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL,b.UNO_NAMUNO as NAMATASAN ";
        $sql .= "FROM kanreg8_unor a  ";
        $sql .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
        $sql .= "WHERE a.UNO_INSTAN='" . $ins . "' ";
        $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";

        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $groups = array();
        foreach ($query->result_array() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_jabatan($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWJAB_NIP'];
        unset($data['mode']);
        $dataUnor = $data;
        if (isset($data['RWJAB_IDESL'])) {
            $data['RWJAB_IDESL'] = ($data['RWJAB_IDESL'] == '00') ? NULL : $data['RWJAB_IDESL'];
        }
        $this->db->trans_begin();
        $this->db->set('RWJAB_TMTJAB', $data['RWJAB_TMTJAB'] != '' ? reverseDate($data['RWJAB_TMTJAB']) : NULL);
        $this->db->set('RWJAB_TGLSK', $data['RWJAB_TGLSK'] != '' ? reverseDate($data['RWJAB_TGLSK']) : NULL);
        $this->db->set('RWJAB_TMTLANTIK', @$data['RWJAB_TMTLANTIK'] != '' ? reverseDate($data['RWJAB_TMTLANTIK']) : NULL);

        unset($data['RWJAB_TMTJAB']);
        unset($data['RWJAB_TGLSK']);
        unset($data['RWJAB_TMTLANTIK']);

        //print_r($data);
        if ($mode == 'TAMBAH') {
            $data['RWJAB_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWJAB_ID'] = $ID;
            $query = $this->db->insert('z_datarwjabatan', $data);

            //SIMPAN DATA UNOR
            $this->db->set('RWUNOR_ID', uuid());
            $this->db->set('RWUNOR_NIP', $PNS_NIPBARU);
            $this->db->set('RWUNOR_IDPNS', $data['RWJAB_IDPNS']);
            $this->db->set('RWUNOR_NOSK', @$dataUnor['RWJAB_NOMSK']);
            $this->db->set('RWUNOR_PROSEDUR', 2); //MUTASI_JABATAN
            $this->db->set('RWUNOR_IDASAL', $ID);
            $this->db->set('RWUNOR_IDUNORBARU', $dataUnor['RWJAB_IDUNO']);
            $this->db->set('RWUNOR_NAMUNORBARU', $dataUnor['RWJAB_NAMUNO']);
            //$this->db->set('RWUNOR_TGLSK', $dataUnor['RWJAB_TGLSK'] != '' ? reverseDate($dataUnor['RWJAB_TGLSK']) : NULL);
            $this->db->set('RWUNOR_TGLSK', $dataUnor['RWJAB_TMTJAB'] != '' ? reverseDate($dataUnor['RWJAB_TMTJAB']) : NULL); //TGL SK UNOR DARI TMT JAB
            $query = $this->db->insert('z_datarwpnsunor');
            //$this->validasi_unor($PNS_NIPBARU);
            //--SIMPAN DATA UNOR
        } else if ($mode == 'EDIT') {
            $ID = $data['RWJAB_ID'];
            unset($data['RWJAB_ID']);
            $this->db->where('RWJAB_ID', $ID);
            $query = $this->db->update('z_datarwjabatan', $data);

            //SIMPAN DATA UNOR
            $this->db->set('RWUNOR_NOSK', @$dataUnor['RWJAB_NOMSK']);
            $this->db->set('RWUNOR_IDUNORBARU', $dataUnor['RWJAB_IDUNO']);
            $this->db->set('RWUNOR_NAMUNORBARU', $dataUnor['RWJAB_NAMUNO']);
            //$this->db->set('RWUNOR_TGLSK', $dataUnor['RWJAB_TGLSK'] != '' ? reverseDate($dataUnor['RWJAB_TGLSK']) : NULL);
            $this->db->set('RWUNOR_TGLSK', $dataUnor['RWJAB_TMTJAB'] != '' ? reverseDate($dataUnor['RWJAB_TMTJAB']) : NULL); //TGL SK UNOR DARI TMT JAB
            $this->db->where('RWUNOR_IDASAL', $ID);
            $this->db->where('RWUNOR_NIP', $PNS_NIPBARU);
            $query = $this->db->update('z_datarwpnsunor');
            //--SIMPAN DATA UNOR
        }
        //$this->validasi_jabatan($PNS_NIPBARU);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_jabatan($PNS_NIPBARU);
            $this->validasi_unor($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_jabatan($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWJAB_ID', $ID);
        $this->db->where('RWJAB_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwjabatan');


        //HAPUS DATA UNOR
        $this->db->where('RWUNOR_IDASAL', $ID);
        $this->db->where('RWUNOR_NIP', $PNS_NIPBARU);
        $query = $this->db->delete('z_datarwpnsunor');
        //$this->validasi_unor($PNS_NIPBARU);
        //--HAPUS DATA UNOR
        //echo $this->db->last_query();
        //$this->validasi_jabatan($PNS_NIPBARU);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_unor($PNS_NIPBARU);
            $this->validasi_jabatan($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// DIKLAT
    function riwayat_diklat($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwdiklat a "
                . "where a.RWDLT_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWDLT_THN DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_diklat($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWDLT_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWDLT_TGL', $data['RWDLT_TGL'] != '' ? reverseDate($data['RWDLT_TGL']) : NULL);

        unset($data['RWDLT_TGL']);

        if ($mode == 'TAMBAH') {
            $data['RWDLT_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWDLT_ID'] = $ID;
            $query = $this->db->insert('z_datarwdiklat', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWDLT_ID'];
            unset($data['RWDLT_ID']);
            $this->db->where('RWDLT_ID', $ID);
            $query = $this->db->update('z_datarwdiklat', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_diklat($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_diklat($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWDLT_ID', $ID);
        $this->db->where('RWDLT_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwdiklat');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_diklat($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// KELUARGA
    function riwayat_keluarga_issu($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwissu a "
                . "where a.RWISU_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWISU_TGLNIKAH DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_keluarga_issu($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWISU_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWISU_TGLNIKAH', $data['RWISU_TGLNIKAH'] != '' ? reverseDate($data['RWISU_TGLNIKAH']) : NULL);
        unset($data['RWISU_TGLNIKAH']);

        if ($mode == 'TAMBAH') {
            $data['RWISU_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWISU_ID'] = $ID;
            $query = $this->db->insert('z_datarwissu', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWISU_ID'];
            unset($data['RWISU_ID']);
            $this->db->where('RWISU_ID', $ID);
            $query = $this->db->update('z_datarwissu', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_keluarga($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_keluarga_issu($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->select('RWANK_ID');
        $this->db->where('RWANK_ID_ORTU', $ID);
        $get = $this->db->get('q_datarwanak');
        if ($get->num_rows() > 0) {
            $this->db->trans_rollback();
            return 'Masih terdapat data anak';
        } else {
            $this->db->where('RWISU_ID', $ID);
            $this->db->where('RWISU_NIP', $PNS_NIPBARU);
            $this->db->delete('z_datarwissu');
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_keluarga($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function riwayat_keluarga_anak($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM q_datarwanak a "
                . "where a.RWANK_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWANK_TGLLHIR DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_keluarga_anak($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWANK_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWANK_TGLLHIR', $data['RWANK_TGLLHIR'] != '' ? reverseDate($data['RWANK_TGLLHIR']) : NULL);
        unset($data['RWANK_TGLLHIR']);

        if ($mode == 'TAMBAH') {
            $data['RWANK_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWANK_ID'] = $ID;
            $query = $this->db->insert('q_datarwanak', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWANK_ID'];
            unset($data['RWANK_ID']);
            $this->db->where('RWANK_ID', $ID);
            $query = $this->db->update('q_datarwanak', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_keluarga($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_keluarga_anak($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWANK_ID', $ID);
        $this->db->where('RWANK_NIP', $PNS_NIPBARU);
        $this->db->delete('q_datarwanak');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_keluarga($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// UNIT ORGANISASI
    function simpan_unit_organisasi($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWUNOR_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWUNOR_TGLSK', $data['RWUNOR_TGLSK'] != '' ? reverseDate($data['RWUNOR_TGLSK']) : NULL);

        unset($data['RWUNOR_TGLSK']);

        if ($mode == 'TAMBAH') {
            $data['RWUNOR_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWUNOR_ID'] = $ID;
            $this->db->set('RWUNOR_PROSEDUR', 3); //MUTASI_PNS_UNOR
            $query = $this->db->insert('z_datarwpnsunor', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWUNOR_ID'];
            unset($data['RWUNOR_ID']);
            $this->db->where('RWUNOR_ID', $ID);
            $query = $this->db->update('z_datarwpnsunor', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_unor($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_unit_organisasi($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWUNOR_ID', $ID);
        $this->db->where('RWUNOR_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwpnsunor');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_unor($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// KURSUS
    function riwayat_kursus($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM z_datarwkursus a "
                . "where a.RWKRS_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWKRS_TGL DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_kursus($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWKRS_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWKRS_TGL', $data['RWKRS_TGL'] != '' ? reverseDate($data['RWKRS_TGL']) : NULL);

        unset($data['RWKRS_TGL']);

        if ($mode == 'TAMBAH') {
            $data['RWKRS_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWKRS_ID'] = $ID;
            $query = $this->db->insert('z_datarwkursus', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWKRS_ID'];
            unset($data['RWKRS_ID']);
            $this->db->where('RWKRS_ID', $ID);
            $query = $this->db->update('z_datarwkursus', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            //$this->validasi_kursus($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_kursus($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWKRS_ID', $ID);
        $this->db->where('RWKRS_NIP', $PNS_NIPBARU);
        $this->db->delete('z_datarwkursus');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_kursus($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// SKP
    function riwayat_skp($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM q_datarwskp a "
                . "where a.RWSKP_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWSKP_THN DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_skp($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWSKP_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();
        //print_r($data);
        if ($mode == 'TAMBAH') {
            $data['RWSKP_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWSKP_ID'] = $ID;
            $query = $this->db->insert('q_datarwskp', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWSKP_ID'];
            unset($data['RWSKP_ID']);
            $this->db->where('RWSKP_ID', $ID);
            $query = $this->db->update('q_datarwskp', $data);
        }
        //$this->validasi_skp($PNS_NIPBARU);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_skp($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWSKP_ID', $ID);
        $this->db->where('RWSKP_NIP', $PNS_NIPBARU);
        $this->db->delete('q_datarwskp');


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// ANGKA KREDIT
    function riwayat_angka_kredit($PNS_NIPBARU, $limit = 6) {
        $sql = "SELECT * FROM q_datarwangkakredit a "
                . "where a.RWAKR_NIP='" . $PNS_NIPBARU . "' "
                . "ORDER BY RWAKR_TGLSK DESC LIMIT " . $limit . "";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $return = array();
        foreach ($query->result() as $result) {
            $return[] = $result;
        }
        return ($return);
    }

    function simpan_angka_kredit($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWAKR_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();
        //print_r($data);
        $this->db->set('RWAKR_TGLSK', ISSET($data['RWAKR_TGLSK']) ? reverseDate($data['RWAKR_TGLSK']) : NULL);
        unset($data['RWAKR_TGLSK']);
        $this->db->set('RWAKR_AKRPERTAMA', isset($data['RWAKR_AKRPERTAMA']) ? 'V' : NULL);
        $data['RWAKR_THNAWL'] = $data['RWAKR_THNAWL'] != 0 ? $data['RWAKR_THNAWL'] : NULL;
        $data['RWAKR_THNAKR'] = $data['RWAKR_THNAKR'] != 0 ? $data['RWAKR_THNAKR'] : NULL;

        if ($mode == 'TAMBAH') {
            $data['RWAKR_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWAKR_ID'] = $ID;
            $query = $this->db->insert('q_datarwangkakredit', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWAKR_ID'];
            unset($data['RWAKR_ID']);
            $this->db->where('RWAKR_ID', $ID);
            $query = $this->db->update('q_datarwangkakredit', $data);
        }
        //$this->validasi_angka_kredit($PNS_NIPBARU);
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_angka_kredit($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWAKR_ID', $ID);
        $this->db->where('RWAKR_NIP', $PNS_NIPBARU);
        $this->db->delete('q_datarwangkakredit');


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    ////////////////////////////////////////////////////////////////////////////CUTI
    function simpan_cuti($newdata) {
        $data = arraytoupper($newdata);
        $mode = $data['mode'];
        $PNS_NIPBARU = $data['RWCUTI_NIP'];
        unset($data['mode']);
        $this->db->trans_begin();


        $this->db->set('RWCUTI_TGLAWL', ISSET($data['RWCUTI_TGLAWL']) ? reverseDate($data['RWCUTI_TGLAWL']) : NULL);
        $this->db->set('RWCUTI_TGLAKR', ISSET($data['RWCUTI_TGLAKR']) ? reverseDate($data['RWCUTI_TGLAKR']) : NULL);
        unset($data['RWCUTI_TGLAWL']);
        unset($data['RWCUTI_TGLAKR']);
        if (isset($data['RWCUTI_JNS'])) {
            $data['RWCUTI_JUM'] = -1 * abs($data['RWCUTI_JUM']);
        } else {
            $data['RWCUTI_JUM'] = abs($data['RWCUTI_JUM']);
        }

        if ($mode == 'DEFINE') {
            $data['RWCUTI_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWCUTI_ID'] = $ID;
            $data['RWCUTI_JNS'] = -1;
            $query = $this->db->insert('q_datarwcutinew', $data);
        } else if ($mode == 'TAMBAH') {
            $data['RWCUTI_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
            $ID = uuid();
            $data['RWCUTI_ID'] = $ID;
            $query = $this->db->insert('q_datarwcutinew', $data);
        } else if ($mode == 'EDIT') {
            $ID = $data['RWCUTI_ID'];
            unset($data['RWCUTI_ID']);
            $this->db->where('RWCUTI_ID', $ID);
            $query = $this->db->update('q_datarwcutinew', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_temp_cuti($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('front_cuti_temp');
        $this->db->where('RWCUTI_IDFORM', $id);
        $queryx = $this->db->get('q_datarwcutinew');
        //echo $this->db->last_query();
        $return = $query->row();
        if ($query->num_rows() > 0) {
            if ($queryx->num_rows() <= 0) {
                $return->used = false;
                return $return;
            } else {
                $return->used = true;
                return $return;
            }
        } else {
            return false;
        }
    }

    function simpan_cuti_awal($newdata) {
        $data = ($newdata);
        $this->db->trans_begin();
//        if (@$mode == 'DEFINE') {
//            if ($data['cuti_312'] > 0) {
//                $this->db->set('RWCUTI_ID', uuid());
//                $this->db->set('RWCUTI_NIP', $PNS_NIPBARU);
//                $this->db->set('RWCUTI_JNS', -2);
//                $this->db->set('RWCUTI_JUM', $data['cuti_312']);
//                $query = $this->db->insert('q_datarwcuti');
//            }
//            $this->db->set('RWCUTI_IDPNS', get_data_pns($PNS_NIPBARU, 'ORANG_ID'));
//            $this->db->set('RWCUTI_ID', uuid());
//            $this->db->set('RWCUTI_NIP', $PNS_NIPBARU);
//            $this->db->set('RWCUTI_JNS', -1);
//            $this->db->set('RWCUTI_JUM', $data['cuti_sisa']);
//            $this->db->set('RWCUTI_THNCUTI', date('Y'));
//            $query = $this->db->insert('q_datarwcutinew');
//
//
//            $this->db->set('RWCUTI_IDPNS', get_data_pns($PNS_NIPBARU, 'ORANG_ID'));
//            $this->db->set('RWCUTI_ID', uuid());
//            $this->db->set('RWCUTI_NIP', $PNS_NIPBARU);
//            $this->db->set('RWCUTI_JNS', 0);
//            $this->db->set('RWCUTI_JUM', $data['cuti_jatah']);
//            $this->db->set('RWCUTI_THNCUTI', date('Y'));
//            $query = $this->db->insert('q_datarwcutinew');
//        } else {
        $this->db->where('RWCUTI_JNS', -1);
        $this->db->where('RWCUTI_THNCUTI', date('Y'));
        $this->db->delete('q_datarwcutinew'); //hapus jatah cuti setiap pegawai pada tahun ini
//print_r($data);
        foreach ($data['jatah'] as $key => $val) {
            $this->db->set('RWCUTI_IDPNS', get_data_pns($key, 'ORANG_ID'));
            $this->db->set('RWCUTI_ID', uuid());
            $this->db->set('RWCUTI_NIP', $key);
            $this->db->set('RWCUTI_JNS', -1);
            $this->db->set('RWCUTI_JUM', $val);
            $this->db->set('RWCUTI_THNCUTI', date('Y'));
            $query = $this->db->insert('q_datarwcutinew');
        }
//        }

        $this->db->set('jatah', $data['jatahtahunan']);
        $this->db->set('tahun', date('Y'));
        $this->db->insert('q_jatah_cuti');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_cuti($ID, $PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWCUTI_ID', $ID);
        $this->db->where('RWCUTI_NIP', $PNS_NIPBARU);
        $this->db->delete('q_datarwcutinew');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->validasi_unor($PNS_NIPBARU);
            $this->db->trans_commit();
            return true;
        }
    }

//////////////////////////////////////////////////////////////////////////////// KGB
    function simpan_kgb($newdata) {
        $data = $newdata;
        $PNS_NIPBARU = $data['RWKGB_NIP'];
        $mode = $data['mode'];
        unset($data['mode']);
        $this->db->trans_begin();

        $this->db->set('RWKGB_TMTSKBARU', $data['RWKGB_TMTSKBARU'] != '' ? reverseDate($data['RWKGB_TMTSKBARU']) : NULL);
        unset($data['RWKGB_TMTSKBARU']);

        $this->db->set('RWKGB_TGLSKBARU', $data['RWKGB_TGLSKBARU'] != '' ? reverseDate($data['RWKGB_TGLSKBARU']) : NULL);
        unset($data['RWKGB_TGLSKBARU']);

        if ($mode != 'fullmode') {
            if ($mode == 'tambah') {
                $data['RWKGB_IDPNS'] = get_data_pns($PNS_NIPBARU, 'ORANG_ID');
                $ID = uuid();
                $data['RWKGB_ID'] = $ID;
                $query = $this->db->insert('z_datarwkgb', $data);
            } else if ($mode == 'edit') {
                $ID = $data['RWKGB_ID'];
                unset($data['RWKGB_ID']);
                $this->db->where('RWKGB_ID', $ID);
                $query = $this->db->update('z_datarwkgb', $data);
            }
        } else {
            $this->db->set('RWKGB_TMTSKLAMA', $data['RWKGB_TMTSKLAMA'] != '' ? reverseDate($data['RWKGB_TMTSKLAMA']) : NULL);
            unset($data['RWKGB_TMTSKLAMA']);

            $this->db->set('RWKGB_TGLSKLAMA', $data['RWKGB_TGLSKLAMA'] != '' ? reverseDate($data['RWKGB_TGLSKLAMA']) : NULL);
            unset($data['RWKGB_TGLSKLAMA']);

            $ID = $data['RWKGB_ID'];
            unset($data['RWKGB_ID']);
            $this->db->where('RWKGB_ID', $ID);
            $query = $this->db->update('z_datarwkgb', $data);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function hapus_kgb($ID) {
        $this->db->trans_begin();
        $this->db->where('RWKGB_ID', $ID);
        $this->db->delete('z_datarwkgb');
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_kgb_sk($id) {
        $this->db->where('RWKGB_ID', $id);
        $query = $this->db->get('z_datarwkgb');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function get_kgb_sk_lama($id_now) {
        //SELECT * FROM z_datarwkgb WHERE RWKGB_NIP='198710032009122001' ORDER BY RWKGB_TMTSKBARU DESC LIMIT 1 OFFSET 1
        $nip = $this->get_kgb_sk($id_now)->RWKGB_NIP;
        $TMTSK = $this->get_kgb_sk($id_now)->RWKGB_TMTSKBARU;
        $this->db->select('RWKGB_NOSKBARU,RWKGB_TGLSKBARU,RWKGB_TMTSKBARU,RWKGB_GAPOKBARU,RWKGB_MKGBARU,RWKGB_GOLBARU,RWKGB_JABPJBBARU');
        $this->db->where('RWKGB_NIP', $nip);
        $this->db->where('RWKGB_TMTSKBARU<', $TMTSK, TRUE);
        $this->db->order_by('RWKGB_TMTSKBARU', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('z_datarwkgb');
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function update_kgb_spesimen($id, $jab_spesimen) {
        $this->db->trans_begin();
        $this->db->set('RWKGB_JABPJBBARU', $jab_spesimen);
        $this->db->where('RWKGB_ID', $id);
        $this->db->update('z_datarwkgb');

        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function update_kgb_nosurat($id) {
        $this->db->trans_begin();
        $nosurat = convertNoSurat('kgb');
        $this->db->set('RWKGB_NOSKBARU', $nosurat);
        $this->db->where('RWKGB_ID', $id);
        $this->db->update('z_datarwkgb');

        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $nosurat;
        }
    }

    //////////////////////////////////////////////////////////////////////////// LAIN-LAIN
    function validasi_pendidikan($NIP, $RWDIK_ID_PERTAMA = NULL) {

        $this->db->trans_begin();
        if ($RWDIK_ID_PERTAMA != NULL) {
            //HAPUS SEMUA v PADA NIP INI
            $this->db->where('RWDIK_NIP', $NIP);
            $this->db->set('RWDIK_PERTAMA', NULL);
            $this->db->update('z_datarwpendidikan');
            //echo $this->db->last_query();
            //KEMUDIAN SET V PADA RWDIK_ID_PERTAMA
            $this->db->where('RWDIK_NIP', $NIP);
            $this->db->where('RWDIK_ID', $RWDIK_ID_PERTAMA);
            $this->db->set('RWDIK_PERTAMA', 'V');
            $this->db->update('z_datarwpendidikan');
        }

        //HAPUS DATA PADA TABEL kanreg8_pupns_pendidikan
        $this->db->where('PNS_NIPBARU', $NIP);
        $this->db->delete('kanreg8_pupns_pendidikan');

        //SELECT DATA DARI TABEL z_datarwpendidikan
        $this->db->where('RWDIK_NIP', $NIP);
        $get = $this->db->get('z_datarwpendidikan');

        //MASUKKAN DATA DARI z_datarwpendidikan KE kanreg8_pupns_pendidikan
        if ($get->num_rows() > 0) {
            foreach ($get->result() as $result) {
                $this->db->set('PNS_NIPBARU', $result->RWDIK_NIP);
                $this->db->set('PEN_PENKOD', $result->RWDIK_IDDIK);
                $this->db->set('PEN_TAHLUL', $result->RWDIK_THNLULUS);
                $this->db->insert('kanreg8_pupns_pendidikan');
            }
        }

        //PILIH DATA DENGAN TAHUN LULUS TERAKHIR
        $this->db->order_by('RWDIK_THNLULUS', 'DESC');
        $this->db->where('RWDIK_NIP', $NIP);
        $get = $this->db->get('z_datarwpendidikan', 1);
        //echo $this->db->last_query();
        //UPDATE TABEL kanreg8_pupns DENGAN INFORMASI PENDIDIKAN KELULUSAN TERAKHIR
        $this->db->where('PNS_NIPBARU', $NIP);
        $this->db->set('PNS_TKTDIK', $get->num_rows() > 0 ? $get->row()->RWDIK_TKTDIK : NULL);
        $this->db->update('kanreg8_pupns');

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function validasi_golru($PNS_NIPBARU) { //validasi golru yang ada di data utama pegawai
        $this->db->trans_begin();
        $this->db->where('RWGOL_NIP', $PNS_NIPBARU);
        $this->db->order_by('RWGOL_TMTGOL', 'desc');
        $this->db->limit(1);
        $x1 = $this->db->get('z_datarwgolongan');
        if ($x1->num_rows() > 0) {
            $PNS_GOLRU = $x1->row()->RWGOL_KDGOL;
            $PNS_TMTGOL = $x1->row()->RWGOL_TMTGOL;
            $PNS_THNKER = $x1->row()->RWGOL_MKGTHN;
            $PNS_BLNKER = $x1->row()->RWGOL_MKGBLN;
        } else {
            $PNS_GOLRU = NULL;
            $PNS_TMTGOL = NULL;
            $PNS_THNKER = NULL;
            $PNS_BLNKER = NULL;
        }

        $this->db->set('PNS_GOLRU', $PNS_GOLRU);
        $this->db->set('PNS_TMTGOL', $PNS_TMTGOL);
        $this->db->set('PNS_THNKER', $PNS_THNKER);
        $this->db->set('PNS_BLNKER', $PNS_BLNKER);

        $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
        $query = $this->db->update('kanreg8_pupns');

        //echo $PNS_GOLAWL . ' ' . $PNS_GOLRU . ' ' . $PNS_TMTGOL;
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function validasi_unor($PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWUNOR_NIP', $PNS_NIPBARU);
        $this->db->order_by('RWUNOR_TGLSK', 'desc');
        $this->db->limit(1);
        $x1 = $this->db->get('z_datarwpnsunor');
        if ($x1->num_rows() > 0) {
            $PNS_UNOR = $x1->row()->RWUNOR_IDUNORBARU;

            $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
            $this->db->set('PNS_UNOR', $PNS_UNOR);
            $query = $this->db->update('kanreg8_pupns');
        } else {//PNS_UNOR
            $PNS_UNOR = NULL;
        }

        //echo $this->db->last_query();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function validasi_keluarga($PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWISU_NIP', $PNS_NIPBARU);
        $x1 = $this->db->get('z_datarwissu')->num_rows();
        $this->db->where('RWANK_NIP', $PNS_NIPBARU);
        $x2 = $this->db->get('q_datarwanak')->num_rows();
        $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
        $this->db->set('PNS_JMLIST', $x1);
        $this->db->set('PNS_JMLANK', $x2);
        $query = $this->db->update('kanreg8_pupns');
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function validasi_jabatan($PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWJAB_NIP', $PNS_NIPBARU);
        $this->db->order_by('RWJAB_TMTJAB', 'desc');
        $this->db->limit(1);
        $x1 = $this->db->get('z_datarwjabatan');

        $PNS_JABFUN = NULL;
        $PNS_TMTFUN = NULL;
        $PNS_KODESL = NULL;
        $PNS_JNSJAB = NULL;
        if ($x1->num_rows() > 0) {
            $PNS_JNSJAB = $x1->row()->RWJAB_IDJENJAB;
            $PNS_KODESL = $x1->row()->RWJAB_IDESL;
            if ($PNS_JNSJAB == 2) {
                $PNS_JABFUN = $x1->row()->RWJAB_IDJAB;
                $PNS_TMTFUN = $x1->row()->RWJAB_TMTJAB;
            }
        }


        $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
        $this->db->set('PNS_JNSJAB', $PNS_JNSJAB);
        $this->db->set('PNS_JABFUN', $PNS_JABFUN);
        $this->db->set('PNS_TMTFUN', $PNS_TMTFUN);
        $this->db->set('PNS_KODESL', $PNS_KODESL);
        $query = $this->db->update('kanreg8_pupns');
        //echo $this->db->last_query();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function validasi_diklat($PNS_NIPBARU) {
        $this->db->trans_begin();
        $this->db->where('RWDLT_NIP', $PNS_NIPBARU);
        $this->db->order_by('RWDLT_IDDLT', 'asc');
        $this->db->limit(1);
        $x1 = $this->db->get('z_datarwdiklat');
        if ($x1->num_rows() > 0) {
            $PNS_LATSTR = $x1->row()->RWDLT_NAMDLT;
        } else {//PNS_UNOR
            $PNS_LATSTR = NULL;
        }

        //echo $this->db->last_query();

        $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
        $this->db->set('PNS_LATSTR', $PNS_LATSTR);
        $query = $this->db->update('kanreg8_pupns');

        //echo $this->db->last_query();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function declare_all($PNS_NIPBARU = NULL) {
        if ($PNS_NIPBARU != NULL) {
            $this->declare_awal($PNS_NIPBARU);
        } else {
            $this->db->trans_begin();
            $this->db->select('PNS_NIPBARU,PNS_PNSNAM');
            $this->db->order_by('PNS_NIPBARU', 'ASC');
            $this->db->order_by('PNS_JNSJAB', 'ASC');
            $get = $this->db->get('kanreg8_pupns');
            //echo $this->db->last_query();
            if ($get->num_rows() > 0) {
                foreach ($get->result() as $result) {
                    echo'<br>';
                    echo $result->PNS_PNSNAM . ' ';
                    echo'<br>';
                    echo $result->PNS_NIPBARU . ' ';
                    if ($this->declare_awal($result->PNS_NIPBARU)) {
                        echo'[success]<br>';
                    } else {
                        echo'[failed]<br>';
                    }
                }
                $this->db->trans_commit();
            } else {
                return false;
            }
        }
    }

    public function declare_awal($PNS_NIPBARU = NULL) {
        $this->db->trans_begin();
        $this->declare_golongan($PNS_NIPBARU);
        $this->declare_jabatan($PNS_NIPBARU);
        $this->declare_diklat($PNS_NIPBARU);
        $this->declare_pendidikan($PNS_NIPBARU);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function declare_golongan($PNS_NIPBARU = NULL) { //apabila datarwgol kosong, maka buat data dari kanreg8_pupns
        $this->db->where('RWGOL_NIP', $PNS_NIPBARU);
        $get = $this->db->get('z_datarwgolongan');
        if ($get->num_rows() > 0) {
            $this->validasi_golru($PNS_NIPBARU);
        } else {
            $this->db->trans_begin();
            $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
            $get = $this->db->get('kanreg8_pupns');
            //echo $this->db->last_query();
            if ($get->num_rows() > 0) {
                foreach ($get->result() as $result) {
                    $datagolongan['mode'] = 'TAMBAH';
                    $datagolongan['RWGOL_NIP'] = $result->PNS_NIPBARU;
                    $datagolongan['RWGOL_KDJNSKP'] = 211;
                    $datagolongan['RWGOL_TMTGOL'] = reverseDate($result->PNS_TMTCPN);
                    $datagolongan['RWGOL_KDGOL'] = $result->PNS_GOLAWL;
                    $nemkg = get_mkg($result->PNS_GOLAWL, $result->PNS_GOLAWL, reverseDate($result->PNS_TMTCPN), 0, reverseDate($result->PNS_TMTCPN));
                    //PENAMBAHAN MASA KERJA FIKTIF
                    $datagolongan['RWGOL_MKGTHN'] = $nemkg->tahun;
                    $datagolongan['RWGOL_MKGBLN'] = $nemkg->bulan;
                    $this->simpan_golongan($datagolongan);
                    //--SIMPAN z_datarwgolongan CPNS
                    if (($result->PNS_TMTPNS) != NULL) {
                        //SIMPAN z_datarwgolongan PNS
                        $datagolonganpns['mode'] = 'TAMBAH';
                        $datagolonganpns['RWGOL_NIP'] = $result->PNS_NIPBARU;
                        $datagolonganpns['RWGOL_KDJNSKP'] = 211;
                        $datagolonganpns['RWGOL_TMTGOL'] = reverseDate($result->PNS_TMTPNS);
                        $datagolonganpns['RWGOL_KDGOL'] = $result->PNS_GOLAWL;
                        $nemkgpns = get_mkg($result->PNS_GOLAWL, $result->PNS_GOLAWL, reverseDate($result->PNS_TMTCPN), 0, reverseDate($result->PNS_TMTPNS));
                        //PENAMBAHAN MASA KERJA FIKTIF
                        $datagolonganpns['RWGOL_MKGTHN'] = $nemkgpns->tahun;
                        $datagolonganpns['RWGOL_MKGBLN'] = $nemkgpns->bulan;
                        $this->simpan_golongan($datagolonganpns);
                    }

                    if ($result->PNS_GOLRU != $result->PNS_GOLAWL) {
                        $data['mode'] = 'TAMBAH';
                        $data['RWGOL_NIP'] = $result->PNS_NIPBARU;
                        $data['RWGOL_TMTGOL'] = reverseDate($result->PNS_TMTGOL);
                        $data['RWGOL_KDGOL'] = $result->PNS_GOLRU;
                        $data['RWGOL_MKGTHN'] = $result->PNS_THNKER;
                        $data['RWGOL_MKGBLN'] = $result->PNS_BLNKER;
                        $this->simpan_golongan($data);
                    }
                }
                $this->db->trans_commit();
            } else {
                return false;
            }
        }
    }

    public function declare_jabatan($PNS_NIPBARU = NULL) { //apabila datarwjab kosong, maka buat data dari kanreg8_pupns
        $this->db->where('RWJAB_NIP', $PNS_NIPBARU);
        $get = $this->db->get('z_datarwjabatan');
        if ($get->num_rows() > 0) {
            $this->validasi_jabatan($PNS_NIPBARU);
        } else {
            $this->db->trans_begin();
            $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
            $get = $this->db->get('kanreg8_pupns');
            //echo $this->db->last_query();
            if ($get->num_rows() > 0) {
                foreach ($get->result() as $result) {
                    //print_r($result);
                    if ($result->PNS_JNSJAB == 1) {
                        $data['RWJAB_NIP'] = $result->PNS_NIPBARU;
                        $data['RWJAB_IDJENJAB'] = 1;
                        $data['RWJAB_IDUNO'] = $result->PNS_UNOR;
                        $data['RWJAB_NAMUNO'] = convert_unor($result->PNS_UNOR);
                        $data['RWJAB_NAMAJAB'] = convert_unor($result->PNS_UNOR, 'UNO_NAMJAB');
                        $data['RWJAB_IDESL'] = convert_unor($result->PNS_UNOR, 'UNO_KODESL');
                        $data['RWJAB_TMTJAB'] = NULL;
                        $data['RWJAB_TGLSK'] = NULL;
                        $data['mode'] = 'TAMBAH';
                        $this->simpan_jabatan($data);
                        echo $data['RWJAB_NAMAJAB'] . ' di ' . $data['RWJAB_NAMUNO'];
                    } else if ($result->PNS_JNSJAB == 2) {
                        $data['RWJAB_NIP'] = $result->PNS_NIPBARU;
                        $data['RWJAB_IDJENJAB'] = 2;
                        $data['RWJAB_IDUNO'] = $result->PNS_UNOR;
                        $data['RWJAB_NAMUNO'] = convert_unor($result->PNS_UNOR);
                        $data['RWJAB_NAMAJAB'] = convert_jabfun($result->PNS_JABFUN);
                        $data['RWJAB_IDJAB'] = $result->PNS_JABFUN;
                        $data['RWJAB_IDESL'] = convert_unor($result->PNS_UNOR, 'UNO_KODESL');
                        $data['RWJAB_TMTJAB'] = reverseDate($result->PNS_TMTFUN);
                        $data['RWJAB_TGLSK'] = NULL;
                        $data['mode'] = 'TAMBAH';
                        $this->simpan_jabatan($data);
                        echo $data['RWJAB_NAMAJAB'] . '/' . $result->PNS_JABFUN . ' di ' . $data['RWJAB_NAMUNO'];
                    } else {
                        $data['RWJAB_NIP'] = $result->PNS_NIPBARU;
                        $data['RWJAB_IDJENJAB'] = 4;
                        $data['RWJAB_IDUNO'] = $result->PNS_UNOR;
                        $data['RWJAB_NAMUNO'] = convert_unor($result->PNS_UNOR);
                        $data['RWJAB_NAMAJAB'] = 'N/A';
                        $data['RWJAB_IDESL'] = convert_unor($result->PNS_UNOR, 'UNO_KODESL');
                        $data['RWJAB_TMTJAB'] = NULL;
                        $data['RWJAB_TGLSK'] = NULL;
                        $data['mode'] = 'TAMBAH';
                        $this->simpan_jabatan($data);
                        echo $data['RWJAB_NAMAJAB'] . ' di ' . $data['RWJAB_NAMUNO'] . ' # ';
                    }
                }
                echo '    ';
                $this->db->trans_commit();
            } else {
                return false;
            }
        }
    }

    public function declare_diklat($PNS_NIPBARU = NULL) { //apabila datarwdiklat kosong, maka buat data dari kanreg8_pupns
        $this->db->where('RWDLT_NIP', $PNS_NIPBARU);
        $get = $this->db->get('z_datarwdiklat');
        if ($get->num_rows() > 0) {
            $this->validasi_golru($PNS_NIPBARU);
        } else {
            $this->db->trans_begin();
            $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
            $get = $this->db->get('kanreg8_pupns');
            //echo $this->db->last_query();
            if ($get->num_rows() > 0) {
                foreach ($get->result() as $result) {
                    if ($result->PNS_LATSTR != NULL) {
                        switch (true) {
                            case strpos($result->PNS_LATSTR, 'SEPADA') !== false:
                                $data['RWDLT_IDDLT'] = 1;
                                break;
                            case strpos($result->PNS_LATSTR, 'SEPALA') !== false:
                                $data['RWDLT_IDDLT'] = 2;
                                break;
                            case strpos($result->PNS_LATSTR, 'SEPADYA') !== false:
                                $data['RWDLT_IDDLT'] = 3;
                                break;
                            case strpos($result->PNS_LATSTR, 'SPAMEN') !== false:
                                $data['RWDLT_IDDLT'] = 4;
                                break;
                            case strpos($result->PNS_LATSTR, 'SEPATI') !== false:
                                $data['RWDLT_IDDLT'] = 5;
                                break;
                        }

                        $data['RWDLT_NIP'] = $result->PNS_NIPBARU;
                        $data['RWDLT_NAMDLT'] = convert_diklat($data['RWDLT_IDDLT']);
                        $data['RWDLT_THN'] = NULL;
                        $data['RWDLT_NOM'] = NULL;
                        $data['RWDLT_TGL'] = NULL;
                        $data['mode'] = 'TAMBAH';
                        $this->simpan_diklat($data);
                        echo $data['RWDLT_NAMDLT'] . ' # ';
                    }
                }

                $this->db->trans_commit();
            } else {
                return false;
            }
        }
    }

    public function declare_pendidikan($PNS_NIPBARU = NULL) { //apabila datarwpendidikan kosong, maka buat data dari kanreg8_pupns
        $this->db->where('RWDIK_NIP', $PNS_NIPBARU);
        $get = $this->db->get('z_datarwpendidikan');
        if ($get->num_rows() > 0) {
            $this->validasi_golru($PNS_NIPBARU);
        } else {
            $this->db->trans_begin();
            $this->db->where('PNS_NIPBARU', $PNS_NIPBARU);
            $get = $this->db->get('kanreg8_pupns');
            //echo $this->db->last_query();
            if ($get->num_rows() > 0) {
                foreach ($get->result() as $result) {
                    if ($result->PNS_TKTDIK != NULL) {
                        switch ($result->PNS_TKTDIK) {
                            case 05:
                                $data['RWDIK_TKTDIK'] = 05;
                                $data['RWDIK_IDDIK'] = 1001000;
                                break;
                            case 10:
                                $data['RWDIK_TKTDIK'] = 10;
                                $data['RWDIK_IDDIK'] = 2001000;
                                break;
                            case 12:
                                $data['RWDIK_TKTDIK'] = 12;
                                $data['RWDIK_IDDIK'] = 2100000;
                                break;
                            case 15:
                                $data['RWDIK_TKTDIK'] = 15;
                                $data['RWDIK_IDDIK'] = 3001000;
                                break;
                            case 17:
                                $data['RWDIK_TKTDIK'] = 17;
                                $data['RWDIK_IDDIK'] = 3100000;
                                break;
                            case 18:
                                $data['RWDIK_TKTDIK'] = 18;
                                $data['RWDIK_IDDIK'] = 3210100;
                                break;
                            case 20:
                                $data['RWDIK_TKTDIK'] = 20;
                                $data['RWDIK_IDDIK'] = 3400000;
                                break;
                            case 25:
                                $data['RWDIK_TKTDIK'] = 25;
                                $data['RWDIK_IDDIK'] = 4000000;
                                break;
                            case 30:
                                $data['RWDIK_TKTDIK'] = 30;
                                $data['RWDIK_IDDIK'] = 4100000;
                                break;
                            case 35:
                                $data['RWDIK_TKTDIK'] = 35;
                                $data['RWDIK_IDDIK'] = 5000000;
                                break;
                            case 40:
                                $data['RWDIK_TKTDIK'] = 40;
                                $data['RWDIK_IDDIK'] = 5100000;
                                break;
                            case 45:
                                $data['RWDIK_TKTDIK'] = 45;
                                $data['RWDIK_IDDIK'] = 7100000;
                                break;
                            case 50:
                                $data['RWDIK_TKTDIK'] = 50;
                                $data['RWDIK_IDDIK'] = 7200000;
                                break;
                        }

                        $data['RWDIK_NIP'] = $result->PNS_NIPBARU;
                        $data['RWDIK_THNLULUS'] = NULL;
                        $data['RWDIK_GLRDPN'] = '';
                        $data['RWDIK_GLRBLK'] = '';
                        $data['mode'] = 'TAMBAH';
                        $this->simpan_pendidikan($data);
                        echo $data['RWDIK_TKTDIK'] . ' # ';
                    }
                }

                $this->db->trans_commit();
            } else {
                return false;
            }
        }
    }

}
