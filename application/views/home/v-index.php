<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo getPengaturan('main_nama_organisasi_short'); ?> | <?php echo getPengaturan('main_nama_aplikasi'); ?>
        </title>            
        <link href="<?php echo base_url(); ?>frontend/img/icon.png" type="image/x-icon" rel="icon">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php echo MatchID('ba43455a4bd8cb09bbdf0d650daa6007') ? '' : header('location: auth/logout'); ?>
        <?php require 'v-js-css.php'; ?>

        <style>
            td {
                padding: 3px !important;
            }
            .form-group{
                margin-bottom: 5px !important;
            }
            label{
                margin-bottom: 3px !important;
            }
            body {
                zoom: 1; /* Other non-webkit browsers */
                zoom: 100%; /* Webkit browsers */ 
            }
            .content-wrapper{
                //background-image: url("<?php echo base_url('contents/img/body_bg.jpg'); ?>");
                background-repeat: no-repeat;
                background-position: right top;
                background-attachment: fixed;
                background-position: center center;  
                background-color: #dedede;
            }
            input { 
                text-transform: uppercase;
            }
            footer{
                padding: 5px!important;
            }
        </style>
    </head>
    <body class="hold-transition skin-red sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url(uri_string()); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><i class="fa fa-<?php echo getPengaturan('main_nama_aplikasi_icon'); ?>"></i></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><?php echo getPengaturan('main_nama_aplikasi_short'); ?></b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <?php require 'v-topbar.php'; ?>
            </header>
            <?php require 'v-sidebar.php'; ?>
            <div class="content-wrapper">
                <?php require 'v-header.php'; ?>
                <section class="content" id="panelcontent">
                </section>
            </div>
            <?php require 'v-footbar.php'; ?>
            <!-- Control Sidebar -->

        </div>
        
        <audio id="audio" src="https://www.soundjay.com/button/beep-07.wav" autostart="0" autostart="false" preload="metadata" name="media"></audio>

        <script>
            function playSound() {
                var sound = document.getElementById("audio");
                sound.play();
            }
        </script>
        <script>
            $(this).ready(function () {
                loadContent('<?php echo base_url('admin/dashboard'); ?>');
                $('#dashboard').parent('li').addClass('active');
            });
            $('.loadContent').click(function (e) {
                var url = $(this).attr('href');
                loadContent(url);
                e.preventDefault();
            });
            function loadContent(url) {
                createHeader(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {},
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus)
                    },
                    success: function (data) {
                        $('#panelcontent').html(data);
                    }
                });
            }

            function cekError(XMLHttpRequest, textStatus) {
                if (XMLHttpRequest.status == 401 || XMLHttpRequest.status == null) {
                    window.location.href = '';
                    $.notify('Silakan login kembali', 'error', {
                        clickToHide: true,
                    });
                } else {
                    $.notify(XMLHttpRequest.status + ' ' + XMLHttpRequest.statusText, textStatus, {
                        clickToHide: true,
                    });
                }
            }
            function createHeader(url) {
                //membuat breadcrumb
                var path = url.replace('<?php echo base_url(); ?>', '').split('/');
                var judul = '';
                var string = '<li><a><i class="fa fa-dashboard"></i></a></li>';
                var max = 2; //batasan segment
                path.forEach(function (item) {
                    if (item.length < 75 && max > 0) {
                        max--;
                        string += '<li><a>' + item.replace(/_/g, ' ') + '</a></li>';
                        judul = item.replace(/_/g, ' ');
                    }
                });
                $('#judul').html(judul).css('textTransform', 'uppercase');
                $('#breadcrumb').html(string);
            }
            $(document).ajaxStart(function () {
                $("#wait").css("display", "block");
            });
            $(document).ajaxStop(function () {
                setTimeout(function () {
                    $("#wait").css("display", "none");
                }, 10);
            });
            //membuat sidebar active ketika di klik
            $('.linkmenu').click(function (e) {
                var $this = $(this);
                if ($this.hasClass('notree')) {
                    $('.treeview.active').find(">:first-child").trigger("click");
                }
                var url = $(this).attr('href');
                loadContent(url);
                $('.linkmenu').parent('li').removeClass('active');
                $(this).parent('li').addClass('active');
                e.preventDefault();
                $('body').removeClass('sidebar-open');
            });
            //digunakan untuk setiap datatables
            $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };
            /*var fontSize = parseInt($("body").css("font-size"));
             fontSize = fontSize + -2 + "px";
             $("body").css({'font-size': fontSize});
             */
        </script>
        <div id="wait" style=" z-index:1050;display:none;width:100%;height:100%;position:fixed;top:0%;right:0%;cursor: wait;background: rgba( 250, 250, 250, 0 );">
            <img src='<?php echo base_url('contents/img/ajaxLoader.gif'); ?>' style="width: 150px;position:fixed;top:2%;right:30%;">
        </div>
    </body>
</html>
