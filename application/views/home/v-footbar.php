<style>
    footer {
        position: fixed;
        width: 100%;
        bottom: 0;
        background-color: #e7e7e7!important;
        z-index: 5000;
    }
</style>
<footer style="font-size: 10px">
    <div class="pull-right">
        <b>Versi</b> 
        <?php //baca versi otomatis
        $myfile = fopen("README.md", "r") or die("Unable to open file!");
        $kalimat= explode(' ',fgets($myfile));
        echo $kalimat[1].' '.$kalimat[2];
        fclose($myfile);
        ?> 
    </div>
    <strong>Copyright &copy; 2018 <a href="#"><?php echo getPengaturan('main_nama_organisasi'); ?></a>.</strong> All rights
    reserved.
</footer>
