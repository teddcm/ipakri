<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!--div class="user-panel">
            <div class="pull-left image">
                <span class="img-circle fa fa-user<?php echo $this->ion_auth->is_admin() ? '-secret' : ''; ?> text-black fa-2x  btn btn-flat" style="color: white"></span>
            </div>
            <div class="pull-left info">
                <p><?php echo $user->full_name; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Cari NIP . . .">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>            
                </div>
            </form-->
        <ul class="sidebar-menu" style="padding-bottom: 10px!important">
            <li class="header">&nbsp;</li>
            <li class="active"><a class="linkmenu notree" href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="header">MENU PEGAWAI</li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i><span>Pegawai</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu" style="display: none;">                    
                    <li><a class="linkmenu" href="<?php echo base_url('admin/pegawai'); ?>"><i class="fa fa-database"></i> <span>Data Pegawai</span></a></li>
                    <li><a class="linkmenu" href="<?php echo base_url('admin/pegawai/nonaktif'); ?>"><i class="fa fa-ban"></i> <span>Data Pegawai NonAktif</span></a></li>
                    <li><a class="linkmenu" href="<?php echo base_url('admin/pegawai/hierarki'); ?>"><i class="fa fa-bar-chart"></i> <span>Hierarki Pegawai</span></a></li>
                    <li><a class="linkmenu" href="<?php echo base_url('admin/report'); ?>"><i class="fa fa-laptop"></i> <span>Report</span></a></li>
                    <li><a class="linkmenu" href="<?php echo base_url('manage/unor'); ?>"><i class="fa fa-building"></i> <span>Pejabat Organisasi</span></a></li>
                </ul>
            </li>
            <?php if ($this->ion_auth->in_group('admin')) { ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-gears"></i><span>Pengaturan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu" style="display: none;">                    
                        <li><a class="linkmenu" href="<?php echo base_url('admin/pengaturan/utama'); ?>"><i class="fa fa-gear"></i> <span>Utama</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('admin/pengaturan/kgb'); ?>"><i class="fa fa-money"></i> <span>KGB</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('admin/pengaturan/cuti'); ?>"><i class="fa fa-calendar"></i> <span>Cuti</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('admin/pengaturan/spesimen'); ?>"><i class="fa fa-group"></i> <span>Spesimen</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('admin/pengaturan/backup'); ?>"><i class="fa fa-database"></i> <span>Backup</span></a></li>
                    </ul>
                </li> <li class="treeview">
                    <a href="#"><i class="fa fa-gears"></i><span>Import</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu" style="display: none;">                    
                        <li><a class="linkmenu" href="<?php echo base_url('admin/importExcel'); ?>"><i class="fa fa-gear"></i> <span>Utama</span></a></li>
                    </ul>
                </li>
            <?php } ?>

            <?php if ($this->ion_auth->in_group('website')) { ?>
                <li class="header">MENU WEBSITE</li>
                <li class="treeview ">
                    <a href="#"><i class="fa fa-globe"></i><span>Website</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu" style="display: none;">                    
                        <!--li><a class="linkmenu" href="<?php echo base_url('front/back/utama'); ?>"><i class="fa fa-angle-right"></i> <span>Utama</span></a></li-->
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/halaman'); ?>"><i class="fa fa-angle-right"></i> <span>Halaman</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/post'); ?>"><i class="fa fa-angle-right"></i> <span>Post</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/profil'); ?>"><i class="fa fa-angle-right"></i> <span>Profil</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/layanan'); ?>"><i class="fa fa-angle-right"></i> <span>Pelayanan</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/linker'); ?>"><i class="fa fa-angle-right"></i> <span>Tautan</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/slider'); ?>"><i class="fa fa-angle-right"></i> <span>Slider</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/welcome'); ?>"><i class="fa fa-angle-right"></i> <span>Selamat Datang</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/album'); ?>"><i class="fa fa-angle-right"></i> <span>Galeri</span></a></li>
                        <li><a class="linkmenu" href="<?php echo base_url('front/back/unduhan'); ?>"><i class="fa fa-angle-right"></i> <span>Unduhan</span></a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>