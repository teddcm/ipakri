<nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown notifications-menu"><a id="counter"></a></li>
            <?php if ($this->ion_auth->is_admin()) { ?>
                <li class="dropdown notifications-menu">
                    <a class="linkmenu" href="<?php echo base_url('manage/user'); //base_url('admin/user/manajemen_user');            ?>" title="Manajemen User">
                        <i class="fa fa-users"></i>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->ion_auth->is_admin()) { ?>
                <li class="dropdown notifications-menu">
                    <a class="linkmenu" href="<?php echo base_url('manage/unor'); ?>" title="Manajemen Unit Organisasi">
                        <i class="fa fa-map"></i>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->ion_auth->is_admin()) { ?>
                <li class="dropdown notifications-menu">
                    <a class="linkmenu" href="<?php echo base_url('manage/unor2'); ?>" title="Manajemen Unit Organisasi">
                        <i class="fa fa-forward"></i>
                    </a>
                </li>
            <?php } ?>
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <span class="user-image fa fa-user<?php echo $this->ion_auth->is_admin() ? '-secret' : ''; ?> fa-2x"></span>
                    <span class="hidden-xs"><?php echo $user->full_name; ?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <span class="img-circle fa fa-user<?php echo $this->ion_auth->is_admin() ? '-secret' : ''; ?> fa-4x  btn btn-flat" style="color: white"></span>
                        <p>
                            <?php echo $user->full_name; ?>
                            <small>Terdaftar sejak <?php echo mdate('%d %F %Y', $user->created_on); ?></small>
                        </p>
                    </li>
                    <!-- Menu Footer-->

                    <li class="user-footer">
                        <div class="row">
                            <div class="col-xs-4 text-center">
                                <a href="#" onclick="loadContent('manage/user_edit/<?php echo $user->id; ?>')" class="btn btn-default btn-flat">Profil</a>
                            </div>
                            <div class="col-xs-4 text-center">
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="<?php echo base_url('auth/logout'); ?> " class="btn btn-default btn-flat">Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<script>

    $(document).ready(function () {
        var x;
        counterSession();
        $('[data-toggle="popover"]').popover();

        $(document).ajaxStart(function () {
            counterSession()
        });
        function counterSession() {
            clearInterval(x);
            var detik = 1800;
            x = setInterval(function () {
                var now = new Date().getTime();
                var distance = countDownDate - now;
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                $('#counter').html((days == 0 ? "" : (days + " hari ")) + ((("00" + hours).slice(-2) + ":")) + ((("00" + minutes).slice(-2) + ":")) + ("00" + seconds).slice(-2));
                if (distance < 0) {
                    clearInterval(x);
                    $.ajax({
                        url: '<?php echo base_url('auth/logout'); ?>',
                        type: 'POST',
                        success: function () {
                            clearInterval(x);
                        }
                    });
                    $('#counter').html("Timeout");
                    alert('Silakan lakukan login kembali');
                    window.location.href = '<?php echo base_url('auth/logout'); ?>';
                }
            }, 1000);
            var countDownDate = new Date().getTime() + (detik + 2) * 1000;
        }
    });
</script>