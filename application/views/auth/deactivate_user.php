<div class="box">
    <form id=myForm method="post" accept-charset="utf-8">
        <div class="box-header  with-border">
            <h3 class="box-title"><?php echo lang('deactivate_heading'); ?></h3>
            <div class="box-comment"></div>
        </div>
        <div class="box-body">
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo sprintf(lang('deactivate_subheading'), $user->username); ?>?            </div>
            <div class="form-group">
                <div class="radio">
                    <label>
                        <input type="radio" name="confirm" value="yes" checked="checked" />
                        Ya
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="confirm" value="no" />
                        Tidak
                    </label>
                </div>
            </div>
            <?php echo form_hidden($csrf); ?>
            <?php echo form_hidden(array('id' => $user->id)); ?>
        </div>
        <div class="box-footer col-xs-12">
            <div class="pull-right">  
                <button  class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> <?php echo lang('deactivate_submit_btn'); ?></button>
                <a href="#" class="btn btn-danger btn-flat" onclick="loadContent('manage/user')"><i class="fa fa-close"></i> Batal </a> 
            </div>
        </div>
    </form>
</div>

<script>
    $('label').each(function () {
        var html = $(this).html().replace(/\*/g, "<span class=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })

    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url(); ?>/manage/deactivate/<?php echo $user->id; ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#myForm").serialize(),
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        loadContent('<?php echo base_url(); ?>/manage/user');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                });
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
</script>
