<div class="box">
    <div class="box-header  with-border">
        <h3 class="box-title"><?php echo lang('index_heading'); ?></h3>
    </div>
    <div class="box-body">
        <div id="infoMessage"><?php echo $message; ?></div>

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table id="tabelUser"cellpadding=0 cellspacing=10 class="dataTable table table-striped table-hover table-bordered">
                    <thead>

                        <tr>
                            <th>No</th>
                            <th><?php echo lang('index_username_th'); ?></th>
                            <th><?php echo lang('index_fname_th'); ?></th>
                            <th><?php echo lang('index_email_th'); ?></th>
                            <th><?php echo lang('index_status_th'); ?></th>
                            <th><?php echo lang('index_action_th'); ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td class="text-center"></td>
                                <td><?php echo htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php echo htmlspecialchars($user->full_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td class="text-center">
                                    <a href="#" onclick="loadContent('manage/<?php echo ($user->active ? 'deactivate' : 'activate') . '/' . $user->id; ?>')" ><?php echo $user->active ? lang('index_active_link') : lang('index_inactive_link'); ?> </a>
                                </td>
                                <td class="text-center">
                                    <a href="#" onclick="loadContent('manage/user_edit/<?php echo $user->id; ?>')" >Edit</a> |
                                    <a href="#" onclick="loadContent('manage/user_hapus/<?php echo $user->id; ?>')" >Hapus</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
    </div>

    <div class="box-footer  with-border">
        <div class="col-xs-12">
            <button type="button" class="btn btn-flat btn-success pull-right" onclick="loadContent('manage/user_create')">
                <i class="fa fa-plus"></i> <?php echo lang('index_create_user_link'); ?>
            </button>
        </div>
    </div>
</div>

<script>
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    $(document).ready(function () {
        var t = $('#tabelUser').DataTable({
            "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],
            "order": [[1, 'asc']],
            aLengthMenu: [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            iDisplayLength: -1
        });

        t.on('order.dt search.dt', function () {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>
