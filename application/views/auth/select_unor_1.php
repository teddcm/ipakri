<?php
$edit = !empty($unor) ? true : false;
?>         
<div id="myModalx" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                Kode Unor Terdiri dari 10 Digit alphanumeric dengan format :
                <br>
                <span class="text-red">AA</span>
                <span class="text-blue">BB</span>
                <span class="text-yellow">CC</span>
                <span class="text-green">DD</span>
                <span class="text-orange">EE</span>
                
                <br>
                <br>Adapun aturan pembuatan kode unor adalah :
                <br><span class="text-red">AA = 2 digit kode eselon I</span>
                <br><span class="text-blue">BB = 2 digit kode eselon II</span>
                <br><span class="text-yellow">CC = 2 digit kode eselon III</span>
                <br><span class="text-green">DD = 2 digit kode eselon IV</span>
                <br><span class="text-orange">EE = 2 digit kode eselon V</span>
                
                <br>
                <br>Adapun urutan penulisan tidak mengikat. Tetapi idealnya dengan urutan sebagai berikut :
                <br>01-09, 0A-0Z, 11-19, 1A-1Z, ... Z1-Z9, ZA-ZZ.
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content">
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-map"></i> <?php echo $edit ? 'Edit' : 'Tambah'; ?> Unit Organisasi</h3>
        <div class="pull-right">
            <a onclick="loadContent('<?php echo base_url('manage/unor'); ?>')" class="btn btn-flat btn-danger">kembali</a>
        </div>
    </div>
    <form id="myForm" class="form-horizontal">
        <input type="hidden" name="mode" value="<?php echo $edit ? 'edit' : 'tambah'; ?>" >     
        <input type="hidden" name="UNO_ID" value="<?php echo $edit ? $unor->UNO_ID : ''; ?>" >
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Unor Atasan</label>
                <div class="col-sm-10" id="cariLokasi">
                    <input class="form-control" type="text" id="NAMATASAN" value="<?php echo $edit ? convert_unor($unor->UNO_DIATASAN_ID) : ''; ?>">
                    <input type="hidden" id="UNO_DIATASAN_ID" name="UNO_DIATASAN_ID" value="<?php echo $edit ? $unor->UNO_DIATASAN_ID : ''; ?>"  >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Kode Unor Atasan</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="KODATASAN" readonly value="<?php echo $edit ? convert_unor($unor->UNO_DIATASAN_ID, 'UNO_KODUNO') : ''; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Unor</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="UNO_NAMUNO" name="UNO_NAMUNO" value="<?php echo $edit ? $unor->UNO_NAMUNO : ''; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Eselon</label>
                <div class="col-sm-10">
                    <?php //print_r($level);?>
                    <select class="form-control" type="text" name="UNO_KODESL" id="UNO_KODESL">
                        <?php
                        foreach ($level as $lv) {
                            $selected = ($edit && $unor->UNO_KODESL == $lv->ESE_KODESL) ? 'selected' : '';
                            echo "<option " . $selected . " value='" . $lv->ESE_KODESL . "'>" . $lv->ESE_NAMESL . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Kode Unor</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input class="form-control" type="text" id="UNO_KODUNO" name="UNO_KODUNO" value="<?php echo $edit ? $unor->UNO_KODUNO : ''; ?>" maxlength="10" >
                        <span class="input-group-addon btn btn-info btn-flat" data-toggle="modal" data-target="#myModalx"><i class="fa fa-question"></i></span>
                    </div>
                </div>
                <div class="col-sm-5"><span class="text-red" id="remaining">Masukkan 10 Karakter</span></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Jabatan Unor</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="UNO_NAMJAB" name="UNO_NAMJAB" value="<?php echo $edit ? $unor->UNO_NAMJAB : ''; ?>" >
                </div>
            </div>
            <div class="box-footer">
                <div class="form-actions">
                    <button type="submit" class="btn btn-success btn-flat">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>


<script>
    /*jQuery(function ($) {
     $("#UNO_KODUNO").mask("?**********", {placeholder: "_"});
     
     });
     */
    $('#UNO_KODUNO').on('keyup focus', function () {
        if (this.value.length === 0) {
            $("#remaining").html("Masukkan 10 Karakter");
        } else if (this.value.length >= 10) {
            $("#remaining").html("");
        } else {
            $("#remaining").html("Kurang " + (10 - this.value.length) + " karakter lagi");
        }
    });
    $('#UNO_NAMUNO').bind('keyup mouseup', function () {
        $('#UNO_NAMJAB').val('KEPALA ' + $('#UNO_NAMUNO').val());
    })

    $('#pilih_unor').tree({
        dataUrl: '<?php echo base_url('admin/json/unor_jqtree/'); ?>',
    });
    $('#select').click(function () {
        var node = $('#pilih_unor').tree('getSelectedNode');
        $("#NAMATASAN").val(node.name);
        $("#UNO_DIATASAN_ID").val(node.id);
        $("#KODATASAN").val(node.kode);
        //$("#UNO_KODUNO").val(node.kode);
        $('#myModal').modal('hide');
        //alert(node.name);
        $('#UNO_KODESL').trigger('change');
    })
    $('#cariLokasi').on('focusin', function (e) {
        $('#myModal').modal('show')
        $("#cariLokasi *").blur();
        e.preventDefault();
    })
    $('#UNO_KODESL').on('change', function () {
        var eselon = Math.floor(parseInt($('#UNO_KODESL').val()) / 10);
        var level = [0,0, 2, 4, 6, 8];
        $("#UNO_KODUNO").val($("#KODATASAN").val().substr(0, level[eselon]));
        $("#UNO_KODUNO").trigger('keyup');
        //alert($('#UNO_KODESL').val());
    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('manage/unor_simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('manage/unor'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault();
    });
    $("#UNO_KODUNO").trigger('keyup');
</script>