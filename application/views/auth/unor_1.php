<div id="myModalx" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                Kode Unor Terdiri dari 10 Digit alphanumeric dengan format :
                <br>
                <span class="text-red">AA</span>
                <span class="text-blue">BB</span>
                <span class="text-yellow">CC</span>
                <span class="text-green">DD</span>
                <span class="text-orange">EE</span>

                <br>
                <br>Adapun aturan pembuatan kode unor adalah :
                <br><span class="text-red">AA = 2 digit kode eselon I</span>
                <br><span class="text-blue">BB = 2 digit kode eselon II</span>
                <br><span class="text-yellow">CC = 2 digit kode eselon III</span>
                <br><span class="text-green">DD = 2 digit kode eselon IV</span>
                <br><span class="text-orange">EE = 2 digit kode eselon V</span>

                <br>
                <br>Adapun urutan penulisan tidak mengikat. Tetapi idealnya dengan urutan sebagai berikut :
                <br>01-09, 0A-0Z, 11-19, 1A-1Z, ... Z1-Z9, ZA-ZZ.
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-map"></i> Manajemen Unit Organisasi</h3>
        <div class="pull-right">
            <span class="btn btn-info btn-flat" data-toggle="modal" data-target="#myModalx"><i class="fa fa-question"></i></span>
            <a onclick="loadContent('<?php echo base_url('manage/unor_create'); ?>')" class="btn btn-success btn-flat">tambah</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="myDataTable" class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th>Kode Unor</th>
                    <th>Nama</th>
                    <th>Eselon</th> 
                    <th width='10%'>Aksi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>  
        </table>
    </div>
</div>

<script>

    $('#myDataTable').dataTable({
        "sPaginationType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "ajax": "<?php echo site_url('manage/unor_ajax_view'); ?>",
        "iDisplayLength": 100,
        "columns": [
            {"data": "UNO_ID", "class": "text-center", },
            {"data": "UNO_KODUNO"},
            {"data": "UNO_NAMUNO"},
            {"data": "ESL"},
            {"data": "UNO_ID", "class": "text-center"},
        ],
        "order": [[1, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            ;
            $('td:eq(1)', row).html(koduno(data.UNO_KODUNO));
            $('td:eq(2)', row).html(createDash(data.UNO_KODESL) + $('td:eq(2)', row).text());
            var aksi = '<a onclick="edit(\'' + id + '\')" class="edit btn btn-xs btn-info">edit</a>\n\
                        <a onclick="hapus(\'' + id + '\')" class="hapus btn btn-xs btn-danger">hapus</a>';
            $('td:eq(-1)', row).html(aksi);

        },
    });

    function hapus(id) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage/unor_delete'); ?>",
                async: false,
                data: {"id": id, "<?php echo key($csrf); ?>": "<?php echo current($csrf); ?>"},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    $('#myDataTable').dataTable().fnReloadAjax();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }

    function edit(id) {
        //alert(id);
        loadContent('<?php echo base_url('manage/unor_edit/'); ?>' + id);
    }
    function createDash(n) {
        if (n != null) {
            var dash = '';
            for (x = 2; x <= n.substring(0, 1); x++) {
                dash += '_ _  ';
            }
            if (n.substring(1, 2) === '2') {
                dash += '_  ';
            }
        } else {
            dash = '???';
        }
        return "<span class='text-red'>" + dash + "</span>";
    }
    function koduno(n) {
        if (n != null) {
            return n.substring(0, 2) + ' ' + n.substring(2, 4) + ' ' + n.substring(4, 6) + ' ' + n.substring(6, 8) + ' ' + n.substring(8, 10);
        } else {
            return n;
        }
    }
    /*$(document).ready(function () {
     var table = $('#myDataTable').DataTable();
     $('#myDataTable tbody').on('click', 'tr', function () {
     var data = table.row(this).data();
     edit(data.UNO_ID);
     });
     });
     */
</script>