<script>
    $.ajax({
        type: "POST",
        url: '<?php echo base_url(uri_string()); ?>',
        data: {input: "post"},
        dataType: "json",
        success: function (result)
        {
            $.notify(result[1], result[0]);
            loadContent('<?php echo base_url(); ?>/manage/user');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            cekError(XMLHttpRequest, textStatus);
        },
    });
</script>
