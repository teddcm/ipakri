<script src="<?php echo base_url(); ?>assets/plugins/jquery-selectall-master/dist/jquery.selectall.min.js"></script>
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content">
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="box">
    <form id=myForm method="post" accept-charset="utf-8">
        <div class="box-header  with-border">
            <h3 class="box-title">Ubah <?php echo $this->ion_auth->is_admin() ? 'Pengguna' : 'Profil'; ?></h3>
        </div>
        <div class="box-body">
            <style>
                input { 
                    text-transform: none!important;
                }
                legend{
                    margin: 0;
                }
                .changeColor{
                    color: blue;
                }
            </style>
            <div class="row">
                <div class="col-lg-2 form-group">
                    <label for="username">Username</label>                    
                    <?php echo form_input($username); ?>
                </div>
                <div class="col-lg-4 form-group">
                    <label for="full_name">* Nama Lengkap</label>                    
                    <?php echo form_input($full_name); ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label for="email">* Email</label>                    
                    <?php echo form_input($email); ?>
                </div>
                <div class="col-lg-3">
                    <?php if ($this->ion_auth->is_admin()) { ?>
                        <label>Administrator</label>
                        <div class="checkbox">
                            <label disabled="disabled">
                                <input id="administrator" name="groups[]" value="1" type="checkbox" <?php echo $this->ion_auth->is_admin($user->id) ? 'checked' : '' ?>>
                                Hak Akses Administrator
                            </label>
                        </div>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-3 form-group">
                    <label for="password">* Password</label>                 
                    <?php echo form_input($password); ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label for="password_confirm">* Konfirmasi Password</label>      
                    <?php echo form_input($password_confirm); ?>
                </div>
                <?php if ($this->ion_auth->is_admin()) { ?>
                    <div class="col-lg-6 form-group">
                        <label>Unor yang dikelola</label>
                        <div id="cariLokasi">
                            <input class="form-control" type="text" id="NAMATASAN" value="<?php echo convert_unor($IDUNO['value']); ?>">
                            <?php echo form_input($IDUNO); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php echo form_hidden('id', $user->id); ?>
                <div class="clearfix"></div>
                <input type="checkbox" name="groups[]" value="2" checked style="display: none">
                <?php if ($this->ion_auth->is_admin()) { ?>
                    <div class="col-lg-12">
                        <hr/>
                        <h4>Hak Akses</h4>
                        <div class="form-group">
                            <fieldset><legend></legend>
                                <?php
                                $listGroups = array('', 'data_utama', 'golongan', 'jabatan', 'peninjauan_masa_kerja', 'diklat', 'kursus', 'pendidikan', 'unit_organisasi', 'skp', 'angka_kredit', 'kgb', 'keluarga', 'cuti', 'xxx', 'xxx', 'xxx', 'xxx');
                                $nList = 1;
                                $tempList = '';
                                $opentag = '';
                                foreach ($groups as $group) {
                                    ?>
                                    <?php
                                    $gID = $group['id'];
                                    $checked = null;
                                    $item = null;
                                    foreach ($currentGroups as $grp) {
                                        if ($gID == $grp->id) {
                                            $checked = ' checked="checked"';
                                            break;
                                        }
                                    }
                                    if ($group['name'] != 'anggota' && $group['name'] != 'admin') {
                                        //if (strpos($group['name'], $listGroups[$nList]) !== false) {
                                        echo $opentag . "<div class='col-md-3'>";
                                        $tempList = $listGroups[$nList];
                                        $nList++;
                                        $opentag = "</div>";
                                        //}
                                        ?>
                                        <div class="checkbox">
                                            <label class="">
                                                <input class="pilihan" type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                                <?php echo htmlspecialchars($group['description'], ENT_QUOTES, 'UTF-8'); ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                }
                                //echo '</fieldset>'
                                ?>
                            </fieldset>
                        </div>

                    </div>
                <?php } ?>
            </div>

            <div class="box-footer col-xs-12">
                <div class="pull-right">  
                    <button  class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    <?php if ($this->ion_auth->is_admin()) { ?>
                        <a href="#" class="btn btn-danger btn-flat" onclick="loadContent('manage/user')"><i class="fa fa-close"></i> Batal </a> 
                    <?php } ?>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function () {
        $("fieldset").selectAll({
            buttonParent: "legend", // or ".classname" etc.
            buttonWrapperHTML: '<span class="pull-left"></span>',

            buttonSelectText: " Pilih Semua",
            buttonSelectBeforeHTML: '<span class="fa fa-check"></span>',
            buttonSelectAfterHTML: "",

            buttonDeSelectText: " Batalkan",
            buttonDeSelectBeforeHTML: '<span class="fa fa-close"></span>',
            buttonDeSelectAfterHTML: "",

            buttonExtraClasses: "btn btn-sm btn-default btn-flat btn-sm"
        });
    });

    $('label').each(function () {
        var html = $(this).html().replace(/\*/g, "<span class=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })

    $('#pilih_unor').tree({
        dataUrl: '<?php echo base_url('admin/json/unor_jqtree/'); ?>',
    });

    $('#select').click(function () {
        var node = $('#pilih_unor').tree('getSelectedNode');
        $("#NAMATASAN").val(node.name);
        $("#IDUNO").val(node.id);
        $("#KODATASAN").val(node.kode);
        //$("#UNO_KODUNO").val(node.kode);
        $('#myModal').modal('hide');
        //alert(node.name);
        $('#UNO_KODESL').trigger('change');
    })
    $('#cariLokasi').on('focusin', function (e) {
        $('#myModal').modal('show')
        $("#cariLokasi *").blur();
        e.preventDefault();
    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url(); ?>/manage/user_edit_simpan/<?php echo $user->id; ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#myForm").serialize(),
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success' || result[0] === 'error') {
                            loadContent('<?php echo base_url(uri_string()); ?>');
                        } else {
                            loadContent('<?php echo base_url(uri_string()); ?>');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                });
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
            $('input[type="checkbox"].pilihan').change(function () {
                if ($(this).prop('checked')) {
                    $(this).parent().parent().addClass('changeColor');
                } else {
                    $(this).parent().parent().removeClass('changeColor');
                }
            });
            $('input[type="checkbox"].pilihan').trigger('change');
            $('.sa-selector').click(function () {
                $('input[type="checkbox"].pilihan').trigger('change');
            });
</script>
