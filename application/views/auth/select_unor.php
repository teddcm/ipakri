<?php
$edit = !empty($unor) ? true : false;
?>         
<div id="myModalx" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                Kode Unor Terdiri dari 10 Digit alphanumeric dengan format :
                <br>
                <span class="text-red">AA</span>
                <span class="text-blue">BB</span>
                <span class="text-yellow">CC</span>
                <span class="text-green">DD</span>
                <span class="text-orange">EE</span>

                <br>
                <br>Adapun aturan pembuatan kode unor adalah :
                <br><span class="text-red">AA = 2 digit kode eselon I</span>
                <br><span class="text-blue">BB = 2 digit kode eselon II</span>
                <br><span class="text-yellow">CC = 2 digit kode eselon III</span>
                <br><span class="text-green">DD = 2 digit kode eselon IV</span>
                <br><span class="text-orange">EE = 2 digit kode eselon V</span>

                <br>
                <br>Adapun urutan penulisan tidak mengikat. Tetapi idealnya dengan urutan sebagai berikut :
                <br>01-09, 0A-0Z, 11-19, 1A-1Z, ... Z1-Z9, ZA-ZZ.
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content">
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-map"></i> <?php echo $edit ? 'Edit' : 'Tambah'; ?> Unit Organisasi</h3>
        <div class="pull-right">
            <a onclick="loadContent('<?php echo base_url('manage/unor'); ?>')" class="btn btn-flat btn-danger">kembali</a>
        </div>
    </div>
    <div class="box-body ">
        <form id="myForm">
            <input type="hidden" name="mode" value="<?php echo $edit ? 'edit' : 'tambah'; ?>" >     
            <input type="hidden" name="UNO_ID" value="<?php echo $edit ? $unor->UNO_ID : ''; ?>" >
            <div class="row">
                <div class="form-group col-md-7">
                    <label>Nama Unor Atasan</label>
                    <div id="cariLokasi">
                        <input class="form-control" type="text" id="NAMATASAN" value="<?php echo $edit ? convert_unor($unor->UNO_DIATASAN_ID) : ''; ?>">
                        <input type="hidden" id="UNO_DIATASAN_ID" name="UNO_DIATASAN_ID" value="<?php echo $edit ? $unor->UNO_DIATASAN_ID : ''; ?>"  >
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label>Kode Unor Atasan</label>
                    <div>
                        <input style="font-size: 16px" class="form-control" type="text" id="KODATASAN" readonly value="<?php echo $edit ? convert_unor($unor->UNO_DIATASAN_ID, 'UNO_KODUNO') : ''; ?>" >
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label>Eselon Atasan</label>
                    <div>
                        <?php //print_r($level);?>
                        <select disabled class="form-control" name="ESLATASAN" id="ESLATASAN">
                            <option></option>
                            <?php
                            $ESLATASAN = $edit ? convert_unor($unor->UNO_DIATASAN_ID, 'UNO_KODESL') : '';
                            foreach ($level as $lv) {
                                $selected = ($edit && $ESLATASAN == $lv->ESE_KODESL) ? 'selected' : '';
                                echo "<option " . $selected . " value='" . $lv->ESE_KODESL . "'>" . $lv->ESE_NAMESL . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-7">
                    <label>Nama Unor</label>
                    <div>
                        <input required class="form-control" type="text" id="UNO_NAMUNO" name="UNO_NAMUNO" value="<?php echo $edit ? $unor->UNO_NAMUNO : ''; ?>" >
                    </div>
                </div>

                <div class="form-group col-md-3">
                    <label>Kode Unor</label>
                    <div class="input-group">
                        <span class="input-group-addon prefix"></span>
                        <input required minlength="2" style="font-size: 16px"  type="text" class="form-control infix text-center" maxlength="2">
                        <span class="input-group-addon suffix"></span>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label>Eselon</label>
                    <div>
                        <?php //print_r($level);?>
                        <select required class="form-control" name="UNO_KODESL" id="UNO_KODESL">
                            <option></option>
                            <?php
                            foreach ($level as $lv) {
                                $selected = ($edit && $unor->UNO_KODESL == $lv->ESE_KODESL) ? 'selected' : '';
                                echo "<option " . $selected . " value='" . $lv->ESE_KODESL . "'>" . $lv->ESE_NAMESL . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group" style="display: none">
                <label>Kode Unor</label>
                <div class="row">
                    <div class="col-md-8 col-md-md-4">
                        <input required class="form-control" type="text" id="UNO_KODUNO" name="UNO_KODUNO" value="<?php echo $edit ? $unor->UNO_KODUNO : ''; ?>" maxlength="10" >
                    </div>
                    <div class="col-md-4 col-md-md-2">
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label>Nama Jabatan Unor</label>
                <div>
                    <input class="form-control" type="text" id="UNO_NAMJAB" name="UNO_NAMJAB" value="<?php echo $edit ? $unor->UNO_NAMJAB : ''; ?>" >
                </div>
            </div>
            <div class="box-footer">
                <div class="form-actions">
                    <button type="submit" class="btn btn-success btn-flat">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>


<style>
    select option:disabled {
        color: #E3E3E3!important;
    }
</style>
<script>
    /*jQuery(function ($) {
     $("#UNO_KODUNO").mask("?**********", {placeholder: "_"});
     
     });
     */
//                    $('#UNO_KODUNO').on('keyup focus', function () {
//                        if (this.value.length === 0) {
//                            $("#remaining").html("Masukkan 10 Karakter");
//                        } else if (this.value.length >= 10) {
//                            $("#remaining").html("");
//                        } else {
//                            $("#remaining").html("Kurang " + (10 - this.value.length) + " karakter lagi");
//                        }
//                    });
    $('#UNO_NAMUNO').bind('keyup mouseup', function () {
        $('#UNO_NAMJAB').val('KEPALA ' + $('#UNO_NAMUNO').val());
    })


    $('#pilih_unor').tree({
        dataUrl: '<?php echo base_url('admin/json/unor_jqtree/'); ?>',
    });

    $('#select').click(function () {
        var node = $('#pilih_unor').tree('getSelectedNode');
        $("#NAMATASAN").val(node.name);
        $("#UNO_DIATASAN_ID").val(node.id);
        $("#KODATASAN").val(node.kode);
        $("#ESLATASAN").val(node.eselon);
        $('#myModal').modal('hide');
        $('#ESLATASAN').trigger('change');
        $('#UNO_KODUNO').val('');
        $("#ESLATASAN").trigger('change');
    })
    $('#cariLokasi').on('focusin', function (e) {
        $('#myModal').modal('show')
        $("#cariLokasi *").blur();
        e.preventDefault();
    })

    $('#ESLATASAN').on('change', function () {
        var eslallow = (Math.floor($("#ESLATASAN").val() == '' ? 0 : (parseInt($("#ESLATASAN").val()) / 10)) + 1);
        $('#UNO_KODESL option').prop('disabled', true);
        $('#UNO_KODESL option[value=' + eslallow + '1]').prop('disabled', false);
        $('#UNO_KODESL option[value=' + eslallow + '2]').prop('disabled', false);
        $('#UNO_KODESL option[value=99]').prop('disabled', false);
        $('#UNO_KODESL').val(eslallow + ($("#ESLATASAN").val() == '' ? '1' : $("#ESLATASAN").val().substr(1, 1)));


        var eselon = $('#ESLATASAN').val() == '' ? 0 : Math.floor(parseInt($('#ESLATASAN').val()) / 10) + 1;
        var length = [0, 0, 2, 4, 6, 8];
        if ($('#UNO_KODUNO').val().length == 10) {
            //alert(eselon)
            $(".infix").val($('#UNO_KODUNO').val().substr(parseInt(length[eselon]), 2));
        }

        var kodatasan = $("#KODATASAN").val() == '' ? '##########' : $("#KODATASAN").val();
        $(".prefix").html(kodatasan.substr(0, length[eselon]));
        $(".infix").html('00');
        $(".suffix").html(length[eselon] == 8 ? '' : (('000000000000').slice(-(10 - (length[eselon] + 2)))));
        $(".infix").trigger('keyup');

    })


    $('.infix').bind('keyup mouseup', function () {
        $('#UNO_KODUNO').val($('.prefix').html() + $('.infix').val() + $('.suffix').html());
    })

    $("#ESLATASAN").trigger('change');



    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url('manage/unor_simpan'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url('manage/unor'); ?>');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault();
    });


</script>