<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo getPengaturan('main_nama_aplikasi'); ?> | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <link href="<?php echo base_url(); ?>frontend/img/icon.png" type="image/x-icon" rel="icon">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/adminlte/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->

        <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </head>
    <body class="hold-transition login-page">
        <style>
            body{
                background-image: url("../contents/img/bg_login.jpg")!important;
                background-size: cover;
            }
            @font-face {
                font-family: 'password';
                font-style: normal;
                font-weight: 400;
                src: url(<?php echo base_url(); ?>assets/adminlte/dist/fonts/password.ttf);
            }

            input.key {
                font-family: 'password';
            }
        </style>
        <div class="login-box" style="display: none">
            <div class="login-logo">
                <?php echo lang('login_heading'); ?>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg"><?php echo lang('login_subheading'); ?></p>
                <div class="login-box-msg" style="color: red" id="infoMessage"><?php echo $message; ?></div>


                <?php echo form_open("auth/login"); ?>
                <div class="form-group has-feedback">
                    <?php echo form_input($identity); ?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_input($password); ?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8 hidden">
                        <div class="checkbox icheck">
                            <label>
                                <?php echo form_checkbox('remember', '1', TRUE, 'id="remember"'); ?> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-offset-8 col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                <?php echo form_close(); ?>
                <a href="forgot_password">Saya lupa password</a><br>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <script>
            $(window).load(function () {
                $('.login-box').show();
            });
        </script>
    </body>
</html>
