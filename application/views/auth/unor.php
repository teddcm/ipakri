
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/js/jquery.treegrid.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/css/jquery.treegrid.css">

<script type="text/javascript">
    function eselon(eselon) {
        var kodesl = {
            '00': 'NON',
            '11': 'I.a',
            '12': 'I.b',
            '21': 'II.a',
            '22': 'II.b',
            '31': 'III.a',
            '32': 'III.b',
            '41': 'IV.a',
            '42': 'IV.b',
            '51': 'V.a',
            '52': 'V.b'};
        return kodesl[eselon];
    }


    $(function () {
        $.getJSON("<?php echo base_url(); ?>admin/json/unor2_jqtree", function (result) {
            var no = 1;
            $.each(result[0], function (i, field) {
                var $tr = $('<tr>').addClass('treegrid-' + field.no).addClass(field.parent == 0 ? '' : 'treegrid-parent-' + field.parent).append(
                        $('<td>').text(field.name),
                        $('<td>').html(field.xpejabat == 1 ? '<i class="fa fa-check text-green sign"></i>' : (field.xpejabat > 1 ? '<i class="fa fa-exclamation text-yellow sign"></i>' : '<i class="fa fa-question text-aqua sign"></i>')).addClass('text-center'),
                        $('<td>').html(field.pejabat),
                        $('<td>').html(koduno(field.kode, field.eselon)),
                        $('<td>').text(eselon(field.eselon)),
                        $('<td>').html('<a onclick="edit(\'' + field.id + '\')" class="edit btn btn-xs btn-info">edit</a> <a onclick="hapus(\'' + field.id + '\')" class="hapus btn btn-xs btn-danger">hapus</a>')<?php echo $this->ion_auth->is_admin()?'':".hide()";?>,
                        $('<td>').text(no++).addClass('text-center'),
                        ).appendTo('.tree');
            });
            $('#treex').hide();
//            $.each(result[1], function (i, field) {
//                $('#treex').show();
//                var $tr = $('<tr>').addClass('treegrid-' + field.no).addClass(field.parent == 0 ? '' : 'treegrid-parent-' + field.parent).append(
//                        $('<td>').text(field.name),
//                        $('<td>').html(field.xpejabat == 1 ? '<i class="fa fa-check text-green sign"></i>' : (field.xpejabat > 1 ? '<i class="fa fa-exclamation text-yellow sign"></i>' : '<i class="fa fa-question text-red sign"></i>')).addClass('text-center'),
//                        $('<td>').html(field.pejabat),
//                        $('<td>').html(koduno(field.kode, field.eselon)),
//                        $('<td>').text(eselon(field.eselon)),
//                        $('<td>').html('<a onclick="edit(\'' + field.id + '\')" class="edit btn btn-xs btn-info">edit</a> <a onclick="hapus(\'' + field.id + '\')" class="hapus btn btn-xs btn-danger">hapus</a>')<?php echo $this->ion_auth->is_admin()?'':".hide()";?>,
//                        $('<td>').text(no++).addClass('text-center'),
//                        ).appendTo('.treex');
//            });
            $('.tree').treegrid();

            $('a#pagetree-expand-all').on('click', function (e) {
                $('.tree').treegrid('expandAll');
            });
            $('a#pagetree-collapse-all').on('click', function (e) {
                $('.tree').treegrid('collapseAll');
            });
            $('span[class=treegrid-expander]').after('<span class="treegrid-expander file"></span>');
            $('.treegrid-expander-expanded,.treegrid-expander-collapsed').before('<span class="treegrid-indent buka"></span>');

        });
    });
</script>
<style>
    .sign{
        font-size: 14px!important;
    }
    .treegrid-indent{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -176px 0;
    }
    .treegrid-indent.buka{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -192px 0;
    }
    .treegrid-expander{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -192px 0; 
    }
    .treegrid-expander.file{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -240px 0;
    }
    .treegrid-expander.treegrid-expander-expanded{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -224px 0;
    }
    .treegrid-expander.treegrid-expander-collapsed{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -208px 0;
    }
</style>
<div id="myModalx" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                Kode Unor Terdiri dari 10 Digit alphanumeric dengan format :
                <br>
                <span class="text-red">AA</span>
                <span class="text-blue">BB</span>
                <span class="text-yellow">CC</span>
                <span class="text-green">DD</span>
                <span class="text-orange">EE</span>

                <br>
                <br>Adapun aturan pembuatan kode unor adalah :
                <br><span class="text-red">AA = 2 digit kode eselon I</span>
                <br><span class="text-blue">BB = 2 digit kode eselon II</span>
                <br><span class="text-yellow">CC = 2 digit kode eselon III</span>
                <br><span class="text-green">DD = 2 digit kode eselon IV</span>
                <br><span class="text-orange">EE = 2 digit kode eselon V</span>

                <br>
                <br>Adapun urutan penulisan tidak mengikat. Tetapi idealnya dengan urutan sebagai berikut :
                <br>01-09, 0A-0Z, 11-19, 1A-1Z, ... Z1-Z9, ZA-ZZ.
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-map"></i> Manajemen Unit Organisasi</h3>
        <div class="pull-right">
            <a href="#" class="btn btn-default btn-flat" id="pagetree-collapse-all">Collapse all</a>        
            <a href="#" class="btn btn-default btn-flat" id="pagetree-expand-all">Expand all</a>        
            <span class="btn btn-info btn-flat" data-toggle="modal" data-target="#myModalx"><i class="fa fa-question"></i></span>
            <a onclick="loadContent('<?php echo base_url('manage/unor_create'); ?>')" class="btn btn-success btn-flat">tambah</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body"><div class="col-md-4">

            <table class="table table-bordered" width="30%">
                <tr>
                    <td colspan="2">Keterangan:</td>
                </tr>
                <tr>
                    <td><i class="fa fa-check text-green sign"></i></td>
                    <td>Pejabat telah terdefinisi</td>
                </tr>
                <tr>
                    <td><i class="fa fa-question text-aqua sign"></i></td>
                    <td>Pejabat belum terdefinisi</td>
                </tr>
                <tr>
                    <td><i class="fa fa-exclamation text-yellow sign"></i></td>
                    <td>Pejabat terdefinisi lebih dari satu</td>
                </tr>
            </table>
        </div>
        <table class="tree table table-bordered  table-hover" style="font-size: 10px;">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th colspan="2">Pejabat</th>
                    <th>Kode Unor</th>
                    <th>Eselon</th> 
                    <th width='10%' <?php echo $this->ion_auth->is_admin()?'':"style='display:none'";?>>Aksi</th>
                    <th width="5%">No</th>
                </tr>
            </thead>
        </table>
        <span id="treex" style="display: none">
            <br>
            <br>
            <table class="treex table table-bordered  table-hover" style="font-size: 10px;">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th colspan="2">Pejabat</th>
                        <th>Kode Unor</th>
                        <th width='15%'>Eselon</th> 
                        <th width='8%' <?php echo $this->ion_auth->is_admin()?'':"style='display:none'";?>>Aksi</th>
                        <th width="5%">No</th>
                    </tr>
                </thead>
            </table>
        </span>
    </div>
</div>

<script>

    function hapus(id) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage/unor_delete'); ?>",
                async: false,
                data: {"id": id, "<?php echo key($csrf); ?>": "<?php echo current($csrf); ?>"},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    loadContent('<?php echo base_url('manage/unor/'); ?>');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }

    function edit(id) {
        //alert(id);
        loadContent('<?php echo base_url('manage/unor_edit/'); ?>' + id);
    }
    function koduno(n, x) {
        if (n != null) {
            return'' +
                    (Math.floor((parseInt(x)) / 10) == 1 ? ('<b style="color:red">' + n.substring(0, 2) + '</b>') : (n.substring(0, 2))) + ' ' +
                    (Math.floor((parseInt(x)) / 10) == 2 ? ('<b style="color:red">' + n.substring(2, 4) + '</b>') : (n.substring(2, 4))) + ' ' +
                    (Math.floor((parseInt(x)) / 10) == 3 ? ('<b style="color:red">' + n.substring(4, 6) + '</b>') : (n.substring(4, 6))) + ' ' +
                    (Math.floor((parseInt(x)) / 10) == 4 ? ('<b style="color:red">' + n.substring(6, 8) + '</b>') : (n.substring(6, 8))) + ' ' +
                    (Math.floor((parseInt(x)) / 10) == 5 ? ('<b style="color:red">' + n.substring(8, 10) + '</b>') : (n.substring(8, 10))) + ' ';
        } else {
            return n;
        }
    }
</script>