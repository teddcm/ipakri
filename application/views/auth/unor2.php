<?php
//echo floor((int) 44 / 10);
//exit;
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/js/jquery.treegrid.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/maxazan-jquery-treegrid/css/jquery.treegrid.css">

<script type="text/javascript">
    function eselon(eselon) {
        var kodesl = {
            '00': 'NON',
            '99': 'NON',
            '11': 'I.a',
            '12': 'I.b',
            '21': 'II.a',
            '22': 'II.b',
            '31': 'III.a',
            '32': 'III.b',
            '41': 'IV.a',
            '42': 'IV.b',
            '51': 'V.a',
            '52': 'V.b'};
        return kodesl[eselon];
    }


</script>
<style>
    .sign{
        font-size: 14px!important;
    }
    .treegrid-indent{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) repeat-x -176px 0;
    }
    .treegrid-indent.buka{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -192px 0;
    }
    .treegrid-expander{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -192px 0; 
    }
    .treegrid-expander.file{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -240px 0;
    }
    .treegrid-expander.treegrid-expander-expanded{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -224px 0;
    }
    .treegrid-expander.treegrid-expander-collapsed{    
        background: url(<?php echo base_url('assets/adminlte/dist/img/tree_icons.png'); ?>) no-repeat -208px 0;
    }
</style>
<div id="myModalx" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sampel">
                Kode Unor Terdiri dari 10 Digit alphanumeric dengan format :
                <br>
                <span class="text-red">AA</span>
                <span class="text-blue">BB</span>
                <span class="text-yellow">CC</span>
                <span class="text-green">DD</span>
                <span class="text-orange">EE</span>

                <br>
                <br>Adapun aturan pembuatan kode unor adalah :
                <br><span class="text-red">AA = 2 digit kode eselon I</span>
                <br><span class="text-blue">BB = 2 digit kode eselon II</span>
                <br><span class="text-yellow">CC = 2 digit kode eselon III</span>
                <br><span class="text-green">DD = 2 digit kode eselon IV</span>
                <br><span class="text-orange">EE = 2 digit kode eselon V</span>

                <br>
                <br>Adapun urutan penulisan tidak mengikat. Tetapi idealnya dengan urutan sebagai berikut :
                <br>01-09, 0A-0Z, 11-19, 1A-1Z, ... Z1-Z9, ZA-ZZ.
            </div>
        </div>
    </div>
</div>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-map"></i> Manajemen Unit Organisasi</h3>
        <div class="pull-right">
            <a href="#" class="btn btn-default btn-flat" id="pagetree-collapse-all">Collapse all</a>        
            <a href="#" class="btn btn-default btn-flat" id="pagetree-expand-all">Expand all</a>        
            <span class="btn btn-info btn-flat" data-toggle="modal" data-target="#myModalx"><i class="fa fa-question"></i></span>
            <a onclick="loadContent('<?php echo base_url('manage/unor_create'); ?>')" class="btn btn-success btn-flat">tambah</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body"><div class="col-md-4">

            <table class="table table-bordered" width="30%">
                <tr>
                    <td colspan="2">Keterangan:</td>
                </tr>
                <tr>
                    <td><i class="fa fa-check text-green sign"></i></td>
                    <td>Pejabat telah terdefinisi</td>
                </tr>
                <tr>
                    <td><i class="fa fa-question text-aqua sign"></i></td>
                    <td>Pejabat belum terdefinisi</td>
                </tr>
                <tr>
                    <td><i class="fa fa-exclamation text-yellow sign"></i></td>
                    <td>Pejabat terdefinisi lebih dari satu</td>
                </tr>
            </table>
        </div>
        <table class="tree table table-bordered  table-hover" style="font-size: 10px;">
            <thead>
                <tr>
                    <th colspan="2">Nama</th>
                    <th colspan="2">Pejabat</th>
                    <th width='10%'>Kode Unor</th>
                    <th width='5%'>Eselon</th> 
                    <th width='8%' <?php echo $this->ion_auth->is_admin() ? '' : "style='display:none'"; ?>>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php

                function koduno($koduno, $eselon) {
                    return
                            ((floor((int) $eselon / 10)) == 1 ? ('<b style="color:red">' . substr($koduno, 0, 2) . '</b>') : substr($koduno, 0, 2)) . " " .
                            ((floor((int) $eselon / 10)) == 2 ? ('<b style="color:red">' . substr($koduno, 2, 2) . '</b>') : substr($koduno, 2, 2)) . " " .
                            ((floor((int) $eselon / 10)) == 3 ? ('<b style="color:red">' . substr($koduno, 4, 2) . '</b>') : substr($koduno, 4, 2)) . " " .
                            ((floor((int) $eselon / 10)) == 4 ? ('<b style="color:red">' . substr($koduno, 6, 2) . '</b>') : substr($koduno, 6, 2)) . " " .
                            ((floor((int) $eselon / 10)) == 5 ? ('<b style="color:red">' . substr($koduno, 8, 2) . '</b>') : substr($koduno, 8, 2)) . " ";
                }

                $kodesl = array(
                    '00' => 'NON',
                    '99' => 'NON',
                    '11' => 'I.a',
                    '12' => 'I.b',
                    '21' => 'II.a',
                    '22' => 'II.b',
                    '31' => 'III.a',
                    '32' => 'III.b',
                    '41' => 'IV.a',
                    '42' => 'IV.b',
                    '51' => 'V.a',
                    '52' => 'V.b');
                $unor1 = $unor[0];
                $bgcolor = 'white';
                $colspan = "colspan='2'";
                $rowspan = "";
                foreach ($unor1 as $u1) {
                    $depth = $u1['depth'];
                    $indent = '';
                    $type = '';
                    $endChild = $u1['endChild'];
                    if ($depth >= 1) {
                        if ($depth == 1) {
                            
                        } elseif ($depth == 2) {
                            $indent .= "<i class='treegrid-indent'></i>";
                        } elseif ($depth == 3) {
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                        } elseif ($depth == 4) {
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                        } elseif ($depth == 5) {
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                            $indent .= "<i class='treegrid-indent'></i>";
                        }
                        $indent .= "<i class='treegrid-expander'></i>";
                    }

                    if ($endChild > 0) {
                        $type .= "<i class='treegrid-expander treegrid-expander-expanded'></i>";
                    } else {
                        $type .= "<i class='treegrid-expander file'></i>";
                    }

                    if ($u1['parent'] == 1) {
                        echo "</tbody><tbody>";
                        if ($bgcolor == 'red') {
                            $bgcolor = 'blue';
                        } else {
                            $bgcolor = 'red';
                        }
                        $rowspan = "<td style='background-color:$bgcolor' rowspan='" . ($endChild + 1) . "'>"
                                . "</td>";
                    }

                    if ($u1['xpejabat'] == 1) {
                        $xpejabat = '<i class="fa fa-check text-green sign"></i>';
                    } else if ($u1['xpejabat'] > 1) {
                        $xpejabat = '<i class="fa fa-exclamation text-yellow sign"></i>';
                    } else {
                        $xpejabat = '<i class="fa fa-question text-aqua sign"></i>';
                    }
                    echo "<tr>$rowspan"
                    . "<td $colspan>$indent $type " . $u1['name'] . "</td>"
                    . "<td>$xpejabat</td>"
                    . "<td>" . $u1['pejabat'] . "</td>"
                    . "<td>" . koduno($u1['kode'], $u1['eselon']) . "</td>"
                    . "<td>" . $kodesl[$u1['eselon']] . "</td>"
                    . ("<td><a onclick=edit('" . $u1['id'] . "') class='edit btn btn-xs btn-info'>edit</a> <a onclick=hapus('" . $u1['id'] . "') class='hapus btn btn-xs btn-danger'>hapus</a></td>")
                    . "</tr>";
                    $rowspan = '';
                    $colspan = '';
                }

                echo "</tbody>";
                ?>
            </tbody>
        </table>
        <span id="treex" style="display: none">
            <br>
            <br>
            <table class="treex table table-bordered  table-hover" style="font-size: 10px;">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th colspan="2">Pejabat</th>
                        <th>Kode Unor</th>
                        <th width='15%'>Eselon</th> 
                        <th width='8%' <?php echo $this->ion_auth->is_admin() ? '' : "style='display:none'"; ?>>Aksi</th>
                        <th width="5%">No</th>
                    </tr>
                </thead>
            </table>
        </span>
    </div>
</div>

<script>

    function hapus(id) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage/unor_delete'); ?>",
                async: false,
                data: {"id": id, "<?php echo key($csrf); ?>": "<?php echo current($csrf); ?>"},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    loadContent('<?php echo base_url('manage/unor/'); ?>');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }

    function edit(id) {
        //alert(id);
        loadContent('<?php echo base_url('manage/unor_edit/'); ?>' + id);
    }
</script>