<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-users"></i> Manajemen User</h3>
        <div class="pull-right">
            <a onclick="loadContent('<?php echo base_url('user/tambah_user'); ?>')" class="btn btn-success btn-xs">tambah</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="myDataTable" class="table">
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th>Username</th>
                    <th>Nama Lengkap</th>
                    <th>Email</th>
                    <th>Level</th> 
                    <th width='10%'>Aksi</th>
                </tr>
            </thead>
            <tbody>

            </tbody>  
        </table>
    </div>
</div>

<script>

    $('#myDataTable').dataTable({
        "sPaginationType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "sDom": '<"F">t<"F">',
        "ajax": "<?php echo site_url('user/user_ajax_view'); ?>",
        "iDisplayLength": 1000,
        "columns": [
            {"data": "user_id", "class": "text-center", },
            {"data": "user_name"},
            {"data": "user_full_name"},
            {"data": "user_email"},
            {"data": "level_name"},
            {"data": "user_id", "class": "text-center"},
        ],
        "order": [[0, 'asc']],
        "rowCallback": function (row, data, iDisplayIndex) {
            var ref = '<?= uri_string(); ?>';
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            var id = $('td:eq(0)', row).text();
            $('td:eq(0)', row).html(index);
            var aksi = (data.level_id > 1 || '<?php echo get_user_info('user_name'); ?>' == 'teddcm') ? '<a onclick="edit(\'' + id + '\')" class="edit btn btn-xs btn-info">edit</a>\n\
                        <a onclick="hapus(\'' + id + '\')" class="hapus btn btn-xs btn-danger">hapus</a>' : '';
            $('td:eq(-1)', row).html(aksi);

        },
    });
    function hapus(id) {
        var e = confirm('Apakah data ini akan dihapus?');
        if (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('user/hapus_user'); ?>",
                async: false,
                data: {"id": id},
                dataType: "json",
                success: function (result)
                {
                    $.notify(result[1], result[0]);
                    if (result[0] === 'success') {
                        $('#myDataTable').dataTable().fnReloadAjax();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    cekError(XMLHttpRequest, textStatus);
                },
            })
        }
    }
    function edit(id) {
        loadContent('<?php echo base_url('user/edit_user/'); ?>' + id);
    }
</script>