<div class="modal fade" id="myModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <form id="newForm">  
            <div class="modal-content">
                <div class="modal-body"><div style="width: 100%;height: 300px;overflow: scroll;">
                        <div id="pilih_unor"></div>
                    </div>
                </div>
                <div class="modal-footer">                        
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-sm" id="select">Select</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="box">
    <form id=myForm method="post" accept-charset="utf-8">
        <div class="box-header  with-border">
            <h3 class="box-title">Ubah <?php echo $this->ion_auth->is_admin() ? 'Pengguna' : 'Profil'; ?></h3>
        </div>
        <div class="box-body">
            <style>

                input { 
                    text-transform: none!important;
                }            
            </style>
            <div class="row">
                <div class="col-lg-3 form-group">
                    <label for="username">Username</label>                    
                    <?php echo form_input($username); ?>
                </div>
                <div class="col-lg-5 form-group">
                    <label for="full_name">* Nama Lengkap</label>                    
                    <?php echo form_input($full_name); ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label for="email">* Email</label>                    
                    <?php echo form_input($email); ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label for="password">* Password</label>                 
                    <?php echo form_input($password); ?>
                </div>
                <div class="col-lg-3 form-group">
                    <label for="password_confirm">* Konfirmasi Password</label>      
                    <?php echo form_input($password_confirm); ?>
                </div>
                <div class="col-lg-6 form-group">
                    <label>Unor yang dikelola</label>
                    <div id="cariLokasi">
                        <input class="form-control" type="text" id="NAMATASAN" value="<?php echo convert_unor($IDUNO['value']); ?>">
                        <?php echo form_input($IDUNO); ?>
                    </div>
                </div>
                <?php echo form_hidden('id', $user->id); ?>
                <div class="clearfix"></div>
                <?php if ($this->ion_auth->is_admin()): ?>
                    <div class="col-lg-12">
                        <hr/>
                        <h4><?php echo lang('edit_user_groups_heading'); ?></h4>
                        <div class="form-group">
                            <?php foreach ($groups as $group): ?>
                                <div class="checkbox">
                                    <label>
                                        <?php
                                        $gID = $group['id'];
                                        $checked = null;
                                        $item = null;
                                        foreach ($currentGroups as $grp) {
                                            if ($gID == $grp->id) {
                                                $checked = ' checked="checked"';
                                                break;
                                            }
                                        }
                                        ?>
                                        <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                        <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                    </label>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="box-footer col-xs-12">
                <div class="pull-right">  
                    <button  class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    <a href="#" class="btn btn-danger btn-flat" onclick="loadContent('manage/user')"><i class="fa fa-close"></i> Batal </a> 
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $('label').each(function () {
        var html = $(this).html().replace(/\*/g, "<span class=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })

    $('#pilih_unor').tree({
        dataUrl: '<?php echo base_url('admin/pegawai/unor_jqtree/'); ?>',
    });

    $('#select').click(function () {
        var node = $('#pilih_unor').tree('getSelectedNode');
        $("#NAMATASAN").val(node.name);
        $("#IDUNO").val(node.id);
        $("#KODATASAN").val(node.kode);
        //$("#UNO_KODUNO").val(node.kode);
        $('#myModal').modal('hide');
        //alert(node.name);
        $('#UNO_KODESL').trigger('change');
    })
    $('#cariLokasi').on('focusin', function (e) {
        $('#myModal').modal('show')
        $("#cariLokasi *").blur();
        e.preventDefault();
    })
    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url(); ?>/manage/user_edit_simpan/<?php echo $user->id; ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#myForm").serialize(),
                    dataType: "json",
                    success: function (result)
                    {
                        $.notify(result[1], result[0]);
                        if (result[0] === 'success' || result[0] === 'error') {
                            loadContent('<?php echo base_url($this->ion_auth->is_admin() ? 'manage/user' : 'admin/dashboard'); ?>');
                        } else {
                            loadContent('<?php echo base_url(uri_string()); ?>');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        cekError(XMLHttpRequest, textStatus);
                    },
                });
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
</script>

