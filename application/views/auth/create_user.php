<div class="box">
    <form id=myForm method="post" accept-charset="utf-8">
        <div class="box-header  with-border">
            <h3 class="box-title">Buat Pengguna</h3>
        </div>
        <div class="box-body">
            <style>

                input { 
                    text-transform: none!important;
                }            
            </style>
            <div class="row">

                <div class="col-lg-3 form-group">

                    <label for="identity">* Username:</label>
                    <input type="text" name="identity" value="" id="identity" class="form-control"  />

                </div>
                <div class="col-lg-5 form-group">

                    <label for="full_name">* Nama Lengkap:</label>                    
                    <input type="text" name="full_name" value="" id="full_name" class="form-control"  />

                </div>
                <div class="col-lg-3 form-group">

                    <label for="email">* Email:</label>                    
                    <input type="text" name="email" value="" id="email" class="form-control"  />


                </div>
                <div class="col-lg-3 form-group">

                    <label for="password">* Password:</label>                 
                    <input type="password" name="password" value="" id="password" class="form-control"  />


                </div>
                <div class="col-lg-3 form-group">

                    <label for="password_confirm">* Konfirmasi Password:</label>      
                    <input type="password" name="password_confirm" value="" id="password_confirm" class="form-control"  />

                </div>
                <!-- /.col -->
            </div>
            <div class="box-footer col-xs-12">
                <div class="pull-right">  
                    <button  class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    <a href="#" class="btn btn-danger btn-flat" onclick="loadContent('manage/user')"><i class="fa fa-close"></i> Batal </a> 
                </div>
            </div>
        </div>

    </form>
</div>

<script>
    $('label').each(function () {
        var html = $(this).html().replace(/\*/g, "<span class=\"asterisk\">*</span>");
        $(this).html(html).find(".asterisk").css("color", "red");
    })

    $("#myForm").submit(function (e) {
        var url = "<?php echo base_url(); ?>/manage/user_create_simpan";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(),
            dataType: "json",
            success: function (result)
            {
                $.notify(result[1], result[0]);
                if (result[0] === 'success') {
                    loadContent('<?php echo base_url(); ?>/manage/user');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                cekError(XMLHttpRequest, textStatus);
            },
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>