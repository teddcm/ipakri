<?php

class MY_Form_validation extends CI_Form_validation {

    public function __construct($rules = array()) {
        parent::__construct($rules);
    }

    public function valid_date($date) {
        $d = DateTime::createFromFormat('d-m-Y', $date);
        $e = DateTime::createFromFormat('Y-m-d', $date);

        return ($d && $d->format('d-m-Y') === $date) || ($e && $e->format('d-m-Y') === $date);
    }

    public function custom($string) {
        if (!preg_match('/^[a-z0-9 !@#$%^&*()\-_=+:;\'\"\\\|,.\/?]+$/i', $string)) {
            return false;
        }
    }

}
