<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sendmail extends CI_Controller {

    public function index() {

        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->email->from('noreply.apdk@gmail.com', 'Your Name');
        $this->email->to('teddcm@gmail.com');
        $this->email->subject('This is my subject');
        $this->email->message('<b>This</b> is my message');
// Set to, from, message, etc.

        $result = $this->email->send();
    }

}
