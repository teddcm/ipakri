<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->lang->load('auth');

        if (!$this->input->is_ajax_request()) {
            show_404();
        }
        if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin()) {
            echo "<script>window.location.href='" . base_url('auth/login') . "';</script>";
            http_response_code(401);
            exit();
        }
        $this->ion_auth->set_message_delimiters('', '');
        $this->form_validation->set_error_delimiters('', '');
    }

    //////////////////////////////////////////////////////////////////////////// ADD BY TEDDY

    public function user() {

        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $this->data['users'] = $this->ion_auth->users()->result();
        $this->data['users'] = $this->ion_auth->users()->result();
        foreach ($this->data['users'] as $k => $user) {
            $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }

        $this->load->view('auth/user', $this->data);
    }

    public function user_create() {
        $this->data['title'] = $this->lang->line('create_user_heading');

        $this->data['full_name'] = array(
            'name' => 'full_name',
            'id' => 'full_name',
            'type' => 'text',
            'class' => 'form-control',
        );
        $this->data['identity'] = array(
            'name' => 'identity',
            'id' => 'identity',
            'type' => 'text',
            'class' => 'form-control',
        );
        $this->data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'text',
            'class' => 'form-control',
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
        );

        $this->load->view('auth/create_user', $this->data);
    }

    public function user_create_simpan() {
        // validate form input

        $tables = $this->config->item('tables', 'ion_auth');

        $this->form_validation->set_rules('full_name', $this->lang->line('create_user_validation_fname_label'), 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'required|is_unique[' . $tables['users'] . '.username]');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        //$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        //$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
            $identity = $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'full_name' => $this->input->post('full_name'),
                    //'company' => $this->input->post('company'),
                    //'phone' => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data)) {
            // check to see if we are creating the user
            // redirect them back to the admin page
            json_result('success', $this->ion_auth->messages());
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            json_result('warn', $this->data['message']);
        }
    }

    public function user_edit($id) {
        $this->data['title'] = $this->lang->line('edit_user_heading');

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth');
        }

        $user = $this->ion_auth->user($id)->row();
        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();

        // display the edit user form
        // set the flash data error message if there is one
        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;
        $this->data['username'] = array(
            'type' => 'text',
            'class' => 'form-control',
            'disabled' => 'TRUE',
            'value' => $this->form_validation->set_value('username', $user->username),
        );
        $this->data['full_name'] = array(
            'name' => 'full_name',
            'id' => 'full_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('full_name', $user->full_name),
            'class' => 'form-control',
        );
        $this->data['IDUNO'] = array(
            'name' => 'IDUNO',
            'id' => 'IDUNO',
            'type' => 'hidden',
            'value' => $this->form_validation->set_value('IDUNO', $user->IDUNO),
        );
        $this->data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'text',
            'value' => $this->form_validation->set_value('company', $user->email),
            'class' => 'form-control',
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
        );

        $this->load->view('auth/edit_user', $this->data);
    }

    public function user_edit_simpan($id) {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth');
        }
        $user = $this->ion_auth->user($id)->row();

        // validate form input
        $this->form_validation->set_rules('email', $this->lang->line('edit_user_validation_email_label'), 'required|valid_email');
        $this->form_validation->set_rules('full_name', $this->lang->line('edit_user_validation_fname_label'), 'required|alpha_numeric_spaces');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id != $this->input->post('id')) {
                json_result('warn', $this->lang->line('error_csrf'));
            }

            // update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $data = array(
                    'full_name' => $this->input->post('full_name'),
                    'email' => $this->input->post('email'),
                    'IDUNO' => $this->input->post('IDUNO'),
                );

                // update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                // Only allow updating groups if user is admin
                if ($this->ion_auth->is_admin()) {
                    //Update the groups user belongs to
                    $groupData = $this->input->post('groups');

                    if (isset($groupData) && !empty($groupData)) {

                        $this->ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ion_auth->add_to_group($grp, $id);
                        }
                    }
                }

                // check to see if we are updating the user
                if ($this->ion_auth->update($user->id, $data)) {
                    json_result('success', $this->ion_auth->messages());
                } else {
                    json_result('error', $this->ion_auth->errors());
                }
            } else {
                $pesan = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
                json_result('warn', $pesan);
            }
        }
    }

    public function deactivate($id = NULL) {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        }

        $id = (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->load->view('auth/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    //show_error($this->lang->line('error_csrf'));
                    json_result('warn', $this->lang->line('error_csrf'));
                } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->deactivate($id);
                    json_result('success', $this->ion_auth->messages());
                }
            } else {
                json_result('error', 'Akun batal dinonaktifkan');
            }
        }
    }

    public function user_hapus($id = NULL) {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            return show_error('You must be an administrator to view this page.');
        }

        $id = (int) $id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->load->view('auth/delete_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    //show_error($this->lang->line('error_csrf'));
                    json_result('warn', $this->lang->line('error_csrf'));
                } else if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->delete_user($id);
                    json_result('success', $this->ion_auth->messages());
                }
            } else {
                json_result('error', 'Akun batal dihapus');
            }
        }
    }

    public function activate($id, $code = false) {
        if ($this->input->post()) {
            if ($code !== false) {
                $activation = $this->ion_auth->activate($id, $code);
            } else if ($this->ion_auth->is_admin()) {
                $activation = $this->ion_auth->activate($id);
            }

            if ($activation) {
                json_result('success', $this->ion_auth->messages());
            } else {
                json_result('success', $this->ion_auth->errors());
            }
        } else {
            $this->load->view('auth/activate_user');
        }
    }

    public function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);
        return array($key => $value);
    }

    public function _valid_csrf_nonce() {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function unor() {
        $this->unor_baru();
    }

    private function unor_lama() {

        $this->data['csrf'] = $this->_get_csrf_nonce();
        $this->load->view('auth/unor', $this->data);
    }

    private function unor_baru() {
        $this->data['csrf'] = $this->_get_csrf_nonce();
        $sql = "SELECT x.PNS_PNSNAM as pejabat, x.xpejabat,COUNT(DISTINCT(c.UNO_ID)) + COUNT(DISTINCT(d.UNO_ID)) + COUNT(DISTINCT(e.UNO_ID)) + COUNT(DISTINCT(f.UNO_ID)) + COUNT(DISTINCT(g.UNO_ID)) AS endChild,a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
        $sql .= "FROM kanreg8_unor a  ";
        $sql .= "LEFT JOIN (SELECT  z.PNS_UNOR,GROUP_CONCAT(DISTINCT z.PNS_PNSNAM SEPARATOR ' / ') as PNS_PNSNAM ,COUNT(z.PNS_NIPBARU)AS xpejabat FROM kanreg8_pupns z WHERE z.PNS_JNSJAB=1 AND z.PNS_KEDHUK<20 GROUP BY z.PNS_UNOR) x ON x.PNS_UNOR=a.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor d ON d.UNO_DIATASAN_ID=c.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor e ON e.UNO_DIATASAN_ID=d.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor f ON f.UNO_DIATASAN_ID=e.UNO_ID ";
        $sql .= "LEFT JOIN kanreg8_unor g ON g.UNO_DIATASAN_ID=f.UNO_ID ";
        $sql .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
//            $sql .= "AND b.UNO_ID IS NULL ";
        $sql .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
        $sql .= "GROUP BY a.UNO_ID ";
        $sql .= "ORDER BY a.UNO_KODUNO ";
        $query = $this->db->query($sql);
//echo $this->db->last_query();
        $groups = array();
        $return = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $result) {
                $return[] = $result;
            }
            $jqtree = create_unor2_jqtree($return);

            $return2 = array();
            $temp = array();
            $sql2 = "SELECT a.UNO_ID,a.UNO_DIATASAN_ID,a.UNO_KODUNO,a.UNO_NAMUNO,a.UNO_NAMJAB,a.UNO_KODESL ";
            $sql2 .= "FROM kanreg8_unor a  ";
            $sql2 .= "LEFT JOIN kanreg8_unor b ON a.UNO_DIATASAN_ID=b.UNO_ID ";
            $sql2 .= "LEFT JOIN kanreg8_unor c ON c.UNO_DIATASAN_ID=a.UNO_ID ";
            $sql2 .= "WHERE a.UNO_INSTAN='" . INSTANSI_KERJA . "' ";
            $sql2 .= "AND a.UNO_ID NOT IN ('" . implode("','", $jqtree[1]) . "') ";
            $sql2 .= "AND a.UNO_NAMUNO NOT LIKE '%###DELETED###%' ";
            $sql2 .= "GROUP BY a.UNO_ID ";
            $sql2 .= "ORDER BY a.UNO_KODUNO ";
            $query2 = $this->db->query($sql2);
//        echo $this->db->last_query();
            foreach ($query2->result_array() as $result2) {
//            $temp['pejabat'] = $result2['pejabat'];
//            $temp['xpejabat'] = $result2['xpejabat'];
                $temp['name'] = $result2['UNO_NAMUNO'];
                $temp['id'] = $result2['UNO_ID'];
                $temp['jabatan'] = $result2['UNO_NAMJAB'];
                $temp['eselon'] = $result2['UNO_KODESL'];
                $temp['kode'] = $result2['UNO_KODUNO'];
                $temp['atasan'] = $result2['UNO_DIATASAN_ID'];
                $return2[] = $temp;
            }
            $this->data['unor'] = (array($jqtree[0], $return2));
            $this->load->view('auth/unor2', $this->data);
        }
        else{
            echo 'Tidak ditemukan data Unor';
        }
    }

    public function unor_ajax_view() {
        $this->load->library('datatables_ssp'); //panggil library datatables
        $sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password, 'port' => $this->db->port,
            'db' => $this->db->database,
            'host' => $this->db->hostname
        );
        $table = 'kanreg8_unor';
        $primaryKey = 'UNO_ID';
        $columns = array(
            array(
                'db' => 'UNO_ID',
                'dt' => 'UNO_ID',
                'formatter' => function( $d ) {
                    return ($d);
                }),
            array('db' => 'UNO_NAMUNO', 'dt' => 'UNO_NAMUNO'),
            array('db' => 'UNO_KODUNO', 'dt' => 'UNO_KODUNO'),
            array('db' => 'UNO_NAMJAB', 'dt' => 'UNO_NAMJAB'),
            array(
                'db' => 'UNO_DIATASAN_ID',
                'dt' => 'NAMATASAN',
                'formatter' => function( $d ) {
                    return convert_unor($d);
                }),
            array('db' => 'UNO_DIATASAN_ID', 'dt' => 'UNO_DIATASAN_ID'),
            array('db' => 'UNO_KODESL', 'dt' => 'UNO_KODESL'),
            array(
                'db' => 'UNO_KODESL',
                'dt' => 'ESL',
                'formatter' => function( $d ) {
                    return convert_eselon($d);
                }),
        );
        $where = 'UNO_NAMUNO NOT LIKE "%###DELETED###%"';
        echo json_encode(
                Datatables_ssp::complex($_GET, $sql_details, $table, $primaryKey, $columns, $where)
        );
    }

    public function unor_create() {
        if ($this->ion_auth->is_admin()) {
            $data['level'] = get_eselon();
            $this->load->view('auth/select_unor', $data);
        }
    }

    public function unor_simpan() {
        $data = $this->input->post();
        $this->load->model('M_manage');
        $this->form_validation->set_rules('UNO_NAMUNO', 'Nama Unor', 'required');
        $this->form_validation->set_rules('UNO_NAMJAB', 'Nama Jabatan Unor', 'required');
        //$this->form_validation->set_rules('UNO_DIATASAN_ID', 'Unor Atasan', 'required');
        $this->form_validation->set_rules('UNO_KODESL', 'Eselon', 'required');
        if ($data['UNO_KODUNO'] == convert_unor($data['UNO_ID'], 'UNO_KODUNO')) {
            $this->form_validation->set_rules('UNO_KODUNO', 'Kode Unor', 'required|min_length[10]|max_length[10]');
        } else {
            $this->form_validation->set_rules('UNO_KODUNO', 'Kode Unor', 'required|min_length[10]|max_length[10]|is_unique[kanreg8_unor.UNO_KODUNO]');
        }
        if ($this->form_validation->run() == true) {
            $validator = $this->M_manage->validasi_status_unor($data['UNO_ID']);
            if (floor(convert_unor($data['UNO_DIATASAN_ID'], 'UNO_KODESL') / 10) >= floor($data['UNO_KODESL'] / 10) && $validator['max_kodesl'] != '99' && $data['UNO_KODESL'] != '99') {
                json_result('warn', 'Level eselon harus lebih rendah daripada unor atasan');
            } else if ($data['mode'] == 'edit' && floor($validator['max_kodesl'] / 10) <= floor($data['UNO_KODESL'] / 10) && $validator['max_kodesl'] != '99' && $data['UNO_KODESL'] != '99') {
                json_result('warn', 'Level eselon harus lebih tinggi daripada unor bawahan');
            } else if ($data['mode'] == 'edit' && $data['UNO_ID'] == $data['UNO_DIATASAN_ID']) {
                json_result('warn', 'Tidak diizinkan memilih unor ini sebagai unor atasan');
//            } else if ($validator['num_rows'] > 0) {
//                json_result('error', 'Tidak dapat mengubah Kode Unor apabila masih memiliki unor bawahan.');
            } else {
                $result = $this->M_manage->simpan_unor($data);
                if ($result) {
                    json_result('success', 'Data berhasil disimpan.');
                } else {
                    json_result('error', 'Data gagal disimpan.');
                }
            }
        } else {
            json_result('warn', validation_errors());
        }
    }

    public function unor_edit($id) {
        if ($this->ion_auth->is_admin()) {
            $data = $this->input->post();
            //$id = safe_decode($id);
            $data['level'] = get_eselon();
            $data['unor'] = get_data_unor($id);
            $this->load->view('auth/select_unor', $data);
        }
    }

    public function unor_delete() {
        if ($this->ion_auth->is_admin()) {
            echo $this->session->flashdata('csrfkey');
            $data = $this->input->post();
            $id = ($data['id']);
            $this->load->model('M_manage');
            if ($this->M_manage->validasi_status_unor($id)['num_rows'] > 0) {
                json_result('error', 'Masih memiliki unor bawahan.');
            } else {
                if ($this->M_manage->hapus_unor($id)) {
                    json_result('success', 'Data berhasil dihapus.');
                } else {
                    json_result('error', 'Data gagal dihapus.');
                }
            }
        } else {
            json_result('error', 'Anda tidak memiliki hak untuk menghapus.');
        }
    }

}
