<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->form_validation->set_error_delimiters('', '');
        $this->load->helper('front');
        $this->data['page'] = $this->uri->segment(3) == NULL ? 'home' : $this->uri->segment(3);
        $this->data['subpage'] = $this->uri->segment(4) == NULL ? NULL : $this->uri->segment(4);
    }

    public function index() {
        $this->load->view('front/f-index', $this->data);
    }

    public function p() { //p for page
        if ($this->data['page'] == 'statistik') {
            $this->load->model('admin/M_dashboard');
            $this->data['sebaranGolru'] = $this->M_dashboard->getSebaranGolru();
            $this->data['sebaranUsia'] = $this->M_dashboard->getSebaranUsia();
            $this->data['sebaranGender'] = $this->M_dashboard->getSebaranGender();
            $this->data['sebaranJenJab'] = $this->M_dashboard->getSebaranJenJab();
            $this->data['sebaranPendidikan'] = $this->M_dashboard->getSebaranPendidikan();
            //print_r($this->data['sebaranUsia']);
        }
        if ($this->data['page'] == 'cuti') {
            $this->load->model('admin/M_pegawai');
            $this->data['dataSpesimen'] = get_spesimen();
            //print_r($this->data['sebaranUsia']);
        }
        $this->load->view('front/f-index', $this->data);
    }

    public function c($id) { //c for custom page
        $this->load->model('front/M_back');
        $data = $this->M_back->jsonHalaman($id);
        $this->data = json_decode(json_encode($data), true);
        $this->data['page'] = '#custom';

        $this->load->view('front/f-index', $this->data);
    }

    public function vb($id) { //b for view berita
        $this->load->model('front/M_back');
        $data = $this->M_back->jsonPost($id);
        $this->data = json_decode(json_encode($data), true);
        $this->data['page'] = '#berita';

        $this->load->view('front/f-index', $this->data);
    }

    public function vp($id) { //b for view pengumuman
        $this->load->model('front/M_back');
        $data = $this->M_back->jsonPost($id);
        $this->data = json_decode(json_encode($data), true);
        $this->data['page'] = '#pengumuman';

        $this->load->view('front/f-index', $this->data);
    }

    public function album($id = NULL) { //b for view pengumuman
        $this->load->model('front/M_back');
        $data = $this->M_back->jsonPost($id);
        $this->data = json_decode(json_encode($data), true);
        $this->data['page'] = 'album';
        if ($id !== NULL) {
            $this->load->view('front/f-index', $this->data);
        } else {
            show_404();
        }
    }

    public function berita($page = NULL) {
        $this->load->model('front/M_back');

        $perpage = 6;
        $result = getPost(1, $page, $perpage);

        $this->load->library('pagination');
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li disabled><a style="background-color:whitesmoke;">';
        $config['cur_tag_close'] = '</a></li>';
        $config['base_url'] = base_url('frontpage/berita');
        $config['total_rows'] = $result['num_rows'];
        $config['per_page'] = $perpage;
        $config['num_links'] = 5;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['datax'] = $result['data'];
        //print_r($result['data']);

        $this->data['page'] = 'beritapengumuman';

        $this->load->view('front/f-index', $this->data);
    }

    public function pengumuman($page = NULL) {
        $this->load->model('front/M_back');

        $perpage = 6;
        $result = getPost(2, $page, $perpage);

        $this->load->library('pagination');
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li disabled><a style="background-color:whitesmoke;">';
        $config['cur_tag_close'] = '</a></li>';
        $config['base_url'] = base_url('frontpage/berita');
        $config['total_rows'] = $result['num_rows'];
        $config['per_page'] = $perpage;
        $config['num_links'] = 5;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['datax'] = $result['data'];
        //print_r($result['data']);

        $this->data['page'] = 'beritapengumuman';

        $this->load->view('front/f-index', $this->data);
    }

    public function cetakcuti() { //p for page
        if($this->input->post()){
        $nip = $this->input->post('nip');
        $atasan = $this->input->post('atasan');
        $pjb = $this->input->post('pjb');
        $this->data['datapeg'] = getPNS($nip);
        $this->data['dataatasan'] = getPNS($atasan);
        $this->data['datapjb'] = getPNS($pjb);
        $this->data['dataatasan'] = getPNS($atasan);
        $this->data['datacuti'] = get_detail_cuti($nip);
        $this->load->view('front/page/form-cetakcutinew', $this->data);
        }else{
            show_404();
        }
    }

    function barcode($kode) {
        $height = '20';
        $width = '2'; //1,2,3,dst
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $barcodeOPT = array(
            'text' => $kode,
            'barHeight' => $height,
            'factor' => $width,
            'stretchText' => true
        );
        $renderOPT = array();

        $render = Zend_Barcode::factory(
                        'code128', 'image', $barcodeOPT, $renderOPT
                )->render();
    }

}
