<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_manage extends CI_Model {

    function validasi_status_unor($UNO_ID) { //validasi status unor 
        $this->db->where('UNO_DIATASAN_ID', $UNO_ID);
        $this->db->where('UNO_NAMUNO NOT LIKE ', "%###DELETED###%");
        $this->db->order_by('UNO_KODESL', 'ASC');
        $query = $this->db->get('kanreg8_unor');
        //echo $this->db->last_query();
        $return['num_rows'] = $query->num_rows(); //apakah memiliki bawahan
        $return['max_kodesl'] = $query->num_rows() > 0 ? $query->row()->UNO_KODESL : 999; //berapa eselon tertinggi bawahan
        return $return;
    }

    function hapus_unor($id) {
        $this->db->trans_begin();
        if (DUMP_DELETED_UNOR) {
            $this->db->where('UNO_ID', $id);
            $this->db->set('UNO_NAMUNO', 'CONCAT("###DELETED###",UNO_NAMUNO)', FALSE);
            $this->db->update('kanreg8_unor');
        } else {
            $this->db->where('UNO_ID', $id);
            $this->db->delete('kanreg8_unor');
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function simpan_unor($newdata) {
        $data = $newdata;
        $mode = $data['mode'];
        unset($data['mode']);
        $data = arraytoupper($data);
        $this->db->trans_begin();
        $this->db->set('UNO_INSTAN', INSTANSI_KERJA);
        if ($mode == 'tambah') {
            $ID = uuid();
            $data['UNO_ID'] = $ID;
            $query = $this->db->insert('kanreg8_unor', $data);
            $id = $this->db->insert_id();
        } else if ($mode == 'edit') {
            $UNO_ID = $data['UNO_ID'];
            unset($data['UNO_ID']);
            $this->db->where('UNO_ID', $UNO_ID);
            $query = $this->db->update('kanreg8_unor', $data);
        }
        //echo $this->db->last_query();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
